using System;

public partial class CIPL : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Userid"] == null || Session["Userid"].ToString() == "")
        {
            Response.Redirect("https://insta.carzonrent.com");
        }

        ClsAdmin cls = new ClsAdmin();
        bool AccessAllowedv = cls.CheckReportAccess(
            Convert.ToInt32(Session["Userid"]));

        if (AccessAllowedv)
        {
            hdnAccessAllowed.Value = "1";
        }
        else
        {
            hdnAccessAllowed.Value = "0";
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if ((Request.UserAgent.IndexOf("AppleWebKit") > 0) || (Request.UserAgent.IndexOf("Unknown") > 0) || (Request.UserAgent.IndexOf("Chrome") > 0))
            {
                Request.Browser.Adapters.Clear();
            }
        }
    }
}
