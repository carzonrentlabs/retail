var gdCtrl = new Object();
var gcGray = "inactivecaption";
var gcToggle = "buttonface";
var gcBG = "#E5E5E5";
var varSelf=fGetById(parent.document,self.name);
var oDatePopup = window.createPopup();
var gMonths = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
var curDate = new Date();
var curToday=[curDate.getFullYear(),curDate.getMonth()+1,curDate.getDate()];
var CalendarCssPath = "../CSS/Concalendar.css";
var retDateFormat = 0;

var giYear;
var giMonth;
var giDay;
fSetCurDate(new Date());

//To set current date of the Calendar
function fSetCurDate(dt)
{
	giYear = dt.getFullYear();
	giMonth = dt.getMonth()+1;
	giDay = dt.getDate();
}

//To format the date as it will be returned to the text box
function fSetDate(iYear, iMonth, iDay)
{
	var cMonth;
	if (iDay == 1 || iDay == 2 || iDay == 3 || iDay == 4 || iDay == 5 || iDay == 6 || iDay == 7 || iDay == 8 || iDay == 9)
	  iDay = '0'+iDay;

	switch(iMonth)
	{
		case 1: cMonth="01"; break;
		case 2: cMonth="02"; break;
		case 3: cMonth="03"; break;
		case 4: cMonth="04"; break;
		case 5: cMonth="05"; break;
		case 6: cMonth="06"; break;
		case 7: cMonth="07"; break;
		case 8: cMonth="08"; break;
		case 9: cMonth="09"; break;
		case 10: cMonth="10"; break;
		case 11: cMonth="11"; break;
		case 12: cMonth="12";
	}
/*
	switch(iMonth)
	{
		case 1: cMonth="Jan"; break;
		case 2: cMonth="Feb"; break;
		case 3: cMonth="Mar"; break;
		case 4: cMonth="Apr"; break;
		case 5: cMonth="May"; break;
		case 6: cMonth="Jun"; break;
		case 7: cMonth="Jul"; break;
		case 8: cMonth="Aug"; break;
		case 9: cMonth="Sep"; break;
		case 10: cMonth="Oct"; break;
		case 11: cMonth="Nov"; break;
		case 12: cMonth="Dec";
	}
	*/
	if (retDateFormat == 0)
	{
		return((iDay + "/" + cMonth + "/" + iYear));
	}
	else
	{
		return((iMonth + "/" + iDay + "/" + iYear));
	}
}

//To set the currently selected date of the calendar in the text box
function fSetSelected(aCell,idx)
{
	var iOffset = 0;
	var iYear = parseInt(tbSelYear.value);
	var iMonth = parseInt(tbSelMonth.value);

	aCell.bgColor = gcBG;
	oDatePopup.hide();

	with (aCell)
	{
		var iDay = parseInt(innerText);
		if (style.color==gcGray)
			iOffset = (idx<7)?-1:1;
		iMonth += iOffset;
		if (iMonth<1) {
			iYear--;
			iMonth = 12;
		}
		else if (iMonth>12)
		{
			iYear++;
			iMonth = 1;
		}
	}
	gdCtrl.value = fSetDate(iYear, iMonth, iDay);
	if (gdCtrl.disabled == false)
	{
		gdCtrl.focus();
	}
}

//Point class
function Point(iX, iY)
{
	this.x = iX;
	this.y = iY;
}

//To set a 2D array based on Year and Month
function fBuildCal(iYear, iMonth) 
{
	var aMonth=new Array();
	for(i=1;i<7;i++)
		aMonth[i]=new Array(i);

	var dCalDate=new Date(iYear, iMonth-1, 1);
	var iDayOfFirst=dCalDate.getDay();
	var iDaysInMonth=new Date(iYear, iMonth, 0).getDate();
	var iOffsetLast=new Date(iYear, iMonth-1, 0).getDate()-iDayOfFirst+1;
	var iDate = 1;
	var iNext = 1;

	for (d = 0; d < 7; d++)
		aMonth[1][d] = (d<iDayOfFirst)?-(iOffsetLast+d):iDate++;

	for (w = 2; w < 7; w++)
		for (d = 0; d < 7; d++)
			aMonth[w][d] = (iDate<=iDaysInMonth)?iDate++:-(iNext++);
	return aMonth;
}

//To darw the inner table for all the dates
function fDrawCal(iYear, iMonth, iCellHeight, sDateTextSize) 
{
	var WeekDay = new Array("S","M","T","W","T","F","S");
	var styleTD1 = " bgcolor='#778899' bordercolor='#778899' valign='middle' align='center' style='font:bold 8pt Arial;";
	var styleTD2 = " bgcolor='"+gcBG+"' bordercolor='black' border=0 valign='middle' align='center' style='font:8pt Arial;";           

	with (oDatePopup.document) 
	{
		write("<tr bgcolor='#778899' bordercolor='#778899'>");
		for(i=0; i<7; i++)
			write("<td "+styleTD1+"color:white'>" + WeekDay[i] + "</td>");
		write("</tr>");

		for (w = 1; w < 7; w++)
		{
			write("<tr>");
			for (d = 0; d < 7; d++)
				write("<td id=calCell "+styleTD2+"cursor:hand;' onMouseOver='this.bgColor=parent.gcToggle' onMouseOut='this.bgColor=parent.gcBG' onclick='parent.fSetSelected(this,"+ ((w-1)*7+d+1) +")'></TD>");
			write("</tr>");
		}
	}
}

//To update date values calculated using fBuildCal
function fUpdateCal(iYear, iMonth) 
{
	myMonth = fBuildCal(iYear, iMonth);
	var dCalDate=new Date(iYear, iMonth-1, 1);
	var dTodaysDate = new Date();
	var i = 0;
	for (w = 0; w < 6; w++)
	{
		for (d = 0; d < 7; d++)
		{
	  		with (oDatePopup.document.all("calCell")[(7*w)+d]) 
	  		{
	  			style.borderStyle='none';
	  			if (myMonth[w+1][d]<0) 
	  			{
	  				style.color = gcGray;
	  				innerText = -myMonth[w+1][d];
	  				style.borderColor = gcBG;
	  				style.background = ((d==0))?"#99CCFF":gcBG;
	  			}
	  			else 
	  			{
					if(myMonth[w+1][d] == giDay && iMonth == giMonth && iYear == giYear)
	  				{
	  					style.color = "white";
	  					style.background="red";
	  					//style.borderStyle='solid';
	  					//style.borderColor = "red";
	  				}
	  				else
	  				{
	  					if(myMonth[w+1][d] == curToday[2] && iMonth == curToday[1] && iYear == curToday[0])
						{
	  						style.color = "red";
	  						style.background = "white";
						}
						else
						{
	  						style.color = ((d==0))?"red":"buttontext";
	  						style.background = ((d==0))?"#99CCFF":gcBG;
	  					}
	  				}
	  				innerText = myMonth[w+1][d];
	  			}
	  		}
	  	}
	}
}

//To set the current year and month
function fSetYearMon(iYear, iMon)
{
	tbSelMonth.options[iMon-1].selected = true;
	for (i = 0; i < tbSelYear.length; i++)
		if (tbSelYear.options[i].value == iYear)
			tbSelYear.options[i].selected = true;
	fUpdateCal(iYear, iMon);
}

//To set the previous month (with validation for current month as first month and others)
function fPrevMonth()
{
	var iMon = tbSelMonth.value;
	var iYear = tbSelYear.value;

	if(iMon==1 && iYear==tbSelYear.options[0].value) return;

	if (--iMon<1) 
	{
		iMon = 12;
		iYear--;
	}

	fSetYearMon(iYear, iMon);
}

//To set the next month (with validation for current month as last month)
function fNextMonth()
{
	var iMon = tbSelMonth.value;
	var iYear = tbSelYear.value;

	if (++iMon>12) 
	{
		iMon = 1;
		iYear++;
	}

	fSetYearMon(iYear, iMon);
}

//To get the XY location of the object where the calendar will be displayed
function fGetXY(aTag)
{
	var oTmp = aTag;
	var pt = new Point(0,0);
	while(oTmp)
	{
		pt.x += oTmp.offsetLeft;
		pt.y += oTmp.offsetTop;
		if (oTmp.tagName.toUpperCase() == "BODY") break;
		oTmp=oTmp.offsetParent;
	}
	return pt;
}

function fGetById(doc, id) {
	return doc.getElementById(id);
}

function fGetWinSize(w) {
	if (w.innerWidth)
		return [w.innerWidth-16,w.innerHeight,w.pageXOffset,w.pageYOffset];
	else if (w.document.compatMode=='CSS1Compat')
		with (w.document.documentElement) return [clientWidth,clientHeight,scrollLeft,scrollTop];
	else
		with (w.document.body) return [clientWidth,clientHeight,scrollLeft,scrollTop];
}

// ValidateDateValue:
// Author: Gurdarshan Singh
// Date Created: 26/05/2004
// Date Modified: 26/05/2004
// Description:
// This method actually validate value passed for valid date and
// this is the method we called from our java script from the application
// for validating whether specified value is a valid date.
// Parameters:
// 1. dtValue: Specify Value which is to be validated for valid Date value
// 2. return: true if valid date otherwise false
function IsValidDateValue(dtValue, iFormatofDateValue)
{
	// Return true when passed value is blank string
	// and for validating required field use Required Field Validator control.
	if (dtValue.length == 0)
	{
		return false;
	}
	// Checking whether length of value passed is in between 8 to 10 characters
	// because date value can be enter in following combination:
	// 1. d/m/yyyy
	// 2. dd/m/yyyy
	// 3. d/mm/yyyy
	// 4. dd/mm/yyyy
	// in all above case minimum length is 8 characters and maximum is 10 characters
	// if not satisfied the condition then returning false so that validation control
	// status set as InValid and page is not submitted and also validation control message
	// is displayed in validation summary
	if (dtValue.length < 8 || dtValue.length > 10)
	{
		return false;
	}
	// spliting passed value on the basis of "/" character and stored in array format
	var strDateArr = dtValue.split("/");
	// Checking whether array generated after split is of length 3
	// if not then returning false
	if (strDateArr.length != 3)
	{
		return false;
	}
	// Checking whether first part of the array of length between 1 and 2 characters
	// if not then returning false
	if (strDateArr[0].length < 1 || strDateArr[0].length > 2)
	{
		return false;
	}
	// Checking whether second part of the array of length between 1 and 2 characters
	// if not then returning false
	if (strDateArr[1].length < 1 || strDateArr[1].length > 2)
	{
		return false;
	}
	// Checking whether Last part of the array of length is 4 characters
	// if not then returning false
	if (strDateArr[2].length != 4)
	{
		return false;
	}
	// checking whether value specified in date control contains valid numbers
	if (isNaN(strDateArr[0]))
	{
		return false;
	}
	if (isNaN(strDateArr[1]))
	{
		return false;
	}
	if (isNaN(strDateArr[2]))
	{
		return false;
	}
	// Checking whether day and month part contains any leading zero if yes then removing that
	// because parseint did not consider second item if first element is zero
	var strFirst = new String();
	var strSecond = new String();
	strFirst = strDateArr[0];
	strSecond = strDateArr[1];
	if (strFirst.substr(0, 1) == "0")
	{
		strDateArr[0] = strFirst.substr(1, 1);
	}
	if (strSecond.substr(0, 1) == "0")
	{
		strDateArr[1] = strSecond.substr(1, 1);
	}
	// Storing day, month and year part of array in variables after converting them to integer value
	if (iFormatofDateValue == 0)
	{
		var iDay = parseInt(strDateArr[0]);
		var iMonth = parseInt(strDateArr[1]);
		var iYear = parseInt(strDateArr[2]);
	}
	else
	{
		var iDay = parseInt(strDateArr[1]);
		var iMonth = parseInt(strDateArr[0]);
		var iYear = parseInt(strDateArr[2]);
	}
	// Checking whether month is between 1 and 12
	// if not then returning false
	if (iMonth < 1 || iMonth > 12)
	{
		return false;
	}
	// Checking whether Day is between 1 and 31
	// if not then returning false
	if (iDay < 1 || iDay > 31)
	{
		return false;
	}
	// Checking whether month is 4, 6, 9, 11 i.e. April, June, September and November
	if (iMonth == 4 || iMonth == 6 || iMonth == 9 || iMonth == 11)
	{
		// if yes then validate whether day is less than equal to 30
		// because April, June, September and November months has maximum 30 days
		// if not then returning false
		if (iDay > 30)
		{
			return false;
		}
	}
	// Checking whether month is 2 i.e. February
	if (iMonth == 2)
	{
		// if yes then validate whether Year is Leap Year
		if (((iYear % 400) == 0) || (((iYear % 4) == 0) && ((iYear % 100) != 0)))
		{
			// if yes then validate whether day is less than equal to 29
			// because Leap Year February month has maximum 29 days
			// if not then returning false
			if (iDay > 29)
			{
				return false;
			}
		}
		else
		{
			// if not a leap year then validate whether day is less than equal to 28
			// because Non Leap Year February month has maximum 28 days
			// if not then returning false
			if (iDay > 28)
			{
				return false;
			}
		}
	}
	// If date is valid then returning true
	return true;
}

// Main: popCtrl is the widget below or above which you want this calendar to appear;
//       dateCtrl is the widget into which you want to put the selected date.
// i.e.: <input type="text" name="dc" readonly><INPUT type="button" value="V" onclick="fPopCalendar(dc,dc);return false">
// iFormatofDateValue: Specify 0 (zero) for dd/mm/yyyy and non 0 (zero) for mm/dd/yyyy
function fPopCalendar(popCtrl, dateCtrl, iFormatofDateValue)
{
	gdCtrl = dateCtrl;
	var sDt;
	var iPos;
	sDt = dateCtrl.value;
	if (isNaN(iFormatofDateValue))
	{
		iFormatofDateValue = 1;
	}
	retDateFormat = iFormatofDateValue;
	if (IsValidDateValue(sDt, iFormatofDateValue))
	{
		var strDateArr = sDt.split('/');
		if (strDateArr.length == 3)
		{
			if (strDateArr[0].length == 1)
			{
				strDateArr[0] = "0" + strDateArr[0];
			}
			if (strDateArr[1].length == 1)
			{
				strDateArr[1] = "0" + strDateArr[1];
			}
			if (iFormatofDateValue == 0)
			{
				sDt = strDateArr[1] + "/" + strDateArr[0] + "/" + strDateArr[2]
			}
			else
			{
				sDt = strDateArr[0] + "/" + strDateArr[1] + "/" + strDateArr[2]
			}
			fSetCurDate(new Date(sDt));
		}
		else
		{
			fSetCurDate(new Date())
		}
	}
	else
	{
		fSetCurDate(new Date())
	}
	fSetYearMon(giYear, giMonth);
	oDatePopup.show(0, popCtrl.offsetHeight, 185, 165, popCtrl);
}

with (oDatePopup.document) 
{
	write("<BODY leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 hspace=0 vspace=0 scroll=no>")
	write("<lin"+"k rel='stylesheet' type='text/css' href='" + CalendarCssPath + "'>");
	write("<table id='outerTable' border='0' bgcolor='#C2D3E5' height='100%' width='100%' cellpadding=0 cellspacing=0>");
	write("<TR>");
	write("<td align=center width='100%' height='18%' valign='middle'>");
	//write("<INPUT id='navPrev' type='button' width='20px' value='&lt;' style='cursor:hand;' onclick='if(tbSelYear.value > tbSelYear.options[0].value) parent.fSetYearMon(parseInt(tbSelYear.value) - 1, tbSelMonth.value);'>");
	write("&nbsp;");
	write("<SPAN style='cursor:hand;' onclick='if(tbSelYear.value > tbSelYear.options[0].value) parent.fSetYearMon(parseInt(tbSelYear.value) - 1, tbSelMonth.value);'><A style='color:buttontext;font:bold 7pt Arial;text-decoration:none' href='#'>&lt;&lt;</A></SPAN>&nbsp;&nbsp;");
	write("<SPAN style='cursor:hand;' onclick='parent.fPrevMonth();'><A style='color:buttontext;font:bold 7pt Arial;text-decoration:none' href='#'>&lt;</A></SPAN>&nbsp;");
	write("<select id='tbSelMonth' name='tbSelMonth' style='font:7pt;' onChange='parent.fUpdateCal(tbSelYear.value, tbSelMonth.value)'>");
	for (i=0; i<12; i++)
		write("<option value='"+(i+1)+"'>"+gMonths[i]+"</option>");
	write("</SELECT>");
	write("<SELECT id='tbSelYear' name='tbSelYear' style='font:7pt;' onChange='parent.fUpdateCal(tbSelYear.value, tbSelMonth.value)'>");
	for(i=2001;i<2051;i++)
		write("<OPTION value='"+i+"'>"+i+"</OPTION>");
	write("</SELECT>");
	write("&nbsp;");
	write("<SPAN style='cursor:hand;' onclick='parent.fNextMonth();'><A style='color:buttontext;font:bold 7pt Arial;text-decoration:none' href='#'>&gt;</A></SPAN>&nbsp;&nbsp;");
	write("<SPAN style='cursor:hand;' onclick='parent.fSetYearMon(parseInt(tbSelYear.value) + 1, tbSelMonth.value)'><A style='color:buttontext;font:bold 7pt Arial;text-decoration:none' href='#'>&gt;&gt;</A></SPAN>");
	write("</td>");
	write("</TR>");
	write("<TR><td align='center'>");
	write("<table width='90%' height='100%' border='0' bgcolor='black' cellspacing=0 cellpadding=0 style='background-color:"+gcBG+"'>");
	fDrawCal(giYear, giMonth, 8, '7');
	write("</table>");
	write("</td>");
	write("</TR>");
	write("<TR><TD align=center valign=middle style='color:buttontext;font:bold 6pt arial;'>");
	write("<SPAN onclick='parent.fSetCurDate(new Date()); parent.fSetYearMon(parent.giYear, parent.giMonth);'><A href='javascript:void(0)' class='BottomAnchor' onmouseover='return true;'>Today : "+gMonths[curToday[1]-1]+" "+curToday[2]+", "+curToday[0]+"</A></SPAN>&nbsp;&nbsp;&nbsp;");
	write("</TR>");
	write("</TABLE>");
}
var tbSelMonth = oDatePopup.document.all("tbSelMonth");
var tbSelYear = oDatePopup.document.all("tbSelYear");
