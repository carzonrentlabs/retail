using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Net;
using ccs;
using System.Xml;
public partial class CCTVPostData : System.Web.UI.Page
{
    DataSet GetBookingDetail = new DataSet();
    ClsAdmin objAdmin = new ClsAdmin();
    StringBuilder strXML = new StringBuilder();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnsubmit.Attributes.Add("onclick", "return validateform();");
        }
        btnsubmit.Attributes.Add("onclick", "return validateform();");

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        DateTime fromDate = Convert.ToDateTime(txtFrom.Text);
        DateTime toDate = Convert.ToDateTime(txtTo.Text);
        ds = objAdmin.GetCCTVBookingDetail_NotPostData(fromDate, toDate);
        Grv_Summary.DataSource = ds;
        Grv_Summary.DataBind();
    }
    protected void Grv_Summary_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string bookingid;
        DataSet GetBookingDetail = new DataSet();
        DataSet ds = new DataSet();
        StringBuilder strXML = new StringBuilder();
        string strXMLResponse = "";
        string booking = "", ReceiptNO = "", StatusResponse = "", ErrorCode = "", ErrorMsg = "";

        //CCTV varialble declaration Start 08 March 2015
        string transactionSessionId = string.Empty;
        string OldBookingId = string.Empty;
        DataSet dsBookingDetails = new DataSet();
        CCTVProxy.RetailInputWebService proxy = new CCTVProxy.RetailInputWebService();
        CCTVProxy.TransactionResponseObject obj = new CCTVProxy.TransactionResponseObject();
        CCTVProxy.TransactionResponseObject obj1 = new CCTVProxy.TransactionResponseObject();
        decimal unitPrice, subTotal;
        //CCTV variable declaration End 08 March 2015
        if (e.CommandName == "Send")
        {
            bookingid = e.CommandArgument.ToString();
            try
            {
                GetBookingDetail = objAdmin.GetCCTVBookginDetails_PostData(Convert.ToInt32(bookingid));

                if (Convert.ToInt32(GetBookingDetail.Tables[0].Rows[0]["PickupCityId"].ToString()) == 33 || Convert.ToInt32(GetBookingDetail.Tables[0].Rows[0]["PickupCityId"].ToString()) == 35 || Convert.ToInt32(GetBookingDetail.Tables[0].Rows[0]["PickupCityId"].ToString()) == 36)
                {

                    if (Convert.ToDecimal(GetBookingDetail.Tables[0].Rows[0]["Basic"].ToString()) < 0)
                    {
                        dsBookingDetails = objAdmin.GetCCTVVoidDetails(Convert.ToInt32(bookingid));
                        transactionSessionId = dsBookingDetails.Tables[0].Rows[0]["TransactionHeaderId"].ToString();
                        OldBookingId = dsBookingDetails.Tables[0].Rows[0]["OldBookingId"].ToString();
                        if (!string.IsNullOrEmpty(transactionSessionId) && dsBookingDetails.Tables[0].Rows.Count > 0)
                        {
                            try
                            {
                                // 1 Transaction Header 
                                obj1 = proxy.BeginTransactionWithTillLookup((Convert.ToInt64(bookingid)), true, dsBookingDetails.Tables[0].Rows[0]["StoreNo"].ToString(), dsBookingDetails.Tables[0].Rows[0]["POSNo"].ToString()
                               , dsBookingDetails.Tables[0].Rows[0]["BookingID"].ToString(), dsBookingDetails.Tables[0].Rows[0]["StoreNo"].ToString(), dsBookingDetails.Tables[0].Rows[0]["POSNo"].ToString(), OldBookingId.ToString(),
                                DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime
                               , false, Session["UserID"].ToString(), dsBookingDetails.Tables[0].Rows[0]["StaffName"].ToString().Trim()
                               , "INR", CCTVProxy.TransactionType.CancellationOfPrevious, true, false, false, CCTVProxy.OutsideOpeningHours.InsideOpeningHours
                               , false, 0, true);


                                //2 Transaction Sale 

                                if (Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString()) > 0)
                                {
                                    obj = proxy.AddTransactionSaleLine
                                    (Convert.ToInt64(obj1.TransactionSessionId), true
                                    , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                    , CCTVProxy.SaleLineAttribute.None, false, CCTVProxy.ScanAttribute.None, false
                                    , dsBookingDetails.Tables[0].Rows[0]["VehicleNo"].ToString().Trim(), dsBookingDetails.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim()
                                    , 1, true, null, Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString())
                                    , true, CCTVProxy.DiscountType.DiscountNotAllowed, false
                                    , null, false, Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString()), true, dsBookingDetails.Tables[0].Rows[0]["StoreNo"].ToString(), 101
                                    , true, OldBookingId.ToString()
                                    , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, true, true);
                                }
                                else
                                {
                                    unitPrice = Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString().Replace('-', ' '));
                                    subTotal = Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString().Replace('-', ' '));

                                    obj = proxy.AddTransactionSaleLine
                                   (Convert.ToInt64(obj1.TransactionSessionId), true
                                   , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                   , CCTVProxy.SaleLineAttribute.VoidedBackorderItem, true, CCTVProxy.ScanAttribute.None, false
                                   , dsBookingDetails.Tables[0].Rows[0]["VehicleNo"].ToString().Trim(), dsBookingDetails.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim()
                                   , 1, true, null, Convert.ToDecimal(unitPrice)
                                   , true, CCTVProxy.DiscountType.DiscountNotAllowed, false
                                   , null, false, Convert.ToDecimal(subTotal), true, dsBookingDetails.Tables[0].Rows[0]["StoreNo"].ToString(), 101
                                   , true, OldBookingId.ToString()
                                   , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, true, true);
                                }

                                // 4 Transaction payment

                                CCTVProxy.PaymentLineAttribute lineAttribute;  //Payment Type (Cash,credit)
                                string paymentTypeId = string.Empty;   //Payment type Id (card number)
                                string cardType = string.Empty;        // type of creadit card (Card issuer)
                                string linepaymentType = string.Empty;

                                if (dsBookingDetails.Tables[0].Rows[0]["PaymentMode"].ToString().Trim() == "Cr" || dsBookingDetails.Tables[0].Rows[0]["PaymentMode"].ToString().Trim() == "Ca")
                                {
                                    lineAttribute = CCTVProxy.PaymentLineAttribute.Cash;
                                    paymentTypeId = null;
                                    cardType = null;
                                    linepaymentType = "Cash";
                                }
                                else
                                {
                                    lineAttribute = CCTVProxy.PaymentLineAttribute.CreditCard;
                                    paymentTypeId = "CC";
                                    cardType = "0";
                                    linepaymentType = "Credit Card";
                                }

                                obj = proxy.AddTransactionPaymentLine(Convert.ToInt64(obj1.TransactionSessionId), true
                                    , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                    , lineAttribute, true, linepaymentType, "INR"
                                    , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"]), true, Convert.ToDecimal(1), true
                                    , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"].ToString())
                                    , true, paymentTypeId
                                    , cardType, null, false, true, true);

                                //3 Transaction total serive tax
                                obj = proxy.AddTransactionTotalLine(Convert.ToInt64(obj1.TransactionSessionId), true, DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                     , CCTVProxy.TotalLineAttribute.FinalDiscountLess, true, "Total sales tax amount", Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["CalculatedAmount"].ToString()), true, true, true);

                                //3 Transaction total
                                obj = proxy.AddTransactionTotalLine(Convert.ToInt64(obj1.TransactionSessionId), true, DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                      , CCTVProxy.TotalLineAttribute.FinalDiscountLess, true, "Total Amont to be paid", Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"].ToString()), true, true, true);

                                //5 Transaction Event
                                //obj = proxy.AddTransactionEvent(Convert.ToInt64(obj1.TransactionSessionId),true,DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime
                                //    ,false,null,false,CCTVProxy.EventLineAttribute.None, false,"Car Rental",true,true);

                                //6 Transaction commit

                                obj = proxy.CommitTransaction(Convert.ToInt64(obj1.TransactionSessionId), true);

                                if (obj.Succeeded == true)
                                {
                                    ReceiptNO = "Success";
                                    int cctvId = objAdmin.SaveCCTVDetails(Convert.ToInt32(bookingid), Convert.ToInt64(obj1.TransactionSessionId), Convert.ToInt32(Session["UserID"].ToString()), ReceiptNO, ReceiptNO);
                                    ClsAdmin.flagComplete = true;
                                }
                                else
                                {
                                    ErrorCode = obj.ErrorCode.ToString();
                                    ErrorMsg = obj.ErrorDescription.ToString();
                                    int cctvId = objAdmin.SaveCCTVDetails(Convert.ToInt32(bookingid), Convert.ToInt64(obj1.TransactionSessionId), Convert.ToInt32(Session["UserID"].ToString()), ErrorCode, ErrorMsg);
                                    StatusResponse = "Status : " + obj.Succeeded.ToString() + ", Error Code : " + ErrorCode + "-Message: " + ErrorMsg;
                                    lblMessage.Text = StatusResponse; //"Error!!!";
                                }

                            }
                            catch (Exception Ex)
                            {
                                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                            }

                        }
                    }
                    else
                    {
                        dsBookingDetails = objAdmin.MilestonePostXMLData(Convert.ToInt32(bookingid));
                        if (dsBookingDetails.Tables[0].Rows.Count > 0)
                        {
                            try
                            {
                                // 1 Transaction Header 
                                obj1 = proxy.BeginTransactionWithTillLookup((Convert.ToInt64(dsBookingDetails.Tables[0].Rows[0]["BookingID"].ToString())), false, dsBookingDetails.Tables[0].Rows[0]["StoreNo"].ToString(), dsBookingDetails.Tables[0].Rows[0]["POSNo"].ToString()
                                   , dsBookingDetails.Tables[0].Rows[0]["BookingID"].ToString(), null, null, "",
                                    DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime
                                   , false, Session["UserID"].ToString(), dsBookingDetails.Tables[0].Rows[0]["StaffName"].ToString().Trim()
                                   , "INR", CCTVProxy.TransactionType.CompletedNormally, true, false, false, CCTVProxy.OutsideOpeningHours.InsideOpeningHours
                                   , false, 0, true);

                                //2 Transaction Sale            

                                if (Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString()) > 0)
                                {
                                    obj = proxy.AddTransactionSaleLine
                                    (Convert.ToInt64(obj1.TransactionSessionId), true
                                    , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                    , CCTVProxy.SaleLineAttribute.None, false, CCTVProxy.ScanAttribute.None, false
                                    , dsBookingDetails.Tables[0].Rows[0]["VehicleNo"].ToString().Trim(), dsBookingDetails.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim()
                                    , 1, true, null, Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString())
                                    , true, CCTVProxy.DiscountType.DiscountNotAllowed, false
                                    , null, false, Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString()), true, null, null
                                    , false, null, null, false, null, true, true);
                                }
                                else
                                {
                                    unitPrice = Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString().Replace('-', ' '));
                                    subTotal = Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString().Replace('-', ' '));

                                    obj = proxy.AddTransactionSaleLine
                                   (Convert.ToInt64(obj1.TransactionSessionId), true
                                   , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                   , CCTVProxy.SaleLineAttribute.None, false, CCTVProxy.ScanAttribute.None, false
                                   , dsBookingDetails.Tables[0].Rows[0]["VehicleNo"].ToString().Trim(), dsBookingDetails.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim()
                                   , 1, true, null, Convert.ToDecimal(unitPrice)
                                   , true, CCTVProxy.DiscountType.DiscountNotAllowed, false
                                   , null, true, Convert.ToDecimal(subTotal), true, null, null
                                   , false, null, null, false, null, true, true);
                                }

                                // 4 Transaction payment

                                CCTVProxy.PaymentLineAttribute lineAttribute;  //Payment Type (Cash,credit)
                                string paymentTypeId = string.Empty;   //Payment type Id (card number)
                                string cardType = string.Empty;        // type of creadit card (Card issuer)
                                string linepaymentType = string.Empty;

                                if (dsBookingDetails.Tables[0].Rows[0]["PaymentMode"].ToString().Trim() == "Cr" || dsBookingDetails.Tables[0].Rows[0]["PaymentMode"].ToString().Trim() == "Ca")
                                {
                                    lineAttribute = CCTVProxy.PaymentLineAttribute.Cash;
                                    paymentTypeId = null;
                                    cardType = null;
                                    linepaymentType = "Cash";
                                }
                                else
                                {
                                    lineAttribute = CCTVProxy.PaymentLineAttribute.CreditCard;
                                    paymentTypeId = "CC";
                                    cardType = "0";
                                    linepaymentType = "Credit Card";
                                }

                                obj = proxy.AddTransactionPaymentLine(Convert.ToInt64(obj1.TransactionSessionId), true
                                    , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                    , lineAttribute, true, linepaymentType, "INR"
                                    , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"]), true, Convert.ToDecimal(1), true
                                    , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"].ToString())
                                    , true, paymentTypeId
                                    , cardType, null, false, true, true);

                                //3 Transaction total serive tax

                                obj = proxy.AddTransactionTotalLine(Convert.ToInt64(obj1.TransactionSessionId), true, DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                     , CCTVProxy.TotalLineAttribute.VAT, true, "Total sales tax amount", Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["CalculatedAmount"].ToString()), true, true, true);

                                //3 Transaction total
                                obj = proxy.AddTransactionTotalLine(Convert.ToInt64(obj1.TransactionSessionId), true, DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                                      , CCTVProxy.TotalLineAttribute.TotalAmountToBePaid, true, "Total Amont to be paid", Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"].ToString()), true, true, true);


                                //5 Transaction Event
                                //obj = proxy.AddTransactionEvent(Convert.ToInt64(obj1.TransactionSessionId),true,DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime
                                //    ,false,null,false,CCTVProxy.EventLineAttribute.None, false,"Car Rental",true,true);

                                //6 Transaction commit
                                obj = proxy.CommitTransaction(Convert.ToInt64(obj1.TransactionSessionId), true);

                                if (obj.Succeeded == true)
                                {
                                    ReceiptNO = "Success";
                                    int cctvId = objAdmin.SaveCCTVDetails(Convert.ToInt32(bookingid), Convert.ToInt64(obj1.TransactionSessionId), Convert.ToInt32(Session["UserID"].ToString()), ReceiptNO, ReceiptNO);
                                    ClsAdmin.flagComplete = true;
                                }
                                else
                                {
                                    ErrorCode = obj.ErrorCode.ToString();
                                    ErrorMsg = obj.ErrorDescription.ToString();
                                    int cctvId = objAdmin.SaveCCTVDetails(Convert.ToInt32(bookingid), Convert.ToInt64(obj1.TransactionSessionId), Convert.ToInt32(Session["UserID"].ToString()), ErrorCode, ErrorMsg);
                                    StatusResponse = "Status : " + obj.Succeeded.ToString() + ", Error Code : " + ErrorCode + "-Message: " + ErrorMsg;
                                    lblMessage.Text = StatusResponse; //"Error!!!";
                                }
                            }
                            catch (Exception Ex)
                            {
                                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                            }
                        }
                    }
                }
            }
            catch//(Exception ex)
            {
                Page.RegisterClientScriptBlock("Script", "<script>alert('No Response!!!')</script>");
            }
        }
    }
}
