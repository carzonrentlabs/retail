using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ccs;

public partial class Reports_RptDateWiseSummary : clsPageAuthorization
{
    ClsAdmin objAdmin = new ClsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLocation();
            BindAgents();
        }
    }

    protected void BindLocation()
    {
        ddlLocations.DataTextField = "CityName";
        ddlLocations.DataValueField = "UnitCityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetCity2(Convert.ToInt32(Session["UserID"]));
        ddlLocations.DataSource = dsLocation;
        ddlLocations.DataBind();
    }

    protected void BindAgents()
    {
        ddlAgents.DataTextField = "Name";
        ddlAgents.DataValueField = "SysUserID";
        DataSet dsAgents = new DataSet();
        dsAgents = objAdmin.GetAgents(Convert.ToInt32(Session["UserID"]));
        ddlAgents.DataSource = dsAgents;
        ddlAgents.DataBind();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        int UnitCityID = Convert.ToInt32(ddlLocations.SelectedValue);
        int SysUserID = Convert.ToInt32(ddlAgents.SelectedValue);
        string fromDate = TextBoxFrom.Text;
        string toDate = TextBoxTo.Text;
        //Response.Redirect("RptDateWiseSummary_Viewer.aspx?UnitCityID=" + UnitCityID + "&SysUserID=" + SysUserID + "&FromDate=" + fromDate + "&ToDate=" + toDate + "&RptName=RetailDateWiseSummary.rpt");
        //return;
        ClsAdmin objadmin = new ClsAdmin();
        DataTable dtEx = new DataTable();
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=DateWiseSummary.ods");
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=DateWiseSummary.xls");
        }

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else
        {
            Response.ContentType = "application/vnd.ms-excel";
        }

        this.EnableViewState = false;
        Response.Write("\r\n");

        
        if (FinYear.SelectedItem.ToString() == "2016-2025")
        {
            dsExcel = objadmin.GetDateWiseSummary(UnitCityID, SysUserID, TextBoxFrom.Text, TextBoxTo.Text);
        }
        else //if (FinYear.SelectedItem.ToString() == "2009-2010")
        {
            dsExcel = objadmin.GetDateWiseSummary_0910(UnitCityID
                , SysUserID, TextBoxFrom.Text
                , TextBoxTo.Text, FinYear.SelectedItem.ToString());
        }

        int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());
        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");
        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='center'><b>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</b></td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                if (j == 1)
                {
                    Response.Write("<td align='center'>" + Convert.ToDateTime(dtEx.Rows[i][iColumns[j]]).ToShortDateString() + "</td>");
                }
                else
                {
                    Response.Write("<td align='center'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        int UnitCityID = Convert.ToInt32(ddlLocations.SelectedValue);
        int SysUserID = Convert.ToInt32(ddlAgents.SelectedValue);
        //string fromDate =extBox.Text
        //string toDate = txtToDate.Text;
        //Response.Redirect("RptDateWiseSummary_Viewer.aspx?UnitCityID=" + UnitCityID + "&SysUserID=" + SysUserID + "&FromDate=" + fromDate + "&ToDate=" + toDate + "&RptName=rptRetailCollectionSummary.rpt");
        //return;
        ClsAdmin objadmin = new ClsAdmin();
        DataTable dtEx = new DataTable();
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=Collectionreport.ods");
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=Collectionreport.xls");
        }

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else
        {
            Response.ContentType = "application/vnd.ms-excel";
        }

        this.EnableViewState = false;
        Response.Write("\r\n");

        
        if (FinYear.SelectedItem.ToString() == "2016-2025")
        {
            dsExcel = objadmin.GetCollectionSummary(UnitCityID, SysUserID, TextBoxFrom.Text, TextBoxTo.Text);
        }
        else //if (FinYear.SelectedItem.ToString() == "2009-2010")
        {
            dsExcel = objadmin.GetCollectionSummary_0910(UnitCityID
                , SysUserID, TextBoxFrom.Text, TextBoxTo.Text
                , FinYear.SelectedItem.ToString());
        }

        int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());
        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");
        int[] iColumns = { 0, 1, 2, 3, 4, 5 };
        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    if (j == 1)
                    {
                        Response.Write("<td align='center'><b>Collection Date</b></td>");
                    }
                    else
                    {
                        Response.Write("<td align='center'><b>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</b></td>");
                    }
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                Response.Write("<td align='center'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        int UnitCityID = Convert.ToInt32(ddlLocations.SelectedValue);
        int SysUserID = Convert.ToInt32(ddlAgents.SelectedValue);
        string fromDate = TextBoxFrom.Text;
        string toDate = TextBoxTo.Text;
        //Response.Redirect("RptDateWiseSummary_Viewer.aspx?UnitCityID=" + UnitCityID + "&SysUserID=" + SysUserID + "&FromDate=" + fromDate + "&ToDate=" + toDate + "&RptName=rptRetailReco.rpt");
        //return;
        ClsAdmin objadmin = new ClsAdmin();
        DataTable dtEx = new DataTable();
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=ReconciliationReport.ods");
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=ReconciliationReport.xls");
        }

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else
        {
            Response.ContentType = "application/vnd.ms-excel";
        }

        this.EnableViewState = false;
        Response.Write("\r\n");

        
        if (FinYear.SelectedItem.ToString() == "2016-2025")
        {
            dsExcel = objadmin.ReconciliationReport(UnitCityID, SysUserID, TextBoxFrom.Text, TextBoxTo.Text);
        }
        else //if (FinYear.SelectedItem.ToString() == "2009-2010")
        {
            dsExcel = objadmin.ReconciliationReport_0910(UnitCityID
                , SysUserID, TextBoxFrom.Text
                , TextBoxTo.Text, FinYear.SelectedItem.ToString());
        }

        int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());
        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");
        //int[] iColumns = { 0, 1, 2, 3, 4, 8, 9, 10, 11, 12, 13, 5, 6, 7 };
        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    if (j == 1)
                    {
                        Response.Write("<td align='center'><b></b></td>");
                        Response.Write("<td align='center'><b>Date</b></td>");
                    }
                    else
                    {
                        Response.Write("<td align='center'><b>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</b></td>");
                    }
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                if (j == 1)
                {
                    if (dtEx.Rows[i][iColumns[j]].ToString() == "")
                    {
                        Response.Write("<td align='center'>OpBal</td>");
                    }
                    else
                    {
                        Response.Write("<td align='center'></td>");
                    }
                    Response.Write("<td align='center'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                else
                {
                    Response.Write("<td align='center'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }
}