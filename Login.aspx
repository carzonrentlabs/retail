<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" Title="Airport Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <title>CarzonRent Internal Software | Airport Login </title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
	<style>
	body{
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  font-style: normal;
}
 
input#btnSendOTP {
    background-color: #FFD227;
    border: 1px solid #c1c1c1;
    font-size: 14px;
    height: 40px;
    border-radius: 5px;
    width: 100px;
    text-transform: uppercase;
    font-weight: 500;
}
input#btnValidate {
    background-color: #FFD227;
    border: 1px solid #c1c1c1;
    font-size: 14px;
    height: 40px;
    border-radius: 5px;
    width: 100px;
    text-transform: uppercase;
    font-weight: 500;
}
.wrapper-inner-login {
    padding: 30px;
	height: 100vh;
}
.form-group.fieldwrapper {
    margin-bottom: 15px;
}
.form-init {
    background-color: #393939;
    padding: 30px;
    width: 500px;
    margin: 0 auto;
    box-shadow: 0px 4px 5px 0px rgba(0, 0, 0, 0.75);
    border-radius: 10px;
}
.field-label label {
    font-size: 14px;
    font-weight: 500;
    line-height: normal;
    color: white;
    padding-bottom: 5px;
} 
.text-field input:focus {outline:none; box-shadow:unset;border-color:#FFD227;}
.text-field input {
    padding: 10px;
    width: 100%;
    height: 40px;
    border: 1px solid #c1c1c1;
    border-radius: 5px;
}
.heading-wrapper h1 {
    font-size: 30px;
    text-align: center;
    margin: 0px;
    line-height: normal;
}
.align-center-form {
    margin-top: 80px;
}

	</style>
</head>
<body>
<div class="login-cor">
	<div class="container">
	<div class="wrapper-inner-login">
		<div class="heading-wrapper">
			<h1>Log in to the CarzonRent :: Internal software</h1>
		</div>
		<div class="align-center-form">
		<form id="frmlogin" runat="server">
 <div class="hm-fieldset">
         <div class="form-init">
             
             
          	<p><asp:Label ID="lblMessage" runat="server" Text="" Font-Bold="true" ForeColor="red"
Font-Size="small"></asp:Label></p>
           
            <div class="form-group fieldwrapper">
                <div class="field-label">
                    <label>Login ID: </label>
                 </div>
                <div class="text-field">
                   <asp:TextBox ID="txtloginid" runat="server"></asp:TextBox>
                </div>
            </div>
             
              <div class="form-group fieldwrapper">
                 <div class="field-label">
                     
                        <label>Password:</label>
                    
                </div>
              <div class="text-field">
                  <asp:TextBox TextMode="Password" ID="txtpass" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="hndPwd" runat="server" />
                </div>
            </div>
            <div class="form-group fieldwrapper">
<asp:Button ID="btnSendOTP" Text="Send OTP" runat="server" OnClick="btnSendOTP_Click" />
</div>
           <div class="form-group fieldwrapper">
                <div class="field-label">
                   <label>Enter OTP:</label>
                 </div>
              <div class="text-field">
                     
                        <asp:TextBox ID="txtotp" runat="server"></asp:TextBox>
                         
                        
                    
                </div>
            </div>
			
            <div class="form-group fieldwrapper">
			<%--<div class="field-label Hiddenblock">
                   <label>&nbsp; </label>
                 </div>
                 <div class="form-group fieldwrapper">
                    <asp:Button ID="btnValidate" Visible="false" Text="Validate" runat="server" OnClick="btnValidate_Click" />
                </div>--%>
                <asp:Button ID="btnValidate" Visible="false" Text="Validate" runat="server" OnClick="btnValidate_Click" />
            </div>
        </div>
		
	</div>
    </form>
	</div>
	</div>
	</div>
</div>
    

</body>
</html>
