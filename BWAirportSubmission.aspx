<%@ Page Language="C#" MasterPageFile="~/CIPL.master" AutoEventWireup="true" CodeFile="BWAirportSubmission.aspx.cs"
    Inherits="BWAirportSubmission" Title="COR - Retail Module" StylesheetTheme="StyleSheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="javascript" src="App_Themes/CommonScript.js" type="text/javascript"></script>

    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%; padding: 1px 1px 1px 1px;
        background-color: Black">
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; padding: 1px 1px 1px 1px;
                    background-color: White">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td class="pgHeading">
                                        Airport Batch/Cash Submission</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 342px" align="left">
                                                    Branch Name:
                                                    <asp:DropDownList ID="ddlLocation" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                        onBlur="GetData()">
                                                    </asp:DropDownList></td>
                                                <td align="left">
                                                    Date:<asp:TextBox ID="txtDate" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                        Height="10px" onBlur="GetData()"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="CaltxtDate" TargetControlID="txtDate" Format="dd-MMM-yyyy"
                                                        runat="server">
                                                    </cc1:CalendarExtender>
                                                    <asp:Label ID="lblWait" runat="server" Font-Bold="True" ForeColor="Red">Please wait...</asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="padding-top: 10px; padding-bottom: 10px">
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="border-right: solid 1px gray">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td align="center" colspan="2" style="height: 21px">
                                                                <strong>** CASH Details **</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="height: 21px">
                                                                Bal. B/F</td>
                                                            <td align="left" style="height: 21px">
                                                                <asp:Label ID="lblCashBF" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="height: 21px">
                                                                No. of Duties</td>
                                                            <td align="left" style="height: 21px">
                                                                <asp:Label ID="lblCashDuties" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Sale</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCashAmount" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Collection</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCashColl" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Balance</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCashBal" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Deposited</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtCashDeposit" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Slip No</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtSlipNo" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="border-right: solid 1px gray; padding-left: 5px">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <strong>** Amex Details **</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Bal. B/F</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblAmexBF" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                No. of Duties</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblAmexDuties" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Sale</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblAmexAmount" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Collection</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblAmexColl" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Balance</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblAmexBal" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Deposited</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtAmexDeposit" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Batch No</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtAmexBatchNo" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="border-right: solid 1px gray; padding-left: 5px">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <strong>** HDFC Details **</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Bal. B/F</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblHdfcBF" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                No. of Duties</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblHDFCDuties" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Sale</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblHDFCAmount" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Collection</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblHdfcColl" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Balance</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblHDFCBal" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Deposited</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtHDFCDeposit" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Batch No</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtHDFCBatchNo" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <strong>** EWallet Details **</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Bal. B/F</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblEWalletBF" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                No. of Duties</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblEWalletDuties" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Sale</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblEWalletAmount" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Collection</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblEWalletColl" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Balance</td>
                                                            <td align="left">
                                                                <asp:Label ID="lblEWalletBal" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Deposited</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtEWalletDeposit" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                Batch No</td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtEWalletBatchNo" runat="server" CssClass="txtBox" onkeypress="return onlyNumbers();"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                            Text="Submit" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnReset" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                            Text="Reset" OnClientClick="frmReset('Reset');" UseSubmitBehavior="False" OnClick="btnReset_Click" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Smaller"
                                            ForeColor="Red"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input id="ChkSave" type="hidden" runat="Server" value="0" style="width: 16px" />
    <input id="hdnCashDuties" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnCashBF" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnCashColl" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnCashAmount" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnCashBal" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnAmexBF" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnAmexColl" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnAmexDuties" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnAmexAmount" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnAmexBal" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnHdfcBF" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnHdfcColl" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnHDFCDuties" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnHDFCAmount" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnHDFCBal" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnEWalletBF" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnEWalletColl" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnEWalletDuties" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnEWalletAmount" type="hidden" runat="Server" value="" style="width: 16px" />
    <input id="hdnEWalletBal" type="hidden" runat="Server" value="" style="width: 16px" />
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="App_Themes/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>

    <script language="javascript" type="text/javascript">
function frmReset(flg)
{
    if (flg=="Reset") 
        document.getElementById("<%=txtDate.ClientID %>").value= "";

    // ********* For CASH 
    document.getElementById('<%=lblCashBF.ClientID %>').innerHTML= "";
    document.getElementById('<%=hdnCashBF.ClientID %>').value= "";
    document.getElementById('<%=lblCashDuties.ClientID %>').innerHTML= "";
    document.getElementById('<%=hdnCashDuties.ClientID %>').value= "";
    document.getElementById("<%=lblCashAmount.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnCashAmount.ClientID %>').value= "";
    document.getElementById("<%=lblCashColl.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnCashColl.ClientID %>').value= "";
    document.getElementById("<%=lblCashBal.ClientID %>").innerHTML="";
    document.getElementById('<%=hdnCashBal.ClientID %>').value= "";
    document.getElementById('<%=txtCashDeposit.ClientID %>').value= "";
    document.getElementById("<%=txtSlipNo.ClientID %>").value= "";

    // ********* For AMEX 
    document.getElementById("<%=lblAmexBF.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnAmexBF.ClientID %>').value= "";
    document.getElementById("<%=lblAmexDuties.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnAmexDuties.ClientID %>').value= "";
    document.getElementById("<%=lblAmexAmount.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnAmexAmount.ClientID %>').value= "";
    document.getElementById("<%=lblAmexColl.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnAmexColl.ClientID %>').value= "";
    document.getElementById("<%=lblAmexBal.ClientID %>").innerHTML="";
    document.getElementById('<%=hdnAmexBal.ClientID %>').value= "";
    document.getElementById('<%=txtAmexDeposit.ClientID %>').value= "";
    document.getElementById("<%=txtAmexBatchNo.ClientID %>").value= "";

    // ********* For HDFC 
    document.getElementById("<%=lblHdfcBF.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnHdfcBF.ClientID %>').value= "";
    document.getElementById("<%=lblHDFCDuties.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnHDFCDuties.ClientID %>').value= "";
    document.getElementById("<%=lblHDFCAmount.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnHDFCAmount.ClientID %>').value= "";
    document.getElementById("<%=lblHdfcColl.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnHdfcColl.ClientID %>').value= "";
    document.getElementById("<%=lblHDFCBal.ClientID %>").innerHTML="";
    document.getElementById('<%=hdnHDFCBal.ClientID %>').value= "";
    document.getElementById('<%=txtHDFCDeposit.ClientID %>').value= "";
    document.getElementById("<%=txtHDFCBatchNo.ClientID %>").value= "";
    
     // ********* For EWallet 
    document.getElementById("<%=lblEWalletBF.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnEWalletBF.ClientID %>').value= "";
    document.getElementById("<%=lblEWalletDuties.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnEWalletDuties.ClientID %>').value= "";
    document.getElementById("<%=lblEWalletAmount.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnEWalletAmount.ClientID %>').value= "";
    document.getElementById("<%=lblEWalletColl.ClientID %>").innerHTML= "";
    document.getElementById('<%=hdnEWalletColl.ClientID %>').value= "";
    document.getElementById("<%=lblEWalletBal.ClientID %>").innerHTML="";
    document.getElementById('<%=hdnEWalletBal.ClientID %>').value= "";
    document.getElementById('<%=txtEWalletDeposit.ClientID %>').value= "";
    document.getElementById("<%=txtEWalletBatchNo.ClientID %>").value= "";
}

function ChkValidation()
{
    if (document.getElementById("<%=ddlLocation.ClientID %>").value == '0')
    {
        alert("Please select branch");
        document.getElementById("<%=ddlLocation.ClientID %>").focus();
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    if (document.getElementById("<%=txtDate.ClientID %>").value == '')
    {
        alert("Please select date");
        //document.getElementById("<%=txtDate.ClientID %>").focus();
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    var DepositAmount = document.getElementById("<%=txtCashDeposit.ClientID %>").value;
    if (DepositAmount=="")
    {
        DepositAmount=0;
    }
    DepositAmount = parseFloat(DepositAmount);
    if (DepositAmount<0)
    {
        alert("Cash deposit should be non-negatvie!");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    if (DepositAmount>0 && document.getElementById("<%=txtSlipNo.ClientID %>").value == '')
    {
        alert("Please enter cash deposit slip no.");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    var DepositAmount = document.getElementById("<%=txtAmexDeposit.ClientID %>").value;
    if (DepositAmount=="")
    {
        DepositAmount=0;
    }
    DepositAmount = parseFloat(DepositAmount);
    if (DepositAmount<0)
    {
        alert("Amex deposit should be non-negatvie!");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    if (DepositAmount>0 && document.getElementById("<%=txtAmexBatchNo.ClientID %>").value == '')
    {
        alert("Please enter Amex batch no.");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    var DepositAmount = document.getElementById("<%=txtHDFCDeposit.ClientID %>").value;
    if (DepositAmount=="")
    {
        DepositAmount=0;
    }
    DepositAmount = parseFloat(DepositAmount);
    if (DepositAmount<0)
    {
        alert("HDFC deposit should be non-negatvie!");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    if (DepositAmount>0 && document.getElementById("<%=txtHDFCBatchNo.ClientID %>").value == '')
    {
        alert("Please enter HDFC batch no.");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }
    
    
    var DepositAmount = document.getElementById("<%=txtEWalletDeposit.ClientID %>").value;
    if (DepositAmount=="")
    {
        DepositAmount=0;
    }
    DepositAmount = parseFloat(DepositAmount);
    if (DepositAmount<0)
    {
        alert("EWallet deposit should be non-negatvie!");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

    if (DepositAmount>0 && document.getElementById("<%=txtEWalletBatchNo.ClientID %>").value == '')
    {
        alert("Please enter EWallet batch no.");
        document.getElementById("<%=ChkSave.ClientID %>").value="0";
        return false;
    }

document.getElementById("<%=ChkSave.ClientID %>").value="1";
}

//******************************
/*Implementation of AJAX*/
//******************************
var xml;
function GetData()
{
    frmReset('Refresh');
    LocationID = document.getElementById("<%=ddlLocation.ClientID %>").value;
    txtDate = document.getElementById("<%=txtDate.ClientID %>").value;
    if (LocationID==0 || txtDate=="")
        return
    document.getElementById("<%=lblWait.ClientID %>").style.display="inline";

    xml=getXMLHTTPRequest();
    if(xml!=null)
    {
    	var url = "BWAirportSubmission_Ajax.aspx";
	    url = url + "?LocationID=" + LocationID;
	    url = url + "&txtDate=" + txtDate;
	    url = url + "&sid=" + Math.random();
	    //document.write(url);

        xml.onreadystatechange= FillResponse;
        xml.open("GET", url, true);
        xml.send(null);
    }
return false;
}

function getXMLHTTPRequest()
{
    var xRequest=null;
    if (window.XMLHttpRequest) 
    {
        xRequest=new XMLHttpRequest();
    }
    else if (typeof ActiveXObject != "undefined")
    {
        xRequest=new ActiveXObject("Microsoft.XMLHTTP");
    }
return xRequest;
}

function FillResponse()
{
    if(xml.readyState == 4)
    {
        //alert(xml.status);
        if(xml.status == 200)
        {
            var retval=xml.responseText; 
        	retval = retval.replace(/^\s+/,"");	//This function removes Left Blank Spaces
	        retval = retval.replace(/\s+$/,"");	//This function removes Right Blank Spaces
	        if (retval!="")
	        {
	           var CashBal = 0;
	           var AmexBal = 0;
	           var HDFCBal = 0; 
	           retvalArray = retval.split("#");
	           document.getElementById('<%=lblCashBF.ClientID %>').innerHTML= retvalArray[0];
	           document.getElementById('<%=hdnCashBF.ClientID %>').value= retvalArray[0];
	           if (retvalArray[0]!="")
	           {
    	           CashBal = parseFloat(retvalArray[0]);
	           }
	            
	           document.getElementById('<%=lblCashDuties.ClientID %>').innerHTML= retvalArray[1];
	           document.getElementById('<%=hdnCashDuties.ClientID %>').value= retvalArray[1];
	           
	           document.getElementById("<%=lblCashAmount.ClientID %>").innerHTML= retvalArray[2];
	           document.getElementById('<%=hdnCashAmount.ClientID %>').value= retvalArray[2];
	           if (retvalArray[2]!="")
	           {
    	           CashBal = CashBal + parseFloat(retvalArray[2]);
	           }
	           	           
	           document.getElementById("<%=lblCashColl.ClientID %>").innerHTML= retvalArray[3];
	           document.getElementById('<%=hdnCashColl.ClientID %>').value= retvalArray[3];
	           if (retvalArray[3]!="")
	           {
    	           CashBal = CashBal - parseFloat(retvalArray[3]);
	           }
	           document.getElementById("<%=lblCashBal.ClientID %>").innerHTML= CashBal;
	           document.getElementById('<%=hdnCashBal.ClientID %>').value= CashBal;

	           document.getElementById("<%=lblAmexBF.ClientID %>").innerHTML= retvalArray[4];
	           document.getElementById('<%=hdnAmexBF.ClientID %>').value= retvalArray[4];
	           if (retvalArray[4]!="")
	           {
    	           AmexBal = parseFloat(retvalArray[4]);
	           }

	           document.getElementById("<%=lblAmexDuties.ClientID %>").innerHTML= retvalArray[5];
	           document.getElementById('<%=hdnAmexDuties.ClientID %>').value= retvalArray[5];
	           
	           document.getElementById("<%=lblAmexAmount.ClientID %>").innerHTML= retvalArray[6];
	           document.getElementById('<%=hdnAmexAmount.ClientID %>').value= retvalArray[6];
	           //document.getElementById('<%=txtAmexDeposit.ClientID %>').value= retvalArray[6];
	           if (retvalArray[6]!="")
	           {
    	           AmexBal = AmexBal + parseFloat(retvalArray[6]);
	           }
	           
	           document.getElementById("<%=lblAmexColl.ClientID %>").innerHTML= retvalArray[7];
	           document.getElementById('<%=hdnAmexColl.ClientID %>').value= retvalArray[7];
	           if (retvalArray[7]!="")
	           {
    	           AmexBal = AmexBal - parseFloat(retvalArray[7]);
	           }
	           document.getElementById("<%=lblAmexBal.ClientID %>").innerHTML= AmexBal;
	           document.getElementById('<%=hdnAmexBal.ClientID %>').value= AmexBal;

	           document.getElementById("<%=lblHdfcBF.ClientID %>").innerHTML= retvalArray[8];
	           document.getElementById('<%=hdnHdfcBF.ClientID %>').value= retvalArray[8];
	           if (retvalArray[8]!="")
	           {
    	           HDFCBal = parseFloat(retvalArray[8]);
	           }
	           
	           document.getElementById("<%=lblHDFCDuties.ClientID %>").innerHTML= retvalArray[9];
	           document.getElementById('<%=hdnHDFCDuties.ClientID %>').value= retvalArray[9];
	           
	           document.getElementById("<%=lblHDFCAmount.ClientID %>").innerHTML= retvalArray[10];
	           document.getElementById('<%=hdnHDFCAmount.ClientID %>').value= retvalArray[10];
	           //document.getElementById('<%=txtHDFCDeposit.ClientID %>').value= retvalArray[10];
	           if (retvalArray[10]!="")
	           {
    	           HDFCBal = HDFCBal + parseFloat(retvalArray[10]);
	           }

	           document.getElementById("<%=lblHdfcColl.ClientID %>").innerHTML= retvalArray[11];
	           document.getElementById('<%=hdnHdfcColl.ClientID %>').value= retvalArray[11];
	           if (retvalArray[11]!="")
	           {
    	           HDFCBal = HDFCBal - parseFloat(retvalArray[11]);
	           }

	           document.getElementById("<%=lblHDFCBal.ClientID %>").innerHTML= HDFCBal;
	           document.getElementById('<%=hdnHDFCBal.ClientID %>').value= HDFCBal;
               
               
	           
	           document.getElementById("<%=lblEWalletBF.ClientID %>").innerHTML= retvalArray[12];
	           document.getElementById('<%=hdnEWalletBF.ClientID %>').value= retvalArray[12];
	           if (retvalArray[12]!="")
	           {
    	           EWalletBal = parseFloat(retvalArray[12]);
	           }
	           
	           document.getElementById("<%=lblEWalletDuties.ClientID %>").innerHTML= retvalArray[13];
	           document.getElementById('<%=hdnEWalletDuties.ClientID %>').value= retvalArray[13];
	           
	           document.getElementById("<%=lblEWalletAmount.ClientID %>").innerHTML= retvalArray[14];
	           document.getElementById('<%=hdnEWalletAmount.ClientID %>').value= retvalArray[14];
	           
	           if (retvalArray[14]!="")
	           {
    	           EWalletBal = EWalletBal + parseFloat(retvalArray[14]);
	           }

	           document.getElementById("<%=lblEWalletColl.ClientID %>").innerHTML= retvalArray[15];
	           document.getElementById('<%=hdnEWalletColl.ClientID %>').value= retvalArray[15];
	           if (retvalArray[15]!="")
	           {
    	           EWalletBal = EWalletBal - parseFloat(retvalArray[15]);
	           }

	           document.getElementById("<%=lblEWalletBal.ClientID %>").innerHTML= EWalletBal;
	           document.getElementById('<%=hdnEWalletBal.ClientID %>').value= EWalletBal;

	           document.getElementById('<%=btnSave.ClientID %>').disabled= false;
	           document.getElementById("<%=lblWait.ClientID %>").style.display="none";
	        }
	        else
	        {
	           frmReset('Refresh'); 
	           
	           document.getElementById('<%=btnSave.ClientID %>').disabled= true;
	           document.getElementById("<%=lblWait.ClientID %>").style.display="none";
               alert("No data found!");
	        }
        }
        else
        {
           frmReset('Refresh'); 
           
           document.getElementById('<%=btnSave.ClientID %>').disabled= true;
           document.getElementById("<%=lblWait.ClientID %>").style.display="none";
           alert("Error while retrieving data!" );
        } 
    }
}
//******************************
/*End of AJAX*/
//******************************

//On page load make lable display none
document.getElementById("<%=lblWait.ClientID %>").style.display="none";
    </script>

</asp:Content>
