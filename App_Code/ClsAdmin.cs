using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using RetailModule;
//using static System.Net.WebRequestMethods;

/// <summary>
/// Summary description for ClsAdmin
/// </summary>
public class ClsAdmin
{
    public ClsAdmin()
    {
        dbUser = "carzonrent";
        dbPWD = "CarZ#58B9";
        //
        // TODO: Add constructor logic here
        //
    }
    public static bool flagComplete = false;
    public string dbUser, dbPWD;



    public DataSet GetCity(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getRetailCity", ObjParam);
    }
    public DataTable GetPaymentMode()
    {
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_RetailPaymentMode");
    }

    public DataSet BindRentalType()
    {
        DataSet RentalType = new DataSet();

        string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

        SqlConnection InstaCon = new SqlConnection(connectionString);

        SqlCommand GetRentalType = InstaCon.CreateCommand();
        GetRentalType.CommandText = "select b.LookupID,b.Description,b.RentalType,b.ServiceType, B.AirportDirection from CorIntRentalTypeMaster as A inner join CorIntRentalTypeLookup as B on A.RentalType=b.RentalType where a.Active =1 and b.Active =1";

        if (InstaCon.State != ConnectionState.Open)
        {
            InstaCon.Open();
        }

        SqlDataAdapter objAdapter = new SqlDataAdapter(GetRentalType);
        objAdapter.Fill(RentalType);

        if (InstaCon.State != ConnectionState.Closed)
        {
            InstaCon.Close();
        }

        return RentalType;
    }

    public DataSet GetCity2(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getRetailCityRPT", ObjParam);
    }

    public DataSet GetAjax_BWAirportDetails(int LocationID, DateTime txtDate)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@UnitCityid", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = LocationID;

        ObjParam[1] = new SqlParameter("@Date", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = txtDate;
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_BranchWiseAirportSubmission", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_BranchWiseAirportSubmission_New1", ObjParam);
    }
    public DataSet GetAgents(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@SysUserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_Agents", ObjParam);
    }

    public DataTable GetCompany()
    {
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "getRetailCompany");
    }

    //public DataTable GetCars(int UserID, String CarNo)
    //{
    //    SqlParameter[] ObjParam = new SqlParameter[2];
    //    ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
    //    ObjParam[0].Direction = ParameterDirection.Input;
    //    ObjParam[0].Value = UserID;

    //    ObjParam[1] = new SqlParameter("@CarNo", SqlDbType.VarChar);
    //    ObjParam[1].Direction = ParameterDirection.Input;
    //    ObjParam[1].Value = CarNo;
    //    return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "getRetailCars", ObjParam);
    //}

    public DataTable GetCars(int UserID, String CarNo, int cityid)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;

        ObjParam[1] = new SqlParameter("@CarNo", SqlDbType.VarChar);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = CarNo;

        ObjParam[2] = new SqlParameter("@CityID", SqlDbType.VarChar);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = cityid;


        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "getRetailCars", ObjParam);
    }

    public DataTable GetCarsBookingIDWise(int UserID, int BookingID)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@UserID", UserID);
        ObjParam[1] = new SqlParameter("@BookingID", BookingID);
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure
            , "PRC_getRetailCarsByBookingID", ObjParam);
    }

    public DataSet AllocateBooking(int CarID, int BookingID, int UserID)
    {
        SqlParameter[] objPram = new SqlParameter[3];
        objPram[0] = new SqlParameter("@CarID", CarID);
        objPram[1] = new SqlParameter("@BookingID", BookingID);
        objPram[2] = new SqlParameter("@UserID", UserID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_AllocateCarToRetail", objPram);
    }

    public DataTable GetLastTransactions(string mobileno, int CityID)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@MobileNo", mobileno);
        ObjParam[1] = new SqlParameter("@CityID", CityID);

        //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure
        //    , "proc_GetLastThreeInvoicesByMobileNo", ObjParam);
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure
            , "Proc_GetLastThreeInvoicesApt", ObjParam);
    }

    public DataTable Search_GuestMobile(String MobileNo)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@MobileNo", SqlDbType.VarChar);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = MobileNo;

        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Search_GuestMobile", ObjParam);
    }

    public DataTable Search_ContBooking(int PkpCityID, int CompID, string BookingID)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@PkpCityID", SqlDbType.VarChar);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = PkpCityID;

        ObjParam[1] = new SqlParameter("@CompID", SqlDbType.VarChar);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = CompID;

        ObjParam[2] = new SqlParameter("@BookingID", SqlDbType.VarChar);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = BookingID;

        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Search_ContBooking", ObjParam);
    }

    public DataTable GetChauffeur(int CarID, bool VendorCarYN)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@CarID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = CarID;

        ObjParam[1] = new SqlParameter("@VendorCarYN", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = VendorCarYN;

        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "getRetailChauffeur", ObjParam);
    }

    public string GetDefaultChauff(int CarID, bool VendorCarYN)
    {
        DataSet ds = new DataSet();
        string chauffID = "";
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@CarID", CarID);
        ObjParam[1] = new SqlParameter("@VendorCarYN", VendorCarYN);
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getDefaultChauff", ObjParam);
        if (ds.Tables[0].Rows.Count > 0)
        {
            chauffID = Convert.ToString(ds.Tables[0].Rows[0]["ChauffID"]);
        }

        return chauffID;
    }

    public DataTable GetRetailPkg(int Cityid, int ClientId, int CatId)
    {
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@Cityid", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = Cityid;

        ObjParam[1] = new SqlParameter("@ClientId", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = ClientId;

        ObjParam[2] = new SqlParameter("@CatId", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = CatId;


        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GetRetailPkg", ObjParam);
        //return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GetRetailPkg_GSTSurcharge_Test3", ObjParam);
    }

    public DataTable GetRetailPkgBasedOnHrsKms(int Cityid, int ClientId, int CatId, int PkgType, int Hrs, int Kms)
    {
        SqlParameter[] ObjParam = new SqlParameter[6];
        ObjParam[0] = new SqlParameter("@Cityid", Cityid);
        ObjParam[1] = new SqlParameter("@ClientId", ClientId);
        ObjParam[2] = new SqlParameter("@CatId", CatId);
        ObjParam[3] = new SqlParameter("@PkgType", PkgType);
        ObjParam[4] = new SqlParameter("@TotalHrs", Hrs);
        ObjParam[5] = new SqlParameter("@Totalkms", Kms);

        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GetRetailPkg_BasedOnHrsKms", ObjParam);
    }

    public DataTable getUnitDetails(int Cityid)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@Cityid", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = Cityid;
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "getUnitDetails", ObjParam);
    }
    // added by BkSharma on dated 14 June 2013 for special type of car calculation
    public DataTable getUnitDetailsSpecialTypeofCar(int Cityid, int CarId)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@Cityid", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = Cityid;

        ObjParam[1] = new SqlParameter("@CarID", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = CarId;

        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "getUnitDetailsSpecialTypeofCar", ObjParam);
    }
    //End of Special car of calculation
    //Added by bk sharma on dated 25 July 2013 for chauffuer list
    public DataTable getChauffuer(int carId, int @userid, String CarType, int CityID)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];

        ObjParam[0] = new SqlParameter("@CarId", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = carId;

        ObjParam[1] = new SqlParameter("@userid", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = @userid;

        ObjParam[2] = new SqlParameter("@CarType", SqlDbType.VarChar);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = CarType;

        ObjParam[3] = new SqlParameter("@CityID", SqlDbType.VarChar);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = CityID;


        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetRetailsChauffeur", ObjParam);

    }



    //End of chauffuer list

    public bool CheckReportAccess(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;

        ObjParam[1] = new SqlParameter("@AccessAllowedYN", SqlDbType.Bit);
        ObjParam[1].Direction = ParameterDirection.Output;

        bool AccessAllowedYN = false;
        try
        {
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PRC_RetailReportAccess", ObjParam);

            AccessAllowedYN = Convert.ToBoolean(ObjParam[1].Value.ToString());
            return AccessAllowedYN;
        }
        catch (Exception ex)
        {
            AccessAllowedYN = false;
        }
        return AccessAllowedYN;
    }



    //string transactionid, string authorizationid, string CCNo, string ExpYYMM, string trackid,
    public int SaveRetailBooking(int Cityid, int CompanyId, String GuestName, String GuestMobile, int carid
        , int ModelId, int CatId, int VendorCarYN, int PkgId, decimal PkgRate, decimal TotalCost
        , String PaymentMode, String Remarks, int ServiceUnitID, int UserID, decimal BasicCost, decimal SerTaxPer
        , decimal EduTaxPer, int UpdateGuest, int GuestID, int ContBookingID, int cCTYpeID, decimal HduTaxPer
        , String PosName, string VoucherNo1, string VoucherNo2, string VoucherNo3, bool VoucherYN, int payBackVal
        , decimal SwachhBharatTax, decimal KrishiKalyanTax
        , decimal CGSTTaxPercent, decimal SGSTTaxPercent, decimal IGSTTaxPercent, decimal CGSTTaxAmt, decimal SGSTTaxAmt, decimal IGSTTaxAmt
        , int GSTClientID, decimal GSTSurchargeAmount, decimal PkgRateWithSurchargeAMT, decimal DiscountAmt, int DiscountID
        , string EmailID, decimal Toll, int LookupID
        , int ChauffeurID, bool VendorChauffeurYN, string GSTIN
        , string CompanyLegalName, string ShiftName)
    // , int ChauffeurId, int VendorChauffYN)    // change param
    {
        SqlParameter[] ObjParam = new SqlParameter[51];
        ObjParam[0] = new SqlParameter("@Cityid", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = Cityid;

        ObjParam[1] = new SqlParameter("@CompanyId", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = CompanyId;

        ObjParam[2] = new SqlParameter("@GuestName", SqlDbType.VarChar);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = GuestName;

        ObjParam[3] = new SqlParameter("@GuestMobile", SqlDbType.VarChar);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = GuestMobile;

        ObjParam[4] = new SqlParameter("@carid", SqlDbType.Int);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = carid;

        ObjParam[5] = new SqlParameter("@ModelId", SqlDbType.Int);
        ObjParam[5].Direction = ParameterDirection.Input;
        ObjParam[5].Value = ModelId;

        ObjParam[6] = new SqlParameter("@CatId", SqlDbType.Int);
        ObjParam[6].Direction = ParameterDirection.Input;
        ObjParam[6].Value = CatId;

        ObjParam[7] = new SqlParameter("@VendorCarYN", SqlDbType.Int);
        ObjParam[7].Direction = ParameterDirection.Input;
        ObjParam[7].Value = VendorCarYN;

        ObjParam[8] = new SqlParameter("@PkgId", SqlDbType.Int);
        ObjParam[8].Direction = ParameterDirection.Input;
        ObjParam[8].Value = PkgId;

        ObjParam[9] = new SqlParameter("@PkgRate", SqlDbType.Decimal);
        ObjParam[9].Direction = ParameterDirection.Input;
        ObjParam[9].Value = PkgRate;

        ObjParam[10] = new SqlParameter("@TotalCost", SqlDbType.Decimal);
        ObjParam[10].Direction = ParameterDirection.Input;
        ObjParam[10].Value = TotalCost;

        ObjParam[11] = new SqlParameter("@PaymentMode", SqlDbType.VarChar);
        ObjParam[11].Direction = ParameterDirection.Input;
        ObjParam[11].Value = PaymentMode;

        ObjParam[12] = new SqlParameter("@Remarks", SqlDbType.VarChar);
        ObjParam[12].Direction = ParameterDirection.Input;
        ObjParam[12].Value = Remarks;

        ObjParam[13] = new SqlParameter("@ServiceUnitID", SqlDbType.Int);
        ObjParam[13].Direction = ParameterDirection.Input;
        ObjParam[13].Value = ServiceUnitID;

        ObjParam[14] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[14].Direction = ParameterDirection.Input;
        ObjParam[14].Value = UserID;

        ObjParam[15] = new SqlParameter("@BasicCost", SqlDbType.Decimal);
        ObjParam[15].Direction = ParameterDirection.Input;
        ObjParam[15].Value = BasicCost;

        ObjParam[16] = new SqlParameter("@SerTax", SqlDbType.Decimal);
        ObjParam[16].Direction = ParameterDirection.Input;
        ObjParam[16].Value = SerTaxPer;

        ObjParam[17] = new SqlParameter("@EduTax", SqlDbType.Decimal);
        ObjParam[17].Direction = ParameterDirection.Input;
        ObjParam[17].Value = EduTaxPer;

        ObjParam[18] = new SqlParameter("@UpdateGuest", SqlDbType.Decimal);
        ObjParam[18].Direction = ParameterDirection.Input;
        ObjParam[18].Value = UpdateGuest;

        ObjParam[19] = new SqlParameter("@GuestID", SqlDbType.Decimal);
        ObjParam[19].Direction = ParameterDirection.Input;
        ObjParam[19].Value = GuestID;

        ObjParam[20] = new SqlParameter("@ContBookingID", SqlDbType.Decimal);
        ObjParam[20].Direction = ParameterDirection.Input;
        ObjParam[20].Value = ContBookingID;

        ObjParam[21] = new SqlParameter("@CCTypeID", SqlDbType.Decimal);
        ObjParam[21].Direction = ParameterDirection.Input;
        ObjParam[21].Value = cCTYpeID;

        ObjParam[22] = new SqlParameter("@NewBookingID", SqlDbType.Int);
        ObjParam[22].Direction = ParameterDirection.Output;

        //string transactionid,string authorizationid,string CCNo,string ExpYYMM 

        //ObjParam[23] = new SqlParameter("@transactionid", SqlDbType.VarChar);
        //ObjParam[23].Direction = ParameterDirection.Input;
        //ObjParam[23].Value = transactionid;

        //ObjParam[24] = new SqlParameter("@authorizationid", SqlDbType.VarChar);
        //ObjParam[24].Direction = ParameterDirection.Input;
        //ObjParam[24].Value = authorizationid;

        //ObjParam[25] = new SqlParameter("@CCNo", SqlDbType.VarChar);
        //ObjParam[25].Direction = ParameterDirection.Input;
        //ObjParam[25].Value = CCNo;

        //ObjParam[26] = new SqlParameter("@EXPYYMM", SqlDbType.VarChar);
        //ObjParam[26].Direction = ParameterDirection.Input;
        //ObjParam[26].Value = ExpYYMM;

        //ObjParam[27] = new SqlParameter("@trackid", SqlDbType.VarChar);
        //ObjParam[27].Direction = ParameterDirection.Input;
        //ObjParam[27].Value = trackid;

        ObjParam[23] = new SqlParameter("@HduTax", SqlDbType.Decimal);
        ObjParam[23].Direction = ParameterDirection.Input;
        ObjParam[23].Value = HduTaxPer;

        ObjParam[24] = new SqlParameter("@POSID", SqlDbType.VarChar);
        ObjParam[24].Direction = ParameterDirection.Input;
        ObjParam[24].Value = PosName;

        ObjParam[25] = new SqlParameter("@VoucherNo1", SqlDbType.VarChar);
        ObjParam[25].Direction = ParameterDirection.Input;
        ObjParam[25].Value = VoucherNo1;

        ObjParam[26] = new SqlParameter("@VoucherNo2", SqlDbType.VarChar);
        ObjParam[26].Direction = ParameterDirection.Input;
        ObjParam[26].Value = VoucherNo2;

        ObjParam[27] = new SqlParameter("@VoucherNo3", SqlDbType.VarChar);
        ObjParam[27].Direction = ParameterDirection.Input;
        ObjParam[27].Value = VoucherNo3;

        ObjParam[28] = new SqlParameter("@VoucherYN", SqlDbType.Bit);
        ObjParam[28].Direction = ParameterDirection.Input;
        ObjParam[28].Value = VoucherYN;

        ObjParam[29] = new SqlParameter("@PayBackStatus", SqlDbType.Int);
        ObjParam[29].Direction = ParameterDirection.Input;
        ObjParam[29].Value = payBackVal;

        ObjParam[30] = new SqlParameter("@SwachhBharatTax", SqlDbType.Decimal);
        ObjParam[30].Direction = ParameterDirection.Input;
        ObjParam[30].Value = SwachhBharatTax;

        ObjParam[31] = new SqlParameter("@KrishiKalyanTax", SqlDbType.Decimal);
        ObjParam[31].Direction = ParameterDirection.Input;
        ObjParam[31].Value = KrishiKalyanTax;

        //ObjParam[29] = new SqlParameter("@ChauffeurID", SqlDbType.Int);
        //ObjParam[29].Direction = ParameterDirection.Input;
        //ObjParam[29].Value = ChauffeurId;

        //ObjParam[30] = new SqlParameter("@VendorChauffYN", SqlDbType.Int);
        //ObjParam[30].Direction = ParameterDirection.Input;
        //ObjParam[30].Value = VendorChauffYN;

        ObjParam[32] = new SqlParameter("@CGSTTaxAmt", SqlDbType.Decimal);
        ObjParam[32].Direction = ParameterDirection.Input;
        ObjParam[32].Value = CGSTTaxAmt;

        ObjParam[33] = new SqlParameter("@SGSTTaxAmt", SqlDbType.Decimal);
        ObjParam[33].Direction = ParameterDirection.Input;
        ObjParam[33].Value = SGSTTaxAmt;

        ObjParam[34] = new SqlParameter("@IGSTTaxAmt", SqlDbType.Decimal);
        ObjParam[34].Direction = ParameterDirection.Input;
        ObjParam[34].Value = IGSTTaxAmt;

        ObjParam[35] = new SqlParameter("@CGSTTaxPercent", SqlDbType.Decimal);
        ObjParam[35].Direction = ParameterDirection.Input;
        ObjParam[35].Value = CGSTTaxPercent;

        ObjParam[36] = new SqlParameter("@SGSTTaxPercent", SqlDbType.Decimal);
        ObjParam[36].Direction = ParameterDirection.Input;
        ObjParam[36].Value = SGSTTaxPercent;

        ObjParam[37] = new SqlParameter("@IGSTTaxPercent", SqlDbType.Decimal);
        ObjParam[37].Direction = ParameterDirection.Input;
        ObjParam[37].Value = IGSTTaxPercent;

        ObjParam[38] = new SqlParameter("@ClientGSTId", SqlDbType.Int);
        ObjParam[38].Direction = ParameterDirection.Input;
        ObjParam[38].Value = GSTClientID;

        ObjParam[39] = new SqlParameter("@GSTSurchargeAmount", SqlDbType.Decimal);
        ObjParam[39].Direction = ParameterDirection.Input;
        ObjParam[39].Value = GSTSurchargeAmount;

        ObjParam[40] = new SqlParameter("@PkgRateWithSurchargeAMT", SqlDbType.Decimal);
        ObjParam[40].Direction = ParameterDirection.Input;
        ObjParam[40].Value = PkgRateWithSurchargeAMT;

        ObjParam[41] = new SqlParameter("@DiscountAmt", SqlDbType.Decimal);
        ObjParam[41].Direction = ParameterDirection.Input;
        ObjParam[41].Value = DiscountAmt;

        ObjParam[42] = new SqlParameter("@DiscountID", SqlDbType.Int);
        ObjParam[42].Direction = ParameterDirection.Input;
        ObjParam[42].Value = DiscountID;

        ObjParam[43] = new SqlParameter("@EmailID", SqlDbType.VarChar);
        ObjParam[43].Direction = ParameterDirection.Input;
        ObjParam[43].Value = EmailID;

        ObjParam[44] = new SqlParameter("@Toll", SqlDbType.Decimal);
        ObjParam[44].Direction = ParameterDirection.Input;
        ObjParam[44].Value = Toll;

        ObjParam[45] = new SqlParameter("@LookupID", SqlDbType.Int);
        ObjParam[45].Direction = ParameterDirection.Input;
        ObjParam[45].Value = LookupID;


        ObjParam[46] = new SqlParameter("@ChauffeurID", SqlDbType.Int);
        ObjParam[46].Direction = ParameterDirection.Input;
        ObjParam[46].Value = ChauffeurID;

        ObjParam[47] = new SqlParameter("@VendorChauffeurYN", SqlDbType.Bit);
        ObjParam[47].Direction = ParameterDirection.Input;
        ObjParam[47].Value = VendorChauffeurYN;

        ObjParam[48] = new SqlParameter("@GSTIN", SqlDbType.VarChar);
        ObjParam[48].Direction = ParameterDirection.Input;
        ObjParam[48].Value = GSTIN;

        ObjParam[49] = new SqlParameter("@LegalName", SqlDbType.VarChar);
        ObjParam[49].Direction = ParameterDirection.Input;
        ObjParam[49].Value = CompanyLegalName;

        ObjParam[50] = new SqlParameter("@ShiftName", SqlDbType.VarChar);
        ObjParam[50].Direction = ParameterDirection.Input;
        ObjParam[50].Value = ShiftName;

        int NewBookingID = 0;
        try
        {
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal", ObjParam); \\live
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal_PayBack", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal_PayBack_SwachhBharat", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal_PayBack_GST", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_RetailBooking_NewRetail_Normal_PayBack_GSTSurchargeAMT", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBooking", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBookingEmail", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBookingToll", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBookingToll_Chauffeur", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBooking_GSTIN", ObjParam);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBooking_Shift", ObjParam);

            NewBookingID = Convert.ToInt32(ObjParam[22].Value.ToString());
            return NewBookingID;
        }
        catch (Exception ex)
        {
            NewBookingID = 0;
        }
        return NewBookingID;
    }


    public int SaveRetailBookingMobile(int Cityid, int CompanyId, String GuestName, String GuestMobile, int carid
        , int ModelId, int CatId, int VendorCarYN, int PkgId, decimal PkgRate, decimal TotalCost
        , String PaymentMode, String Remarks, int ServiceUnitID, int UserID, decimal BasicCost, decimal SerTaxPer
        , decimal EduTaxPer, int UpdateGuest, int GuestID, int ContBookingID, int cCTYpeID, decimal HduTaxPer
        , String PosName, string VoucherNo1, string VoucherNo2, string VoucherNo3, bool VoucherYN, int payBackVal
        , decimal SwachhBharatTax, decimal KrishiKalyanTax
        , decimal CGSTTaxPercent, decimal SGSTTaxPercent, decimal IGSTTaxPercent, decimal CGSTTaxAmt, decimal SGSTTaxAmt, decimal IGSTTaxAmt
        , int GSTClientID, decimal GSTSurchargeAmount, decimal PkgRateWithSurchargeAMT, decimal DiscountAmt, int DiscountID
        , string EmailID, decimal Toll, int LookupID, int ChauffeurID, bool VendorChauffeurYN, string GSTIN, string CompanyLegalName)
    // , int ChauffeurId, int VendorChauffYN)    // change param
    {
        SqlParameter[] ObjParam = new SqlParameter[50];
        ObjParam[0] = new SqlParameter("@Cityid", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = Cityid;

        ObjParam[1] = new SqlParameter("@CompanyId", SqlDbType.Int);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = CompanyId;

        ObjParam[2] = new SqlParameter("@GuestName", SqlDbType.VarChar);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = GuestName;

        ObjParam[3] = new SqlParameter("@GuestMobile", SqlDbType.VarChar);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = GuestMobile;

        ObjParam[4] = new SqlParameter("@carid", SqlDbType.Int);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = carid;

        ObjParam[5] = new SqlParameter("@ModelId", SqlDbType.Int);
        ObjParam[5].Direction = ParameterDirection.Input;
        ObjParam[5].Value = ModelId;

        ObjParam[6] = new SqlParameter("@CatId", SqlDbType.Int);
        ObjParam[6].Direction = ParameterDirection.Input;
        ObjParam[6].Value = CatId;

        ObjParam[7] = new SqlParameter("@VendorCarYN", SqlDbType.Int);
        ObjParam[7].Direction = ParameterDirection.Input;
        ObjParam[7].Value = VendorCarYN;

        ObjParam[8] = new SqlParameter("@PkgId", SqlDbType.Int);
        ObjParam[8].Direction = ParameterDirection.Input;
        ObjParam[8].Value = PkgId;

        ObjParam[9] = new SqlParameter("@PkgRate", SqlDbType.Decimal);
        ObjParam[9].Direction = ParameterDirection.Input;
        ObjParam[9].Value = PkgRate;

        ObjParam[10] = new SqlParameter("@TotalCost", SqlDbType.Decimal);
        ObjParam[10].Direction = ParameterDirection.Input;
        ObjParam[10].Value = TotalCost;

        ObjParam[11] = new SqlParameter("@PaymentMode", SqlDbType.VarChar);
        ObjParam[11].Direction = ParameterDirection.Input;
        ObjParam[11].Value = PaymentMode;

        ObjParam[12] = new SqlParameter("@Remarks", SqlDbType.VarChar);
        ObjParam[12].Direction = ParameterDirection.Input;
        ObjParam[12].Value = Remarks;

        ObjParam[13] = new SqlParameter("@ServiceUnitID", SqlDbType.Int);
        ObjParam[13].Direction = ParameterDirection.Input;
        ObjParam[13].Value = ServiceUnitID;

        ObjParam[14] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[14].Direction = ParameterDirection.Input;
        ObjParam[14].Value = UserID;

        ObjParam[15] = new SqlParameter("@BasicCost", SqlDbType.Decimal);
        ObjParam[15].Direction = ParameterDirection.Input;
        ObjParam[15].Value = BasicCost;

        ObjParam[16] = new SqlParameter("@SerTax", SqlDbType.Decimal);
        ObjParam[16].Direction = ParameterDirection.Input;
        ObjParam[16].Value = SerTaxPer;

        ObjParam[17] = new SqlParameter("@EduTax", SqlDbType.Decimal);
        ObjParam[17].Direction = ParameterDirection.Input;
        ObjParam[17].Value = EduTaxPer;

        ObjParam[18] = new SqlParameter("@UpdateGuest", SqlDbType.Decimal);
        ObjParam[18].Direction = ParameterDirection.Input;
        ObjParam[18].Value = UpdateGuest;

        ObjParam[19] = new SqlParameter("@GuestID", SqlDbType.Decimal);
        ObjParam[19].Direction = ParameterDirection.Input;
        ObjParam[19].Value = GuestID;

        ObjParam[20] = new SqlParameter("@ContBookingID", SqlDbType.Decimal);
        ObjParam[20].Direction = ParameterDirection.Input;
        ObjParam[20].Value = ContBookingID;

        ObjParam[21] = new SqlParameter("@CCTypeID", SqlDbType.Decimal);
        ObjParam[21].Direction = ParameterDirection.Input;
        ObjParam[21].Value = cCTYpeID;

        ObjParam[22] = new SqlParameter("@NewBookingID", SqlDbType.Int);
        ObjParam[22].Direction = ParameterDirection.Output;

        //string transactionid,string authorizationid,string CCNo,string ExpYYMM 

        //ObjParam[23] = new SqlParameter("@transactionid", SqlDbType.VarChar);
        //ObjParam[23].Direction = ParameterDirection.Input;
        //ObjParam[23].Value = transactionid;

        //ObjParam[24] = new SqlParameter("@authorizationid", SqlDbType.VarChar);
        //ObjParam[24].Direction = ParameterDirection.Input;
        //ObjParam[24].Value = authorizationid;

        //ObjParam[25] = new SqlParameter("@CCNo", SqlDbType.VarChar);
        //ObjParam[25].Direction = ParameterDirection.Input;
        //ObjParam[25].Value = CCNo;

        //ObjParam[26] = new SqlParameter("@EXPYYMM", SqlDbType.VarChar);
        //ObjParam[26].Direction = ParameterDirection.Input;
        //ObjParam[26].Value = ExpYYMM;

        //ObjParam[27] = new SqlParameter("@trackid", SqlDbType.VarChar);
        //ObjParam[27].Direction = ParameterDirection.Input;
        //ObjParam[27].Value = trackid;

        ObjParam[23] = new SqlParameter("@HduTax", SqlDbType.Decimal);
        ObjParam[23].Direction = ParameterDirection.Input;
        ObjParam[23].Value = HduTaxPer;

        ObjParam[24] = new SqlParameter("@POSID", SqlDbType.VarChar);
        ObjParam[24].Direction = ParameterDirection.Input;
        ObjParam[24].Value = PosName;

        ObjParam[25] = new SqlParameter("@VoucherNo1", SqlDbType.VarChar);
        ObjParam[25].Direction = ParameterDirection.Input;
        ObjParam[25].Value = VoucherNo1;

        ObjParam[26] = new SqlParameter("@VoucherNo2", SqlDbType.VarChar);
        ObjParam[26].Direction = ParameterDirection.Input;
        ObjParam[26].Value = VoucherNo2;

        ObjParam[27] = new SqlParameter("@VoucherNo3", SqlDbType.VarChar);
        ObjParam[27].Direction = ParameterDirection.Input;
        ObjParam[27].Value = VoucherNo3;

        ObjParam[28] = new SqlParameter("@VoucherYN", SqlDbType.Bit);
        ObjParam[28].Direction = ParameterDirection.Input;
        ObjParam[28].Value = VoucherYN;

        ObjParam[29] = new SqlParameter("@PayBackStatus", SqlDbType.Int);
        ObjParam[29].Direction = ParameterDirection.Input;
        ObjParam[29].Value = payBackVal;

        ObjParam[30] = new SqlParameter("@SwachhBharatTax", SqlDbType.Decimal);
        ObjParam[30].Direction = ParameterDirection.Input;
        ObjParam[30].Value = SwachhBharatTax;

        ObjParam[31] = new SqlParameter("@KrishiKalyanTax", SqlDbType.Decimal);
        ObjParam[31].Direction = ParameterDirection.Input;
        ObjParam[31].Value = KrishiKalyanTax;

        //ObjParam[29] = new SqlParameter("@ChauffeurID", SqlDbType.Int);
        //ObjParam[29].Direction = ParameterDirection.Input;
        //ObjParam[29].Value = ChauffeurId;

        //ObjParam[30] = new SqlParameter("@VendorChauffYN", SqlDbType.Int);
        //ObjParam[30].Direction = ParameterDirection.Input;
        //ObjParam[30].Value = VendorChauffYN;

        ObjParam[32] = new SqlParameter("@CGSTTaxAmt", SqlDbType.Decimal);
        ObjParam[32].Direction = ParameterDirection.Input;
        ObjParam[32].Value = CGSTTaxAmt;

        ObjParam[33] = new SqlParameter("@SGSTTaxAmt", SqlDbType.Decimal);
        ObjParam[33].Direction = ParameterDirection.Input;
        ObjParam[33].Value = SGSTTaxAmt;

        ObjParam[34] = new SqlParameter("@IGSTTaxAmt", SqlDbType.Decimal);
        ObjParam[34].Direction = ParameterDirection.Input;
        ObjParam[34].Value = IGSTTaxAmt;

        ObjParam[35] = new SqlParameter("@CGSTTaxPercent", SqlDbType.Decimal);
        ObjParam[35].Direction = ParameterDirection.Input;
        ObjParam[35].Value = CGSTTaxPercent;

        ObjParam[36] = new SqlParameter("@SGSTTaxPercent", SqlDbType.Decimal);
        ObjParam[36].Direction = ParameterDirection.Input;
        ObjParam[36].Value = SGSTTaxPercent;

        ObjParam[37] = new SqlParameter("@IGSTTaxPercent", SqlDbType.Decimal);
        ObjParam[37].Direction = ParameterDirection.Input;
        ObjParam[37].Value = IGSTTaxPercent;

        ObjParam[38] = new SqlParameter("@ClientGSTId", SqlDbType.Int);
        ObjParam[38].Direction = ParameterDirection.Input;
        ObjParam[38].Value = GSTClientID;

        ObjParam[39] = new SqlParameter("@GSTSurchargeAmount", SqlDbType.Decimal);
        ObjParam[39].Direction = ParameterDirection.Input;
        ObjParam[39].Value = GSTSurchargeAmount;

        ObjParam[40] = new SqlParameter("@PkgRateWithSurchargeAMT", SqlDbType.Decimal);
        ObjParam[40].Direction = ParameterDirection.Input;
        ObjParam[40].Value = PkgRateWithSurchargeAMT;

        ObjParam[41] = new SqlParameter("@DiscountAmt", SqlDbType.Decimal);
        ObjParam[41].Direction = ParameterDirection.Input;
        ObjParam[41].Value = DiscountAmt;

        ObjParam[42] = new SqlParameter("@DiscountID", SqlDbType.Int);
        ObjParam[42].Direction = ParameterDirection.Input;
        ObjParam[42].Value = DiscountID;

        ObjParam[43] = new SqlParameter("@EmailID", SqlDbType.VarChar);
        ObjParam[43].Direction = ParameterDirection.Input;
        ObjParam[43].Value = EmailID;

        ObjParam[44] = new SqlParameter("@Toll", SqlDbType.Decimal);
        ObjParam[44].Direction = ParameterDirection.Input;
        ObjParam[44].Value = Toll;

        ObjParam[45] = new SqlParameter("@LookupID", SqlDbType.Int);
        ObjParam[45].Direction = ParameterDirection.Input;
        ObjParam[45].Value = LookupID;


        ObjParam[46] = new SqlParameter("@ChauffeurID", SqlDbType.Int);
        ObjParam[46].Direction = ParameterDirection.Input;
        ObjParam[46].Value = ChauffeurID;

        ObjParam[47] = new SqlParameter("@VendorChauffeurYN", SqlDbType.Bit);
        ObjParam[47].Direction = ParameterDirection.Input;
        ObjParam[47].Value = VendorChauffeurYN;

        ObjParam[48] = new SqlParameter("@GSTIN", SqlDbType.VarChar);
        ObjParam[48].Direction = ParameterDirection.Input;
        ObjParam[48].Value = GSTIN;

        ObjParam[49] = new SqlParameter("@LegalName", SqlDbType.VarChar);
        ObjParam[49].Direction = ParameterDirection.Input;
        ObjParam[49].Value = CompanyLegalName;


        int NewBookingID = 0;
        try
        {
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal", ObjParam); \\live
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal_PayBack", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal_PayBack_SwachhBharat", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_RetailBooking_NewRetail_Normal_PayBack_GST", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_RetailBooking_NewRetail_Normal_PayBack_GSTSurchargeAMT", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBooking", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBookingEmail", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBookingToll", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBookingToll_Chauffeur", ObjParam);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBooking_Mobile", ObjParam);
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CreateRetailBooking_Mobile1", ObjParam);

            NewBookingID = Convert.ToInt32(ObjParam[22].Value.ToString());
            return NewBookingID;
        }
        catch (Exception ex)
        {
            NewBookingID = 0;
        }
        return NewBookingID;
    }

    public int BWAirportSubmission(int LocationID, DateTime TranDate, int CashDuties, decimal CashAmount,
        decimal CashDeposit, string CashSlipNo, int AmexDuties, decimal AmexAmount, decimal AmexDeposit,
        string AmexBatchNo, int HDFCDuties, decimal HDFCAmount, decimal HDFCDeposit, string HDFCBatchNo,
        int UserID, int EWalletDuties, decimal EWalletAmount, decimal EWalletDeposit, string EWalletBatchNo)
    {
        SqlParameter[] ObjParam = new SqlParameter[20];
        ObjParam[0] = new SqlParameter("@LocationID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = LocationID;

        ObjParam[1] = new SqlParameter("@TranDate", SqlDbType.DateTime);
        ObjParam[1].Direction = ParameterDirection.Input;
        ObjParam[1].Value = TranDate;

        ObjParam[2] = new SqlParameter("@CashDuties", SqlDbType.Int);
        ObjParam[2].Direction = ParameterDirection.Input;
        ObjParam[2].Value = CashDuties;

        ObjParam[3] = new SqlParameter("@CashAmount", SqlDbType.Decimal);
        ObjParam[3].Direction = ParameterDirection.Input;
        ObjParam[3].Value = CashAmount;

        ObjParam[4] = new SqlParameter("@CashDeposit", SqlDbType.Decimal);
        ObjParam[4].Direction = ParameterDirection.Input;
        ObjParam[4].Value = CashDeposit;

        ObjParam[5] = new SqlParameter("@CashSlipNo", SqlDbType.VarChar);
        ObjParam[5].Direction = ParameterDirection.Input;
        ObjParam[5].Value = CashSlipNo;

        ObjParam[6] = new SqlParameter("@AmexDuties", SqlDbType.Int);
        ObjParam[6].Direction = ParameterDirection.Input;
        ObjParam[6].Value = AmexDuties;

        ObjParam[7] = new SqlParameter("@AmexAmount", SqlDbType.Decimal);
        ObjParam[7].Direction = ParameterDirection.Input;
        ObjParam[7].Value = AmexAmount;

        ObjParam[8] = new SqlParameter("@AmexDeposit", SqlDbType.Decimal);
        ObjParam[8].Direction = ParameterDirection.Input;
        ObjParam[8].Value = AmexDeposit;

        ObjParam[9] = new SqlParameter("@AmexBatchNo", SqlDbType.VarChar);
        ObjParam[9].Direction = ParameterDirection.Input;
        ObjParam[9].Value = AmexBatchNo;

        ObjParam[10] = new SqlParameter("@HDFCDuties", SqlDbType.Int);
        ObjParam[10].Direction = ParameterDirection.Input;
        ObjParam[10].Value = HDFCDuties;

        ObjParam[11] = new SqlParameter("@HDFCAmount", SqlDbType.Decimal);
        ObjParam[11].Direction = ParameterDirection.Input;
        ObjParam[11].Value = HDFCAmount;

        ObjParam[12] = new SqlParameter("@HDFCDeposit", SqlDbType.Decimal);
        ObjParam[12].Direction = ParameterDirection.Input;
        ObjParam[12].Value = HDFCDeposit;

        ObjParam[13] = new SqlParameter("@HDFCBatchNo", SqlDbType.VarChar);
        ObjParam[13].Direction = ParameterDirection.Input;
        ObjParam[13].Value = HDFCBatchNo;

        ObjParam[14] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[14].Direction = ParameterDirection.Input;
        ObjParam[14].Value = UserID;

        ObjParam[15] = new SqlParameter("@EWalletDuties", SqlDbType.Int);
        ObjParam[15].Direction = ParameterDirection.Input;
        ObjParam[15].Value = EWalletDuties;

        ObjParam[16] = new SqlParameter("@EWalletAmount", SqlDbType.Decimal);
        ObjParam[16].Direction = ParameterDirection.Input;
        ObjParam[16].Value = EWalletAmount;

        ObjParam[17] = new SqlParameter("@EWalletDeposit", SqlDbType.Decimal);
        ObjParam[17].Direction = ParameterDirection.Input;
        ObjParam[17].Value = EWalletDeposit;

        ObjParam[18] = new SqlParameter("@EWalletBatchNo", SqlDbType.VarChar);
        ObjParam[18].Direction = ParameterDirection.Input;
        ObjParam[18].Value = EWalletBatchNo;

        ObjParam[19] = new SqlParameter("@NewTranID", SqlDbType.Int);
        ObjParam[19].Direction = ParameterDirection.Output;

        //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_BWAirportSubmission", ObjParam);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Save_BWAirportSubmission_New", ObjParam);
        int NewTrandID = Convert.ToInt32(ObjParam[19].Value.ToString());
        return NewTrandID;
    }
    public SqlDataReader Get_UniqueTrackId()
    {
        return SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ProcUniqueTrackID");
    }

    public DataSet GetDataForExcelReport(int UnitID, int sysUserID, string fromdate, string todate)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure
            , "prc_Rpt_Retail", ObjParam);
    }

    public DataSet GetDataForExcelReport_1015(int UnitID, int sysUserID
        , string fromdate, string todate, string FinYear)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset_lastFinYear(CommandType.StoredProcedure
            , "prc_Rpt_Retail", FinYear, ObjParam);
    }

    public DataSet GetDataForExcelReport_0910(int UnitID
        , int sysUserID, string fromdate, string todate
        , string FinYear)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset_lastFinYear(
            CommandType.StoredProcedure
            , "prc_Rpt_Retail", FinYear, ObjParam);
    }

    public DataSet GetHandOverReport(int UnitID, int sysUserID, string fromdate, string todate)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitID", UnitID);
        ObjParam[1] = new SqlParameter("@Userid", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_RetailHandOver", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_RetailHandOver_New", ObjParam);
    }

    public DataSet GetHandOverReport_0910(int UnitID, int sysUserID
        , string fromdate, string todate, string FinYear)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitID", UnitID);
        ObjParam[1] = new SqlParameter("@Userid", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset_lastFinYear(CommandType.StoredProcedure
            , "prc_Rpt_RetailHandOver", FinYear, ObjParam);
    }

    public DataSet GetDataForExcelSummary(int UnitID
        , int sysUserID, string fromdate, string todate)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_rpt_retail_summary", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_Retail_Summary_new", ObjParam);
    }

    public DataSet GetDataForExcelSummary_0910(int UnitID
        , int sysUserID, string fromdate, string todate
        , string FinYear)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset_lastFinYear(CommandType.StoredProcedure
            , "prc_rpt_retail_summary", FinYear, ObjParam);
    }

    public DataSet GetDateWiseSummary(int UnitID, int sysUserID, string fromdate, string todate)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_rpt_retail_summarytp", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_Retail_SummaryTP_New", ObjParam);
    }

    public DataSet GetDateWiseSummary_0910(int UnitID, int sysUserID
        , string fromdate, string todate, string FinYear)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset_lastFinYear(CommandType.StoredProcedure
            , "prc_rpt_retail_summarytp", FinYear, ObjParam);
    }

    public DataSet GetCollectionSummary(int UnitID, int sysUserID, string fromdate, string todate)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_Collection_SummaryTP", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_Collection_SummaryTP_New", ObjParam);
    }

    public DataSet GetCollectionSummary_0910(int UnitID, int sysUserID
        , string fromdate, string todate, string FinYear)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset_lastFinYear(CommandType.StoredProcedure
            , "prc_Rpt_Collection_SummaryTP", FinYear, ObjParam);
    }

    public DataSet ReconciliationReport(int UnitID, int sysUserID, string fromdate, string todate)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_RetailReco", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_Rpt_RetailReco_New", ObjParam);
    }

    public DataSet ReconciliationReport_0910(int UnitID, int sysUserID
        , string fromdate, string todate, string FinYear)
    {
        SqlParameter[] ObjParam = new SqlParameter[4];
        ObjParam[0] = new SqlParameter("@UnitCityid", UnitID);
        ObjParam[1] = new SqlParameter("@SysUserID", sysUserID);
        ObjParam[2] = new SqlParameter("@fromDate", fromdate);
        ObjParam[3] = new SqlParameter("@toDate", todate);
        return SqlHelper.ExecuteDataset_lastFinYear(CommandType.StoredProcedure
            , "prc_Rpt_RetailReco", FinYear, ObjParam);
    }

    public DataSet GetBookingDetail(int BookingID)
    {
        SqlParameter[] objPram = new SqlParameter[1];
        objPram[0] = new SqlParameter("@BookingID", BookingID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetRetailData", objPram);
    }

    public DataSet GetvoidDetailBooking(int BookingID)
    {
        SqlParameter[] objPram = new SqlParameter[1];
        objPram[0] = new SqlParameter("@BookingID", BookingID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getvoidDetail", objPram);
    }


    public DataSet UpdateRetailStatus(int BookingID, string ReceiptNo, string status)
    {
        SqlParameter[] objPram = new SqlParameter[3];
        objPram[0] = new SqlParameter("@BookingID", BookingID);
        objPram[1] = new SqlParameter("@ReceiptNo", ReceiptNo);
        objPram[2] = new SqlParameter("@status", status);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_updateStatus", objPram);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_updateStatus_New", objPram);
    }

    public DataSet UpdatevoidStatus(string BookingID, string ReceiptNo, string status)
    {
        SqlParameter[] objPram = new SqlParameter[3];
        objPram[0] = new SqlParameter("@BookingID", BookingID);
        objPram[1] = new SqlParameter("@ReceiptNo", ReceiptNo);
        objPram[2] = new SqlParameter("@status", status);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_updatevoiddata", objPram);
    }


    public DataSet GetVoidDetail(string fromdate, string todate, string POSNO)
    {
        SqlParameter[] objPram = new SqlParameter[3];
        objPram[0] = new SqlParameter("@fromdate", fromdate);
        objPram[1] = new SqlParameter("@todate", todate);
        objPram[2] = new SqlParameter("@POSNO", POSNO);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getvoidDetailDateWise", objPram);
    }
    public DataSet GetVoidDetail_New(string fromdate, string todate, string POSNO)
    {
        SqlParameter[] objPram = new SqlParameter[3];
        objPram[0] = new SqlParameter("@fromdate", fromdate);
        objPram[1] = new SqlParameter("@todate", todate);
        objPram[2] = new SqlParameter("@POSNO", POSNO);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getvoidDetailDateWise_New", objPram);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "getvoidDetailDateWise_New_1", objPram);
    }

    public DataSet VoucherAvailable(int clientID)
    {
        SqlParameter[] objPram = new SqlParameter[1];
        objPram[0] = new SqlParameter("@ClientID", clientID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Proc_VoucherAvailable", objPram);
    }

    public static string CheckVoucher(int voucherNo, int ClientID, DateTime PickUpDate, int Flag)
    {
        SqlParameter[] objPram = new SqlParameter[4];
        objPram[0] = new SqlParameter("@VoucherNo", voucherNo);
        objPram[1] = new SqlParameter("@ClientID", ClientID);
        objPram[2] = new SqlParameter("@PicKUpDate", PickUpDate);
        objPram[3] = new SqlParameter("@flg", Flag);
        string status = SqlHelper.ExecuteScalar("Proc_ValidateVoucher1", objPram).ToString();
        return status;
    }

    public static int VoucherExists(int voucherNo)
    {
        SqlParameter[] objPram = new SqlParameter[1];
        objPram[0] = new SqlParameter("@VoucherNo", voucherNo);
        string status = SqlHelper.ExecuteScalar("Proc_CheckVoucher_1", objPram).ToString();
        return Convert.ToInt32(status);
    }

    public DataSet Handover_AddVoidBooking(int BookingID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BookingID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = BookingID;

        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Handover_AddVoidBooking", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Handover_AddVoidBooking_New", ObjParam);
    }


    public DataSet GetReliever(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", SqlDbType.Int);
        ObjParam[0].Direction = ParameterDirection.Input;
        ObjParam[0].Value = UserID;
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetReliever", ObjParam);
    }

    public DataSet Get_Detail(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", UserID);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "get_HandoverDetail", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "get_HandoverDetail_New", ObjParam);
    }

    public void SaveHandOver(int HandOverID, decimal Current_Cash_Bal, decimal Current_Amex_Bal
        , decimal Current_HDFC_Bal, decimal Cash_Rejected, decimal Amex_Rejected, decimal HDFC_Rejected
        , decimal Cash_Deposited, decimal Settlement_Amex, decimal Settlement_HDFC, decimal HandedOver_Cash
        , decimal HandedOver_Amex, decimal HandedOver_HDFC, int HandedOver_To, int ModifiedBy
        , decimal Current_EWallet_Bal, decimal EWallet_Rejected, decimal Settlement_EWallet, decimal HandedOver_EWallet)
    {
        SqlParameter[] param = new SqlParameter[19];
        param[0] = new SqlParameter("@HandOverID", HandOverID);
        param[1] = new SqlParameter("@Current_Cash_Bal", Current_Cash_Bal);
        param[2] = new SqlParameter("@Current_Amex_Bal", Current_Amex_Bal);
        param[3] = new SqlParameter("@Current_HDFC_Bal", Current_HDFC_Bal);
        param[4] = new SqlParameter("@Cash_Rejected", Cash_Rejected);
        param[5] = new SqlParameter("@Amex_Rejected", Amex_Rejected);
        param[6] = new SqlParameter("@HDFC_Rejected", HDFC_Rejected);
        param[7] = new SqlParameter("@Cash_Deposited", Cash_Deposited);
        param[8] = new SqlParameter("@Settlement_Amex", Settlement_Amex);
        param[9] = new SqlParameter("@Settlement_HDFC", Settlement_HDFC);
        param[10] = new SqlParameter("@HandedOver_Cash", HandedOver_Cash);
        param[11] = new SqlParameter("@HandedOver_Amex", HandedOver_Amex);
        param[12] = new SqlParameter("@HandedOver_HDFC", HandedOver_HDFC);
        param[13] = new SqlParameter("@HandedOver_To", HandedOver_To);
        param[14] = new SqlParameter("@ModifiedBy", ModifiedBy);

        param[15] = new SqlParameter("@Current_EWallet_Bal", Current_EWallet_Bal);
        param[16] = new SqlParameter("@EWallet_Rejected", EWallet_Rejected);
        param[17] = new SqlParameter("@Settlement_EWallet", Settlement_EWallet);
        param[18] = new SqlParameter("@HandedOver_EWallet", HandedOver_EWallet);

        //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_SaveHandOver", param);
        //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_SaveHandOver_New", param);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_SaveHandOver_New1", param);

    }

    public void VoidableInvoices
        (int BatchID, int BookingID, string Mode, decimal Amount, string Reason, int CreatedBy)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@BatchID", BatchID);
        param[1] = new SqlParameter("@BookingID", BookingID);
        param[2] = new SqlParameter("@Mode", Mode);
        param[3] = new SqlParameter("@Amount", Amount);
        param[4] = new SqlParameter("@Reason", Reason);
        param[5] = new SqlParameter("@CreatedBy", CreatedBy);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_Insert_Update_VoidableInvoices", param);
    }

    public void HotelInvoices
    (int BatchID, int BookingID, string Mode, decimal Amount, string Reason, int CreatedBy)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@BatchID", BatchID);
        param[1] = new SqlParameter("@BookingID", BookingID);
        param[2] = new SqlParameter("@Mode", Mode);
        param[3] = new SqlParameter("@Amount", Amount);
        param[4] = new SqlParameter("@Reason", Reason);
        param[5] = new SqlParameter("@CreatedBy", CreatedBy);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_Insert_Update_HotelInvoices", param);
    }

    public DataSet GetDetail_Voidable(int BatchID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BatchID", BatchID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "get_VoidableDetail", ObjParam);
    }

    public DataSet GetDetail_Hotel(int BatchID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BatchID", BatchID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "get_HotelDetail", ObjParam);
    }

    public DataSet Get_VoidDetail(int CityID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@CityID", CityID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "get_voidableDetail_City", ObjParam);
    }

    public void RejecteInvoice(int InvoiceID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@BookingID", InvoiceID);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_update_retectStatus", ObjParam);
    }

    public DataSet prc_TakeHandOverDetail(int UserID)
    {
        SqlParameter[] ObjParam = new SqlParameter[1];
        ObjParam[0] = new SqlParameter("@UserID", UserID);
        //return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_TakeHandOverDetail", ObjParam);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "prc_TakeHandOverDetail_New", ObjParam);
    }
    public void AcceptHandOver(int UserID, decimal CashOpBal, decimal AmexOpBal, decimal HDFCOpBal, decimal EWalletOpBal)
    {
        SqlParameter[] ObjParam = new SqlParameter[5];
        ObjParam[0] = new SqlParameter("@UserID", UserID);
        ObjParam[1] = new SqlParameter("@CashOpBal", CashOpBal);
        ObjParam[2] = new SqlParameter("@AmexOpBal", AmexOpBal);
        ObjParam[3] = new SqlParameter("@HDFCOpBal", HDFCOpBal);
        ObjParam[4] = new SqlParameter("@EWalletOpBal", EWalletOpBal);
        //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_AcceptHandover", ObjParam);
        //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_AcceptHandover_New", ObjParam);
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "prc_AcceptHandover_New1", ObjParam);
    }

    // function for post data add by alka on 27-11-2012


    public DataSet GetBookingDetail_PostData(DateTime FromDate, DateTime Todate)
    {
        SqlParameter[] objPram = new SqlParameter[2];
        objPram[0] = new SqlParameter("@FromDate", FromDate);
        objPram[1] = new SqlParameter("@Todate", Todate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetRetailData_PostData", objPram);
    }
    public DataSet GetBookingDetail_PostData_1(int BookingID)
    {
        SqlParameter[] objPram = new SqlParameter[1];
        objPram[0] = new SqlParameter("@BookingID", BookingID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetRetailData_PostData_1", objPram);
    }

    public DataSet MilestonePostXMLData(int BookingID)
    {
        try
        {
            SqlParameter[] objPram = new SqlParameter[1];
            objPram[0] = new SqlParameter("@BookingID", BookingID);
            return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_MilestonePostXMLData", objPram);
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }

    }

    //Start CCTV 
    public int SaveCCTVDetails(int BookingID, long TransactionHeaderID, int SysUserId, string ReceiptNo, string SummaryNo)
    {
        try
        {
            SqlParameter[] objPram = new SqlParameter[5];
            objPram[0] = new SqlParameter("@BookingID", BookingID);
            objPram[1] = new SqlParameter("@TransactionHeaderID", TransactionHeaderID);
            objPram[2] = new SqlParameter("@SysUserId", SysUserId);
            objPram[3] = new SqlParameter("@ReceiptNo", ReceiptNo);
            objPram[4] = new SqlParameter("@SummaryNo", SummaryNo);
            return SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_SaveCCTVDetails", objPram);
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }

    public DataSet GetCCTVVoidDetails(int BookingID)
    {
        SqlParameter[] objPram = new SqlParameter[1];
        objPram[0] = new SqlParameter("@BookingID", BookingID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_MilestonePostXMLData_Void", objPram);
    }
    public DataSet GetCCTVBookginDetails_PostData(int BookingID)
    {
        SqlParameter[] objPram = new SqlParameter[1];
        objPram[0] = new SqlParameter("@BookingId", BookingID);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCCTVPostingData", objPram);
    }
    public DataSet GetCCTVBookingDetail_NotPostData(DateTime FromDate, DateTime Todate)
    {
        SqlParameter[] objPram = new SqlParameter[2];
        objPram[0] = new SqlParameter("@FromDate", FromDate);
        objPram[1] = new SqlParameter("@Todate", Todate);
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetCCTVRetailData_NotPostData", objPram);
    }

    //End CCTV 

    public string GetMobileAppURL()
    {
        string MobileAppURL = "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetMobileStoreURL");
        if (ds.Tables[0].Rows.Count > 0)
        {
            MobileAppURL = " or download COR Ride App " + ds.Tables[0].Rows[0]["StoreURL"].ToString();
        }
        return MobileAppURL;
    }
    public decimal GetGSTSurchargesAmount(int PkgID)
    {
        SqlParameter[] objPram = new SqlParameter[2];
        objPram[0] = new SqlParameter("@PKGId", PkgID);
        objPram[1] = new SqlParameter("@ClientId", 714);
        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_GetGSTSurchargeAmount", objPram);
        if (dt.Rows.Count > 0)
        {
            return Convert.ToDecimal(dt.Rows[0]["GSTSurchargeAmount"]);
        }
        else
        {
            return 0;
        }

    }

    public string GetUnitPhoneDetails(int UnitID)
    {
        string UnitPhone = "";
        string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

        SqlConnection InstaCon = new SqlConnection(connectionString);

        SqlCommand GetSOSDetail = InstaCon.CreateCommand();
        GetSOSDetail.CommandText = "select top 1 UnitPhone2 from corintunitmaster where unitid =  " + UnitID;

        if (InstaCon.State != ConnectionState.Open)
        {
            InstaCon.Open();
        }
        DataSet ds = new DataSet();
        SqlDataAdapter objAdapter = new SqlDataAdapter(GetSOSDetail);
        objAdapter.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["UnitPhone2"])))
            {
                if (Convert.ToString(ds.Tables[0].Rows[0]["UnitPhone2"]) != "0" && Convert.ToString(ds.Tables[0].Rows[0]["UnitPhone2"]) != "-")
                {
                    UnitPhone = Convert.ToString(ds.Tables[0].Rows[0]["UnitPhone2"]);
                }
            }
        }
        if (InstaCon.State != ConnectionState.Closed)
        {
            InstaCon.Close();
        }
        return UnitPhone;
    }

    public DataSet GetDiscountDetails(int CityID)
    {
        DataSet ds = new DataSet();
        string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

        SqlConnection InstaCon = new SqlConnection(connectionString);

        SqlCommand GetDiscountData = InstaCon.CreateCommand();
        GetDiscountData.CommandText = "select ID, Discount, DiscountDesc from CorIntDialDiscount where cast(GETDATE() as date) between FromDate and ToDate and CityID = @CityID and Active = 1 ";
        GetDiscountData.Parameters.AddWithValue("@CityID", CityID);
        if (InstaCon.State != ConnectionState.Open)
        {
            InstaCon.Open();
        }

        SqlDataAdapter objAdapter = new SqlDataAdapter(GetDiscountData);
        objAdapter.Fill(ds);

        if (InstaCon.State != ConnectionState.Closed)
        {
            InstaCon.Close();
        }
        return ds;
    }

    public bool CheckCGST_IGSTApplicable(int CityID, string StateCode)
    {
        bool CGSTApplicableYN = true;
        DataSet ds = new DataSet();
        string connectionString = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();

        SqlConnection InstaCon = new SqlConnection(connectionString);

        SqlCommand GetStateCode = InstaCon.CreateCommand();
        GetStateCode.CommandText = "select state.StateId, state.StateCode from CORIntCityMaster as city inner join CorIntStateMaster as state on city.StateId = state.StateId where city.CityID = @CityID ";
        GetStateCode.Parameters.AddWithValue("@CityID", CityID);
        if (InstaCon.State != ConnectionState.Open)
        {
            InstaCon.Open();
        }

        SqlDataAdapter objAdapter = new SqlDataAdapter(GetStateCode);
        objAdapter.Fill(ds);

        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["StateCode"].ToString() == StateCode)
            {
                CGSTApplicableYN = true;
            }
            else
            {
                CGSTApplicableYN = false;
            }
        }

        if (InstaCon.State != ConnectionState.Closed)
        {
            InstaCon.Close();
        }
        return CGSTApplicableYN;
    }

    public DataTable GenerateOTP(string LoginID,string Password,string otp)
    {
        SqlParameter[] objPram = new SqlParameter[3];
        objPram[0] = new SqlParameter("@loginID", LoginID);
        objPram[1] = new SqlParameter("@loginpwd", Password);
        objPram[2] = new SqlParameter("@OTP", otp);
        
        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Prc_UpdateUserLoginOpt", objPram);
        return dt;
    }
    public DataTable ValidateOTP(string LoginID, string Password, string otp,string IPAdd)
    {
        SqlParameter[] objPram = new SqlParameter[4];
        objPram[0] = new SqlParameter("@loginID", LoginID);
        objPram[1] = new SqlParameter("@loginpwd", Password);
        objPram[2] = new SqlParameter("@OTP", otp);
        objPram[3] = new SqlParameter("@IPAddress", IPAdd);
        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "procLoginCheck_AccessBasis_OTP", objPram);
        return dt;
    }
}