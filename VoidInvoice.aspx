<%@ Page Language="C#" MasterPageFile="~/CIPL.master"AutoEventWireup="true" CodeFile="VoidInvoice.aspx.cs" Inherits="VoidInvoice" Title="COR - Retail Module" StylesheetTheme="StyleSheet"%>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPage" Runat="Server">
     <script language="javascript" src="App_Themes/CommonScript.js" type="text/javascript"></script>
         <table width="90%" align="center" style="margin-top:50px">
        <tr>
            <td align="center" style="padding:1px 1px 1px 1px; background-color:Black">
                <table width="100%" align="center" style="padding:5px 5px 5px 5px; background-color:White">
                    <tr>
                        <td style="height: 90px">
                            <table width="100%" align="center">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblMessage" runat="server" Text="" Font-Bold="true" ForeColor="red" Font-Size="small"></asp:Label>
                                    </td>
                                  
                                </tr>
                                <tr>
                                    <td style="height: 23px; width: 412px;">
                                    </td>
                                    <td style="height: 23px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:4px; vertical-align:middle; text-align:right; width: 412px;">
                                        From:
                                        <input id="txtFromDate" readonly="readonly" type="text" onfocus="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtFromDate,ctl00$cphPage$txtFromDate, 1);return false;" runat="server" style="font-family:Verdana; font-size:x-small; height:10px" />
                                        &nbsp;
                                        </td>
                                        <td style="height:4px; vertical-align:middle; text-align:left;">
                                            To:
                                            <input id="txtToDate" readonly="readonly" type="text" onfocus="if(self.varPop)varPop.fPopCalendar(ctl00$cphPage$txtToDate,ctl00$cphPage$txtToDate, 1);return false;" runat="server" style="font-family:Verdana; font-size:x-small; height:10px" />&nbsp;
                                            <asp:DropDownList ID="dllPOSNO" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                                <asp:ListItem Text="-- Select POS No --" Value=""></asp:ListItem>
                                                <asp:ListItem Text="P0001" Value="P0001"></asp:ListItem>
                                                <asp:ListItem Text="P0002" Value="P0002"></asp:ListItem>
                                                <asp:ListItem Text="P0003" Value="P0003"></asp:ListItem>
                                                <asp:ListItem Text="P0004" Value="P0004"></asp:ListItem>
                                            </asp:DropDownList>
                                            <input style="WIDTH: 16px" id="hdnpos" type="hidden" runat="Server" />
                                            </td>
                                            
                                            
                                </tr>
                                <tr>
                                    <td style="height: 23px; width: 412px;">
                                    </td>
                                    <td style="height: 23px">
                                    </td>
                                </tr>
                            </table>
                            &nbsp;
                            <asp:Button ID="GetDetail" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                Text="Get Detail" OnClick="GetDetail_Click" /></td>
                                </tr>
                                <tr>
                                           <td colspan="2" align="center">
               <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Record Found"  AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand"  >
                   <Columns>
                   <asp:TemplateField  HeaderText ="Old BookingID"  >
                   <ItemTemplate  >
                   <asp:Label ID="lbl_BookingID" runat="server" Text='<%#Eval("BookingID") %>' ></asp:Label>                   
                   </ItemTemplate>
                   </asp:TemplateField> 
                   
                   <asp:TemplateField  HeaderText ="New VoidID"  >
                   <ItemTemplate  >
                   <asp:Label ID="lbl_newBookingID" runat="server" Text='<%#Eval("NewVoidID") %>' ></asp:Label>                   
                   </ItemTemplate>
                   </asp:TemplateField> 
                   
                    <asp:TemplateField HeaderText="Client Name"  >
                   <ItemTemplate  >
                   <asp:Label ID="lbl_ClientCoName" runat="server" Text='<%#Eval("CompanyName") %>' ></asp:Label>                   
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Guest Name"  >
                   <ItemTemplate  >
                   <asp:Label ID="lbl_CustomerName" runat="server" Text='<%#Eval("CustomerName") %>' ></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Receipt No"  >
                   <ItemTemplate  >
                   <asp:Label ID="lbl_ReceiptNo" runat="server" Text='<%#Eval("ReceiptNo") %>' ></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>

                   <asp:TemplateField HeaderText="Store No"  >
                   <ItemTemplate  >
                   <asp:Label ID="lbl_StoreNo" runat="server" Text='<%#Eval("StoreNo") %>' ></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="POS No"  >
                   <ItemTemplate  >
                   <asp:Label ID="lbl_POSNo" runat="server" Text='<%#Eval("POSNo") %>' ></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>                                  
                     <asp:TemplateField HeaderText="Send" >
                           <ItemTemplate>
                               <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("BookingID") %>' CausesValidation="False" CommandName="Send"
                                   Text="Send"></asp:LinkButton>
                           </ItemTemplate>
                       </asp:TemplateField>                   
                 
                   </Columns>
               </asp:GridView>
           </td>
</tr>
                    
                </table>
            </td>
        </tr>
    </table>
         <div>
            <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
                top: 0px" name="CalFrame" src="App_Themes/CorCalendar.htm" frameborder="0" width="260"
                scrolling="no" height="182"></iframe>
</div>
<script language="javascript" type="text/javascript">
function validateform()
{
    if(document.getElementById("<%=dllPOSNO.ClientID %>").value == '')
    {
        alert("Please Select POS No.");
        document.getElementById('<%=dllPOSNO.ClientID %>').focus();
        return false;
    }
    if(document.getElementById("<%=txtFromDate.ClientID %>").value == '')
    {
        alert("Please Enter From Date.");
        document.getElementById('<%=txtFromDate.ClientID %>').focus();
        return false;
    }
    if(document.getElementById("<%=txtToDate.ClientID %>").value == '')
    {
        alert("Please Enter To Date.");
        document.getElementById('<%=txtToDate.ClientID %>').focus();
        return false;
    }        
}
</script>
</asp:Content>
