﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for OL_TaxHeads
/// </summary>
public class OL_TaxHeads
{
    private double _ServiceTaxPercent;
    private double _EduCessPercent;
    private double _HduCessPercent;
    private bool _KMWisePackage;
    private double _SwachhBharatTaxPercent;
    private double _KrishiKalyanTaxPercent;
    private bool _GSTEnabledYN;
    private double _CGSTPercent;
    private double _SGSTPercent;
    private double _IGSTPercent;
    private int _ClientGSTId;

    public double ServiceTaxPercent
    {
        get { return _ServiceTaxPercent; }
        set { _ServiceTaxPercent = value; }
    }


    public double EduCessPercent
    {
        get { return _EduCessPercent; }
        set { _EduCessPercent = value; }
    }


    public double HduCessPercent
    {
        get { return _HduCessPercent; }
        set { _HduCessPercent = value; }
    }
    private double _DSTPercent;

    public double DSTPercent
    {
        get { return _DSTPercent; }
        set { _DSTPercent = value; }
    }


    public bool KMWisePackage
    {
        get { return _KMWisePackage; }
        set { _KMWisePackage = value; }
    }


    public double SwachhBharatTaxPercent
    {
        get { return _SwachhBharatTaxPercent; }
        set { _SwachhBharatTaxPercent = value; }
    }


    public double KrishiKalyanTaxPercent
    {
        get { return _KrishiKalyanTaxPercent; }
        set { _KrishiKalyanTaxPercent = value; }
    }


    public bool GSTEnabledYN
    {
        get { return _GSTEnabledYN; }
        set { _GSTEnabledYN = value; }
    }


    public double CGSTPercent
    {
        get { return _CGSTPercent; }
        set { _CGSTPercent = value; }
    }


    public double SGSTPercent
    {
        get { return _SGSTPercent; }
        set { _SGSTPercent = value; }
    }


    public double IGSTPercent
    {
        get { return _IGSTPercent; }
        set { _IGSTPercent = value; }
    }


    public int ClientGSTId
    {
        get { return _ClientGSTId; }
        set { _ClientGSTId = value; }
    }

}