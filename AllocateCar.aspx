<%@ Page Language="C#" MasterPageFile="~/CIPL.master" AutoEventWireup="true" CodeFile="AllocateCar.aspx.cs"
    Inherits="AllocateCar" Title="Allocate Car" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <table width="100%" align="center">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMessage" runat="server" Text="" Font-Bold="true" ForeColor="red"
                    Font-Size="small"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 23px; width: 412px;"></td>
            <td style="height: 23px"></td>
        </tr>
        <tr>
            <td style="height: 4px; vertical-align: middle; text-align: right; width: 412px;">BookingID:
            <asp:TextBox ID="txtBookingID" AutoPostBack="true" runat="server" OnTextChanged="txtBookingID_TextChanged"></asp:TextBox>
                &nbsp;
            </td>
            <td style="height: 4px; vertical-align: middle; text-align: left;">Car:
                <asp:DropDownList ID="ddlcar" runat="server"></asp:DropDownList>
                &nbsp;
                <asp:Button ID="btnsubmit" runat="server" Text="Allocate Car" OnClick="btnsubmit_Click" />
            </td>
        </tr>
        <tr>
            <td style="height: 23px; width: 412px;"></td>
            <td style="height: 23px"></td>
        </tr>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute; top: 0px"
            name="CalFrame" src="App_Themes/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
    <script language="javascript" type="text/javascript">
        function validateform() {
            var bkID = document.getElementById("<%=txtBookingID.ClientID %>").value;
            bkID = bkID.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
            bkID = bkID.replace(/\s+$/, ""); 	//Removes Right Blank Spaces

            if (bkID == '') {
                alert("Please Enter BookingID.");
                document.getElementById('<%=txtBookingID.ClientID %>').focus();
                return false;
            }
        }
    </script>
</asp:Content>
