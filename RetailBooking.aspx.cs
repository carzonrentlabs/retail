using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using RetailModule;
using System.Net;
using ccs;
using System.Xml;
using System.IO;
using System.ServiceModel;

public partial class RetailBooking : clsPageAuthorization
{
    ClsAdmin objAdmin = new ClsAdmin();
    public int ServiceUnitId = 0;
    public decimal ServiceTax = 0, EduTax = 0, HduTax = 0, ttlTax = 0, FinalAmt = 0, BasicAmt = 0, TollAmt = 0, TaxAmt = 0, SwachhBharatTax = 0, KrishiKalyanTax = 0;
    public decimal CGSTTaxPercent = 0, SGSTTaxPercent = 0, IGSTTaxPercent = 0;
    public decimal CGSTTaxAmt = 0, SGSTTaxAmt = 0, IGSTTaxAmt = 0;

    int GSTClientID = 0;
    bool GSTEnabledYN = false;
    static int Bookingid = 0;
    //bool flagComplete = true;
    string PosName = "";

    SendSMSService.SendSMS SendSMSMailService = new SendSMSService.SendSMS();
    //SendSMSServiceTest.SendSMS SendSMSMailServiceTest = new SendSMSServiceTest.SendSMS();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Show3.Visible = false;
            Show4.Visible = false;
            Show5.Visible = false;
            //showP.Visible = false; //Entered By rahul 
            //showT.Visible = false; //Entered By rahul 

            Session["StatusID"] = 0;
            hdnpos.Value = "";
            BindPickupCity();
            BindCompany();
            BindPayment();
            BindRentalType();
            getdiscount();
        }

        if (gridData.Rows.Count > 0)
        {
            divTran.Visible = true;
        }
        else
        {
            divTran.Visible = false;
        }
        hdtax.Value = "0";
        lblmessage.Text = "";
        getUnitDetails();
        Btsave.Attributes.Add("onclick", "return validateForm();");
        btReset.Attributes.Add("onclick", "return validateForm();");
        ReadOnlyObjects();
        ScriptManager.RegisterStartupScript(this, typeof(string), "script", "javascriptz:SelctPos();", true);
    }
    private void BindPayment()
    {
        DataTable dtPayment = new DataTable();
        dtPayment = objAdmin.GetPaymentMode();
        if (dtPayment.Rows.Count > 0)
        {
            ddlPayMode.DataSource = dtPayment;
            ddlPayMode.DataTextField = "PaymentType";
            ddlPayMode.DataValueField = "CCTypeID";
            ddlPayMode.DataBind();
            ddlPayMode.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlPayMode.DataSource = null;
            ddlPayMode.DataBind();
        }
    }

    private void BindRentalType()
    {
        DataSet rentalType = new DataSet();
        rentalType = objAdmin.BindRentalType();
        if (rentalType.Tables[0].Rows.Count > 0)
        {
            ddlRentalType.DataSource = rentalType.Tables[0];
            ddlRentalType.DataTextField = "Description";
            ddlRentalType.DataValueField = "LookupID";
            ddlRentalType.DataBind();
            ddlRentalType.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlRentalType.DataSource = null;
            ddlRentalType.DataBind();
        }

    }

    protected void ReadOnlyObjects()
    {
        if (Convert.ToInt32(hdnGuestID.Value) > 0)
        {
            txtGuestName.ReadOnly = true;
            txtGuestMobile.ReadOnly = true;
        }
        else
        {
            txtGuestName.ReadOnly = false;
            txtGuestMobile.ReadOnly = false;
        }
    }

    protected void BindPickupCity()
    {
        ddlPkpCity.DataTextField = "CityName";
        ddlPkpCity.DataValueField = "CityID";
        DataSet dscity = new DataSet();
        dscity = objAdmin.GetCity(Convert.ToInt32(Session["UserID"]));
        ddlPkpCity.DataSource = dscity;
        ddlPkpCity.DataBind();

        if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 26
                || Convert.ToInt32(ddlPkpCity.SelectedValue) == 215)
        {
            if (txtToll.Text == "")
            {
                txtToll.Text = "110";
            }
        }
        else
        {
            txtToll.Text = ""; //rahul
        }
    }

    protected void BindCompany()
    {
        DataTable dsCompany = new DataTable();
        ddlCompanyName.DataTextField = "CompanyName";
        ddlCompanyName.DataValueField = "CompanyID";
        ddlCompanyName.SelectedValue = "714";
        dsCompany = objAdmin.GetCompany();
        ddlCompanyName.DataSource = dsCompany;
        ddlCompanyName.DataBind();
    }

    protected void LoadCars()
    {
        DataTable dtCars = objAdmin.GetCars(Convert.ToInt32(Session["UserID"])
            , txtSearchCar.Text, Convert.ToInt32(ddlPkpCity.SelectedValue));
        if (dtCars.Rows.Count > 0)
        {
            ddlSelectCar.DataTextField = "ddlText";
            ddlSelectCar.DataValueField = "ddlValue";
            ddlSelectCar.DataSource = dtCars;
            ddlSelectCar.DataBind();
        }
        else
        {
            ddlSelectCar.DataTextField = "ddlText";
            ddlSelectCar.DataValueField = "ddlValue";
            ddlSelectCar.DataSource = dtCars;
            ddlSelectCar.DataBind();
            //ddlSelectChauffuer.Items.Clear();
        }
    }

    protected void GetLastTransactions()
    {
        DataTable dtCars = objAdmin.GetLastTransactions(txtSearchMobile.Text
            , Convert.ToInt32(ddlPkpCity.SelectedValue));
        if (dtCars.Rows.Count > 0)
        {
            gridData.DataSource = dtCars;
            gridData.DataBind();
            divTran.Visible = true;
        }
        else
        {
            gridData.DataSource = dtCars;
            gridData.DataBind();
            divTran.Visible = false;

        }
    }

    protected void txtSearchCar_TextChanged(object sender, EventArgs e)
    {
        if (txtSearchCar.Text != "")
        {
            LoadCars();
        }
        else
        {
            Reset();
        }
        ddlSelectCar_SelectedIndexChanged(sender, e);
    }
    protected void ddlSelectCar_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCars();

        fillChauffeur();

        getUnitDetails();

        ddlPkg_SelectedIndexChanged1(sender, e);
    }

    //private void FillChauffuer()
    //{
    //    try
    //    {
    //        String ddlValue = ddlSelectCar.SelectedValue.ToString();
    //        if (ddlValue != "")
    //        {
    //            String ddlCarText = ddlSelectCar.SelectedItem.Text;
    //            String carType = "";
    //            if (ddlCarText.Contains("Vendor"))
    //            {
    //                carType = "Vendor";
    //                ViewState ["VendorChauffYN"] = 1;
    //            }
    //            else
    //            {
    //                carType = "Own";
    //                ViewState["VendorChauffYN"] = 0;
    //            }
    //            String[] ddlValueArray = new String[4];
    //            char[] splitter = { '#' };
    //            ddlValueArray = ddlValue.Split(splitter);
    //            int CarId = Convert.ToInt32(ddlValueArray[0]);
    //            DataTable dtChauffuer = objAdmin.getChauffuer(CarId, Convert.ToInt32(Session["UserID"]), carType, Convert.ToInt32(ddlPkpCity.SelectedValue));
    //            ddlSelectChauffuer.DataTextField = "ChauffeurName";
    //            ddlSelectChauffuer.DataValueField = "VendorChauffeurID";
    //            ddlSelectChauffuer.DataSource = dtChauffuer;
    //            ddlSelectChauffuer.DataBind();
    //        }

    //    }
    //    catch (Exception Ex)
    //    {
    //        throw new Exception(Ex.Message);

    //    }

    //}
    protected void fillCars()
    {
        try
        {
            String ddlValue = ddlSelectCar.SelectedValue.ToString();

            if (ddlValue != "")
            {
                String[] ddlValueArray = new String[4];
                char[] splitter = { '#' };
                ddlValueArray = ddlValue.Split(splitter);
                int Catid = Convert.ToInt32(ddlValueArray[2]);//Car Category Id
                int Cityid = Convert.ToInt32(ddlPkpCity.SelectedValue);
                int Clientid = Convert.ToInt32(ddlCompanyName.SelectedValue);

                DataTable dtRetailPkg = objAdmin.GetRetailPkg(Cityid, Clientid, Catid);


                ddlPkg.DataTextField = "PkgText";
                ddlPkg.DataValueField = "PkgID";
                ddlPkg.DataSource = dtRetailPkg;
                ddlPkg.DataBind();
            }
            else
            {
                ddlPkg.Items.Clear();
            }
        }
        catch (Exception e)
        {
        }
    }

    protected void fillChauffeur()
    {
        try
        {
            string ddlValue = ddlSelectCar.SelectedValue.ToString();

            if (ddlValue != "")
            {
                String[] ddlValueArray = new String[4];
                char[] splitter = { '#' };
                ddlValueArray = ddlValue.Split(splitter);
                int CarID = Convert.ToInt32(ddlValueArray[0]); //CarId
                bool VendorCarYN = true;
                if (Convert.ToString(ddlValueArray[3]) == "1")
                {
                    VendorCarYN = true;
                }
                else
                {
                    VendorCarYN = false;
                }

                DataTable dtChauffeur = objAdmin.GetChauffeur(CarID, VendorCarYN);

                string Chauffid = objAdmin.GetDefaultChauff(CarID, VendorCarYN);

                ddlSelectChauffeur.DataTextField = "ddlText";
                ddlSelectChauffeur.DataValueField = "ddlValue";
                ddlSelectChauffeur.DataSource = dtChauffeur;
                ddlSelectChauffeur.DataBind();
                ddlSelectChauffeur.SelectedValue = Chauffid;
            }
            else
            {
                ddlSelectChauffeur.Items.Clear();
            }
        }
        catch (Exception e)
        {
        }
    }

    protected void ddlCompanyName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string clientname;
        clientname = ddlCompanyName.SelectedValue.ToString();
        if (clientname == "1209")//"619")
        {
            Show3.Visible = true;
            Show4.Visible = true;
            Show5.Visible = true;
        }
        //else if (clientname == "8220") //harcore
        //{
        //    Show3.Visible = false;
        //    Show4.Visible = false;
        //    Show5.Visible = false;
        //    showP.Visible = true;
        //    showT.Visible = true;
        //}
        else
        {
            Show3.Visible = false;
            Show4.Visible = false;
            Show5.Visible = false;
        }
        Reset();
    }
    protected void Reset()
    {
        ddlPkg.Items.Clear();
        ddlSelectCar.Items.Clear();
        ddlSelectChauffeur.Items.Clear();
        txtSearchCar.Text = "";
        txtAdjCost.Text = "";
        lblBasicAmt.Text = "";
        lbldiscountdesc.Text = "";
        lbldiscountAmt.Text = "";
        lblNetBillingAmt.Text = "";
        //txtToll.Text = "";
        //txtParking.Text = ""; //rahul
        //txtToll.Text = ""; //rahul

        if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 26
                || Convert.ToInt32(ddlPkpCity.SelectedValue) == 215)
        {
            if (txtToll.Text == "")
            {
                txtToll.Text = "110";
            }
        }
        else
        {
            txtToll.Text = ""; //rahul
        }
    }
    protected void ddlPkpCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (Session["POSID"] != "")
        //{
        //    Session["POSID"] = "";
        //}
        if (hdnpos.Value.Trim() != "")
        {
            hdnpos.Value = "";
        }
        SearchByMobile();
        getUnitDetails();

        getdiscount();

        Reset();
    }

    protected void txtGstNo_TextChanged(object sender, EventArgs e)
    {
        getUnitDetails();
    }

    protected Decimal SplitValue()
    {
        decimal PkgRate = 0;
        try
        {
            String ddlValue = ddlPkg.SelectedItem.Text;
            String[] ddlValueArray = new String[4];
            char[] splitter = { '-' };
            ddlValueArray = ddlValue.Split(splitter);
            String str1 = ddlValueArray[1];

            String i = str1.Substring(4, str1.Length - 4);
            PkgRate = Convert.ToDecimal(i);
            return PkgRate;
        }
        catch (Exception e)
        {
            return 0;
        }

    }
    protected void getdiscount()
    {
        DataSet getdiscount = new DataSet();
        try
        {
            if (Convert.ToInt32(ddlPkpCity.SelectedValue) > 0)
            {
                getdiscount = objAdmin.GetDiscountDetails(Convert.ToInt32(ddlPkpCity.SelectedValue));
                if (getdiscount.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= getdiscount.Tables[0].Rows.Count - 1; i++)
                    {
                        hdndiscount.Value = getdiscount.Tables[0].Rows[i]["Discount"].ToString();
                        hdndiscountstring.Value = getdiscount.Tables[0].Rows[i]["DiscountDesc"].ToString();
                        hdndiscountID.Value = getdiscount.Tables[0].Rows[i]["ID"].ToString();
                    }
                }
                else
                {
                    hdndiscount.Value = "0";
                    hdndiscountID.Value = "0";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void getUnitDetails()
    {
        try
        {
            //Added by bk sharma on dated 14 june 2013 for Get Car Id
            int CarID = 0;
            int ModelId = 0;
            int Catid = 0;
            DataTable dtgetUnitDetails = new DataTable();
            if (ddlSelectCar.SelectedValue.ToString() != "")
            {
                String ddlValue = ddlSelectCar.SelectedValue.ToString();
                String[] ddlValueArray = new String[4];
                char[] splitter = { '#' };
                ddlValueArray = ddlValue.Split(splitter);
                CarID = Convert.ToInt32(ddlValueArray[0]);
                ModelId = Convert.ToInt32(ddlValueArray[1]);
                Catid = Convert.ToInt32(ddlValueArray[2]);//Car Category Id
            }
            if (CarID != 0)
            {
                dtgetUnitDetails = objAdmin.getUnitDetailsSpecialTypeofCar(Convert.ToInt32(ddlPkpCity.SelectedValue), CarID);
            }
            else
            {
                dtgetUnitDetails = objAdmin.getUnitDetails(Convert.ToInt32(ddlPkpCity.SelectedValue));
            }
            //End for Get Car id   
            // DataTable dtgetUnitDetails = objAdmin.getUnitDetails(Convert.ToInt32(ddlPkpCity.SelectedValue));
            OL_TaxHeads objheads = TaxCalculation(Convert.ToInt32(ddlCompanyName.SelectedValue), Convert.ToInt32(ddlPkpCity.SelectedValue)
                , System.DateTime.Now.ToString("yyyy-MM-dd HH:mm"), CarID, 0);
            if (objheads.GSTEnabledYN == true && dtgetUnitDetails.Rows.Count > 0)
            {
                ServiceTax = Convert.ToDecimal(objheads.ServiceTaxPercent);
                EduTax = Convert.ToDecimal(objheads.EduCessPercent);
                HduTax = Convert.ToDecimal(objheads.HduCessPercent);
                SwachhBharatTax = Convert.ToDecimal(objheads.SwachhBharatTaxPercent);
                KrishiKalyanTax = Convert.ToDecimal(objheads.KrishiKalyanTaxPercent);

                if (txtGstNo.Text.Trim() != "")
                {
                    string Statecode = "";
                    Statecode = txtGstNo.Text.Substring(0, 2);

                    bool CGSTApplicable = objAdmin.CheckCGST_IGSTApplicable(Convert.ToInt32(ddlPkpCity.SelectedValue), Statecode);
                    if (CGSTApplicable)
                    {
                        CGSTTaxPercent = Convert.ToDecimal(objheads.CGSTPercent);
                        SGSTTaxPercent = Convert.ToDecimal(objheads.SGSTPercent);
                        IGSTTaxPercent = Convert.ToDecimal(objheads.IGSTPercent);
                    }
                    else
                    {
                        CGSTTaxPercent = 0;
                        SGSTTaxPercent = 0;
                        IGSTTaxPercent = Convert.ToDecimal(objheads.CGSTPercent) + Convert.ToDecimal(objheads.SGSTPercent);
                    }
                }
                else
                {
                    CGSTTaxPercent = Convert.ToDecimal(objheads.CGSTPercent);
                    SGSTTaxPercent = Convert.ToDecimal(objheads.SGSTPercent);
                    IGSTTaxPercent = Convert.ToDecimal(objheads.IGSTPercent);
                }

                GSTClientID = Convert.ToInt32(objheads.ClientGSTId);
                GSTEnabledYN = objheads.GSTEnabledYN;
                if (ViewState["CGSTTaxAmt"] != null)
                    CGSTTaxAmt = (decimal)ViewState["CGSTTaxAmt"];
                if (ViewState["SGSTTaxAmt"] != null)
                    SGSTTaxAmt = (decimal)ViewState["SGSTTaxAmt"];
                if (ViewState["IGSTTaxAmt"] != null)
                    IGSTTaxAmt = (decimal)ViewState["IGSTTaxAmt"];
                ServiceUnitId = Convert.ToInt32(dtgetUnitDetails.Rows[0]["UnitID"].ToString());
            }
            else if (dtgetUnitDetails.Rows.Count > 0 && objheads.GSTEnabledYN == false)
            {
                ServiceTax = Convert.ToDecimal(dtgetUnitDetails.Rows[0]["ServiceTaxPercent"].ToString());
                EduTax = Convert.ToDecimal(dtgetUnitDetails.Rows[0]["EduCessPercent"].ToString());
                HduTax = Convert.ToDecimal(dtgetUnitDetails.Rows[0]["HduCessPercent"].ToString());
                SwachhBharatTax = Convert.ToDecimal(dtgetUnitDetails.Rows[0]["SwachhBharatTaxPercent"].ToString());
                KrishiKalyanTax = Convert.ToDecimal(dtgetUnitDetails.Rows[0]["KrishiKalyanTaxPercent"].ToString());
                ServiceUnitId = Convert.ToInt32(dtgetUnitDetails.Rows[0]["UnitID"].ToString());
            }
        }
        catch (Exception e)
        {

        }
    }
    protected void PopUpReport()
    {
        string strscript = "", printurl = "";
        printurl = ConfigurationManager.AppSettings["InvoiceUrl"] + "?BookingID=" + Bookingid;
        strscript = "<script language='javascript'>window.open('"
            + printurl + "','HistoryWin','width=1000,height=800,top=0,left=0,menubar=0,scrollbars=yes,resizable=1');</script>";
        if (!ClientScript.IsClientScriptBlockRegistered(strscript))
            Page.ClientScript.RegisterClientScriptBlock(typeof(Label), "myScript", strscript);
    }

    protected void ddlPkg_SelectedIndexChanged1(object sender, EventArgs e)
    {
        Decimal PkgRate = SplitValue();
        txtAdjCost.Text = PkgRate.ToString();
        //     + Convert.ToInt32(txtParking.Text == "" ? "0" : txtParking.Text)
        //    + Convert.ToInt32(txtToll.Text == "" ? "0" : txtToll.Text);
        txtAdjCost.Focus();
        txtAdjCost_TextChanged(sender, e);
    }
    //protected void btnReset_ServerClick(object sender, EventArgs e)
    //{
    //    ClsAdmin.flagComplete = false;
    //    lblmessage.Text = "";
    //}
    protected void Btsave_Click(object sender, EventArgs e)
    {
        //if (RegularExpressionValidator1.Text == "Invalid Email Address" && txtEmailID.Text != "")
        //{
        //    message1.Text = "Invalid Email Address.";
        //    Page.RegisterClientScriptBlock("Script", "<script>alert('Invalid Email Address.')</script>");
        //    return;
        //}

        int payBackVal = 0;
        if (Session["StatusID"] == "1")
        {
            Btsave.Enabled = false;
            btReset.Enabled = true;
            if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 350 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 360)
            {
                btnRePrint.Enabled = false;
            }
            else
            {
                btnRePrint.Enabled = true;
            }
            return;
        }

        decimal gSTSurchargeAmount = objAdmin.GetGSTSurchargesAmount(Convert.ToInt32(ddlPkg.SelectedValue));
        decimal gSTSurchargeAmountWithTax = 0;

        if (GSTEnabledYN == true)
        {
            ttlTax = CGSTTaxPercent + SGSTTaxPercent + IGSTTaxPercent;
        }
        else
        {
            ttlTax = ServiceTax + SwachhBharatTax + KrishiKalyanTax + (ServiceTax * EduTax) / 100 + (ServiceTax * HduTax) / 100;
        }

        if (gSTSurchargeAmount > 0)
        {
            gSTSurchargeAmountWithTax = (gSTSurchargeAmount * (ttlTax + 100)) / 100;
        }

        if (Convert.ToDecimal(txtAdjCost.Text) < gSTSurchargeAmountWithTax)
        {
            message1.Text = "The total amount cannot be lower than Rs. " + gSTSurchargeAmountWithTax;
            Page.RegisterClientScriptBlock("Script", "<script>alert('Total cost should not be less than='" + Convert.ToString(gSTSurchargeAmountWithTax) + "')</script>");
            return;
        }

        if (ddlPayMode.SelectedIndex == 0 || ddlPayMode.SelectedValue == "0")
        {
            Page.RegisterClientScriptBlock("Script", "<script>alert('select payment mode.')</script>");
            return;
        }


        if (ChkSave.Value.ToString() == "0")
        {
            return;
        }
        if (chkPayBack.Checked == true)
        {
            payBackVal = 1;

        }
        else
        {
            payBackVal = 0;

        }


        DataSet GetBookingDetail = new DataSet();

        int UpdateGuest, tmpContDSID;

        String ddlValue = ddlSelectCar.SelectedValue.ToString();
        String[] ddlValueArray = new String[4];
        char[] splitter = { '#' };
        ddlValueArray = ddlValue.Split(splitter);
        int CarID = Convert.ToInt32(ddlValueArray[0]);
        int ModelId = Convert.ToInt32(ddlValueArray[1]);
        int Catid = Convert.ToInt32(ddlValueArray[2]);//Car Category Id
        int VendorCarYN = Convert.ToInt32(ddlValueArray[3]);
        string paymentMode = "CC";


        //*****************

        //if (string.IsNullOrEmpty(ddlSelectChauffeur.SelectedItem.Value))
        if (ddlSelectChauffeur.Items.Count <= 0)
        {
            message1.Text = "Cannot Close as Chauffeur is not available with the car.";
            Page.RegisterClientScriptBlock("Script", "<script>alert('Cannot Close as Chauffeur is not available with the car.')</script>");
            return;
        }

        String ddlchaufValue = ddlSelectChauffeur.SelectedValue.ToString();

        String[] ddlChauffeurArray = new String[2];
        ddlChauffeurArray = ddlchaufValue.Split(splitter);
        int ChauffeurID = Convert.ToInt32(ddlChauffeurArray[0]); //CarId
        bool VendorChaufYN = true;
        if (Convert.ToString(ddlChauffeurArray[1]) == "1")
        {
            VendorChaufYN = true;
        }
        else
        {
            VendorChaufYN = false;
        }
        //************


        Decimal PkgRate = SplitValue();
        Decimal pkgRateWithSurchargeAMT = PkgRate;
        if (gSTSurchargeAmountWithTax > 0)
        {
            PkgRate = PkgRate - gSTSurchargeAmountWithTax;
        }

        if (Convert.ToInt32(hdnGuestID.Value.ToString()) > 0)
        {
            UpdateGuest = 1;
        }
        else
        {
            UpdateGuest = 0;
        }

        if (ddlOptions.SelectedValue == "0")
        {
            tmpContDSID = 0;
        }
        else
        {
            if (txtSearchMobile.Text.Trim().Length > 0)
            {
                tmpContDSID = Convert.ToInt32(txtSearchMobile.Text);
            }
            else
            {
                tmpContDSID = 0;
            }
        }

        //Code Entered By Rahul
        if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 350 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 360)
        {

            if (hdnpos.Value == "")
            {
                if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 350)
                {
                    hdnpos.Value = PosID1.SelectedItem.Text.ToString();
                }
                if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 360)
                {
                    hdnpos.Value = PosID.SelectedItem.Text.ToString();
                }
                PosName = hdnpos.Value.ToString();
            }
            else
            {
                PosName = hdnpos.Value.ToString();
            }
        }

        if (ddlPayMode.SelectedValue == "5" || Convert.ToInt16(ddlPayMode.SelectedValue) == 5)
        {
            paymentMode = "Ca";
        }

        //**********Start
        int i = 0;
        string clientname;
        clientname = ddlCompanyName.SelectedValue.ToString();

        string URL = objAdmin.GetMobileAppURL();
        string smsstatus = "";

        if (clientname == "1209") //"619") 
        {
            DataSet dsvoucher = objAdmin.VoucherAvailable(Convert.ToInt32(clientname));
            if (dsvoucher != null && dsvoucher.Tables[0].Rows.Count > 0)
            {
                string Date_status3 = "", Vstatus3 = "", Date_status2 = "", vstatus2 = "", Vstatus1 = "", Date_status1 = "";
                int Vexists1 = 0, Vexists2 = 0, Vexists3 = 0;

                if (TextBox2.Text != "" || TextBox3.Text != "")
                {
                    if (TextBox1.Text == TextBox2.Text || TextBox2.Text == TextBox3.Text || TextBox1.Text == TextBox3.Text)
                    {
                        Page.RegisterClientScriptBlock("Script", "<script>alert('Duplicate Coupon No Entered.')</script>");
                        return;
                    }
                }

                if (TextBox1.Text != "")
                {
                    Vexists1 = ClsAdmin.VoucherExists(Convert.ToInt32(TextBox1.Text));
                    if (Vexists1 > 0)
                    {
                        Page.RegisterClientScriptBlock("Script", "<script>alert('Coupon No. " + Convert.ToInt32(TextBox1.Text) + " already used against, COR" + Vexists1 + "')</script>");
                        return;
                    }

                    Vstatus1 = ClsAdmin.CheckVoucher(Convert.ToInt32(TextBox1.Text), Convert.ToInt32(clientname), DateTime.Now, 1);
                    if (Vstatus1 == "1")
                    {
                        Date_status1 = ClsAdmin.CheckVoucher(Convert.ToInt32(TextBox1.Text), Convert.ToInt32(clientname), DateTime.Now, 2);
                        if (Date_status1 == "1")
                        {

                        }
                        else
                        {
                            //TextBox1.Text = "";
                            Page.RegisterClientScriptBlock("Script", "<script>alert('Coupon No 1 Expired')</script>");
                            return;
                        }
                    }
                    else
                    {
                        //TextBox1.Text = "";
                        Page.RegisterClientScriptBlock("Script", "<script>alert('Please Enter Valid Coupon No 1')</script>");
                        return;
                    }
                }

                if (TextBox2.Text != "")
                {
                    Vexists2 = ClsAdmin.VoucherExists(Convert.ToInt32(TextBox2.Text));
                    if (Vexists2 > 0)
                    {
                        Page.RegisterClientScriptBlock("Script", "<script>alert('Coupon No. " + Convert.ToInt32(TextBox2.Text) + " already used against, COR" + Vexists2 + "')</script>");
                        return;
                    }

                    vstatus2 = ClsAdmin.CheckVoucher(Convert.ToInt32(TextBox2.Text), Convert.ToInt32(clientname), DateTime.Now, 1);
                    if (vstatus2 == "1")
                    {
                        Date_status2 = ClsAdmin.CheckVoucher(Convert.ToInt32(TextBox1.Text), Convert.ToInt32(clientname), DateTime.Now, 2);
                        if (Date_status2 == "1")
                        {

                        }
                        else
                        {
                            //TextBox2.Text = "";
                            Page.RegisterClientScriptBlock("Script", "<script>alert('Coupon No 2 Expired')</script>");
                            return;
                        }
                    }
                    else
                    {
                        //TextBox2.Text = "";
                        Page.RegisterClientScriptBlock("Script", "<script>alert('Please Enter Valid Coupon No 2')</script>");
                        return;
                    }
                }


                if (TextBox3.Text != "")
                {
                    Vexists3 = ClsAdmin.VoucherExists(Convert.ToInt32(TextBox3.Text));
                    if (Vexists3 > 0)
                    {
                        Page.RegisterClientScriptBlock("Script", "<script>alert('Coupon No. " + Convert.ToInt32(TextBox3.Text) + " already used against, COR" + Vexists3 + "')</script>");
                        return;
                    }

                    Vstatus3 = ClsAdmin.CheckVoucher(Convert.ToInt32(TextBox3.Text), Convert.ToInt32(clientname), DateTime.Now, 1);
                    if (Vstatus3 == "1")
                    {
                        Date_status3 = ClsAdmin.CheckVoucher(Convert.ToInt32(TextBox1.Text), Convert.ToInt32(clientname), DateTime.Now, 2);
                        if (Date_status3 == "1")
                        {

                        }
                        else
                        {
                            //TextBox3.Text = "";
                            Page.RegisterClientScriptBlock("Script", "<script>alert('Coupon No 3 Expired')</script>");
                            return;
                        }
                    }
                    else
                    {
                        //TextBox3.Text = "";
                        Page.RegisterClientScriptBlock("Script", "<script>alert('Please Enter Valid Coupon No 3')</script>");
                        return;
                    }
                }

                i = objAdmin.SaveRetailBooking(Convert.ToInt32(ddlPkpCity.SelectedValue),
               Convert.ToInt32(ddlCompanyName.SelectedValue), txtGuestName.Text, txtGuestMobile.Text, CarID,
               ModelId, Catid, VendorCarYN, Convert.ToInt32(ddlPkg.SelectedValue), PkgRate,
               Convert.ToDecimal(txtAdjCost.Text), paymentMode,
               txtRemarks.Text, ServiceUnitId, Convert.ToInt32(Session["UserID"]), Convert.ToDecimal(lblBasicAmt.Text),
               ServiceTax, EduTax, UpdateGuest, Convert.ToInt32(hdnGuestID.Value.ToString()), tmpContDSID,
               Convert.ToInt32(ddlPayMode.SelectedValue), HduTax, PosName.ToString(), TextBox1.Text, TextBox2.Text, TextBox3.Text
               , true, payBackVal, SwachhBharatTax, KrishiKalyanTax, CGSTTaxPercent, SGSTTaxPercent, IGSTTaxPercent, CGSTTaxAmt, SGSTTaxAmt
               , IGSTTaxAmt, GSTClientID, gSTSurchargeAmount, pkgRateWithSurchargeAMT, Convert.ToDecimal(hdndiscountAmt.Value), Convert.ToInt32(hdndiscountID.Value)
               , txtEmailID.Text, Convert.ToDecimal(txtToll.Text), Convert.ToInt32(ddlRentalType.SelectedValue), ChauffeurID, VendorChaufYN
               , txtGstNo.Text.Trim(), txtLegalName.Text.Trim(), ddlShift.SelectedValue);
                //, Convert.ToInt32(ddlSelectChauffuer.SelectedValue), Convert.ToInt32(ViewState["VendorChauffYN"].ToString()));
                Bookingid = i;
                if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 2 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 29
                    || Convert.ToInt32(ddlPkpCity.SelectedValue) == 33 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 34
                    || Convert.ToInt32(ddlPkpCity.SelectedValue) == 35 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 36)
                {
                    string smsScript = "An outstation duty No." + Bookingid + " has been initiated at " + ddlPkpCity.SelectedItem.Text + ".";

                    string UnitPhone = objAdmin.GetUnitPhoneDetails(ServiceUnitId);
                    if (string.IsNullOrEmpty(UnitPhone))
                    {
                        UnitPhone = "011-41841212";
                    }

                    //string smsScriptGuest = "Dear Guest, Thank you for using our Car Rental services at " + ddlPkpCity.SelectedItem.Text 
                    //+ " booking id " + Bookingid.ToString() + ", for your next requirement you may call us on 011-41841212" + URL + ", feedbacks are welcome on crm@carzonrent.com";
                    string smsScriptGuest = "Dear Guest, Thank you for using our Car Rental services at " + ddlPkpCity.SelectedItem.Text + " booking id " + Bookingid.ToString()
                        + ", for your next requirement you may call us on " + UnitPhone + URL + ", feedbacks are welcome on crm@carzonrent.com";

                    smsstatus = SendSMSMailService.SendSMSDetails(smsScriptGuest, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString()); //live webService
                    //smsstatus = SendSMSMailServiceTest.SendSMSDetails(smsScriptGuest, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString()); //Test WebService

                    smsstatus = SendSMSMailService.SendSMSDetails_smsdetails_GetDetails("Feedback", "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString(), 0);

                    //for client  " + ddlCompanyName.SelectedItem.Text + ", through Car No " + ddlSelectCar.SelectedItem.Text + "  and mobile no " + txtGuestMobile.Text + ".";
                    if (Convert.ToDouble(txtAdjCost.Text) >= 3500)
                    {
                        smsstatus = SendSMSMailService.SendSMSDetails(smsScript, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString());
                    }


                }


                //for sending sms when amount is greater than 3500

                //End sms

                if (txtEmailID.Text != "")
                {
                    string emailstatus = SendSMSMailService.SendInvoiceLink(Bookingid);
                }
            }
        }
        //**********End
        else
        {

            i = objAdmin.SaveRetailBooking(Convert.ToInt32(ddlPkpCity.SelectedValue),
           Convert.ToInt32(ddlCompanyName.SelectedValue), txtGuestName.Text, txtGuestMobile.Text, CarID,
           ModelId, Catid, VendorCarYN, Convert.ToInt32(ddlPkg.SelectedValue), PkgRate,
           Convert.ToDecimal(txtAdjCost.Text), paymentMode,
           txtRemarks.Text, ServiceUnitId, Convert.ToInt32(Session["UserID"]), Convert.ToDecimal(lblBasicAmt.Text),
           ServiceTax, EduTax, UpdateGuest, Convert.ToInt32(hdnGuestID.Value.ToString()), tmpContDSID,
           Convert.ToInt32(ddlPayMode.SelectedValue), HduTax, PosName.ToString(), "", "", "", false, payBackVal, SwachhBharatTax, KrishiKalyanTax
           , CGSTTaxPercent, SGSTTaxPercent, IGSTTaxPercent, CGSTTaxAmt, SGSTTaxAmt, IGSTTaxAmt, GSTClientID, gSTSurchargeAmount, pkgRateWithSurchargeAMT
           , Convert.ToDecimal(hdndiscountAmt.Value), Convert.ToInt32(hdndiscountID.Value)
           , txtEmailID.Text, Convert.ToDecimal(txtToll.Text), Convert.ToInt32(ddlRentalType.SelectedValue), ChauffeurID, VendorChaufYN
           , txtGstNo.Text.Trim(), txtLegalName.Text.Trim()
           , ddlShift.SelectedValue);
            //, Convert.ToInt32(ddlSelectChauffuer.SelectedValue), Convert.ToInt32(ViewState["VendorChauffYN"].ToString()));
            Bookingid = i;
            if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 2 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 29
                || Convert.ToInt32(ddlPkpCity.SelectedValue) == 33 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 34
                || Convert.ToInt32(ddlPkpCity.SelectedValue) == 35 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 36)
            {
                string smsScript = "An outstation duty No. " + Bookingid + " has been initiated at " + ddlPkpCity.SelectedItem.Text + ".";
                if (Convert.ToDouble(txtAdjCost.Text) >= 3500)
                {
                    smsstatus = SendSMSMailService.SendSMSDetails(smsScript, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString());
                }

                smsstatus = SendSMSMailService.SendSMSDetails_smsdetails_GetDetails("Feedback", "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString(), 0);
            }
            //for sending sms when amount is greater than 3500
            //End sms
        }

        if (i > 0)
        {
            Session["StatusID"] = "1";
            lblmessage.Text = "New BookingID is: " + i.ToString();
            StringBuilder strXML = new StringBuilder();
            string strXMLResponse = "";
            string UnitPhone = objAdmin.GetUnitPhoneDetails(ServiceUnitId);
            if (string.IsNullOrEmpty(UnitPhone))
            {
                UnitPhone = "011-41841212";
            }

            //string smsScriptGuest = "Dear Guest, Thank you for using our Car Rental services at " + ddlPkpCity.SelectedItem.Text 
            //+ " booking id " + Bookingid.ToString() + ", for your next requirement you may call us on 011-41841212" + URL 
            //+ ", feedbacks are welcome on crm@carzonrent.com";
            string smsScriptGuest = "Dear Guest, Thank you for using our Car Rental services at " + ddlPkpCity.SelectedItem.Text
                + " booking id " + Bookingid.ToString() + ", for your next requirement you may call us on " + UnitPhone + URL
                + ", feedbacks are welcome on crm@carzonrent.com";


            if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 26)
            {
                string smsScript = "Thank you for choosing Carzonrent. Your payable amount is INR " + txtAdjCost.Text.ToString()
                    + ". Invoice details are " + ConfigurationManager.AppSettings["InvoiceUrl"]
                    + "?bookingid=" + Bookingid.ToString() + ". Helpdesk airportblr@carzonrent.com. Phone 9972502292";
                smsstatus = SendSMSMailService.SendSMSDetails_WithLaterOption(smsScript, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString(), 0);
            }
            else if (Convert.ToInt32(ddlPkpCity.SelectedValue) != 33 && Convert.ToInt32(ddlPkpCity.SelectedValue) != 35
                && Convert.ToInt32(ddlPkpCity.SelectedValue) != 36)
            {
                smsstatus = SendSMSMailService.SendSMSDetails(smsScriptGuest, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString()); //Live Web Service
                //smsstatus = SendSMSMailServiceTest.SendSMSDetails(smsScriptGuest, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString()); //Test Web Service

                smsstatus = SendSMSMailService.SendSMSDetails_smsdetails_GetDetails("Feedback", "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString(), 0);
            }

            if (txtEmailID.Text != "")
            {
                string emailstatus = SendSMSMailService.SendInvoiceLink(Bookingid);
            }
            //Comment for testing purpose

            //Code Entered By Rahul
            //if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 350)
            //if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 33 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 34 
            //|| Convert.ToInt32(ddlPkpCity.SelectedValue) == 35 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 36)

            #region CCTV
            //CCTV Code Start on 08 March 2015
            //if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 33 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 35
            //    || Convert.ToInt32(ddlPkpCity.SelectedValue) == 36)
            if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 330 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 350
                || Convert.ToInt32(ddlPkpCity.SelectedValue) == 360)
            {
                DataSet dsBookingDetails = new DataSet();
                dsBookingDetails = objAdmin.MilestonePostXMLData(Convert.ToInt32(i));
                CCTVProxy.RetailInputWebService proxy = new CCTVProxy.RetailInputWebService();
                CCTVProxy.TransactionResponseObject obj = new CCTVProxy.TransactionResponseObject();
                CCTVProxy.TransactionResponseObject obj1 = new CCTVProxy.TransactionResponseObject();
                decimal unitPrice, subTotal;
                string booking = "", ReceiptNO = "", StatusResponse = "", ErrorCode = "", ErrorMsg = "";
                try
                {

                    // 1 Transaction Header 
                    obj1 = proxy.BeginTransactionWithTillLookup((Convert.ToInt64(dsBookingDetails.Tables[0].Rows[0]["BookingID"].ToString()))
                        , false, dsBookingDetails.Tables[0].Rows[0]["StoreNo"].ToString(), dsBookingDetails.Tables[0].Rows[0]["POSNo"].ToString()
                       , dsBookingDetails.Tables[0].Rows[0]["BookingID"].ToString(), null, null, "",
                        DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime
                       , false, Session["UserID"].ToString(), dsBookingDetails.Tables[0].Rows[0]["StaffName"].ToString().Trim()
                       , "INR", CCTVProxy.TransactionType.CompletedNormally, true, false, false, CCTVProxy.OutsideOpeningHours.InsideOpeningHours
                       , false, 0, true);

                    //2 Transaction Sale            

                    if (Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString()) > 0)
                    {
                        obj = proxy.AddTransactionSaleLine
                        (Convert.ToInt64(obj1.TransactionSessionId), true
                        , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                        , CCTVProxy.SaleLineAttribute.None, false, CCTVProxy.ScanAttribute.None, false
                        , dsBookingDetails.Tables[0].Rows[0]["VehicleNo"].ToString().Trim(), dsBookingDetails.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim()
                        , 1, true, null, Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString())
                        , true, CCTVProxy.DiscountType.DiscountNotAllowed, false
                        , null, false, Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString()), true, null, null
                        , false, null, null, false, null, true, true);
                    }
                    else
                    {
                        unitPrice = Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString().Replace('-', ' '));
                        subTotal = Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["Basic"].ToString().Replace('-', ' '));

                        obj = proxy.AddTransactionSaleLine
                       (Convert.ToInt64(obj1.TransactionSessionId), true
                       , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                       , CCTVProxy.SaleLineAttribute.None, false, CCTVProxy.ScanAttribute.None, false
                       , dsBookingDetails.Tables[0].Rows[0]["VehicleNo"].ToString().Trim(), dsBookingDetails.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim()
                       , 1, true, null, Convert.ToDecimal(unitPrice)
                       , true, CCTVProxy.DiscountType.DiscountNotAllowed, false
                       , null, true, Convert.ToDecimal(subTotal), true, null, null
                       , false, null, null, false, null, true, true);
                    }

                    // 4 Transaction payment

                    CCTVProxy.PaymentLineAttribute lineAttribute;  //Payment Type (Cash,credit)
                    string paymentTypeId = string.Empty;   //Payment type Id (card number)
                    string cardType = string.Empty;        // type of creadit card (Card issuer)
                    string linepaymentType = string.Empty;

                    if (dsBookingDetails.Tables[0].Rows[0]["PaymentMode"].ToString().Trim() == "Cr"
                        || dsBookingDetails.Tables[0].Rows[0]["PaymentMode"].ToString().Trim() == "Ca")
                    {
                        lineAttribute = CCTVProxy.PaymentLineAttribute.Cash;
                        paymentTypeId = null;
                        cardType = null;
                        linepaymentType = "Cash";
                    }
                    else
                    {
                        lineAttribute = CCTVProxy.PaymentLineAttribute.CreditCard;
                        paymentTypeId = "CC";
                        cardType = "0";
                        linepaymentType = "Credit Card";
                    }

                    obj = proxy.AddTransactionPaymentLine(Convert.ToInt64(obj1.TransactionSessionId), true
                        , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                        , lineAttribute, true, linepaymentType, "INR"
                        , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"]), true, Convert.ToDecimal(1), true
                        , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"].ToString())
                        , true, paymentTypeId
                        , cardType, null, false, true, true);

                    //3 Transaction total serive tax

                    obj = proxy.AddTransactionTotalLine(Convert.ToInt64(obj1.TransactionSessionId), true
                        , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                         , CCTVProxy.TotalLineAttribute.VAT, true, "Total sales tax amount"
                         , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["CalculatedAmount"].ToString()), true, true, true);

                    //3 Transaction total
                    obj = proxy.AddTransactionTotalLine(Convert.ToInt64(obj1.TransactionSessionId), true
                        , DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime, false, null, false
                          , CCTVProxy.TotalLineAttribute.TotalAmountToBePaid, true, "Total Amont to be paid"
                          , Convert.ToDecimal(dsBookingDetails.Tables[0].Rows[0]["TotalAmount"].ToString()), true, true, true);


                    //5 Transaction Event
                    //obj = proxy.AddTransactionEvent(Convert.ToInt64(obj1.TransactionSessionId),true
                    //,DateTimeOffset.Parse(dsBookingDetails.Tables[0].Rows[0]["AccountingDate"].ToString().Trim()).UtcDateTime
                    //    ,false,null,false,CCTVProxy.EventLineAttribute.None, false,"Car Rental",true,true);

                    //6 Transaction commit
                    obj = proxy.CommitTransaction(Convert.ToInt64(obj1.TransactionSessionId), true);

                    if (obj.Succeeded == true)
                    {
                        ReceiptNO = "Success";
                        //smsstatus = SendSMSMailService.SendSMSDetails(smsScriptGuest, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString());
                        int cctvId = objAdmin.SaveCCTVDetails(Convert.ToInt32(i), Convert.ToInt64(obj1.TransactionSessionId)
                            , Convert.ToInt32(Session["UserID"].ToString()), ReceiptNO, ReceiptNO);
                        //objAdmin.UpdateRetailStatus(Convert.ToInt32(i), ReceiptNO, ReceiptNO);
                        ClsAdmin.flagComplete = true;
                    }
                    else
                    {
                        ErrorCode = obj.ErrorCode.ToString();
                        ErrorMsg = obj.ErrorDescription.ToString();
                        int cctvId = objAdmin.SaveCCTVDetails(Convert.ToInt32(i), Convert.ToInt64(obj1.TransactionSessionId)
                            , Convert.ToInt32(Session["UserID"].ToString()), ErrorCode, ErrorMsg);
                        StatusResponse = "Status : " + obj.Succeeded.ToString() + ", Error Code : " + ErrorCode + "-Message: " + ErrorMsg;
                        //objAdmin.UpdateRetailStatus(Convert.ToInt32(i), ReceiptNO, StatusResponse);
                        lblmessage.Text = StatusResponse; //"Error!!!";
                    }
                }
                catch (Exception Ex)
                {
                    ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                }
            }
            //CCTV Code End on 08 March 2015
            #endregion

            // if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 33)
            if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 330)
            {
                string booking = "", ReceiptNO = "", StatusResponse = "", ErrorCode = "", ErrorMsg = "";
                try
                {
                    int paymentType = 0;
                    if (ddlPayMode.SelectedValue == "5")
                    {
                        paymentType = 1;
                    }
                    else
                    {
                        paymentType = 3;
                    }
                    GetBookingDetail = objAdmin.GetBookingDetail(Convert.ToInt32(i));
                    strXML = strXML.Append("<?xml version='1.0' encoding='UTF-8' ?>");
                    strXML = strXML.Append("<TransactionData xsi:noNamespaceSchemaLocation='http://localhost/_URL_/Carzonrent.xsd' "); //This is for live
                    strXML = strXML.Append("xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");
                    strXML = strXML.Append("<ServicePartnerNo>" + "1000000583" + "</ServicePartnerNo>"); //This is for live
                    strXML = strXML.Append("<Password>Crzornt</Password>");
                    strXML = strXML.Append("<Transactions> <Transaction>");
                    strXML = strXML.Append("<TransactionNo>" + GetBookingDetail.Tables[0].Rows[0]["BookingID"].ToString().Trim() + "</TransactionNo>");
                    strXML = strXML.Append("<OriginalRefNo>" + GetBookingDetail.Tables[0].Rows[0]["BookingID"].ToString().Trim() + "</OriginalRefNo>");
                    strXML = strXML.Append("<EntryType>" + 1 + "</EntryType>");
                    strXML = strXML.Append("<StoreNo>" + "CZ012" + "</StoreNo>");
                    strXML = strXML.Append("<POSNo>" + "OR101" + "</POSNo>");
                    strXML = strXML.Append("<StaffID>" + Convert.ToInt32(Session["UserID"]).ToString() + "</StaffID>");
                    strXML = strXML.Append("<StaffName>" + GetBookingDetail.Tables[0].Rows[0]["StaffName"].ToString().Trim() + "</StaffName>");
                    strXML = strXML.Append("<TransactionDate>" + GetBookingDetail.Tables[0].Rows[0]["AccountingDate"].ToString().Trim() + "</TransactionDate>");
                    strXML = strXML.Append("<TransactionTime>" + GetBookingDetail.Tables[0].Rows[0]["AccountingTime"].ToString().Trim() + "</TransactionTime>");
                    strXML = strXML.Append("<DiscountAmount>" + 00.00 + "</DiscountAmount>");
                    strXML = strXML.Append("<TotalDiscount>" + 00.00 + "</TotalDiscount>");
                    strXML = strXML.Append("<TableNo>0</TableNo>");
                    strXML = strXML.Append("<NoOfCovers>0</NoOfCovers>");
                    strXML = strXML.Append("<CustomerName>" + GetBookingDetail.Tables[0].Rows[0]["CustomerName"].ToString().Trim() + "</CustomerName>");
                    strXML = strXML.Append("<Address></Address>");
                    strXML = strXML.Append("<Gender></Gender>");
                    strXML = strXML.Append("<PassportNo></PassportNo>");
                    strXML = strXML.Append("<Nationality>" + "IN" + "</Nationality>");
                    strXML = strXML.Append("<PortofBoarding></PortofBoarding>");
                    strXML = strXML.Append("<PortofDisembarkation></PortofDisembarkation>");
                    strXML = strXML.Append("<FlightNo></FlightNo>");
                    strXML = strXML.Append("<SectorNo></SectorNo>");
                    strXML = strXML.Append("<BoardingPassNo></BoardingPassNo>");
                    strXML = strXML.Append("<SeatNo></SeatNo>");
                    strXML = strXML.Append("<Airlines></Airlines>");
                    strXML = strXML.Append("<ServiceChargeAmount>" + 0 + "</ServiceChargeAmount>");
                    strXML = strXML.Append("<NetTransactionAmount>" + Convert.ToDouble(lblBasicAmt.Text) + "</NetTransactionAmount>");
                    strXML = strXML.Append("<GrossTransactionAmount>" + Convert.ToDouble(txtAdjCost.Text) + "</GrossTransactionAmount>");
                    strXML = strXML.Append("<CustomerType>" + 0 + "</CustomerType>");
                    strXML = strXML.Append("<Items>");
                    strXML = strXML.Append("<Item>");
                    strXML = strXML.Append("<ItemCode>" + "Cab" + "</ItemCode>");
                    strXML = strXML.Append("<ItemDescription>" + "Delhi" + "</ItemDescription>");
                    strXML = strXML.Append("<ItemCategory>" + "Cab" + "</ItemCategory>");
                    strXML = strXML.Append("<ItemCategoryDescription>" + "Special Cab" + "</ItemCategoryDescription>");
                    strXML = strXML.Append("<ProductGroup>" + "Cab" + "</ProductGroup>");
                    strXML = strXML.Append("<ProductGroupDescription>" + GetBookingDetail.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim() + "</ProductGroupDescription>");
                    strXML = strXML.Append("<BarcodeNo >0</BarcodeNo>");
                    strXML = strXML.Append("<Quantity>" + 1 + "</Quantity>");
                    strXML = strXML.Append("<Price>" + Convert.ToDouble(lblBasicAmt.Text) + "</Price>");
                    strXML = strXML.Append("<NetAmount>" + Convert.ToDouble(lblBasicAmt.Text) + "</NetAmount>");
                    strXML = strXML.Append("<PriceInclusiveTax>" + 1 + "</PriceInclusiveTax>");
                    strXML = strXML.Append("<ChangedPrice>" + 1 + "</ChangedPrice>");
                    strXML = strXML.Append("<ScaleItem>" + 1 + "</ScaleItem>");
                    strXML = strXML.Append("<WeighingItem>" + 1 + "</WeighingItem>");
                    strXML = strXML.Append("<ItemSerialNo></ItemSerialNo>");
                    strXML = strXML.Append("<UOM>" + "Pics" + "</UOM>");
                    strXML = strXML.Append("<LineDiscount>" + 0 + "</LineDiscount>");
                    strXML = strXML.Append("<TotalDiscount>" + 0 + "</TotalDiscount>");
                    strXML = strXML.Append("<PeriodicDiscount>" + 0 + "</PeriodicDiscount>");
                    strXML = strXML.Append("<PromotionNo></PromotionNo>");
                    strXML = strXML.Append("<TaxAmount>" + Convert.ToDouble(GetBookingDetail.Tables[0].Rows[0]["TotalTaxAmount"]) + "</TaxAmount>");
                    strXML = strXML.Append("<TaxRate>" + Convert.ToDouble(GetBookingDetail.Tables[0].Rows[0]["TotalTaxRate"]) + "</TaxRate>");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("</Item> ");
                    strXML = strXML.Append("</Items>");
                    strXML = strXML.Append("<ServiceCharges>");
                    strXML = strXML.Append("<Charge> ");
                    strXML = strXML.Append("<Type>" + 1 + "</Type> ");
                    strXML = strXML.Append("<TypeDescription>" + "Service Charges" + "</TypeDescription> ");
                    strXML = strXML.Append("<ServiceChargeAmount>0</ServiceChargeAmount>");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("</Charge> ");
                    strXML = strXML.Append("</ServiceCharges>");
                    strXML = strXML.Append("<Payments> ");
                    strXML = strXML.Append("<Payment> ");
                    strXML = strXML.Append("<TenderType>" + paymentType + "</TenderType> ");
                    strXML = strXML.Append("<CardNo></CardNo> ");
                    strXML = strXML.Append("<CurrencyCode>" + "INR" + "</CurrencyCode> ");
                    strXML = strXML.Append("<ExchangeRate>" + 1 + "</ExchangeRate> ");
                    strXML = strXML.Append("<AmountTendered>" + Convert.ToDouble(txtAdjCost.Text) + "</AmountTendered> ");
                    strXML = strXML.Append("<AmountInCurrency>" + Convert.ToDouble(00.00) + "</AmountInCurrency> ");
                    strXML = strXML.Append("</Payment>");
                    strXML = strXML.Append("</Payments>");
                    strXML = strXML.Append("</Transaction>");
                    strXML = strXML.Append("</Transactions>");
                    strXML = strXML.Append("</TransactionData>");
                    //code comment for prevent post data to the webservioce
                    WebReference.DIALService s1 = new WebReference.DIALService();
                    strXMLResponse = s1.SaveTransaction(strXML.ToString());
                    //if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 33)
                    //{
                    //    WebReference.DIALService s1 = new WebReference.DIALService();
                    //    strXMLResponse = s1.SaveTransaction(strXML.ToString());
                    //}
                    //else
                    //{
                    //    //WebReference.DIALService s1 = new WebReference.DIALService();
                    //    //strXMLResponse = s1.SaveTransaction(strXML.ToString());
                    //    WebReference.T3DIALService s1 = new WebReference.T3DIALService();
                    //    strXMLResponse = s1.SaveTransaction(strXML.ToString());
                    //}
                    DataSet dt = new DataSet();
                    StringReader stream = new StringReader(strXMLResponse);
                    XmlTextReader reader = new XmlTextReader(stream);
                    dt.ReadXml(reader);
                    booking = dt.Tables[1].Rows[0]["Number"].ToString();
                    ReceiptNO = "";
                    StatusResponse = dt.Tables[1].Rows[0]["Status"].ToString();
                    if (StatusResponse == "Success")
                    {
                        ReceiptNO = "Success";
                        smsstatus = SendSMSMailService.SendSMSDetails(smsScriptGuest, "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString());

                        smsstatus = SendSMSMailService.SendSMSDetails_smsdetails_GetDetails("Feedback", "91" + txtGuestMobile.Text.Trim(), Bookingid.ToString(), 0);

                        objAdmin.UpdateRetailStatus(Convert.ToInt32(booking), ReceiptNO, StatusResponse);
                        ClsAdmin.flagComplete = true;
                        PopUpReport();
                    }
                    else
                    {
                        ErrorCode = dt.Tables[1].Rows[0]["ErrorCode"].ToString();
                        ErrorMsg = dt.Tables[1].Rows[0]["ErrorMsg"].ToString();
                        StatusResponse = "Status : " + StatusResponse + ", Error Code : " + ErrorCode + "-Message: " + ErrorMsg;
                        objAdmin.UpdateRetailStatus(Convert.ToInt32(i), ReceiptNO, StatusResponse);
                        lblmessage.Text = StatusResponse; //"Error!!!";
                    }
                    //End Comment
                }
                catch (Exception ex)
                {
                    lblmessage.Text = "No Response!!!";
                }
            }
            // else if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 35 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 36)
            else if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 350 || Convert.ToInt32(ddlPkpCity.SelectedValue) == 360)
            {
                string booking = "", ReceiptNO = "", StatusResponse = "", ErrorCode = "", ErrorMsg = "";
                try
                {
                    int paymentType = 0;
                    if (ddlPayMode.SelectedValue == "5")
                    {
                        paymentType = 1;
                    }
                    else
                    {
                        paymentType = 3;
                    }
                    GetBookingDetail = objAdmin.GetBookingDetail(Convert.ToInt32(i));
                    strXML = strXML.Append("<?xml version='1.0' encoding='UTF-8' ?>");
                    strXML = strXML.Append("<TransactionData xsi:noNamespaceSchemaLocation='http://localhost/_URL_/Carzonrent.xsd' "); //This is for live
                    strXML = strXML.Append("xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");
                    strXML = strXML.Append("<ServicePartnerNo>" + "1000000583" + "</ServicePartnerNo>"); //This is for live
                    strXML = strXML.Append("<Password>Crzornt</Password>");
                    strXML = strXML.Append("<Transactions> <Transaction>");
                    strXML = strXML.Append("<TransactionNo>" + GetBookingDetail.Tables[0].Rows[0]["BookingID"].ToString().Trim() + "</TransactionNo>");
                    strXML = strXML.Append("<OriginalRefNo>" + GetBookingDetail.Tables[0].Rows[0]["BookingID"].ToString().Trim() + "</OriginalRefNo>");
                    strXML = strXML.Append("<EntryType>" + 1 + "</EntryType>");
                    strXML = strXML.Append("<StoreNo>" + GetBookingDetail.Tables[0].Rows[0]["StoreNo"].ToString() + "</StoreNo>");
                    strXML = strXML.Append("<POSNo>" + GetBookingDetail.Tables[0].Rows[0]["POSNo"].ToString() + "</POSNo>");
                    strXML = strXML.Append("<StaffID>" + Convert.ToInt32(Session["UserID"]).ToString() + "</StaffID>");
                    strXML = strXML.Append("<StaffName>" + GetBookingDetail.Tables[0].Rows[0]["StaffName"].ToString().Trim() + "</StaffName>");
                    strXML = strXML.Append("<TransactionDate>" + GetBookingDetail.Tables[0].Rows[0]["AccountingDate"].ToString().Trim() + "</TransactionDate>");
                    strXML = strXML.Append("<TransactionTime>" + GetBookingDetail.Tables[0].Rows[0]["AccountingTime"].ToString().Trim() + "</TransactionTime>");
                    strXML = strXML.Append("<DiscountAmount>" + 00.00 + "</DiscountAmount>");
                    strXML = strXML.Append("<TotalDiscount>" + 00.00 + "</TotalDiscount>");
                    strXML = strXML.Append("<TableNo>0</TableNo>");
                    strXML = strXML.Append("<NoOfCovers>0</NoOfCovers>");
                    strXML = strXML.Append("<CustomerName>" + GetBookingDetail.Tables[0].Rows[0]["CustomerName"].ToString().Trim() + "</CustomerName>");
                    strXML = strXML.Append("<Address></Address>");
                    strXML = strXML.Append("<Gender></Gender>");
                    strXML = strXML.Append("<PassportNo></PassportNo>");
                    strXML = strXML.Append("<Nationality>" + "IN" + "</Nationality>");
                    strXML = strXML.Append("<PortofBoarding></PortofBoarding>");
                    strXML = strXML.Append("<PortofDisembarkation></PortofDisembarkation>");
                    strXML = strXML.Append("<FlightNo></FlightNo>");
                    strXML = strXML.Append("<SectorNo></SectorNo>");
                    strXML = strXML.Append("<BoardingPassNo></BoardingPassNo>");
                    strXML = strXML.Append("<SeatNo></SeatNo>");
                    strXML = strXML.Append("<Airlines></Airlines>");
                    strXML = strXML.Append("<ServiceChargeAmount>" + 0 + "</ServiceChargeAmount>");
                    strXML = strXML.Append("<NetTransactionAmount>" + Convert.ToDouble(lblBasicAmt.Text) + "</NetTransactionAmount>");
                    strXML = strXML.Append("<GrossTransactionAmount>" + Convert.ToDouble(txtAdjCost.Text) + "</GrossTransactionAmount>");
                    strXML = strXML.Append("<CustomerType>" + 0 + "</CustomerType>");
                    strXML = strXML.Append("<Items>");
                    strXML = strXML.Append("<Item>");
                    strXML = strXML.Append("<ItemCode>" + "Cab" + "</ItemCode>");
                    strXML = strXML.Append("<ItemDescription>" + "Delhi" + "</ItemDescription>");
                    strXML = strXML.Append("<ItemCategory>" + "Cab" + "</ItemCategory>");
                    strXML = strXML.Append("<ItemCategoryDescription>" + "Special Cab" + "</ItemCategoryDescription>");
                    strXML = strXML.Append("<ProductGroup>" + "Cab" + "</ProductGroup>");
                    strXML = strXML.Append("<ProductGroupDescription>" + GetBookingDetail.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim() + "</ProductGroupDescription>");
                    strXML = strXML.Append("<BarcodeNo >0</BarcodeNo>");
                    strXML = strXML.Append("<Quantity>" + 1 + "</Quantity>");
                    strXML = strXML.Append("<Price>" + Convert.ToDouble(lblBasicAmt.Text) + "</Price>");
                    strXML = strXML.Append("<NetAmount>" + Convert.ToDouble(lblBasicAmt.Text) + "</NetAmount>");
                    strXML = strXML.Append("<PriceInclusiveTax>" + 1 + "</PriceInclusiveTax>");
                    strXML = strXML.Append("<ChangedPrice>" + 1 + "</ChangedPrice>");
                    strXML = strXML.Append("<ScaleItem>" + 1 + "</ScaleItem>");
                    strXML = strXML.Append("<WeighingItem>" + 1 + "</WeighingItem>");
                    strXML = strXML.Append("<ItemSerialNo></ItemSerialNo>");
                    strXML = strXML.Append("<UOM>" + "Pics" + "</UOM>");
                    strXML = strXML.Append("<LineDiscount>" + 0 + "</LineDiscount>");
                    strXML = strXML.Append("<TotalDiscount>" + 0 + "</TotalDiscount>");
                    strXML = strXML.Append("<PeriodicDiscount>" + 0 + "</PeriodicDiscount>");
                    strXML = strXML.Append("<PromotionNo></PromotionNo>");
                    strXML = strXML.Append("<TaxAmount>" + Convert.ToDouble(GetBookingDetail.Tables[0].Rows[0]["TotalTaxAmount"]) + "</TaxAmount>");
                    strXML = strXML.Append("<TaxRate>" + Convert.ToDouble(GetBookingDetail.Tables[0].Rows[0]["TotalTaxRate"]) + "</TaxRate>");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("</Item> ");
                    strXML = strXML.Append("</Items>");
                    strXML = strXML.Append("<ServiceCharges>");
                    strXML = strXML.Append("<Charge> ");
                    strXML = strXML.Append("<Type>" + 1 + "</Type> ");
                    strXML = strXML.Append("<TypeDescription>" + "Service Charges" + "</TypeDescription> ");
                    strXML = strXML.Append("<ServiceChargeAmount>0</ServiceChargeAmount>");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("</Charge> ");
                    strXML = strXML.Append("</ServiceCharges>");
                    strXML = strXML.Append("<Payments> ");
                    strXML = strXML.Append("<Payment> ");
                    strXML = strXML.Append("<TenderType>" + paymentType + "</TenderType> ");
                    strXML = strXML.Append("<CardNo></CardNo> ");
                    strXML = strXML.Append("<CurrencyCode>" + "INR" + "</CurrencyCode> ");
                    strXML = strXML.Append("<ExchangeRate>" + 1 + "</ExchangeRate> ");
                    strXML = strXML.Append("<AmountTendered>" + Convert.ToDouble(txtAdjCost.Text) + "</AmountTendered> ");
                    strXML = strXML.Append("<AmountInCurrency>" + Convert.ToDouble(00.00) + "</AmountInCurrency> ");
                    strXML = strXML.Append("</Payment>");
                    strXML = strXML.Append("</Payments>");
                    strXML = strXML.Append("</Transaction>");
                    strXML = strXML.Append("</Transactions>");
                    strXML = strXML.Append("</TransactionData>");
                    //code comment for prevent post data to the webservioce
                    WebReference.T3DIALService.Service s1 = new WebReference.T3DIALService.Service();
                    strXMLResponse = s1.SaveTransaction(strXML.ToString());
                    DataSet dt = new DataSet();
                    StringReader stream = new StringReader(strXMLResponse);
                    XmlTextReader reader = new XmlTextReader(stream);
                    dt.ReadXml(reader);
                    booking = dt.Tables[1].Rows[0]["Number"].ToString();
                    ReceiptNO = "";
                    StatusResponse = dt.Tables[1].Rows[0]["Status"].ToString();
                    if (StatusResponse == "Success")
                    {
                        ReceiptNO = "Success";
                        objAdmin.UpdateRetailStatus(Convert.ToInt32(booking), ReceiptNO, StatusResponse);
                        ClsAdmin.flagComplete = true;
                        PopUpReport();
                    }
                    else
                    {
                        ErrorCode = dt.Tables[1].Rows[0]["ErrorCode"].ToString();
                        ErrorMsg = dt.Tables[1].Rows[0]["ErrorMsg"].ToString();
                        StatusResponse = "Status : " + StatusResponse + ", Error Code : " + ErrorCode + "-Message: " + ErrorMsg;
                        objAdmin.UpdateRetailStatus(Convert.ToInt32(i), ReceiptNO, StatusResponse);
                        lblmessage.Text = StatusResponse; //"Error!!!";
                    }
                    //End Comment
                }
                catch (Exception ex)
                {
                    lblmessage.Text = "No Response!!!";
                }
            }
            else
            {
                ClsAdmin.flagComplete = true;
                PopUpReport();
            }

            //End of comment for testing purpose
            //Response.Redirect("RetailInvoicePrint.aspx?BookingID=" + i.ToString(), true);
        }
        else
        {
            lblmessage.Text = "Error!!!";
        }

        if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 215
            || Convert.ToInt32(ddlPkpCity.SelectedValue) == 229)
        {
            ddlPkpCity.ClearSelection();
        }

        Btsave.Enabled = false;
        btReset.Enabled = true;
        btnRePrint.Enabled = true;
    }
    protected void btReset_Click(object sender, EventArgs e)
    {
        //ClsAdmin.flagComplete = false;
        Session["StatusID"] = "0";
        Btsave.Enabled = true;
        btReset.Enabled = false;
        btnRePrint.Enabled = false;
        ddlPayMode.ClearSelection();
        //Reset();
        txtSearchCar.Text = "";
        txtAdjCost.Text = "";
        txtGuestMobile.Text = "";
        txtEmailID.Text = "";
        txtGuestName.Text = "";
        txtRemarks.Text = "";
        ddlSelectCar.Items.Clear();
        ddlSelectChauffeur.Items.Clear();
        ddlPkg.Items.Clear();
        lblBasicAmt.Text = "";
        txtSearchMobile.Text = "";
        hdnGuestID.Value = "0";
        txtGuestName.ReadOnly = false;
        txtGuestMobile.ReadOnly = false;
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";

        if (Convert.ToInt32(ddlPkpCity.SelectedValue) == 26
                || Convert.ToInt32(ddlPkpCity.SelectedValue) == 215)
        {
            if (txtToll.Text == "")
            {
                txtToll.Text = "110";
            }
        }
        else
        {
            txtToll.Text = ""; //rahul
        }
        ddlRentalType.SelectedValue = "0";
        txtLegalName.Text = "";
        txtGstNo.Text = "";
    }

    protected void txtToll_TextChanged(object sender, EventArgs e)
    {
        //txtAdjCost_TextChanged(sender, e);

        if (GSTEnabledYN == true)
        {
            ttlTax = CGSTTaxPercent + SGSTTaxPercent + IGSTTaxPercent;
        }
        else
        {
            ttlTax = ServiceTax + SwachhBharatTax + KrishiKalyanTax + (ServiceTax * EduTax) / 100 + (ServiceTax * HduTax) / 100;
        }

        hdtax.Value = Convert.ToDecimal(ttlTax).ToString();

        if (string.IsNullOrEmpty(txtToll.Text))
        {
            txtToll.Text = "0";
        }

        TollAmt = Convert.ToDecimal(txtToll.Text);

        decimal tollamtlimit = 0;
        if (ddlPkpCity.SelectedValue == "26" || ddlPkpCity.SelectedValue == "215")
        {
            tollamtlimit = 2000;
        }
        else
        {
            tollamtlimit = 250;
        }

        if (TollAmt > tollamtlimit)
        {
            message1.Text = "Toll Amount cannot be greater than " + tollamtlimit + "..";
            Page.RegisterClientScriptBlock("Script", "<script>alert('Toll Amount cannot be greater than " + tollamtlimit + "..')</script>");
            txtToll.Focus();
            return;
        }
        else if (TollAmt < 0)
        {
            message1.Text = "Toll Amount cannot be less than 0..";
            Page.RegisterClientScriptBlock("Script", "<script>alert('Toll Amount cannot be less than 0..')</script>");
            txtToll.Focus();
            return;
        }
        else
        {
            message1.Text = "";
        }

        //FinalAmt = Convert.ToDecimal(txtAdjCost.Text);
        BasicAmt = Convert.ToDecimal(lblBasicAmt.Text);

        TaxAmt = System.Math.Round(((BasicAmt + TollAmt) * (ttlTax)) / 100);

        ViewState["CGSTTaxAmt"] = ((BasicAmt + TollAmt) * CGSTTaxPercent) / 100;
        ViewState["SGSTTaxAmt"] = ((BasicAmt + TollAmt) * SGSTTaxPercent) / 100;
        ViewState["IGSTTaxAmt"] = ((BasicAmt + TollAmt) * IGSTTaxPercent) / 100;


        txtAdjCost.Text = System.Math.Round(BasicAmt + TaxAmt + TollAmt).ToString();
        FinalAmt = Convert.ToDecimal(FinalAmt);
        //TaxAmt = FinalAmt - BasicAmt;
        //lblBasicAmt.Text = (BasicAmt - TollAmt).ToString();

        if (hdndiscount.Value != "")
        {
            if (Convert.ToDecimal(hdndiscount.Value) > 0)
            {
                hdndiscountAmt.Value = Convert.ToString(Math.Round((FinalAmt * Convert.ToDecimal(hdndiscount.Value)) / 100, 2));  //discount code
                lbldiscountAmt.Text = hdndiscountAmt.Value;
                lbldiscountdesc.Text = hdndiscountstring.Value;
                lblNetBilling.Text = "Net Billing Cost";
                lblNetBillingAmt.Text = Convert.ToString(Math.Round(FinalAmt - Convert.ToDecimal(lbldiscountAmt.Text), 0));
            }
            else
            {
                hdndiscountAmt.Value = "0";
                lbldiscountAmt.Text = "";
                lbldiscountdesc.Text = "";
                lblNetBilling.Text = "";
                lblNetBillingAmt.Text = "";
            }
        }
        else
        {
            hdndiscountAmt.Value = "0";
            lbldiscountAmt.Text = "";
            lbldiscountdesc.Text = "";
            lblNetBilling.Text = "";
            lblNetBillingAmt.Text = "";
        }

        if (!string.IsNullOrEmpty(ddlPkg.SelectedValue))
        {
            decimal gSTSurchargeAmount = objAdmin.GetGSTSurchargesAmount(Convert.ToInt32(ddlPkg.SelectedValue));
            decimal gSTSurchargeAmountWithTax = 0;
            if (gSTSurchargeAmount > 0)
            {
                gSTSurchargeAmountWithTax = (gSTSurchargeAmount * (ttlTax + 100)) / 100;
            }
            if (FinalAmt < gSTSurchargeAmountWithTax)
            {
                message1.Text = "The total amount cannot be lower than Rs. " + Convert.ToString(gSTSurchargeAmountWithTax);
                Page.RegisterClientScriptBlock("Script", "<script>alert('Total cost should not be less than='" + Convert.ToString(gSTSurchargeAmountWithTax) + "')</script>");
                return;
            }
            else
            {
                message1.Text = string.Empty;
            }
        }
    }

    protected void txtAdjCost_TextChanged(object sender, EventArgs e)
    {
        if (GSTEnabledYN == true)
        {
            ttlTax = CGSTTaxPercent + SGSTTaxPercent + IGSTTaxPercent;
        }
        else
        {
            ttlTax = ServiceTax + SwachhBharatTax + KrishiKalyanTax + (ServiceTax * EduTax) / 100 + (ServiceTax * HduTax) / 100;
        }

        hdtax.Value = Convert.ToDecimal(ttlTax).ToString();

        if (string.IsNullOrEmpty(txtToll.Text))
        {
            txtToll.Text = "0";
        }

        TollAmt = Convert.ToDecimal(txtToll.Text);

        FinalAmt = Convert.ToDecimal(txtAdjCost.Text);

        BasicAmt = System.Math.Round(FinalAmt / (ttlTax + 100) * 100);

        ViewState["CGSTTaxAmt"] = (BasicAmt * CGSTTaxPercent) / 100;
        ViewState["SGSTTaxAmt"] = (BasicAmt * SGSTTaxPercent) / 100;
        ViewState["IGSTTaxAmt"] = (BasicAmt * IGSTTaxPercent) / 100;

        TaxAmt = FinalAmt - BasicAmt;
        BasicAmt = BasicAmt - TollAmt;
        lblBasicAmt.Text = (BasicAmt).ToString();

        if (hdndiscount.Value != "")
        {
            if (Convert.ToDecimal(hdndiscount.Value) > 0)
            {
                hdndiscountAmt.Value = Convert.ToString(Math.Round((FinalAmt * Convert.ToDecimal(hdndiscount.Value)) / 100, 2));  //discount code
                lbldiscountAmt.Text = hdndiscountAmt.Value;
                lbldiscountdesc.Text = hdndiscountstring.Value;
                lblNetBilling.Text = "Net Billing Cost";
                lblNetBillingAmt.Text = Convert.ToString(Math.Round(FinalAmt - Convert.ToDecimal(lbldiscountAmt.Text), 0));
            }
            else
            {
                hdndiscountAmt.Value = "0";
                lbldiscountAmt.Text = "";
                lbldiscountdesc.Text = "";
                lblNetBilling.Text = "";
                lblNetBillingAmt.Text = "";
            }
        }
        else
        {
            hdndiscountAmt.Value = "0";
            lbldiscountAmt.Text = "";
            lbldiscountdesc.Text = "";
            lblNetBilling.Text = "";
            lblNetBillingAmt.Text = "";
        }

        if (!string.IsNullOrEmpty(ddlPkg.SelectedValue))
        {
            decimal gSTSurchargeAmount = objAdmin.GetGSTSurchargesAmount(Convert.ToInt32(ddlPkg.SelectedValue));
            decimal gSTSurchargeAmountWithTax = 0;
            if (gSTSurchargeAmount > 0)
            {
                gSTSurchargeAmountWithTax = (gSTSurchargeAmount * (ttlTax + 100)) / 100;
            }
            if (FinalAmt < gSTSurchargeAmountWithTax)
            {
                message1.Text = "The total amount cannot be lower than Rs. " + Convert.ToString(gSTSurchargeAmountWithTax);
                Page.RegisterClientScriptBlock("Script", "<script>alert('Total cost should not be less than='" + Convert.ToString(gSTSurchargeAmountWithTax) + "')</script>");
                return;
            }
            else
            {
                message1.Text = string.Empty;
            }
        }
    }
    protected void txtSearchMobile_TextChanged(object sender, EventArgs e)
    {
        SearchByMobile();
    }

    private void SearchByMobile()
    {
        message1.Text = "Please wait...";
        if (txtSearchMobile.Text != "")
        {
            if (ddlOptions.SelectedValue == "0")
            {
                DataTable dtGuestDetails = objAdmin.Search_GuestMobile(txtSearchMobile.Text);
                if (dtGuestDetails.Rows.Count > 0)
                {
                    hdnGuestID.Value = dtGuestDetails.Rows[0][0].ToString();
                    txtGuestName.Text = dtGuestDetails.Rows[0][1].ToString();
                    txtGuestMobile.Text = dtGuestDetails.Rows[0][2].ToString();
                    txtEmailID.Text = dtGuestDetails.Rows[0][3].ToString();
                    message1.Text = "";
                }
                else
                {
                    hdnGuestID.Value = "0";
                    txtGuestName.Text = "";
                    txtGuestMobile.Text = "";
                    txtEmailID.Text = "";
                    message1.Text = "No Guest found!";
                }
            }
            else
            {
                int tmpPkpCityID, tmpCompID;
                string tmpSearch;

                tmpPkpCityID = Convert.ToInt32(ddlPkpCity.SelectedValue);
                tmpCompID = Convert.ToInt32(ddlCompanyName.SelectedValue);
                tmpSearch = txtSearchMobile.Text.ToString();

                DataTable dtGuestDetails = objAdmin.Search_ContBooking(tmpPkpCityID, tmpCompID, tmpSearch);
                if (dtGuestDetails.Rows.Count > 0)
                {
                    hdnGuestID.Value = dtGuestDetails.Rows[0][0].ToString();
                    txtGuestName.Text = dtGuestDetails.Rows[0][1].ToString();
                    txtGuestMobile.Text = dtGuestDetails.Rows[0][2].ToString();
                    txtEmailID.Text = dtGuestDetails.Rows[0][3].ToString();
                    message1.Text = "";
                }
                else
                {
                    hdnGuestID.Value = "0";
                    txtGuestName.Text = "";
                    txtGuestMobile.Text = "";
                    txtEmailID.Text = "";
                    txtSearchMobile.Text = "";
                    message1.Text = "This booking does not exists for this Location / Company!";
                }
            }
            if (Convert.ToInt32(ddlPkpCity.SelectedItem.Value) > 0)
            //&& Convert.ToInt32(ddlPkpCity.SelectedValue) == 26)
            {
                GetLastTransactions();
            }
            else
            {
                gridData.DataSource = null;
                gridData.DataBind();
                divTran.Visible = false;
            }
        }
        else
        {
            hdnGuestID.Value = "0";
            txtGuestName.Text = "";
            txtGuestMobile.Text = "";
            txtEmailID.Text = "";
            message1.Text = "";
        }
        ReadOnlyObjects();
    }

    protected void btnRePrint_Click(object sender, EventArgs e)
    {
        PopUpReport();
    }

    private OL_TaxHeads TaxCalculation(int clientId, int pickupcityId, string datein, int carid, int subsidiaryId)
    {
        DataSet ds = new DataSet();
        OL_TaxHeads objhead = new OL_TaxHeads();

        SendSMSService.TaxDetails objtax = SendSMSMailService.GetTaxHead(clientId, pickupcityId, datein, carid, subsidiaryId);
        if (objtax != null)
        {
            objhead.CGSTPercent = Convert.ToDouble(objtax.CGSTPercent);
            objhead.SGSTPercent = Convert.ToDouble(objtax.SGSTPercent);
            objhead.IGSTPercent = Convert.ToDouble(objtax.IGSTPercent);
            objhead.GSTEnabledYN = Convert.ToBoolean(objtax.GSTEnabledYN);
            objhead.ServiceTaxPercent = Convert.ToDouble(objtax.ServiceTaxPercent);
            objhead.EduCessPercent = Convert.ToDouble(objtax.EduCessPercent);
            objhead.HduCessPercent = Convert.ToDouble(objtax.HduCessPercent);
            objhead.KrishiKalyanTaxPercent = Convert.ToDouble(objtax.KrishiKalyanTaxPercent);
            objhead.SwachhBharatTaxPercent = Convert.ToDouble(objtax.SwachhBharatTaxPercent);
            objhead.KMWisePackage = Convert.ToBoolean(objtax.KMWisePackage);
            objhead.ClientGSTId = objtax.ClientGSTId == "" ? 0 : Convert.ToInt32(objtax.ClientGSTId);
        }
        return objhead;
    }

    public static string Base64Encode(string plainText)
    {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }
}