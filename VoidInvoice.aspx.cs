using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using RetailModule;
using System.Net;
using ccs;
using System.Xml;
using System.IO;

public partial class VoidInvoice : System.Web.UI.Page
{
    ClsAdmin objAdmin = new ClsAdmin();
    DataSet ds = new DataSet();
    public decimal ServiceTax = 0, EduTax = 0, HduTax = 0, ttlTax = 0, FinalAmt = 0, BasicAmt = 0, TaxAmt = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
        GetDetail.Attributes.Add("onclick", "return validateform();");
        //string POSID = dllPOSNO.SelectedValue.ToString();
        //if (POSID != "")
        //{
        //    hdnpos.Value = "";
        //}
    }
    protected void GetDetail_Click(object sender, EventArgs e)
    {
        string POSID = dllPOSNO.SelectedValue.ToString();
        string fromDate = txtFromDate.Value;
        string toDate = txtToDate.Value;

        ds = objAdmin.GetVoidDetail(fromDate, toDate, POSID);
        GridView1.DataSource = ds;
        GridView1.DataBind();
        
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string bookingid;
        DataSet GetBookingDetail = new DataSet();
        //string strXML = "";
        StringBuilder strXML = new StringBuilder();
        string strXMLResponse = "";
        string booking = "", ReceiptNO = "", StatusResponse = "", ErrorCode = "", ErrorMsg = "";
        
        if (e.CommandName == "Send")
        {
            bookingid = e.CommandArgument.ToString();
            try
            {
                GetBookingDetail = objAdmin.GetvoidDetailBooking(Convert.ToInt32(bookingid));
                if (Convert.ToInt32(GetBookingDetail.Tables[0].Rows[0]["PickupCityId"].ToString()) == 33)
                {
                    int paymentType = 0;
                    if (GetBookingDetail.Tables[0].Rows[0]["PaymentMode"].ToString() == "0")
                    {
                        paymentType = 1;
                    }
                    else
                    {
                        paymentType = 3;
                    }
                    ServiceTax = Convert.ToDecimal(GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString());
                    EduTax = Convert.ToDecimal(GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString());
                    HduTax = Convert.ToDecimal(GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString());
                    ttlTax = ServiceTax + (ServiceTax * EduTax) / 100 + (ServiceTax * HduTax) / 100;
                    FinalAmt = Convert.ToDecimal(GetBookingDetail.Tables[0].Rows[0]["TotalAmount"].ToString().Trim());
                    BasicAmt = System.Math.Round(FinalAmt / (ttlTax + 100) * 100);
                    TaxAmt = FinalAmt - BasicAmt;

                    strXML = strXML.Append("<?xml version='1.0' encoding='UTF-8' ?>");
                    //strXML = strXML.Append("<TransactionData xsi:noNamespaceSchemaLocation='http://125.63.89.188/_URL_/Carzonrent.xsd' ");//This is for test server
                    strXML = strXML.Append("<TransactionData xsi:noNamespaceSchemaLocation='http://111.93.35.93/_URL_/Carzonrent.xsd' "); //This is for Live Server
                    strXML = strXML.Append("xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");
                    //strXML = strXML.Append("<ServicePartnerNo>" + "2000000003" + "</ServicePartnerNo>");//This is for test server
                    strXML = strXML.Append("<ServicePartnerNo>" + "1000000583" + "</ServicePartnerNo>");//This is for Live server
                    strXML = strXML.Append("<Password>Crzornt</Password>");
                    strXML = strXML.Append("<Transactions> <Transaction>");
                    strXML = strXML.Append("<TransactionNo>" + GetBookingDetail.Tables[0].Rows[0]["NewVoidID"].ToString().Trim() + "</TransactionNo>");
                    strXML = strXML.Append("<OriginalRefNo>" + GetBookingDetail.Tables[0].Rows[0]["BookingID"].ToString().Trim() + "</OriginalRefNo>");
                    strXML = strXML.Append("<EntryType>" + 2 + "</EntryType>");
                    strXML = strXML.Append("<StoreNo>" + "CZ012" + "</StoreNo>");
                    strXML = strXML.Append("<POSNo>" + "OR101" + "</POSNo>");
                    strXML = strXML.Append("<StaffID>" + Convert.ToInt32(Session["UserID"]).ToString() + "</StaffID>");
                    strXML = strXML.Append("<StaffName>" + GetBookingDetail.Tables[0].Rows[0]["StaffName"].ToString().Trim() + "</StaffName>");
                    strXML = strXML.Append("<TransactionDate>" + GetBookingDetail.Tables[0].Rows[0]["AccountingDate"].ToString().Trim() + "</TransactionDate>");
                    strXML = strXML.Append("<TransactionTime>" + GetBookingDetail.Tables[0].Rows[0]["AccountingTime"].ToString().Trim() + "</TransactionTime>");
                    strXML = strXML.Append("<DiscountAmount>" + 00.00 + "</DiscountAmount>");
                    strXML = strXML.Append("<TotalDiscount>" + 00.00 + "</TotalDiscount>");
                    strXML = strXML.Append("<TableNo>0</TableNo>");
                    strXML = strXML.Append("<NoOfCovers>0</NoOfCovers>");
                    strXML = strXML.Append("<CustomerName>" + GetBookingDetail.Tables[0].Rows[0]["CustomerName"].ToString().Trim() + "</CustomerName>");
                    strXML = strXML.Append("<Address></Address>");
                    strXML = strXML.Append("<Gender></Gender>");
                    strXML = strXML.Append("<PassportNo></PassportNo>");
                    strXML = strXML.Append("<Nationality>" + "IN" + "</Nationality>");
                    strXML = strXML.Append("<PortofBoarding></PortofBoarding>");
                    strXML = strXML.Append("<PortofDisembarkation></PortofDisembarkation>");
                    strXML = strXML.Append("<FlightNo></FlightNo>");
                    strXML = strXML.Append("<SectorNo></SectorNo>");
                    strXML = strXML.Append("<BoardingPassNo></BoardingPassNo>");
                    strXML = strXML.Append("<SeatNo></SeatNo>");
                    strXML = strXML.Append("<Airlines></Airlines>");
                    strXML = strXML.Append("<ServiceChargeAmount>" + 0 + "</ServiceChargeAmount>");
                    strXML = strXML.Append("<NetTransactionAmount>" + Convert.ToDouble(BasicAmt) + "</NetTransactionAmount>");
                    strXML = strXML.Append("<GrossTransactionAmount>" + GetBookingDetail.Tables[0].Rows[0]["TotalAmount"].ToString().Trim() + "</GrossTransactionAmount>");
                    strXML = strXML.Append("<CustomerType>" + 0 + "</CustomerType>");
                    strXML = strXML.Append("<Items>");
                    strXML = strXML.Append("<Item>");
                    strXML = strXML.Append("<ItemCode>" + "Cab" + "</ItemCode>");
                    //strXML = strXML.Append("<ItemDescription>" + ddlPkpCity.SelectedItem.Text + "</ItemDescription>");
                    strXML = strXML.Append("<ItemDescription>" + "Delhi" + "</ItemDescription>");

                    strXML = strXML.Append("<ItemCategory>" + "Cab" + "</ItemCategory>");
                    strXML = strXML.Append("<ItemCategoryDescription>" + "Special Cab" + "</ItemCategoryDescription>");
                    strXML = strXML.Append("<ProductGroup>" + "Cab" + "</ProductGroup>");
                    strXML = strXML.Append("<ProductGroupDescription>" + GetBookingDetail.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim() + "</ProductGroupDescription>");
                    strXML = strXML.Append("<BarcodeNo >0</BarcodeNo>");
                    strXML = strXML.Append("<Quantity>" + 1 + "</Quantity>");
                    strXML = strXML.Append("<Price>" + Convert.ToDouble(BasicAmt) + "</Price>");
                    strXML = strXML.Append("<NetAmount>" + Convert.ToDouble(BasicAmt) + "</NetAmount>");
                    //strXML = strXML.Append("<PriceInclusiveTax>" + txtAdjCost.Text + "</PriceInclusiveTax>");
                    strXML = strXML.Append("<PriceInclusiveTax>" + 1 + "</PriceInclusiveTax>");
                    //strXML = strXML.Append("<ChangedPrice>" + txtAdjCost.Text + "</ChangedPrice>");
                    strXML = strXML.Append("<ChangedPrice>" + 1 + "</ChangedPrice>");
                    strXML = strXML.Append("<ScaleItem>" + 1 + "</ScaleItem>");
                    strXML = strXML.Append("<WeighingItem>" + 1 + "</WeighingItem>");
                    strXML = strXML.Append("<ItemSerialNo></ItemSerialNo>");
                    strXML = strXML.Append("<UOM>" + "Pics" + "</UOM>");
                    strXML = strXML.Append("<LineDiscount>" + 0 + "</LineDiscount>");
                    strXML = strXML.Append("<TotalDiscount>" + 0 + "</TotalDiscount>");
                    strXML = strXML.Append("<PeriodicDiscount>" + 0 + "</PeriodicDiscount>");
                    strXML = strXML.Append("<PromotionNo></PromotionNo>");
                    strXML = strXML.Append("<TaxAmount>" + Convert.ToDouble(00.00) + "</TaxAmount>");
                    strXML = strXML.Append("<TaxRate>" + Convert.ToDouble(00.00) + "</TaxRate>");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");
                    //strXML = strXML.Append("<ServiceTaxAmount>0</ServiceTaxAmount> ");
                    //strXML = strXML.Append("<ServiceTaxRate>0</ServiceTaxRate> ");
                    //strXML = strXML.Append("<ServiceTaxeCessRate>0</ServiceTaxeCessRate> ");
                    //strXML = strXML.Append("<ServiceTaxeCessAmount>0</ServiceTaxeCessAmount> ");
                    //strXML = strXML.Append("<ServiceTaxSHECessRate>0</ServiceTaxSHECessRate> ");
                    //strXML = strXML.Append("<ServiceTaxSHECessAmount>0</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("</Item> ");
                    strXML = strXML.Append("</Items>");
                    strXML = strXML.Append("<ServiceCharges>");
                    strXML = strXML.Append("<Charge> ");
                    strXML = strXML.Append("<Type>" + 1 + "</Type> ");
                    strXML = strXML.Append("<TypeDescription>" + "Service Charges" + "</TypeDescription> ");
                    //strXML = strXML.Append("<ServiceChargeAmount>" + Convert.ToDouble(00.00) + "</ServiceChargeAmount>");
                    //strXML = strXML.Append("<ServiceTaxAmount>" + Convert.ToDouble(00.00) + "</ServiceTaxAmount> ");
                    //strXML = strXML.Append("<ServiceTaxRate>" + Convert.ToDouble(00.00) + "</ServiceTaxRate> ");
                    //strXML = strXML.Append("<ServiceTaxeCessRate>" + Convert.ToDouble(00.00) + "</ServiceTaxeCessRate> ");
                    //strXML = strXML.Append("<ServiceTaxeCessAmount>" + Convert.ToDouble(00.00) + "</ServiceTaxeCessAmount> ");
                    //strXML = strXML.Append("<ServiceTaxSHECessRate>" + Convert.ToDouble(00.00) + "</ServiceTaxSHECessRate> ");
                    //strXML = strXML.Append("<ServiceTaxSHECessAmount>" + Convert.ToDouble(00.00) + "</ServiceTaxSHECessAmount> ");

                    strXML = strXML.Append("<ServiceChargeAmount>0</ServiceChargeAmount>");
                    //strXML = strXML.Append("<ServiceTaxAmount>0</ServiceTaxAmount> ");
                    //strXML = strXML.Append("<ServiceTaxRate>0</ServiceTaxRate> ");
                    //strXML = strXML.Append("<ServiceTaxeCessRate>0</ServiceTaxeCessRate> ");
                    //strXML = strXML.Append("<ServiceTaxeCessAmount>0</ServiceTaxeCessAmount> ");
                    //strXML = strXML.Append("<ServiceTaxSHECessRate>0</ServiceTaxSHECessRate> ");
                    //strXML = strXML.Append("<ServiceTaxSHECessAmount>0</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");

                    strXML = strXML.Append("</Charge> ");
                    strXML = strXML.Append("</ServiceCharges>");
                    strXML = strXML.Append("<Payments> ");
                    strXML = strXML.Append("<Payment> ");
                    strXML = strXML.Append("<TenderType>" + paymentType + "</TenderType> ");
                    strXML = strXML.Append("<CardNo></CardNo> ");
                    strXML = strXML.Append("<CurrencyCode>" + "INR" + "</CurrencyCode> ");
                    strXML = strXML.Append("<ExchangeRate>" + 1 + "</ExchangeRate> ");
                    strXML = strXML.Append("<AmountTendered>" + GetBookingDetail.Tables[0].Rows[0]["TotalAmount"].ToString().Trim() + "</AmountTendered> ");
                    strXML = strXML.Append("<AmountInCurrency>" + Convert.ToDouble(00.00) + "</AmountInCurrency> ");
                    strXML = strXML.Append("</Payment>");
                    strXML = strXML.Append("</Payments>");
                    strXML = strXML.Append("</Transaction>");
                    strXML = strXML.Append("</Transactions>");
                    strXML = strXML.Append("</TransactionData>");
                    //WebReference.Service1 S1 = new WebReference.Service1();
                    //strXMLResponse = S1.InsertTransactions(strXML);
                    WebReference.DIALService s1 = new WebReference.DIALService();
                    strXMLResponse = s1.SaveTransaction(strXML.ToString());
                    DataSet dt = new DataSet();
                    StringReader stream = new StringReader(strXMLResponse);
                    XmlTextReader reader = new XmlTextReader(stream);
                    dt.ReadXml(reader);

                    booking = dt.Tables[1].Rows[0]["Number"].ToString();
                    //ReceiptNO = dt.Tables[1].Rows[0]["ReceiptNo"].ToString();
                    ReceiptNO = "";
                    StatusResponse = dt.Tables[1].Rows[0]["Status"].ToString();
                    if (StatusResponse == "Success")
                    {
                        ReceiptNO = "Void Success";
                        objAdmin.UpdatevoidStatus(bookingid, ReceiptNO, StatusResponse);
                        ClsAdmin.flagComplete = true;
                        lblMessage.Text = "'Booking void successfully!'";
                    }
                    else
                    {

                        ErrorCode = dt.Tables[1].Rows[0]["ErrorCode"].ToString();
                        ErrorMsg = dt.Tables[1].Rows[0]["ErrorMsg"].ToString();
                        StatusResponse = "Status : " + StatusResponse + ", Error Code : " + ErrorCode + ", Erroe Message :" + ErrorMsg;
                        lblMessage.Text = StatusResponse;
                        //Page.RegisterClientScriptBlock("Script", "<script>alert(' " + StatusResponse + "')</script>");
                    }
                    string POSID = dllPOSNO.SelectedValue.ToString();
                    string fromDate = txtFromDate.Value;
                    string toDate = txtToDate.Value;

                    ////ds = objAdmin.GetVoidDetail(fromDate, toDate, POSID);
                    ds = objAdmin.GetVoidDetail_New(fromDate, toDate, POSID);
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
            }
            catch//(Exception ex)
            {
                Page.RegisterClientScriptBlock("Script", "<script>alert('No Response!!!')</script>");
            }
        }
    }
}