using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ccs;

public partial class HandOver_Detail : clsPageAuthorization
{
    ClsAdmin objAdmin = new ClsAdmin();
    DataSet DS = new DataSet();
    DataSet DSNew = new DataSet();
    DataSet GetSummary = new DataSet();
    static DataTable GridViewDT = new DataTable();
    static DataTable GridViewDTNew = new DataTable();
    double CashDeposited, AmexSettlement, HDFCSettlement, EWalletSettlement;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int BookingID;
            BookingID = 0;
            DS = objAdmin.Handover_AddVoidBooking(BookingID);
            GridView1.DataSource = DS;
            GridView1.DataBind();
            GridViewDT = DS.Tables[0];
            txtBookingID.Focus();
            DSNew = objAdmin.Handover_AddVoidBooking(BookingID);
            GridViewNew.DataSource = DSNew;
            GridViewNew.DataBind();
            GridViewDTNew = DSNew.Tables[0];

            BindUser();
            getDetail();
            Handover.Attributes.Add("onclick", "return validateForm();");

            txtBookingID.Enabled = true;
            GridView1.Enabled = true;
            lblCashDeposited.Enabled = true;
            lblAmexSettlement.Enabled = true;
            lblHDFCSettlement.Enabled = true;
            lblEWalletSettlement.Enabled = true;
            Reliver.Enabled = true;
            Handover.Enabled = true;
            btnBookingId.Enabled = true;
        }
    }

    protected void getDetail()
    {
        GetSummary = objAdmin.Get_Detail(Convert.ToInt32(Session["UserID"]));
        for (int i = 0; i < GetSummary.Tables[0].Rows.Count; i++)
        {
            lblCashbal.Text = GetSummary.Tables[0].Rows[i]["Opening_Cash"].ToString();
            lblAmexbal.Text = GetSummary.Tables[0].Rows[i]["Opening_Amex"].ToString();
            lblHDFCbal.Text = GetSummary.Tables[0].Rows[i]["Opening_HDFC"].ToString();
            lblTotbal.Text = (Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Opening_Cash"])
            + Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Opening_Amex"])
            + Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Opening_HDFC"])).ToString();
            lblEWalletbal.Text = GetSummary.Tables[0].Rows[i]["Opening_EWallet"].ToString();


            lblCashSale.Text = GetSummary.Tables[0].Rows[i]["Current_Cash_Bal"].ToString();
            lblAmexSale.Text = GetSummary.Tables[0].Rows[i]["Current_Amex_Bal"].ToString();
            lblHDFCSale.Text = GetSummary.Tables[0].Rows[i]["Current_HDFC_Bal"].ToString();
            lblTotSale.Text = (Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Current_Cash_Bal"])
            + Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Current_Amex_Bal"])
            + Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Current_HDFC_Bal"])).ToString();
            lblEWalletSale.Text = GetSummary.Tables[0].Rows[i]["Current_EWallet_Bal"].ToString();

            lblCashRejected.Text = "0"; //***********************
            lblAmexRejected.Text = "0"; //***********************
            lblHDFCRejected.Text = "0"; //***********************
            lblTotlRejected.Text = "0"; //***********************
            lblEWalletRejected.Text = "0"; //***********************

            lblCashTotal.Text = (Convert.ToDouble(GetSummary.Tables[0].Rows[i][1]) +
            Convert.ToDouble(GetSummary.Tables[0].Rows[i][4])).ToString();
            lblAmexTotal.Text = (Convert.ToDouble(GetSummary.Tables[0].Rows[i][2]) +
            Convert.ToDouble(GetSummary.Tables[0].Rows[i][5])).ToString();
            lblHDFCTotal.Text = (Convert.ToDouble(GetSummary.Tables[0].Rows[i][3]) +
            Convert.ToDouble(GetSummary.Tables[0].Rows[i][6])).ToString();
            lblTotlTotal.Text = (Convert.ToDouble(lblTotbal.Text) + Convert.ToDouble(lblTotSale.Text)).ToString();

            lblEWalletTotal.Text = (Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Opening_EWallet"]) +
            Convert.ToDouble(GetSummary.Tables[0].Rows[i]["Current_EWallet_Bal"])).ToString();

            lblCashTotalVoidable.Text = "0";
            lblAmexTotalVoidable.Text = "0";
            lblHDFCTotalVoidable.Text = "0";
            lblEWalletTotalVoidable.Text = "0";

            lblCashTotalHotel.Text = "0";
            lblAmexTotalHotel.Text = "0";
            lblHDFCTotalHotel.Text = "0";
            lblEWalletTotalHotel.Text = "0";

            lblUserName.Text = GetSummary.Tables[0].Rows[i][8].ToString();
            lblLocation.Text = GetSummary.Tables[0].Rows[i][9].ToString();

            ViewState["BatchID"] = Convert.ToInt32(GetSummary.Tables[0].Rows[i][10]);
            lblCashDeposited.Text = GetSummary.Tables[0].Rows[i][11].ToString();
            lblAmexSettlement.Text = GetSummary.Tables[0].Rows[i][12].ToString();
            lblHDFCSettlement.Text = GetSummary.Tables[0].Rows[i][13].ToString();
            lblEWalletSettlement.Text = GetSummary.Tables[0].Rows[i]["Settlement_EWallet"].ToString();

            if ((Reliver.Items.FindByValue(GetSummary.Tables[0].Rows[i][14].ToString()) != null) && (GetSummary.Tables[0].Rows[i][14].ToString() != "0"))
            {
                Reliver.Items.FindByValue(GetSummary.Tables[0].Rows[i][14].ToString()).Selected = true;
            }

            DataSet ds1 = new DataSet();
            ds1 = objAdmin.GetDetail_Voidable(Convert.ToInt32(ViewState["BatchID"]));
            if (ds1.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds1;
                GridView1.DataBind();

                foreach (DataRow DtRow in ds1.Tables[0].Rows)
                {
                    GridViewDT.ImportRow(DtRow);
                    if (DtRow["Mode"].ToString() == "CASH")
                    {
                        lblCashTotalVoidable.Text = (Convert.ToDouble(lblCashTotalVoidable.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                    if (DtRow["Mode"].ToString() == "AMEX")
                    {
                        lblAmexTotalVoidable.Text = (Convert.ToDouble(lblAmexTotalVoidable.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                    if (DtRow["Mode"].ToString() == "HDFC")
                    {
                        lblHDFCTotalVoidable.Text = (Convert.ToDouble(lblHDFCTotalVoidable.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                    if (DtRow["Mode"].ToString() == "EWallet")
                    {
                        lblHDFCTotalVoidable.Text = (Convert.ToDouble(lblEWalletTotalVoidable.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                }
            }


            //*********************************************************************************
            DataSet ds2 = new DataSet();
            ds2 = objAdmin.GetDetail_Hotel(Convert.ToInt32(ViewState["BatchID"]));
            if (ds2.Tables[0].Rows.Count > 0)
            {
                GridViewNew.DataSource = ds2;
                GridViewNew.DataBind();

                foreach (DataRow DtRow in ds2.Tables[0].Rows)
                {
                    GridViewDTNew.ImportRow(DtRow);
                    if (DtRow["Mode"].ToString() == "CASH")
                    {
                        lblCashTotalHotel.Text = (Convert.ToDouble(lblCashTotalHotel.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                    if (DtRow["Mode"].ToString() == "AMEX")
                    {
                        lblAmexTotalHotel.Text = (Convert.ToDouble(lblAmexTotalHotel.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                    if (DtRow["Mode"].ToString() == "HDFC")
                    {
                        lblHDFCTotalHotel.Text = (Convert.ToDouble(lblHDFCTotalHotel.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                    if (DtRow["Mode"].ToString() == "EWallet")
                    {
                        lblHDFCTotalHotel.Text = (Convert.ToDouble(lblEWalletTotalHotel.Text)
                            + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                    }
                }
            }
            //*********************************************************************************

            lblDate.Text = DateTime.Now.ToShortDateString();

            CashDeposited = Convert.ToDouble(lblCashDeposited.Text.Trim() == "" ? "0" : lblCashDeposited.Text);
            AmexSettlement = Convert.ToDouble(lblAmexSettlement.Text.Trim() == "" ? "0" : lblAmexSettlement.Text);
            HDFCSettlement = Convert.ToDouble(lblHDFCSettlement.Text.Trim() == "" ? "0" : lblHDFCSettlement.Text);
            EWalletSettlement = Convert.ToDouble(lblEWalletSettlement.Text.Trim() == "" ? "0" : lblEWalletSettlement.Text);

            CashHandOver.Text = (Convert.ToDouble(lblCashTotal.Text)
            - Convert.ToDouble(lblCashTotalVoidable.Text) - Convert.ToDouble(CashDeposited)
            - Convert.ToDouble(lblCashTotalHotel.Text)).ToString();
            AmexHandOver.Text = (Convert.ToDouble(lblAmexTotal.Text)
            - Convert.ToDouble(lblAmexTotalVoidable.Text) - Convert.ToDouble(AmexSettlement)
            - Convert.ToDouble(lblAmexTotalHotel.Text)).ToString();
            HDFCHandOver.Text = (Convert.ToDouble(lblHDFCTotal.Text)
            - Convert.ToDouble(lblHDFCTotalVoidable.Text) - Convert.ToDouble(HDFCSettlement)
            - Convert.ToDouble(lblHDFCTotalHotel.Text)).ToString();

            EWalletHandOver.Text = (Convert.ToDouble(lblEWalletTotal.Text)
            - Convert.ToDouble(lblEWalletTotalVoidable.Text) - Convert.ToDouble(EWalletSettlement)
            - Convert.ToDouble(lblEWalletTotalHotel.Text)).ToString();
        }
    }

    protected void BindUser()
    {
        DataSet dscity = new DataSet();
        dscity = objAdmin.GetReliever(Convert.ToInt32(Session["UserID"]));
        Reliver.Items.Add(new ListItem("--Select--", ""));
        Reliver.AppendDataBoundItems = true;
        Reliver.DataSource = dscity;
        Reliver.DataTextField = "UserName";
        Reliver.DataValueField = "UserID";
        Reliver.DataBind();
    }

    protected void btnBookingId_Click(object sender, EventArgs e)
    {
        int BookingID;
        lblError.Visible = false;
        lblError.Text = "";

        int tmpRowCnt = GridView1.Rows.Count;
        string[] strReson = new string[GridView1.Rows.Count + 1];
        int iCtr = 0;

        foreach (GridViewRow gvr in GridView1.Rows)
        {
            TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
            if (txtReson.Text != "")
            {
                strReson[iCtr] = txtReson.Text;
            }
            else
            {
                strReson[iCtr] = "";
            }
            iCtr += 1;
        }

        BookingID = Convert.ToInt32(txtBookingID.Text.Trim() == "" ? "0" : txtBookingID.Text);

        if (BookingID.ToString() == "0" || BookingID.ToString() == "")
        {
            //Page.RegisterClientScriptBlock("Script", "<script>alert('Please Enter Booking ID')</script>");
            lblError.Visible = true;
            lblError.Text = "Please Enter Booking ID";
            txtBookingID.Focus();
            return;
        }

        foreach (DataRow DtRow in GridViewDT.Rows)
        {
            if (Convert.ToInt32(DtRow["BookingID"]) == BookingID)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('Booking ID already Entered')</script>");
                lblError.Visible = true;
                lblError.Text = "Booking ID already Entered";
                txtBookingID.Focus();
                return;
            }
        }

        DS = objAdmin.Handover_AddVoidBooking(BookingID);
        txtBookingID.Text = "";
        if (DS.Tables[0].Columns.Count == 1)
        {
            if (Convert.ToInt32(DS.Tables[0].Rows[0][0].ToString()) == 1)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('the BookingID is already available for Void!')</script>");
                lblError.Visible = true;
                lblError.Text = "the BookingID is already available for Void!";
                return;
            }
            if (Convert.ToInt32(DS.Tables[0].Rows[0][0].ToString()) == 2)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('Booking ID is already voided!')</script>");
                lblError.Visible = true;
                lblError.Text = "Booking ID is already voided!";
                return;
            }
            if (Convert.ToInt32(DS.Tables[0].Rows[0][0].ToString()) == 3)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('the BookingID was available for Void, but rejected!')</script>");
                lblError.Visible = true;
                lblError.Text = "the BookingID was available for Void, but rejected!";
                return;
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "This BookingID has some issue!";
                //Page.RegisterClientScriptBlock("Script", "<script>alert('This BookingID has some issue!')</script>");
                return;
            }
        }
        else
        {
            foreach (DataRow DtRow in DS.Tables[0].Rows)
            {
                GridViewDT.ImportRow(DtRow);
                if (DtRow["Mode"].ToString() == "CASH")
                {
                    lblCashTotalVoidable.Text = (Convert.ToDouble(lblCashTotalVoidable.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
                if (DtRow["Mode"].ToString() == "AMEX")
                {
                    lblAmexTotalVoidable.Text = (Convert.ToDouble(lblAmexTotalVoidable.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
                if (DtRow["Mode"].ToString() == "HDFC")
                {
                    lblHDFCTotalVoidable.Text = (Convert.ToDouble(lblHDFCTotalVoidable.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
                if (DtRow["Mode"].ToString() == "EWallet")
                {
                    lblHDFCTotalVoidable.Text = (Convert.ToDouble(lblEWalletTotalVoidable.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
            }
            if (GridViewDT.Rows.Count > 0)
            {
                GridView1.DataSource = GridViewDT;
                GridView1.DataBind();

                iCtr = 0;
                if (GridView1.Rows.Count > 1)
                {
                    iCtr = 0;
                    foreach (GridViewRow gvr in GridView1.Rows)
                    {
                        TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
                        if (strReson[iCtr] != "" && strReson[iCtr] != null)
                        {
                            txtReson.Text = strReson[iCtr].ToString();
                        }
                        else
                        {
                            txtReson.Text = "";
                        }
                        iCtr += 1;
                    }
                }
            }
            CashDeposited = Convert.ToDouble(lblCashDeposited.Text.Trim() == "" ? "0" : lblCashDeposited.Text);
            AmexSettlement = Convert.ToDouble(lblAmexSettlement.Text.Trim() == "" ? "0" : lblAmexSettlement.Text);
            HDFCSettlement = Convert.ToDouble(lblHDFCSettlement.Text.Trim() == "" ? "0" : lblHDFCSettlement.Text);
            EWalletSettlement = Convert.ToDouble(lblEWalletSettlement.Text.Trim() == "" ? "0" : lblEWalletSettlement.Text);

            CashHandOver.Text = (Convert.ToDouble(lblCashTotal.Text)
             - Convert.ToDouble(lblCashTotalVoidable.Text) - Convert.ToDouble(CashDeposited)).ToString();
            AmexHandOver.Text = (Convert.ToDouble(lblAmexTotal.Text)
             - Convert.ToDouble(lblAmexTotalVoidable.Text) - Convert.ToDouble(AmexSettlement)).ToString();
            HDFCHandOver.Text = (Convert.ToDouble(lblHDFCTotal.Text)
             - Convert.ToDouble(lblHDFCTotalVoidable.Text) - Convert.ToDouble(HDFCSettlement)).ToString();
            EWalletHandOver.Text = (Convert.ToDouble(lblEWalletTotal.Text)
             - Convert.ToDouble(lblEWalletTotalVoidable.Text) - Convert.ToDouble(EWalletSettlement)).ToString();
            txtBookingID.Focus();
        }
    }

    protected void Handover_Click(object sender, EventArgs e)
    {
        TextBox VoidReason;
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            VoidReason = (TextBox)GridView1.Rows[i].FindControl("Reasons");

            objAdmin.VoidableInvoices(Convert.ToInt32(ViewState["BatchID"]), Convert.ToInt32(GridView1.Rows[i].Cells[0].Text),
                GridView1.Rows[i].Cells[1].Text.ToString(), Convert.ToDecimal(GridView1.Rows[i].Cells[2].Text),
                VoidReason.Text, Convert.ToInt32(Session["Userid"]));
        }

        TextBox HotelReason;
        for (int i = 0; i < GridViewNew.Rows.Count; i++)
        {
            HotelReason = (TextBox)GridViewNew.Rows[i].FindControl("Reasons");

            objAdmin.HotelInvoices(Convert.ToInt32(ViewState["BatchID"]), Convert.ToInt32(GridViewNew.Rows[i].Cells[0].Text),
                GridViewNew.Rows[i].Cells[1].Text.ToString(), Convert.ToDecimal(GridViewNew.Rows[i].Cells[2].Text),
                HotelReason.Text, Convert.ToInt32(Session["Userid"]));
        }

        //CashDeposited = Convert.ToDouble(lblCashDeposited.Text.Trim() == "" ? "0" : lblCashDeposited.Text);
        //AmexSettlement = Convert.ToDouble(lblAmexSettlement.Text.Trim() == "" ? "0" : lblAmexSettlement.Text);
        //HDFCSettlement = Convert.ToDouble(lblHDFCSettlement.Text.Trim() == "" ? "0" : lblHDFCSettlement.Text);

        CashHandOver.Text = (Convert.ToDouble(lblCashTotal.Text)
        - Convert.ToDouble(lblCashTotalVoidable.Text) - Convert.ToDouble(lblCashDeposited.Text)
        - Convert.ToDouble(lblCashTotalHotel.Text)).ToString();
        AmexHandOver.Text = (Convert.ToDouble(lblAmexTotal.Text)
        - Convert.ToDouble(lblAmexTotalVoidable.Text) - Convert.ToDouble(lblAmexSettlement.Text)
        - Convert.ToDouble(lblAmexTotalHotel.Text)).ToString();
        HDFCHandOver.Text = (Convert.ToDouble(lblHDFCTotal.Text)
        - Convert.ToDouble(lblHDFCTotalVoidable.Text) - Convert.ToDouble(lblHDFCSettlement.Text)
        - Convert.ToDouble(lblHDFCTotalHotel.Text)).ToString();
        EWalletHandOver.Text = (Convert.ToDouble(lblEWalletTotal.Text)
                - Convert.ToDouble(lblEWalletTotalVoidable.Text) - Convert.ToDouble(lblEWalletSettlement.Text)
                - Convert.ToDouble(lblEWalletTotalHotel.Text)).ToString();

        objAdmin.SaveHandOver(Convert.ToInt32(ViewState["BatchID"]), Convert.ToDecimal(lblCashSale.Text)
            , Convert.ToDecimal(lblAmexSale.Text), Convert.ToDecimal(lblHDFCSale.Text)
            , Convert.ToDecimal(lblCashRejected.Text), Convert.ToDecimal(lblAmexRejected.Text)
            , Convert.ToDecimal(lblHDFCRejected.Text), Convert.ToDecimal(lblCashDeposited.Text)
            , Convert.ToDecimal(lblAmexSettlement.Text), Convert.ToDecimal(lblHDFCSettlement.Text)
            , Convert.ToDecimal(CashHandOver.Text), Convert.ToDecimal(AmexHandOver.Text)
            , Convert.ToDecimal(HDFCHandOver.Text), Convert.ToInt32(Reliver.SelectedValue)
            , Convert.ToInt32(Session["Userid"])
            //add ewallet code here
            , Convert.ToDecimal(lblEWalletSale.Text), Convert.ToDecimal(lblEWalletRejected.Text)
            , Convert.ToDecimal(lblEWalletSettlement.Text), Convert.ToDecimal(EWalletHandOver.Text)
            );


        txtBookingID.Enabled = false;
        GridView1.Enabled = false;
        lblCashDeposited.Enabled = false;
        lblAmexSettlement.Enabled = false;
        lblHDFCSettlement.Enabled = false;
        lblEWalletSettlement.Enabled = false;
        Reliver.Enabled = false;
        Handover.Enabled = false;
        btnBookingId.Enabled = false;
        //Page.RegisterClientScriptBlock("Script", "<script>alert('HandOver Made')</script>");
        //ClientScript.RegisterClientScriptBlock(this.GetType(),"Script", "<script>alert('HandOver Made')</script>");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Script", "alert('HandOver Sucessfully Made');", true);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "RemoveBookingID")
            {
                foreach (DataRow DtRow in GridViewDT.Rows)
                {
                    //************
                    int tmpRowCnt = GridView1.Rows.Count;
                    string[] strReson = new string[GridView1.Rows.Count];
                    int iCtr = 0;

                    foreach (GridViewRow gvr in GridView1.Rows)
                    {
                        if (gvr.Cells[0].Text.ToString() != e.CommandArgument.ToString())
                        {
                            TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
                            if (txtReson.Text != "")
                            {
                                strReson[iCtr] = txtReson.Text;
                            }
                            else
                            {
                                strReson[iCtr] = "";
                            }
                            iCtr += 1;
                        }
                    }

                    if (DtRow[0].ToString() == e.CommandArgument.ToString())
                    {
                        GridViewDT.Rows.Remove(DtRow);
                        GridView1.DataSource = GridViewDT;
                        GridView1.DataBind();
                        lblError.Text = "BookingID " + e.CommandArgument.ToString() + " removed successfully!";
                        lblError.Visible = true;
                    }
                    iCtr = 0;

                    if (GridView1.Rows.Count >= 0)
                    {
                        lblCashTotalVoidable.Text = "0";
                        lblAmexTotalVoidable.Text = "0";
                        lblHDFCTotalVoidable.Text = "0";
                        foreach (GridViewRow gvr in GridView1.Rows)
                        {
                            if (gvr.Cells[1].Text.ToString() == "CASH")
                            {
                                lblCashTotalVoidable.Text = (Convert.ToDouble(lblCashTotalVoidable.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }
                            if (gvr.Cells[1].Text.ToString() == "AMEX")
                            {
                                lblAmexTotalVoidable.Text = (Convert.ToDouble(lblAmexTotalVoidable.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }
                            if (gvr.Cells[1].Text.ToString() == "HDFC")
                            {
                                lblHDFCTotalVoidable.Text = (Convert.ToDouble(lblHDFCTotalVoidable.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }
                            if (gvr.Cells[1].Text.ToString() == "EWallet")
                            {
                                lblEWalletTotalVoidable.Text = (Convert.ToDouble(lblEWalletTotalVoidable.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }

                            if (gvr.Cells[0].Text.ToString() != e.CommandArgument.ToString())
                            {
                                TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
                                if (strReson[iCtr] != "")
                                {
                                    txtReson.Text = strReson[iCtr].ToString();
                                }
                                else
                                {
                                    txtReson.Text = "";
                                }
                                iCtr += 1;

                            }

                        }
                    }
                    else
                    {

                    }

                    CashDeposited = Convert.ToDouble(lblCashDeposited.Text.Trim() == "" ? "0" : lblCashDeposited.Text);
                    AmexSettlement = Convert.ToDouble(lblAmexSettlement.Text.Trim() == "" ? "0" : lblAmexSettlement.Text);
                    HDFCSettlement = Convert.ToDouble(lblHDFCSettlement.Text.Trim() == "" ? "0" : lblHDFCSettlement.Text);
                    EWalletSettlement = Convert.ToDouble(lblEWalletSettlement.Text.Trim() == "" ? "0" : lblEWalletSettlement.Text);

                    CashHandOver.Text = (Convert.ToDouble(lblCashTotal.Text)
                     - Convert.ToDouble(lblCashTotalVoidable.Text) - Convert.ToDouble(CashDeposited)).ToString();
                    AmexHandOver.Text = (Convert.ToDouble(lblAmexTotal.Text)
                     - Convert.ToDouble(lblAmexTotalVoidable.Text) - Convert.ToDouble(AmexSettlement)).ToString();
                    HDFCHandOver.Text = (Convert.ToDouble(lblHDFCTotal.Text)
                     - Convert.ToDouble(lblHDFCTotalVoidable.Text) - Convert.ToDouble(HDFCSettlement)).ToString();
                    EWalletHandOver.Text = (Convert.ToDouble(lblEWalletTotal.Text)
                     - Convert.ToDouble(lblEWalletTotalVoidable.Text) - Convert.ToDouble(EWalletSettlement)).ToString();
                    txtBookingID.Focus();

                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    //******************************************************************************************************
    protected void btnBookingIdNew_Click(object sender, EventArgs e)
    {
        int BookingID;
        lblError.Visible = false;
        lblError.Text = "";

        int tmpRowCnt = GridViewNew.Rows.Count;
        string[] strReson = new string[GridViewNew.Rows.Count + 1];
        int iCtr = 0;

        foreach (GridViewRow gvr in GridViewNew.Rows)
        {
            TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
            if (txtReson.Text != "")
            {
                strReson[iCtr] = txtReson.Text;
            }
            else
            {
                strReson[iCtr] = "";
            }
            iCtr += 1;
        }

        BookingID = Convert.ToInt32(txtBookingIDNew.Text.Trim() == "" ? "0" : txtBookingIDNew.Text);

        if (BookingID.ToString() == "0" || BookingID.ToString() == "")
        {
            //Page.RegisterClientScriptBlock("Script", "<script>alert('Please Enter Booking ID')</script>");
            lblErrorNew.Visible = true;
            lblErrorNew.Text = "Please Enter Booking ID";
            txtBookingIDNew.Focus();
            return;
        }

        foreach (DataRow DtRow in GridViewDTNew.Rows)
        {
            if (Convert.ToInt32(DtRow["BookingID"]) == BookingID)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('Booking ID already Entered')</script>");
                lblErrorNew.Visible = true;
                lblErrorNew.Text = "Booking ID already Entered";
                txtBookingIDNew.Focus();
                return;
            }
        }

        DSNew = objAdmin.Handover_AddVoidBooking(BookingID);
        txtBookingIDNew.Text = "";
        if (DSNew.Tables[0].Columns.Count == 1)
        {
            if (Convert.ToInt32(DSNew.Tables[0].Rows[0][0].ToString()) == 1)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('the BookingID is already available for Void!')</script>");
                lblErrorNew.Visible = true;
                lblErrorNew.Text = "the BookingID is already available for Void!";
                return;
            }
            if (Convert.ToInt32(DSNew.Tables[0].Rows[0][0].ToString()) == 2)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('Booking ID is already voided!')</script>");
                lblErrorNew.Visible = true;
                lblErrorNew.Text = "Booking ID is already voided!";
                return;
            }
            if (Convert.ToInt32(DSNew.Tables[0].Rows[0][0].ToString()) == 3)
            {
                //Page.RegisterClientScriptBlock("Script", "<script>alert('the BookingID was available for Void, but rejected!')</script>");
                lblErrorNew.Visible = true;
                lblErrorNew.Text = "the BookingID was available for Void, but rejected!";
                return;
            }
            else
            {
                lblErrorNew.Visible = true;
                lblErrorNew.Text = "This BookingID has some issue!";
                //Page.RegisterClientScriptBlock("Script", "<script>alert('This BookingID has some issue!')</script>");
                return;
            }
        }
        else
        {
            foreach (DataRow DtRow in DSNew.Tables[0].Rows)
            {
                GridViewDTNew.ImportRow(DtRow);
                if (DtRow["Mode"].ToString() == "CASH")
                {
                    lblCashTotalHotel.Text = (Convert.ToDouble(lblCashTotalHotel.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
                if (DtRow["Mode"].ToString() == "AMEX")
                {
                    lblAmexTotalHotel.Text = (Convert.ToDouble(lblAmexTotalHotel.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
                if (DtRow["Mode"].ToString() == "HDFC")
                {
                    lblHDFCTotalHotel.Text = (Convert.ToDouble(lblHDFCTotalHotel.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
                if (DtRow["Mode"].ToString() == "EWallet")
                {
                    lblEWalletTotalHotel.Text = (Convert.ToDouble(lblEWalletTotalHotel.Text)
                        + Convert.ToDouble(DtRow["TotalCost"])).ToString();
                }
            }
            if (GridViewDTNew.Rows.Count > 0)
            {
                GridViewNew.DataSource = GridViewDTNew;
                GridViewNew.DataBind();

                iCtr = 0;
                if (GridViewNew.Rows.Count > 1)
                {
                    iCtr = 0;
                    foreach (GridViewRow gvr in GridViewNew.Rows)
                    {
                        TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
                        if (strReson[iCtr] != "" && strReson[iCtr] != null)
                        {
                            txtReson.Text = strReson[iCtr].ToString();
                        }
                        else
                        {
                            txtReson.Text = "";
                        }
                        iCtr += 1;
                    }
                }
            }
            CashDeposited = Convert.ToDouble(lblCashDeposited.Text.Trim() == "" ? "0" : lblCashDeposited.Text);
            AmexSettlement = Convert.ToDouble(lblAmexSettlement.Text.Trim() == "" ? "0" : lblAmexSettlement.Text);
            HDFCSettlement = Convert.ToDouble(lblHDFCSettlement.Text.Trim() == "" ? "0" : lblHDFCSettlement.Text);
            EWalletSettlement = Convert.ToDouble(lblEWalletSettlement.Text.Trim() == "" ? "0" : lblEWalletSettlement.Text);

            CashHandOver.Text = (Convert.ToDouble(lblCashTotal.Text)
             - Convert.ToDouble(lblCashTotalVoidable.Text) - Convert.ToDouble(CashDeposited)
             - Convert.ToDouble(lblCashTotalHotel.Text)).ToString();
            AmexHandOver.Text = (Convert.ToDouble(lblAmexTotal.Text)
             - Convert.ToDouble(lblAmexTotalVoidable.Text) - Convert.ToDouble(AmexSettlement)
              - Convert.ToDouble(lblAmexTotalHotel.Text)).ToString();
            HDFCHandOver.Text = (Convert.ToDouble(lblHDFCTotal.Text)
             - Convert.ToDouble(lblHDFCTotalVoidable.Text) - Convert.ToDouble(HDFCSettlement)
              - Convert.ToDouble(lblHDFCTotalHotel.Text)).ToString();
            EWalletHandOver.Text = (Convert.ToDouble(lblEWalletTotal.Text)
             - Convert.ToDouble(lblEWalletTotalVoidable.Text) - Convert.ToDouble(EWalletSettlement)
              - Convert.ToDouble(lblEWalletTotalHotel.Text)).ToString();
            txtBookingIDNew.Focus();
        }
    }

    protected void GridViewNew_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "RemoveBookingID")
            {
                foreach (DataRow DtRow in GridViewDTNew.Rows)
                {
                    //************
                    int tmpRowCnt = GridViewNew.Rows.Count;
                    string[] strReson = new string[GridViewNew.Rows.Count];
                    int iCtr = 0;

                    foreach (GridViewRow gvr in GridViewNew.Rows)
                    {
                        if (gvr.Cells[0].Text.ToString() != e.CommandArgument.ToString())
                        {
                            TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
                            if (txtReson.Text != "")
                            {
                                strReson[iCtr] = txtReson.Text;
                            }
                            else
                            {
                                strReson[iCtr] = "";
                            }
                            iCtr += 1;
                        }
                    }

                    if (DtRow[0].ToString() == e.CommandArgument.ToString())
                    {
                        GridViewDTNew.Rows.Remove(DtRow);
                        GridViewNew.DataSource = GridViewDTNew;
                        GridViewNew.DataBind();
                        lblErrorNew.Text = "BookingID " + e.CommandArgument.ToString() + " removed successfully!";
                        lblErrorNew.Visible = true;
                    }
                    iCtr = 0;

                    if (GridViewNew.Rows.Count >= 0)
                    {
                        lblCashTotalHotel.Text = "0";
                        lblAmexTotalHotel.Text = "0";
                        lblHDFCTotalHotel.Text = "0";
                        foreach (GridViewRow gvr in GridViewNew.Rows)
                        {
                            if (gvr.Cells[1].Text.ToString() == "CASH")
                            {
                                lblCashTotalHotel.Text = (Convert.ToDouble(lblCashTotalHotel.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }
                            if (gvr.Cells[1].Text.ToString() == "AMEX")
                            {
                                lblAmexTotalHotel.Text = (Convert.ToDouble(lblAmexTotalHotel.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }
                            if (gvr.Cells[1].Text.ToString() == "HDFC")
                            {
                                lblHDFCTotalHotel.Text = (Convert.ToDouble(lblHDFCTotalHotel.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }
                            if (gvr.Cells[1].Text.ToString() == "EWallet")
                            {
                                lblEWalletTotalHotel.Text = (Convert.ToDouble(lblEWalletTotalHotel.Text)
                                    + Convert.ToDouble(gvr.Cells[2].Text)).ToString();
                            }

                            if (gvr.Cells[0].Text.ToString() != e.CommandArgument.ToString())
                            {
                                TextBox txtReson = (TextBox)gvr.FindControl("Reasons");
                                if (strReson[iCtr] != "")
                                {
                                    txtReson.Text = strReson[iCtr].ToString();
                                }
                                else
                                {
                                    txtReson.Text = "";
                                }
                                iCtr += 1;

                            }

                        }
                    }
                    else
                    {

                    }

                    CashDeposited = Convert.ToDouble(lblCashDeposited.Text.Trim() == "" ? "0" : lblCashDeposited.Text);
                    AmexSettlement = Convert.ToDouble(lblAmexSettlement.Text.Trim() == "" ? "0" : lblAmexSettlement.Text);
                    HDFCSettlement = Convert.ToDouble(lblHDFCSettlement.Text.Trim() == "" ? "0" : lblHDFCSettlement.Text);
                    EWalletSettlement = Convert.ToDouble(lblEWalletSettlement.Text.Trim() == "" ? "0" : lblEWalletSettlement.Text);

                    CashHandOver.Text = (Convert.ToDouble(lblCashTotal.Text)
                     - Convert.ToDouble(lblCashTotalVoidable.Text) - Convert.ToDouble(CashDeposited)
                      - Convert.ToDouble(lblCashTotalHotel.Text)).ToString();
                    AmexHandOver.Text = (Convert.ToDouble(lblAmexTotal.Text)
                     - Convert.ToDouble(lblAmexTotalVoidable.Text) - Convert.ToDouble(AmexSettlement)
                      - Convert.ToDouble(lblAmexTotalHotel.Text)).ToString();
                    HDFCHandOver.Text = (Convert.ToDouble(lblHDFCTotal.Text)
                     - Convert.ToDouble(lblHDFCTotalVoidable.Text) - Convert.ToDouble(HDFCSettlement)
                      - Convert.ToDouble(lblHDFCTotalHotel.Text)).ToString();
                    EWalletHandOver.Text = (Convert.ToDouble(lblEWalletTotal.Text)
                     - Convert.ToDouble(lblEWalletTotalVoidable.Text) - Convert.ToDouble(EWalletSettlement)
                      - Convert.ToDouble(lblEWalletTotalHotel.Text)).ToString();
                    txtBookingID.Focus();

                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    //******************************************************************************************************
    protected void btnVoidable_Click(object sender, EventArgs e)
    {
        TextBox VoidReason;
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            VoidReason = (TextBox)GridView1.Rows[i].FindControl("Reasons");

            objAdmin.VoidableInvoices(Convert.ToInt32(ViewState["BatchID"]), Convert.ToInt32(GridView1.Rows[i].Cells[0].Text),
                GridView1.Rows[i].Cells[1].Text.ToString(), Convert.ToDecimal(GridView1.Rows[i].Cells[2].Text),
                VoidReason.Text, Convert.ToInt32(Session["Userid"]));
        }
    }
    protected void btnHotel_Click(object sender, EventArgs e)
    {
        TextBox HotelReason;
        for (int i = 0; i < GridViewNew.Rows.Count; i++)
        {
            HotelReason = (TextBox)GridViewNew.Rows[i].FindControl("Reasons");

            objAdmin.HotelInvoices(Convert.ToInt32(ViewState["BatchID"]), Convert.ToInt32(GridViewNew.Rows[i].Cells[0].Text),
                GridViewNew.Rows[i].Cells[1].Text.ToString(), Convert.ToDecimal(GridViewNew.Rows[i].Cells[2].Text),
                HotelReason.Text, Convert.ToInt32(Session["Userid"]));
        }
    }
}