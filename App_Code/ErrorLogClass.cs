﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using context = System.Web.HttpContext;


public class ErrorLogClass
{
    public static string LogErrorToLogFile(Exception ee, string userFriendlyError)
    {
        try
        {
            string path = context.Current.Server.MapPath("~/ErrorLogging/");
            // check if directory exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = "@" + DateTime.Now.ToString() + ":" + "Error page : " + context.Current.Request.Url.ToString() + ":" + "Error : " + ee.ToString();
                writer.WriteLine(error);
                //writer.WriteLine("==========================================");
                writer.Flush();
                writer.Close();
            }
            return userFriendlyError;
        }
        catch
        {
            throw;
        }
    }

    public static void LoginfoToLogFile(string details, string userFriendlyMessage, string ModuleName)
    {
        try
        {
            string path = context.Current.Server.MapPath("~/InfoLogging/");
            // check if directory exists
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path + DateTime.Today.ToString("dd-MMM-yy") + ".log";
            // check if file exist
            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }
            // log the error now
            using (StreamWriter writer = File.AppendText(path))
            {
                string error = ModuleName + " @ " + DateTime.Now.ToString() + " :Info : " + details + ";" + userFriendlyMessage;
                writer.WriteLine(error);
                writer.Flush();
                writer.Close();
            }
            //return userFriendlyMessage;
        }
        catch (Exception ex)
        {
            //throw;
            //return ex.ToString();
        }
    }
}

