using System;
using System.Data;
using System.Web.UI;
using ccs;

public partial class AcceptHandOver : clsPageAuthorization
//public partial class AcceptHandOver : System.Web.UI.Page
{
    ClsAdmin objAdmin = new ClsAdmin();
    DataSet GetSummary = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetSummary = objAdmin.prc_TakeHandOverDetail(Convert.ToInt32(Session["UserID"]));

            if (GetSummary.Tables[0].Rows.Count < 1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "ShowMessage();", true);
                btnAccept.Enabled = false;
            }

            if (GetSummary.Tables[0].Columns.Count == 1)
            {
                if (Convert.ToInt32(GetSummary.Tables[0].Rows[0][0].ToString()) == 3)
                {
                    //Handover accepted and logged out due to some reason
                    //Response.Redirect("RetailBookingMobile.aspx");
                    Response.Redirect("RetailBooking.aspx");
                }
                if (Convert.ToInt32(GetSummary.Tables[0].Rows[0][0].ToString()) == 2)
                {
                    //Handover to be modified
                    Response.Redirect("HandOver_Detail.aspx");
                }
                if (Convert.ToInt32(GetSummary.Tables[0].Rows[0][0].ToString()) == 1)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "ShowMessage();", true);
                    btnAccept.Enabled = false;
                    //Response.Redirect("HandOver_Detail.aspx");
                }
            }
            else
            {
                GridView1.DataSource = GetSummary;
                GridView1.DataBind();
                if (GetSummary.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < GetSummary.Tables[0].Rows.Count; i++)
                    {
                        txtuser.Text = GetSummary.Tables[0].Rows[i][5].ToString(); ;
                        txtlocation.Text = GetSummary.Tables[0].Rows[i][6].ToString(); ;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "ShowMessage();", true);
                    btnAccept.Enabled = false;
                }
            }
        }
    }

    protected void btnAccept_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            objAdmin.AcceptHandOver(Convert.ToInt32(Session["Userid"]),
                Convert.ToDecimal(GridView1.Rows[i].Cells[1].Text),
                Convert.ToDecimal(GridView1.Rows[i].Cells[2].Text),
                Convert.ToDecimal(GridView1.Rows[i].Cells[3].Text),
                Convert.ToDecimal(GridView1.Rows[i].Cells[4].Text));
            //Response.Redirect("RetailBookingMobile.aspx");
            Response.Redirect("RetailBooking.aspx");
        }
    }
    protected void btnReject_Click(object sender, EventArgs e)
    {
        Response.Redirect("https://insta.carzonrent.com/login.asp");
    }
}