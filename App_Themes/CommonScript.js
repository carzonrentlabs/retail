function onlyNumbers(evt)
{    
	var e = event || evt; // for trans-browser compatibility    
	var charCode = e.which || e.keyCode;    
    //(46 for dot) and (48 to 57 for 0-9)
	if ((charCode>=48 && charCode<=57) || charCode==46)
		return true;    
	else
	    return false;
}

function TrimValue(fieldName, fieldValue)
{


//var valueToPreserve;
//valueToPreserve = pastvalue
var s = new String(fieldValue);
var str = ""
var strSpace = " ";
var i,j,intCount;
//Trimming of Text Values
//to replace the enter pressed with space
	for(intCount=0;intCount<s.length;intCount++)
		if (!((s.charCodeAt(intCount)!=13) && (s.charCodeAt(intCount)!=10)))
		s=s.replace(s.charAt(intCount)," ");
			for (i=0;i<s.length && s.charAt(i) == strSpace;i++);
				for (j=s.length; j>=0 && s.charAt(j - 1) == strSpace; j--);
					for (;i<j;i++)
					str = str + s.charAt(i);


	//Replacing blank value with pastvalue
	if (str=="")
	{
	alert("Blank values are not allowed");
	//fieldName.value = valueToPreserve;
	fieldName.select();
	fieldName.focus();    
	return false;
	}
	//Else put the Trimmed value 
	else
	{
	fieldName.value = str;
	return true
	}
}

// JavaScript Document//Common Js for all Pages goes here
function isBlank(obj,displayName)
{
	var str="";
	str=obj.value;
	var len=str.length;
	var i;
	for(i=0;i<len;++i)
	{
		if(str.charAt(i)!=" ")
		{
			return false;
		}
	}
	alert(displayName+" cannot be left blank");
	obj.focus();
	return true;
}



function isNumber(obj,displayName)
{
//alert("sdfsdadsa");
	var str=lTrim(obj.value);
	for(i=0;i<str.length;i++)
	{
		if(str.charAt(i)<'0'||str.charAt(i)>'9')
		{	
			if((str.charAt(0))=="-")
			{
				i=i+1;
					continue;
			}
			alert(displayName+" should be numeric.");
			obj.focus();
			return false;
		}
	}
	return true;
} 

function hasNoSplChars(obj,displayName)
{
	var s="";
	s=lTrim(obj.value);
	splChars=new Array("##");
	for(i=0;i>s.length;i++)
		for(j=0;j<splChars.length;j++)
		{
			if(s.charAt(i)==splChars[j])
			{
				alert(displayName+" cannot have special characters like ##.");
				obj.focus();
				return false;
			}
		}
	return true;
}

function isDDMMYYYYDate(objFormField)
{
	var lsFieldVal = objFormField.value;
	var lsFieldLength = lsFieldVal.length;
	var thirdchar = lsFieldVal.substring(2,3);
	var sixthchar = lsFieldVal.substring(5,6);
	var separator = '/';
	if 	(
		(lsFieldLength == 10) &&
		(thirdchar == separator) &&
		(sixthchar == separator)
		)
		{
		//split the chars
		var liDayPart
		var liMonthPart
		var liYearPart
	/*	if (lsFieldVal.substr(0,1) == 0)
			liDayPart = parseInt(lsFieldVal.substr(1,2));
		else
			liDayPart = parseInt(lsFieldVal.substr(0,2));

		if (lsFieldVal.substring(3,4) == 0)
			liMonthPart = parseInt(lsFieldVal.substring(4,5));
		else
			liMonthPart = parseInt(lsFieldVal.substring(3,5));*/

        if (lsFieldVal.substr(0,1) == 0)
			liMonthPart= parseInt(lsFieldVal.substr(1,2));
		else
			liMonthPart = parseInt(lsFieldVal.substr(0,2));

		if (lsFieldVal.substring(3,4) == 0)
			liDayPart = parseInt(lsFieldVal.substring(4,5));
		else
			liDayPart = parseInt(lsFieldVal.substring(3,5));
		liYearPart = parseInt(lsFieldVal.substring(6,10));

		//check overall validity
		if (
			(liDayPart >= 1) &&
			(liDayPart <= 31) &&
			(liMonthPart >= 1) &&
			(liMonthPart <= 12) &&
			(liYearPart >= 1999) &&
			(liYearPart <= 2050)
			)
			{
			//check for 30-day months
			if (
				(liMonthPart==4) ||
				(liMonthPart==6) ||
				(liMonthPart==9) ||
				(liMonthPart==11) 
				)
				{
				if ( liDayPart <= 30)
					{
					retval = true;
					}
				else
					{
					retval = false;
					}
				}
			else	//not a 30-day month
				{
				//if february
				if (liMonthPart == 2)
					{
					//check for leap year
					if ( (liYearPart % 4) != 0) 
						{
						liFebDays = 28;
						}
					else
						{
						if ((liYearPart % 400) == 0)
							{
							liFebDays = 29;
							}
						else
							{
							if ((liYearPart % 100) ==0)
								{
								liFebDays = 28;
								}
							else
								{
								liFebDays = 29;
								}
							}
						}

					if (liDayPart <= liFebDays)
						{
						retval = true;
						}
					else
						{
						retval = false;
						}
					}
				else	//31-day month
					{
					retval = true;
					}
				}
			}
		else
			{
				retval = false;
			}
		}
	else
		{
		retval = false;
		}

	return retval;
}


function trim(string)
{
	return string.replace(/^\s*|\s*$/g,"");
} 


	function showLength(field, countfield,maxLimit) 
	{			
			//setting the limit of the textarea to 500
			if (field.value.length > maxLimit) // if too long...trim it!
				field.value = field.value.substring(0, maxLimit);
			// otherwise, update 'characters left' counter
			else 
				if (document.all)
					{eval(countfield).innerText =  field.value.length;}
					else
					
					{						
						var strCtrlName = new String();
						var strtmp = new String();
						strCtrlName = countfield;
						strtmp = strCtrlName.substr(strCtrlName.lastIndexOf(".")+1,strCtrlName.length - strCtrlName.lastIndexOf("."));	
						document.getElementById(strtmp).innerHTML = field.value.length;
					}
					
	}
	
	function OnLoadshowLength(field1,field2)
	{
			var str = new String();
			str = field1;
				eval(field2).innerText = str.length ;
	}	

function validateKeys(e,act){
if (document.layers)
     Key = "e.which";
else
     Key = "window.event.keyCode";

switch(act) {
   case "alpha":re = /[^a-z]+/i;break;
   case "numeric":re = /[^0-9]+/i;break;
   case "alphanumeric":re = /[^a-z0-9 ]+/i;break;
   case "Floating":re = /[^0-9.]+/i;break; 
   case "Money":re = /[^0-9.,]+/i;break;
   case "pnonenumeric":re = /[^0-9-,]+/i;break; 
   case "addressalphanumeric":re = /[^a-z0-9-,/]+/i;break;
   case "ChekBankname":re = /[^a-z0-9-]+/i;break;
   
   }
if(re.test(eval("String.fromCharCode("+Key+")")))    
    if((eval(Key))!=32)
    {
    eval(Key+"=null")
   }
}

function validateKeys1(e,act){
if (document.layers)
     Key = "e.which";
else
     Key = "window.event.keyCode";

switch(act) {
   case "alpha":re = /[^a-z]+/i;break;
   case "numeric":re = /[^0-9]+/i;break;
   case "alphanumeric":re = /[^a-z0-9 ]+/i;break;
   case "Floating":re = /[^0-9.]+/i;break; 
   case "Money":re = /[^0-9.,]+/i;break;
   case "pnonenumeric":re = /[^0-9-,]+/i;break; 
   case "addressalphanumeric":re = /[^a-z0-9-,/]+/i;break;
   case "ChekBankname":re = /[^a-z0-9-]+/i;break;
   
   }
if(re.test(eval("String.fromCharCode("+Key+")")))    
    
    eval(Key+"=null")
   
}


function isNumeric(fieldValue)
{
	var num = '0123456789';
	
	var i,j,found;
	found = false;
	
	for(i=0;i<fieldValue.length;i++)
	{
		for(j=0;j<num.length;j++)
		{
			if(fieldValue.charAt(i) == num.charAt(j))
			{
				found = true; break;
			}
		}
		if(found == false) {return false;}
		if(found == true) {found=false;}
	}
	
	return true;
}


function isFloating(fieldValue)
{
	var num = '0123456789.';
	var dot='.';
	
	var i,j,found;
	var count=0;
	found = false;
	
	for(i=0;i<fieldValue.length;i++)
	{
		for(j=0;j<num.length;j++)
		{
			if(fieldValue.charAt(i) == num.charAt(j))
			{
				found = true;break;
			}
		}
		if(fieldValue.charAt(i) == dot.charAt(0))
		{
			count = parseInt(count) + 1;
		}
		if(count > 1)
		{
			    found=false;
		}
		if(found == false) {return false;}
		if(found == true) {found=false;}
	}
	
	return true;
}

///
function emailvalidation(entered, alertbox)
{
if (entered.value !== "")
{
    with (entered)
    {
    apos=value.indexOf("@");
    dotpos=value.lastIndexOf(".");
    lastpos=value.length-1;
        if (apos<1 || dotpos-apos<2 || lastpos-dotpos>3 || lastpos-dotpos<2) 
        {
            if (alertbox) 
            {
              alert(alertbox);
            } 
              return false;
        }
        else 
        {
            return true;
        }
    }
}

}
////

function check_email(e) {
ok = "1234567890qwertyuiop[]asdfghjklzxcvbnm.@-_QWERTYUIOPASDFGHJKLZXCVBNM";

for(i=0; i < e.length ;i++){
if(ok.indexOf(e.charAt(i))<0){ 
return (false);
}	
} 

if (document.images) {
re = /(@.*@)|(\.\.)|(^\.)|(^@)|(@$)|(\.$)|(@\.)/;
re_two = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
if (!e.match(re) && e.match(re_two)) {
return (-1);		
} 

}

}
function isValidPassword(fieldValue)
{
	var val = fieldValue;
	//alert(val);
	var upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var lower = 'abcdefghijklmnopqrstuvwxyz';
	var num = '0123456789';
		
	var i, j;
	var foundNum, foundAlpha;
	foundNum = false;
	foundAlpha = false;
	foundConsecutive = false;
		
	for(i=0;i<val.length;i++)
	{
		for(j=0;j<upper.length;j++)
		{
			if(val.charAt(i) == upper.charAt(j))
			{
				foundAlpha = true;
				//"found upper case letter"
				break;
			}
		}
		if(foundAlpha == false)
		{
			for(j=0;j<lower.length;j++)
			{
				if(val.charAt(i) == lower.charAt(j))
				{
					foundAlpha = true;
					//"found lower case letter"
					break;
				}	
			}
		}
		if(foundNum == false)
		{
			for(j=0;j<num.length;j++)
			{
				if(val.charAt(i) == num.charAt(j))
				{
					foundNum = true;
					//"found number"
					break;
				}
			}
		}
	            
	}
	if (foundAlpha == true && foundNum == true)
	
    {
        var k;
		for(k=0;k<val.length;k++)
		{
		
			if (val.charAt(k) == val.charAt(k+1))
				
			{
				//"Repeat characters"
				foundConsecutive = true;
				break;
			}
			
		}
		if (foundConsecutive == true)
		{
		    return false;
		}
		else
		{
		    return true;
		}
		
    }
    else
    {
        return false;
    }

}


function isValidPWD(fieldValue)
{
	var val = fieldValue;
	//alert(val);
	var upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var lower = 'abcdefghijklmnopqrstuvwxyz';
	var num = '0123456789';
		
	var i, j;
	var foundNum, foundAlpha;
	foundNum = false;
	foundAlpha = false;
	foundConsecutive = false;
		
	for(i=0;i<val.length;i++)
	{
		for(j=0;j<upper.length;j++)
		{
			if(val.charAt(i) == upper.charAt(j))
			{
				foundAlpha = true;
				//"found upper case letter"
				break;
			}
		}
		if(foundAlpha == false)
		{
			for(j=0;j<lower.length;j++)
			{
				if(val.charAt(i) == lower.charAt(j))
				{
					foundAlpha = true;
					//"found lower case letter"
					break;
				}	
			}
		}
		if(foundNum == false)
		{
			for(j=0;j<num.length;j++)
			{
				if(val.charAt(i) == num.charAt(j))
				{
					foundNum = true;
					//"found number"
					break;
				}
			}
		}
	            
	}
	if (foundAlpha == true && foundNum == true)
	
    {
      return true;
    }
    else
    {
        return false;
    }

}

function isMoneyValue(obj,fieldName,precision,scale)
{
    var fieldValue = obj.value;
    if(isFloating(fieldValue)==false)
    {
        alert(fieldName+' should be numeric or floating point value');
        obj.focus();
        return false;
    }
    else
    {
        // variable defines how much value can store before decimal point
        var before_decimal=precision-scale;
            
        // chack for floating point value
        if(fieldValue.indexOf('.')!=-1)
        {
            var field_array=fieldValue.split('.');
            if((field_array[0].length > before_decimal)||(field_array[1].length > scale))
            {
                alert(fieldName+' value can contain maximum of '+ before_decimal +' digits before decmial point and '+scale+' digits after decimal point');
                obj.focus();
                return false;
            }
            return true;
        }
        else  // for non-floating point value
        {
           // check for whole number
           if(fieldValue.length>before_decimal)
           {
                alert(fieldName+' value can contain maximum of '+ before_decimal +' digits before decmial point and '+scale+' digits after decimal point');
                //alert(fieldName+' value can not be greater than '+ before_decimal +' digits');
                obj.focus();
                return false;
           }
           return true;
        }
    }
}

function isValidPercentage(obj,fieldName)
{
    var fieldValue = obj.value;
    
    // Chack for numeric value
    if(isFloating(fieldValue)==false)
    {
        alert(fieldName+' should be numeric or floating point value');
        obj.focus();
        return false;
    }
    else
    {
        if((fieldValue<1)||(fieldValue>100))
        {
            alert(fieldName+' should be between 1 to 100');
            obj.focus();
            return false;
        }
        else
        {
            return true;
        }
    }
}
//check percentage between 0 - 100
function isValidPercentage0_100(obj,fieldName)
{
    var fieldValue = obj.value;
    
    // Chack for numeric value
    if(isFloating(fieldValue)==false)
    {
        alert(fieldName+' should be numeric or floating point value');
        obj.focus();
        return false;
    }
    else
    {
        if((fieldValue<0)||(fieldValue>100))
        {
            alert(fieldName+' should be between 0 to 100');
            obj.focus();
            return false;
        }
        else
        {
            return true;
        }
    }
}

function ObjectIsBlank(obj)
{
    var str="";
	str=obj.value;
	var len=str.length;
	var i;
	for(i=0;i<len;++i)
	{
		if(str.charAt(i)!=" ")
		{
			return false;
		}
	}
	return true;
}

/// vijay sinha For checking floating values  
function FloatCheck(ObjectValue,AllowComma)
{
    var start_format = " .+-0123456789";
	var number_format = " .0123456789";
	if (AllowComma == 1) number_format=number_format + ',';
	var check_char;
	var decimal = false;
	var trailing_blank = false;
	var digits = false;
	
	
	check_char = start_format.indexOf(ObjectValue.charAt(0))
	
	if (check_char == 1)
		decimal = true;
	else if (check_char < 1)
		return false;
	
	
	for (var i = 1; i < ObjectValue.length; i++)
	{
		check_char = number_format.indexOf(ObjectValue.charAt(i))
		if (check_char < 0)
			return false;
		else if (check_char == 1)
		{
			if (decimal)		
				return false;
			else
				decimal = true;
		}
		else if (check_char == 0)
		{
			if (decimal || digits) trailing_blank = true;
			
		}
		else if (trailing_blank)
			return false;
		else
			digits = true;
	}	
	
	return true;
}

function FloatCheckWithSigns(ObjectValue,AllowSigns)
{
    var number_format = " .0123456789";
	if (AllowSigns == 1) number_format=number_format + '+' + '-';
	var check_char;
	var decimal = false;
	var trailing_blank = false;
	var digits = false;
	
	
	check_char = number_format.indexOf(ObjectValue.charAt(0))
	
	if (check_char == 1)
		decimal = true;
	else if (check_char < 1)
		return false;
		
	
	for (var i = 1; i < ObjectValue.length; i++)
	{
		check_char = number_format.indexOf(ObjectValue.charAt(i))
		if (check_char < 0)
			return false;
		else if (check_char == 1)
		{
			if (decimal)		
				return false;
			else
				decimal = true;
		}
		else if (check_char == 0)
		{
			if (decimal || digits) trailing_blank = true;
			
		}
		else if (trailing_blank)
			return false;
		else
			digits = true;
	}	
	
	return true;
}



/// vijay sinha     For checking floating values with precision 
function ValidateFloat(ObjectValue,FieldName,AllowComma,precision,scale)
{

 if(FloatCheck(ObjectValue,AllowComma)==true)
 {
   
    var len = precision - scale; 
    var m=1;
    var pre9=0;
    var n=1;
    var scale9=0;
    var checkvalue;
    var i=0;
    
    for(i=0;i<len;i++)
    {
        pre9 = pre9 + (9*m);
        m=m*10;
    }
    
    for(i=0;i<scale;i++)
    {
        scale9 = scale9 + (9*n);
        n=n*10;
    }
    
   
    checkvalue = parseFloat(pre9 + '.' + scale9);
    
    if(ObjectValue>checkvalue)
    {
        alert(FieldName+' value can contain maximum of '+ len +' digits before decmial point and '+scale+' digits after decimal point');
        //alert(FieldName + "'s" + ' value cannot be greater than ' +  checkvalue + '.');
        return false;
    }
 } 
 else 
    {
        alert(FieldName + ' should be Numeric or Floating value only.');
        return false;
    }    
    return true;
} 

//To Allow signs pass 1 else 0
function ValidateFloatWithSigns(ObjectValue,FieldName,AllowSigns,precision,scale)
{

 if(FloatCheckWithSigns(ObjectValue,AllowSigns)==true)
 {
   
    var len = precision - scale; 
    var m=1;
    var pre9=0;
    var n=1;
    var scale9=0;
    var checkvalue;
    var i=0;
    
    for(i=0;i<len;i++)
    {
        pre9 = pre9 + (9*m);
        m=m*10;
    }
    
    for(i=0;i<scale;i++)
    {
        scale9 = scale9 + (9*n);
        n=n*10;
    }
    
   
    checkvalue = parseFloat(pre9 + '.' + scale9);
    
    if(ObjectValue>checkvalue)
    {
        alert(FieldName+' value can contain maximum of '+ len +' digits before decmial point and '+scale+' digits after decimal point');
        //alert(FieldName + "'s" + ' value cannot be greater than ' +  checkvalue + '.');
        return false;
    }
 } 
 else 
    {
        alert(FieldName + ' should be Numeric or Floating value only.');
        return false;
    }    
    return true;
} 

/// vijay sinha     For validating existence of Excel Files
function IsExcel(filename)
{
    var extallowed = ".xls";
    
    while(filename.indexOf("\\") != -1)
    {
        filename = filename.slice(filename.indexOf("\\") + 1);
    }    
        ext = filename.slice(filename.indexOf(".")).toLowerCase();
    
        
    if(extallowed == ext)
        return true;
    else
    {
        alert('Select Excel file only.');
        return false;
    }
    
}


/// vijay sinha     For validating range of numeric values
function ValidateInt(fieldvalue)
{
    if(isNumeric(fieldvalue)==false)
    {
        return false;
    }
    else
    {
        if(parseInt(fieldvalue)> 2147483647)
        {
        alert('Input value cannot be greater than 2147483647');
        return false;
        }
//        if(parseInt(fieldvalue)< -2147483648)
//        {
//        alert('The input value cannot be less than -2147483648');
//        return false;
//        }
    }
    return true;
    
}
//Check Date in mm/dd/yyyy format
/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}
//checks for valid date in mm/dd/yyyy format
function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}



//checks for valid date in mm/dd/yyyy format
function isValidDate(dtStr,FieldName)
{
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The format of " + FieldName + " should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month for "+ FieldName)
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day for "+FieldName)
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear+" for "+FieldName  )
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date for " + FieldName)
		return false
	}
return true
}

function replace(value)
{
    var counter = 1;
    var index = 0;
    while(counter >0)
    {
        index = value.indexOf(',');
        if(index == -1)
        {
            counter = 0;
        }
        else
        {
            value= value.replace( ",", "");
        }
    }
    return value;
}

function hasDots(value)
{
    var num = '0123456789.,';
    var dot='.';
    var i,j,found;
    var count=0;
    found = false;
    for(i=0;i<value.length;i++)
    {
       if(value.charAt(i) == dot.charAt(0))
       {
            return true;
       }
     }
     return false;
}

function isValidMoneyWithComma(fieldValue)
{
    var num = '0123456789.,';
    var dot='.';
    var i,j,found;
    var count=0;
    found = false;

    for(i=0;i<fieldValue.length;i++)
    {
    for(j=0;j<num.length;j++)
    {
        if(fieldValue.charAt(i) == num.charAt(j))
        {
	        found = true;
	        break;
        }
    }
    if(fieldValue.charAt(i) == dot.charAt(0))
    {
        count = parseInt(count) + 1;
    }
    if(count > 1)
    {
        found=false;
    }
    
    if(found == false) {return false;}
    if(found == true) {found=false;}
    }
	return true;
}
//checks the user entry for a valid currency format
function isValidCurrency(obj, fieldName, precision, scale)
{ 
    var value = obj.value;
    if(isValidMoneyWithComma(value) == false)
    {
        alert(fieldName+' should be numeric or floating point value');
        return false;
    }
    if (hasDots(value) == true)
    {
        var splitValue = value.split(".");
        var afterDecimalValue = splitValue[1];
        if (afterDecimalValue.length > 2)
        {
            alert("Enter " + fieldName + " in ###,###,###.## format");
            return false;
        }
    }
    var splitValue = value.split(".");
    var newValue = splitValue[0];
    var commaCount =1;
    for(i=0;i<newValue.length;i++)
    {
        if(newValue.charAt(i) == ",")
        {
            commaCount = commaCount + 1;
        }
    }
    if (commaCount>1)
    {
        var arryValue = newValue.split(",");
        if (arryValue[0].length > 3 || arryValue[0].length < 1)
        {
            alert("Enter " + fieldName + " in ###,###,###.## format");
            return false;
        }
        for(i=1;i<commaCount;i++)
        {
            if(arryValue[i].length != 3)
            {
                alert("Enter " + fieldName + " in ###,###,###.## format");
                return false;
            }
        }
    }
        // variable defines how much value can store before decimal point
        var before_decimal=precision-scale;
            
        // check for floating point value
        if(value.indexOf('.')!=-1)
        {
            var field_array=value.split('.');
            field_array[0] = replace(field_array[0]);
            if((field_array[0].length > before_decimal)||(field_array[1].length > scale))
            {
                alert(fieldName+' value can contain maximum of '+ before_decimal +' digits before decmial point and '+scale+' digits after decimal point');
                return false;
            }
        }
        else  // for non-floating point value
        {
            value = replace(value);
           // check for whole number
           if(value.length>before_decimal)
           {
                alert(fieldName+' value can contain maximum of '+ before_decimal +' digits before decmial point and '+scale+' digits after decimal point');
                //alert(fieldName+' value can not be greater than '+ before_decimal +' digits')
                return false;
           }
        }
        return true;
}

function isValueZero_to_One(obj, fieldName)
{
    var fieldValue = obj.value;
    if(fieldValue > 1 || fieldValue < 0 )
    {
        alert(fieldName+' should be a floating point value (range 0 - 1)');
        obj.focus();
        return false;
    }
    else if (fieldValue == 1)
    {
        return true;
    }
    else
    {
        // chack for floating point value
        if(fieldValue.indexOf('.')!=-1)
        {
            var field_array=fieldValue.split('.');
            if(field_array[1].length > 7)
            {
                alert(fieldName+' value can contain maximum of 1 digit before decmial point and 7 digits after decimal point');
                obj.focus();
                return false;
            }
        }
       
    }
    return true;
}



//'=========================================================================================
//'Function: CheckDate(dateFirst,dateSecond)
//'Purpose: To compares Two dates
//'Return Values:	 -1 -> dateFirst < dateSecond
//'				  0 -> dateFirst = dateSecond
//'				  1 -> dateFirst > dateSecond
//'=========================================================================================
//function CheckDate(dateFirst,dateSecond)
//{
//	If (Year(dateFirst) = Year(dateSecond)) Then
//	    {
//		If Month(dateFirst) = Month(dateSecond) Then
//			If Day(dateFirst) = Day(dateSecond) Then
//				CheckDate = 0		''first date = second date
//			ElseIf Day(dateFirst) < Day(dateSecond) Then
//				CheckDate = -1		''first date < second date
//			Else
//				CheckDate = 1		''first date > second date
//			End If
//		ElseIf Month(dateFirst) < Month(dateSecond) Then
//			CheckDate = -1			''first date < second date
//		Else
//			CheckDate = 1			''first date > second date
//		End If
//		}
//	ElseIf Year(dateFirst) < Year(dateSecond) Then
//		CheckDate = -1				''first date < second date
//	Else
//		CheckDate = 1				''first date > second date
//	End If
//}

function betweennumber(obj,displayName)
{
    if (obj.value != "")
    {
	    if(!(obj.value >= 0 &&  obj.value <= 180))
	    {
		    alert(displayName+" values should be between 0 to 180");
		    obj.focus();
		    return true;
	    }
	}
	return false;
} 

function NotisZero(obj,displayName)
{
	if(obj.value <= 0)
	{
		alert(displayName+" can't be zero.");
		obj.focus();
		return true;
	}
	return false;
} 

//function datediff(dateFirst,dateSecond)
//{
//	alert("Test");
//	if(dateFirst.value > dateSecond.value)
//	{
//		alert("From date should be less than to Date);
//		obj.focus();
//		return true;
//	}
//	return false;
//} 