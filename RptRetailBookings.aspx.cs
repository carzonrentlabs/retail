using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ccs;

public partial class Reports_RptRetailBookings : clsPageAuthorization
{
    ClsAdmin objAdmin = new ClsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLocation();
            BindAgents();
        }
    }

    protected void BindLocation()
    {
        ddlLocations.DataTextField = "CityName";
        ddlLocations.DataValueField = "UnitCityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetCity2(Convert.ToInt32(Session["UserID"]));
        ddlLocations.DataSource = dsLocation;
        ddlLocations.DataBind();
    }

    protected void BindAgents()
    {
        ddlAgents.DataTextField = "Name";
        ddlAgents.DataValueField = "SysUserID";
        DataSet dsAgents = new DataSet();
        dsAgents = objAdmin.GetAgents(Convert.ToInt32(Session["UserID"]));
        ddlAgents.DataSource = dsAgents;
        ddlAgents.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int UnitCityID = Convert.ToInt32(ddlLocations.SelectedValue);
        int SysUserID = Convert.ToInt32(ddlAgents.SelectedValue);
        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        //Response.Redirect("RptRetailBookings_Crystal.aspx?UnitCityID=" + UnitCityID + "&SysUserID=" + SysUserID + "&FromDate=" + fromDate + "&ToDate=" + toDate);
        //return;
        ClsAdmin objadmin = new ClsAdmin();
        DataTable dtEx = new DataTable();
        double SumTotalCost = 0.0;
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();

        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=DetailReport.ods");
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=DetailReport.xls");
        }
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else
        {
            Response.ContentType = "application/vnd.ms-excel";
        }

        this.EnableViewState = false;
        Response.Write("\r\n");

        if (FinYear.SelectedItem.ToString() == "2016-2025")
        {
            dsExcel = objadmin.GetDataForExcelReport(UnitCityID, SysUserID, fromDate, toDate);
        }
        //else if (FinYear.SelectedItem.ToString() == "2010-2015")
        //{
        //    dsExcel = objadmin.GetDataForExcelReport_0910(UnitCityID
        //        , SysUserID, fromDate, toDate, FinYear.SelectedValue.ToString());
        //}
        else //if (FinYear.SelectedItem.ToString() == "2009-2010")
        {
            dsExcel = objadmin.GetDataForExcelReport_0910(UnitCityID, SysUserID
                , fromDate, toDate, FinYear.SelectedValue.ToString());
        }

        for (int i = 0; i < dsExcel.Tables[0].Rows.Count; i++)
        {
            if (dsExcel.Tables[0].Rows[i]["TotalCost"].ToString() != "")
            {
                SumTotalCost = SumTotalCost + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["TotalCost"].ToString());
            }
        }
        int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());
        if (totalCount > 0)
        {
            DataRow dr;
            dr = dsExcel.Tables[0].NewRow();
            dsExcel.Tables[0].Rows.Add(dr);
            //dsExcel.Tables[0].Rows[totalCount]["UserName"] = "Total";
            dsExcel.Tables[0].Rows[totalCount]["TotalCost"] = SumTotalCost;
        }


        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");
        int[] iColumns = { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 2, 13, 14, 15, 16, 24, 25, 26, 27, 28, 17, 29, 12 };
        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    if (j == 12)
                    {
                        Response.Write("<td align='center'><b>" + "Created By" + "</b></td>");
                    }
                    else
                    {
                        Response.Write("<td align='center'><b>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</b></td>");
                    }
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                if (j == 8)
                {
                    Response.Write("<td align='center'>" + "'" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
                else
                {
                    Response.Write("<td align='center'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
                }
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        FillGrid();
    }

    protected void CustomersGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CustomersGridView.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    void FillGrid()
    {
        int UnitCityID = Convert.ToInt32(ddlLocations.SelectedValue);
        int SysUserID = Convert.ToInt32(ddlAgents.SelectedValue);
        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        ClsAdmin objadmin = new ClsAdmin();
        DataSet dsExcel = new DataSet();
        if (FinYear.SelectedItem.ToString() == "2016-2025")
        {
            dsExcel = objadmin.GetDataForExcelReport(UnitCityID, SysUserID, fromDate, toDate);
        }
        //else if (FinYear.SelectedItem.ToString() == "2010-2015")
        //{
        //    dsExcel = objadmin.GetDataForExcelReport_0910(UnitCityID
        //        , SysUserID, fromDate, toDate, FinYear.SelectedValue.ToString());
        //}
        else //if (FinYear.SelectedItem.ToString() == "2009-2010")
        {
            dsExcel = objadmin.GetDataForExcelReport_0910(UnitCityID, SysUserID
                , fromDate, toDate, FinYear.SelectedValue.ToString());
        }
        CustomersGridView.DataSource = dsExcel;
        CustomersGridView.DataBind();
    }

        protected void Button2_Click(object sender, EventArgs e)
    {
        int UnitCityID = Convert.ToInt32(ddlLocations.SelectedValue);
        int SysUserID = Convert.ToInt32(ddlAgents.SelectedValue);
        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        //Response.Redirect("RptRetailBookingSummary_Viewer.aspx?UnitCityID=" + UnitCityID + "&SysUserID=" + SysUserID + "&FromDate=" + fromDate + "&ToDate=" + toDate);
        //return;

        ClsAdmin objadmin = new ClsAdmin();
        DataTable dtEx = new DataTable();
        double CountCash = 0.0;
        double TotalCash = 0.0;
        double CountAmex = 0.0;
        double TotalAmex = 0.0;
        double CountHDFC = 0.0;
        double TotalHDFC = 0.0;
        double CountEWallet = 0.0;
        double TotalEWallet = 0.0;
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=Summaryreport.ods");
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=Summaryreport.xls");
        }

        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else
        {
            Response.ContentType = "application/vnd.ms-excel";
        }

        this.EnableViewState = false;
        Response.Write("\r\n");


        if (FinYear.SelectedItem.ToString() == "2016-2025")
        {
            dsExcel = objadmin.GetDataForExcelSummary(UnitCityID
                , SysUserID, fromDate, toDate);
        }
        else //if (FinYear.SelectedItem.ToString() == "2009-2010")
        {
            dsExcel = objadmin.GetDataForExcelSummary_0910(UnitCityID
                , SysUserID, fromDate, toDate
                , FinYear.SelectedValue.ToString());
        }

        for (int i = 0; i < dsExcel.Tables[0].Rows.Count; i++)
        {
            if (dsExcel.Tables[0].Rows[i]["CashDuties"].ToString() != "")
            {
                CountCash = CountCash + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["CashDuties"].ToString());
            }
            if (dsExcel.Tables[0].Rows[i]["CashAmount"].ToString() != "")
            {
                TotalCash = TotalCash + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["CashAmount"].ToString());
            }
            if (dsExcel.Tables[0].Rows[i]["AmexDuties"].ToString() != "")
            {
                CountAmex = CountAmex + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["AmexDuties"].ToString());
            }
            if (dsExcel.Tables[0].Rows[i]["AmexAmount"].ToString() != "")
            {
                TotalAmex = TotalAmex + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["AmexAmount"].ToString());
            }
            if (dsExcel.Tables[0].Rows[i]["HDFCDuties"].ToString() != "")
            {
                CountHDFC = CountHDFC + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["HDFCDuties"].ToString());
            }
            if (dsExcel.Tables[0].Rows[i]["HDFCAmount"].ToString() != "")
            {
                TotalHDFC = TotalHDFC + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["HDFCAmount"].ToString());
            }
            if (dsExcel.Tables[0].Rows[i]["EWalletDuties"].ToString() != "")
            {
                CountEWallet = CountEWallet + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["EWalletDuties"].ToString());
            }
            if (dsExcel.Tables[0].Rows[i]["EWalletAmount"].ToString() != "")
            {
                TotalEWallet = TotalEWallet + Convert.ToDouble(dsExcel.Tables[0].Rows[i]["EWalletAmount"].ToString());
            }
        }

        int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());
        if (totalCount > 0)
        {
            DataRow dr;
            dr = dsExcel.Tables[0].NewRow();
            dsExcel.Tables[0].Rows.Add(dr);
            dsExcel.Tables[0].Rows[totalCount]["UserName"] = "Total";
            dsExcel.Tables[0].Rows[totalCount]["CashDuties"] = CountCash;
            dsExcel.Tables[0].Rows[totalCount]["CashAmount"] = TotalCash;
            dsExcel.Tables[0].Rows[totalCount]["AmexDuties"] = CountAmex;
            dsExcel.Tables[0].Rows[totalCount]["AmexAmount"] = TotalAmex;
            dsExcel.Tables[0].Rows[totalCount]["HDFCDuties"] = CountHDFC;
            dsExcel.Tables[0].Rows[totalCount]["HDFCAmount"] = TotalHDFC;
            dsExcel.Tables[0].Rows[totalCount]["EWalletDuties"] = CountEWallet;
            dsExcel.Tables[0].Rows[totalCount]["EWalletAmount"] = TotalEWallet;
        }
        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");
        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='center'><b>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</b></td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                Response.Write("<td align='center'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }
}