using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ccs;


public partial class BWAirportSubmission : clsPageAuthorization
{
    ClsAdmin objAdmin = new ClsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblCashBF.Text = hdnCashBF.Value;
        lblCashDuties.Text = hdnCashDuties.Value;
        lblCashAmount.Text = hdnCashAmount.Value;
        lblCashColl.Text = hdnCashColl.Value;
        lblCashBal.Text = hdnCashBal.Value;

        lblAmexBF.Text = hdnAmexBF.Value;
        lblAmexDuties.Text = hdnAmexDuties.Value;
        lblAmexAmount.Text = hdnAmexAmount.Value;
        lblAmexColl.Text = hdnAmexColl.Value;
        lblAmexBal.Text = hdnAmexBal.Value;

        lblHdfcBF.Text = hdnHdfcBF.Value;
        lblHDFCDuties.Text = hdnHDFCDuties.Value;
        lblHDFCAmount.Text = hdnHDFCAmount.Value;
        lblHdfcColl.Text = hdnHdfcColl.Value;
        lblHDFCBal.Text = hdnHDFCBal.Value;

        btnSave.Attributes.Add("OnClick", "ChkValidation()");
        btnSave.Enabled = true;
        if (!IsPostBack)
        {
            btnSave.Enabled = false;
            BindLocation();
        }
    }

    protected void BindLocation()
    {
        ddlLocation.DataTextField = "CityName";
        ddlLocation.DataValueField = "UnitCityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetCity2(Convert.ToInt32(Session["UserID"]));
        ddlLocation.DataSource = dsLocation;
        ddlLocation.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ChkSave.Value.ToString() == "0")
        {
            return;
        }
        int LocationID, CashDuties, AmexDuties, HDFCDuties, EWalletDuties, UserID;
        DateTime TranDate;
        Decimal CashAmount, CashDeposit, AmexAmount, AmexDeposit, HDFCAmount, HDFCDeposit, EWalletAmount, EWalletDeposit;
        string CashSlipNo, AmexBatchNo, HDFCBatchNo, EWalletBatchNo;

        LocationID = Convert.ToInt32(ddlLocation.SelectedValue);
        TranDate = Convert.ToDateTime(txtDate.Text.ToString());
        CashDuties = Convert.ToInt32((hdnCashDuties.Value == "") ? "0" : hdnCashDuties.Value);
        CashAmount = Convert.ToDecimal((hdnCashAmount.Value == "") ? "0" : hdnCashAmount.Value);
        CashDeposit = Convert.ToDecimal((txtCashDeposit.Text == "") ? "0" : txtCashDeposit.Text);
        CashSlipNo = txtSlipNo.Text == "" ? "0" : txtSlipNo.Text;

        AmexDuties = Convert.ToInt32((hdnAmexDuties.Value == "") ? "0" : hdnAmexDuties.Value);
        AmexAmount = Convert.ToDecimal((hdnAmexAmount.Value == "") ? "0" : hdnAmexAmount.Value);
        AmexDeposit = Convert.ToDecimal((txtAmexDeposit.Text == "") ? "0" : txtAmexDeposit.Text);
        AmexBatchNo = txtAmexBatchNo.Text == "" ? "0" : txtAmexBatchNo.Text;

        HDFCDuties = Convert.ToInt32((hdnHDFCDuties.Value == "") ? "0" : hdnHDFCDuties.Value);
        HDFCAmount = Convert.ToDecimal((hdnHDFCAmount.Value == "") ? "0" : hdnHDFCAmount.Value);
        HDFCDeposit = Convert.ToDecimal((txtHDFCDeposit.Text == "") ? "0" : txtHDFCDeposit.Text);
        HDFCBatchNo = txtHDFCBatchNo.Text == "" ? "0" : txtHDFCBatchNo.Text;

        EWalletDuties = Convert.ToInt32((hdnEWalletDuties.Value == "") ? "0" : hdnEWalletDuties.Value);
        EWalletAmount = Convert.ToDecimal((hdnEWalletAmount.Value == "") ? "0" : hdnEWalletAmount.Value);
        EWalletDeposit = Convert.ToDecimal((txtEWalletDeposit.Text == "") ? "0" : txtEWalletDeposit.Text);
        EWalletBatchNo = txtEWalletBatchNo.Text == "" ? "0" : txtEWalletBatchNo.Text;

        UserID = Convert.ToInt32(Session["UserID"]);

        int NewTranID = objAdmin.BWAirportSubmission(LocationID, TranDate, CashDuties, CashAmount, CashDeposit,
            CashSlipNo, AmexDuties, AmexAmount, AmexDeposit, AmexBatchNo, HDFCDuties, HDFCAmount, HDFCDeposit,
            HDFCBatchNo, UserID, EWalletDuties, EWalletAmount, EWalletDeposit, EWalletBatchNo);

        if (NewTranID > 0)
        {
            lblMessage.Text = "Batch Submission ID: " + NewTranID.ToString();
        }
        else
        {
            lblMessage.Text = "Error while saving...";
        }
        btnSave.Enabled = false;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        btnSave.Enabled = false;
    }

}
