using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using static System.Net.WebRequestMethods;

public partial class Login : System.Web.UI.Page
{
    ClsAdmin objAdmin = new ClsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["Userid"] = 504;
        // Session["Userid"] = 1173;
        // DDAshift1 
        //Session["Userid"] = 1290;
        // blr shift1 
        //Session["Userid"] = 1286;

        //Session["Userid"] = 1285;
        //Session["Userid"] = 618;
        //Session["Userid"] = 839;
        //Session["Userid"] = 1797;
        //Session["Userid"] = 1283;
        //Session["Userid"] = 4;
        //Session["Userid"] = 1285;
        //Session.Abandon();

        //if (Session["Userid"] == null || Session["Userid"].ToString() == "")
        //{
        //    if (Request.Form["SysUserID"].ToString() != "" && Request.Form["SysUserID"] != null)
        //        Session["Userid"] = int.Parse(Request.Form["SysUserID"]);
        //}
        //Response.Redirect("AcceptHandOver.aspx"); //To be Uncommented
    }

    protected void btnSendOTP_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtloginid.Text == "")
            {
                lblMessage.Text = "Please enter Login id.";
            }
            else if (txtpass.Text == "")
            {
                lblMessage.Text = "Please Enter Password";
            }
            else
            {
                string otp = GenerateOtp();
                DataTable dt = new DataTable();
                dt = objAdmin.GenerateOTP(txtloginid.Text, txtpass.Text, otp);
                hndPwd.Value = txtpass.Text;
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["result"].ToString() == "1")
                    {
                        btnValidate.Visible = true;
                        btnSendOTP.Visible = false;
                        txtpass.Enabled = false;
                        lblMessage.Text = dt.Rows[0]["Message"].ToString();
                    }
                    else
                    {
                        lblMessage.Text = dt.Rows[0]["Message"].ToString();
                    }
                }
            }
        }
        catch(Exception ex)
        {
            lblMessage.Text =ex.Message.ToString();
        }
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtloginid.Text == "")
            {
                lblMessage.Text = "Please enter Login id.";
            }
            else if (txtotp.Text == "")
            {
                lblMessage.Text = "Please enter OTP.";
            }
            else
            {
                DataTable dt = new DataTable();
                string IpAddress = Request.ServerVariables["REMOTE_ADDR"];
                dt = objAdmin.ValidateOTP(txtloginid.Text, Convert.ToString(hndPwd.Value), txtotp.Text, IpAddress);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["LoginStatus"].ToString() == "0")
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            Session["Userid"] = Convert.ToInt32(dr["LoginUserId"]);
                            Response.Redirect("AcceptHandOver.aspx");

                        }
                        lblMessage.Text = dt.Rows[0]["Message"].ToString();
                    }
                    else if(dt.Rows[0]["LoginStatus"].ToString() == "5")
                    {
                        lblMessage.Text = dt.Rows[0]["Message"].ToString();
                        btnValidate.Visible = false;
                        btnSendOTP.Visible = true;
                        txtpass.Enabled = true;
                        txtotp.Text = "";
                    }
                    else
                    {
                        lblMessage.Text = dt.Rows[0]["Message"].ToString();
                    }
                }
            }

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message.ToString();
        }

    }

    public string GenerateOtp()
    {
        Random random = new Random();
        int otp = random.Next(100000, 999999); // Generate a 6-digit OTP
        return otp.ToString();
    }
}