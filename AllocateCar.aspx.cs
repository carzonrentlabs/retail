using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Net;
using ccs;
using System.Xml;
public partial class AllocateCar : System.Web.UI.Page
{
    DataSet GetBookingDetail = new DataSet();
    ClsAdmin objAdmin = new ClsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void LoadCars()
    {
        DataTable dtCars = objAdmin.GetCarsBookingIDWise
            (Convert.ToInt32(Session["UserID"])
            , Convert.ToInt32(txtBookingID.Text));
        if (dtCars.Rows.Count > 0)
        {
            ddlcar.DataTextField = "ddlText";
            ddlcar.DataValueField = "ddlValue";
            ddlcar.DataSource = dtCars;
            ddlcar.DataBind();
            ddlcar.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            ddlcar.DataTextField = "ddlText";
            ddlcar.DataValueField = "ddlValue";
            ddlcar.DataSource = dtCars;
            ddlcar.DataBind();
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (ddlcar.SelectedValue == "0")
        {
            lblMessage.Text = "Please select Car.";
            return;
        }
        DataSet ds = new DataSet();

        ds = objAdmin.AllocateBooking(Convert.ToInt32(ddlcar.SelectedValue)
            , Convert.ToInt32(txtBookingID.Text)
            , Convert.ToInt32(Session["UserID"]));

        if (ds.Tables[0].Rows.Count > 0)
        {
            lblMessage.Text = ds.Tables[0].Rows[0]["AllocationRemarks"].ToString();
        }
        else
        {
            lblMessage.Text = "Unable to Proccess, Please try again.";
        }
    }

    protected void txtBookingID_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtBookingID.Text))
        {
            LoadCars();
        }
    }
}
