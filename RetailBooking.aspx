<%@ Page Language="C#" MasterPageFile="~/CIPL.master" AutoEventWireup="true" CodeFile="RetailBooking.aspx.cs"
    Inherits="RetailBooking" Title="COR - Retail Module" StylesheetTheme="StyleSheet" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphPage">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnRePrint" />
            <asp:PostBackTrigger ControlID="Btsave" />
        </Triggers>
        <ContentTemplate>
            <center>
                <table style="border-right: black 1px solid; border-top: black 1px solid; margin-top: 50px; border-left: black 1px solid; width: 80%; border-bottom: black 1px solid; text-align: left"
                    onload="SelctPos();">
                    <tbody>
                        <tr>
                            <td style="height: 144px" colspan="3">
                                <table style="width: 100%">
                                    <tbody>
                                        <tr>
                                            <td style="width: 120px; height: 39px">Pickup Terminal</td>
                                            <td style="height: 39px" colspan="2">
                                                <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 55px; text-align: left">
                                                                <asp:DropDownList ID="ddlPkpCity" TabIndex="1" runat="server" onChange="SelctPos();"
                                                                    OnSelectedIndexChanged="ddlPkpCity_SelectedIndexChanged" AutoPostBack="True"
                                                                    Font-Names="Verdana" Font-Size="X-Small">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="text-align: right">
                                                                <asp:DropDownList ID="ddlOptions" runat="server" onChange="ResetSearch();" Font-Names="Verdana"
                                                                    Font-Size="X-Small">
                                                                    <asp:ListItem Selected="True" Value="0">Search from mobile</asp:ListItem>
                                                                    <asp:ListItem Value="1">Continuation Booking</asp:ListItem>
                                                                </asp:DropDownList>&nbsp;
                                                                <asp:TextBox ID="txtSearchMobile" TabIndex="4" runat="server" AutoPostBack="True"
                                                                    Font-Names="Verdana" Font-Size="X-Small" onchange="lblWait();" OnTextChanged="txtSearchMobile_TextChanged"
                                                                    Height="10px" Width="84px"></asp:TextBox></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="show">
                                            <td>Pos Terminal</td>
                                            <td>
                                                <asp:DropDownList ID="PosID" runat="server" AutoPostBack="False" Font-Names="Verdana"
                                                    Font-Size="X-Small">
                                                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                    <asp:ListItem Value="P0001">P0001</asp:ListItem>
                                                    <asp:ListItem Value="P0002">P0002</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr id="show1">
                                            <td>Pos Terminal</td>
                                            <td>
                                                <asp:DropDownList ID="PosID1" runat="server" AutoPostBack="False" Font-Names="Verdana"
                                                    Font-Size="X-Small">
                                                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                    <asp:ListItem Value="P0003">P0003</asp:ListItem>
                                                    <asp:ListItem Value="P0004">P0004</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 120px">Company Name</td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlCompanyName" TabIndex="2" runat="server" OnSelectedIndexChanged="ddlCompanyName_SelectedIndexChanged"
                                                    AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                            <td align="center">
                                                <asp:Label ID="message1" runat="server" Font-Bold="True" Font-Names="Verdana" ForeColor="Red"></asp:Label></td>
                                        </tr>
                                        <tr id="Show3" runat="server">
                                            <td>Coupon 1</td>
                                            <td>
                                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="Show4" runat="server">
                                            <td>Coupon 2</td>
                                            <td>
                                                <asp:TextBox ID="TextBox2" runat="server" Height="25px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="Show5" runat="server">
                                            <td>Coupon 3</td>
                                            <td>
                                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table style="width: 100%">
                                    <tbody>
                                        <tr>
                                            <td>Company / Organization Name</td>
                                            <td>
                                                <asp:TextBox ID="txtLegalName" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Height="10px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Payment Mode</td>
                                            <td>
                                                <asp:DropDownList ID="ddlPayMode" TabIndex="5" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>Select Car</td>
                                            <td>
                                                <asp:DropDownList ID="ddlSelectCar" runat="server" OnSelectedIndexChanged="ddlSelectCar_SelectedIndexChanged"
                                                    AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>Select Chauffeur</td>
                                            <td>
                                                <asp:DropDownList ID="ddlSelectChauffeur" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>Rental Type</td>
                                            <td>
                                                <asp:DropDownList ID="ddlRentalType" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>Remarks</td>
                                            <td>
                                                <asp:TextBox ID="txtRemarks" TabIndex="11" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Toll</td>
                                            <td>
                                                <asp:TextBox ID="txtToll" TabIndex="11" runat="server"
                                                    AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small" OnTextChanged="txtToll_TextChanged"
                                                    Height="10px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Basic Amount</td>
                                            <td>
                                                <asp:TextBox ID="lblBasicAmt" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Width="53px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox>
                                                <%--  OnTextChanged="lblBasicAmt_TextChanged"--%>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Label ID="lbldiscountdesc" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldiscountAmt" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Width="53px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><%--Sanatization Charges--%></td>
                                            <td>
                                                <%--<asp:TextBox ID="lblSanatizationChrgs" runat="server" BorderColor="White" BorderStyle="None" Font-Names="Verdana" Font-Size="X-Small" ReadOnly="True" Width="53px"></asp:TextBox>--%>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td style="width: 1px; background-color: gray"></td>
                            <td valign="top" align="center">
                                <table style="width: 80%; text-align: left">
                                    <tbody>
                                        <tr>
                                            <td>GST Number</td>
                                            <td>
                                                <asp:TextBox ID="txtGstNo" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Height="10px" AutoPostBack="true" OnTextChanged="txtGstNo_TextChanged"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Guest Name</td>
                                            <td>
                                                <asp:TextBox ID="txtGuestName" TabIndex="6" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Height="10px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Guest Mobile</td>
                                            <td>
                                                <asp:TextBox ID="txtGuestMobile" TabIndex="7" runat="server" Font-Names="Verdana"
                                                    Font-Size="X-Small" Height="10px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Guest EmailID</td>
                                            <td>
                                                <asp:TextBox ID="txtEmailID" TabIndex="8" runat="server" Font-Names="Verdana"
                                                    Font-Size="X-Small" Height="10px"></asp:TextBox>

                                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                                    ControlToValidate="txtEmailID"
                                                    Text="Invalid Email"
                                                    ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                    runat="server" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Car No</td>
                                            <td>
                                                <asp:TextBox ID="txtSearchCar" TabIndex="9" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                                    Font-Size="X-Small" OnTextChanged="txtSearchCar_TextChanged" Height="10px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td>Package</td>
                                            <td>
                                                <asp:DropDownList ID="ddlPkg" TabIndex="10" runat="server" OnSelectedIndexChanged="ddlPkg_SelectedIndexChanged1"
                                                    AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>Net Payable</td>
                                            <td>
                                                <%--<asp:TextBox onblur="calcBasicReverse();" ID="txtAdjCost" TabIndex="10" runat="server"
                                                    AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small" OnTextChanged="txtAdjCost_TextChanged"
                                                    Height="10px"></asp:TextBox>--%>
                                                <asp:TextBox ID="txtAdjCost" TabIndex="11" runat="server"
                                                    AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small" OnTextChanged="txtAdjCost_TextChanged"
                                                    Height="10px" Style="margin-bottom: 0px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--Net Billing Cost--%>
                                                <asp:Label ID="lblNetBilling" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNetBillingAmt" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                                    Width="53px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Shift
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlShift" runat="server" Font-Names="Verdana"
                                                    Font-Size="X-Small">
                                                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                    <asp:ListItem Value="Shift1">Shift1</asp:ListItem>
                                                    <asp:ListItem Value="Shift2">Shift2</asp:ListItem>
                                                    <asp:ListItem Value="Shift3">Shift3</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td align="left">
                                                <img src="App_Themes/payback.png" style="height: 38px; width: 102px" />
                                            </td>
                                            <td style="height: 25px" align="left">
                                                <asp:CheckBox ID="chkPayBack" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center" colspan="2">
                                                <asp:Label ID="lblmessage" runat="server" Width="139px" ForeColor="Red"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </center>
            <input style="width: 16px" id="ChkSave" type="hidden" value="0" runat="Server" />
            <input style="width: 16px" id="hdnGuestID" type="hidden" value="0" runat="Server" />
            <input style="width: 16px" id="hdnCCTypeID" type="hidden" runat="Server" />
            <input style="width: 16px" id="hdnpos" type="hidden" runat="Server" />
            <input style="width: 16px" id="hdtax" type="hidden" runat="Server" />
            <input style="width: 16px" id="hdndiscount" type="hidden" runat="Server" />
            <input style="width: 16px" id="hdndiscountstring" type="hidden" runat="Server" />
            <input style="width: 16px" id="hdndiscountID" type="hidden" runat="Server" />
            <input style="width: 16px" id="hdndiscountAmt" type="hidden" runat="Server" />
            <center>
                <table style="width: 485px">
                    <tr>
                        <td align="center" colspan="3">&nbsp;<asp:Button ID="Btsave" runat="server" Font-Bold="True" OnClick="Btsave_Click"
                            Text="Save" TabIndex="12" />
                            <asp:Button ID="btReset" runat="server" Font-Bold="True" OnClick="btReset_Click"
                                Text="Reset" Enabled="False" />
                            <asp:Button ID="btnRePrint" runat="server" Font-Bold="True" Text="Re-Print" Enabled="False"
                                OnClick="btnRePrint_Click" /></td>
                    </tr>
                </table>
            </center>
            <center id="divTran" runat="server">
                <br />
                <hr style="width: 485px" />
                <table style="width: 100%">
                    <tr>
                        <td valign="top" align="center">
                            <table style="width: 100%; text-align: center">
                                <tbody>
                                    <tr>
                                        <td>Last 3 transactions :</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="gridData" runat="server" CssClass="table table-condensed" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SLNo.">
                                                        <ItemTemplate>
                                                            <%#(Container.DataItemIndex+1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="GuestName" HeaderText="GuestName" />
                                                    <asp:BoundField DataField="CityName" HeaderText="CityName" />
                                                    <asp:BoundField DataField="Usage Date" HeaderText="Usage Date" />
                                                    <asp:BoundField DataField="Car Model" HeaderText="Car Model" />
                                                    <asp:BoundField DataField="Rental Type" HeaderText="Rental Type" />
                                                    <asp:BoundField DataField="Total Cost" HeaderText="Total Cost" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function calcBasicReverse() {
            var ttlTax = 0, FinalAmt = 0, BasicAmt = 0, TaxAmt = 0, parking = 0, toll = 0;

            ttlTax = document.getElementById('<%=hdtax.ClientID %>').value;

            FinalAmt = parseFloat(document.getElementById('<%=txtAdjCost.ClientID %>').value);

            //alert("Tax Amt: " + parseFloat(parseFloat(ttlTax)+100));
            //alert("Basic Amt : " + Math.round(FinalAmt/(parseFloat(parseFloat(ttlTax)+100))*100));
            //BasicAmt = Math.round(FinalAmt/(ttlTax+100)*100);
            BasicAmt = Math.round(FinalAmt / (parseFloat(parseFloat(ttlTax) + 100)) * 100);

            TaxAmt = FinalAmt - BasicAmt;
            if (isNaN(BasicAmt)) {
                BasicAmt = 0;
            }
            document.getElementById('<%=lblBasicAmt.ClientID %>').value = BasicAmt;
        }

        function SelctPos() {

            CityID = document.getElementById("<%=ddlPkpCity.ClientID %>").selectedIndex;
            CityText = document.getElementById("<%=ddlPkpCity.ClientID %>").options[CityID].value;
            var hiddenposid = document.getElementById("<%=hdnpos.ClientID %>").value;
            var obj = document.getElementById("show");
            var obj1 = document.getElementById("show1");

            if (CityText == 360) {
                if (hiddenposid == "") {
                    obj.style.display = "block";
                    obj1.style.display = "none";
                }
                else {
                    obj.style.display = "none";
                    obj1.style.display = "none";
                }
            }
            else if (CityText == 350) {
                if (hiddenposid == "") {
                    obj.style.display = "none";
                    obj1.style.display = "block";
                }
                else {
                    obj.style.display = "none";
                    obj1.style.display = "none";
                }
            }
            else {
                obj.style.display = "none";
                obj1.style.display = "none";
            }
        }

        function validateForm() {
            var GSTIN = document.getElementById('<%=txtGstNo.ClientID %>').value;

            GSTIN = GSTIN.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
            GSTIN = GSTIN.replace(/\s+$/, ""); 	//Removes Right Blank Spaces
            if (GSTIN != "") {
                //07AABCC5486C1ZT
                var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
                if (gstinformat.test(GSTIN)) {
                    var LegalName = document.getElementById('<%=txtLegalName.ClientID %>').value;
                    LegalName = LegalName.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
                    LegalName = LegalName.replace(/\s+$/, ""); 	//Removes Right Blank Spaces
                    if (LegalName == "") {
                        alert('Please enter Client Legal Name');
                        return (false);
                    }
                }
                else {
                    alert('Please Enter Valid GSTIN Number');
                    return (false);
                }
            }



            var emailid = document.getElementById('<%=txtEmailID.ClientID %>').value;
            if (emailid != "") {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                //var address = document.getElementById[email].value;
                if (reg.test(emailid) == false) {
                    alert('Invalid Email Address');
                    return (false);
                }
            }

            var tollamt = document.getElementById('<%=txtToll.ClientID %>').value;




            if (isNaN(parseInt(tollamt))) {
                alert("Please Enter Correct toll Amt..");
                document.getElementById('<%=txtToll.ClientID %>').focus();
                return false;
            }

            if (parseInt(tollamt) < 0) {
                alert("Toll Amount cannot be less than 0..");
                //document.getElementById('<%=txtToll.ClientID %>').focus();
                return false;
            }

            CityName = document.getElementById("<%=ddlPkpCity.ClientID %>").selectedIndex;
            CityText = document.getElementById("<%=ddlPkpCity.ClientID %>").options[CityName].value;

            if (CityText == "26" || CityText == "215") {
                tollamtlimit = 2000
            }
            else {
                tollamtlimit = 250
            }
            if (parseInt(tollamt) > tollamtlimit) {
                alert("Toll Amount cannot be greater than " + tollamtlimit + "..");
                //document.getElementById('<%=txtToll.ClientID %>').focus();
                return false;
            }

            //alert("you arehere .");
            //alert(tollamt);
            //alert(isNaN(parseInt(tollamt)));
            //alert(parseInt(tollamt));
            //return false;

            strShift = document.getElementById('<%=ddlShift.ClientID %>').selectedIndex;

            if (strShift == 0) {
                alert("Please Select Shift..");
                document.getElementById('<%=ddlShift.ClientID %>').focus();
                return false;
            }

            RentalType = document.getElementById('<%=ddlRentalType.ClientID %>').selectedIndex;

            if (RentalType == 0) {
                alert("Please Select Rental Type..");
                document.getElementById('<%=ddlRentalType.ClientID %>').focus();
                return false;
            }


            CityID = document.getElementById("<%=ddlPkpCity.ClientID %>").selectedIndex;
            CityText = document.getElementById("<%=ddlPkpCity.ClientID %>").options[CityID].value;

            if (CityText == 360) {
                PosID = document.getElementById("<%=PosID.ClientID %>").selectedIndex;
                PosText = document.getElementById("<%=PosID.ClientID %>").options[PosID].value;
                if (PosText == 0) {
                    alert("Please Select POS Terminal");
                    document.getElementById("<%=PosID.ClientID %>").focus();
                    return false;
                }
            }

            if (CityText == 350) {
                PosID1 = document.getElementById("<%=PosID1.ClientID %>").selectedIndex;
                PosText1 = document.getElementById("<%=PosID1.ClientID %>").options[PosID1].value;
                if (PosText1 == 0) {
                    alert("Please Select POS Terminal");
                    document.getElementById("<%=PosID1.ClientID %>").focus();
                    return false;
                }
            }


            CompanyName = document.getElementById("<%=ddlCompanyName.ClientID %>").selectedIndex;
            CompanyText = document.getElementById("<%=ddlCompanyName.ClientID %>").options[CompanyName].value;
            //var clientid = document.getElementById("<%=ddlCompanyName.ClientID%>").value;
            //alert(document.getElementById('<%=TextBox1.ClientID %>').value);
            //var obj2 = document.getElementById("Show3");
            //var obj3 = document.getElementById("Show4");
            //var obj4 = document.getElementById("Show5");
            // alert(CompanyText);
            // return false;
            if (CompanyText == 1209)//619)
            {
                if (isNaN(parseInt(document.getElementById('<%=TextBox1.ClientID %>').value))) {
                    alert("Please Enter Coupon No..");
                    document.getElementById('<%=TextBox1.ClientID %>').focus();
                    return false;
                }

                if (document.getElementById('<%=TextBox2.ClientID %>').value != "") {
                    if (isNaN(parseInt(document.getElementById('<%=TextBox2.ClientID %>').value))) {
                        alert("Coupon No Numeric Only");
                        document.getElementById('<%=TextBox2.ClientID %>').focus();
                        return false;
                    }
                }

                if (document.getElementById('<%=TextBox3.ClientID %>').value != "") {

                    if (isNaN(parseInt(document.getElementById('<%=TextBox3.ClientID %>').value))) {
                        alert("Coupon No Numeric Only");
                        document.getElementById('<%=TextBox3.ClientID %>').focus();
                        return false;
                    }
                }
            }

            if (document.getElementById("<%=ddlPkpCity.ClientID %>").value == '0') {
                alert("Please select pickup city");
                document.getElementById("<%=ddlPkpCity.ClientID %>").focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }



            if (document.getElementById("<%=ddlOptions.ClientID %>").value == '1') {
                if (document.getElementById("<%=txtSearchMobile.ClientID %>").value == '') {
                    alert("Please continuation BookingID");
                    document.getElementById("<%=txtSearchMobile.ClientID %>").focus();
                    document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                    return false;
                }
            }

            if (document.getElementById("<%=ddlPayMode.ClientID %>").value == '0') {
                alert("Please select payment mode");
                document.getElementById("<%=ddlPayMode.ClientID %>").focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }

            var GuestName = document.getElementById('<%=txtGuestName.ClientID %>').value;
            GuestName = GuestName.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
            GuestName = GuestName.replace(/\s+$/, ""); 	//Removes Right Blank Spaces

            if (GuestName == '') {
                alert("Please Enter Guest Name");
                document.getElementById("<%=txtGuestName.ClientID %>").focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }

            if (document.getElementById("<%=ddlSelectCar.ClientID %>").value == '') {
                alert("Please Select  a Car");
                document.getElementById("<%=ddlSelectCar.ClientID %>").focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }

            if (document.getElementById("<%=ddlPkg.ClientID %>").value == '') {
                alert("Please Select Package");
                document.getElementById("<%=ddlPkg.ClientID %>").focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }

            var GuestName = document.getElementById('<%=txtGuestName.ClientID %>').value;
            GuestName = GuestName.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
            GuestName = GuestName.replace(/\s+$/, ""); 	//Removes Right Blank Spaces

            if (GuestName == '') {
                alert('Please Enter Guest Name');
                document.getElementById('<%=txtGuestName.ClientID %>').focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }
            document.getElementById("<%=ChkSave.ClientID %>").value = "1";
        }

        function resetForm() {
            document.getElementById("<%=txtGuestName.ClientID %>").value = "";
            document.getElementById("<%=txtGuestMobile.ClientID %>").value = "";
            document.getElementById("<%=txtSearchCar.ClientID %>").value = "";
            document.getElementById("<%=ddlSelectCar.ClientID %>").length = 0;
            document.getElementById("<%=ddlPkg.ClientID %>").length = 0;
            document.getElementById("<%=txtAdjCost.ClientID %>").value = "";
            document.getElementById("<%=txtRemarks.ClientID %>").value = "";
        }

        function lblWait() {
            document.getElementById("<%=message1.ClientID %>").innerHTML = "Please wait...";
        }

        function ResetSearch() {
            document.getElementById("<%=txtSearchMobile.ClientID %>").value = "";
        }
    </script>
</asp:Content>
