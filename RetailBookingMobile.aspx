<%@ Page Language="C#" MasterPageFile="~/CIPL1.master" AutoEventWireup="true" CodeFile="RetailBookingMobile.aspx.cs"
    Inherits="RetailBookingMobile" Title="COR - Retail Module" StylesheetTheme="StyleSheet" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphPage">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnRePrint" />
            <asp:PostBackTrigger ControlID="Btsave" />
        </Triggers>
        <ContentTemplate>

            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <main class="main-wrapper">

                <div class="container">
                    <center>
                        <table class="width80percent">
                            <tbody>
                                <tr>
                                    <td valign="top" class="width100mobile">
                                        <table>
                                            <tbody>
                                                <tr style="visibility: hidden;">
                                                    <td>Company Name</td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlCompanyName" TabIndex="2" runat="server" OnSelectedIndexChanged="ddlCompanyName_SelectedIndexChanged"
                                                            AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small">
                                                        </asp:DropDownList></td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Pickup CityName</td>
                                    <td>
                                        <%--<asp:RadioButtonList ID="reCity" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="reCity_SelectedIndexChanged">
                                            <asp:ListItem Selected="True" Value="33">Delhi Apt DOM</asp:ListItem>
                                            <asp:ListItem Value="35">T3 DOM</asp:ListItem>
                                            <asp:ListItem Value="36">T3 Int</asp:ListItem>
                                        </asp:RadioButtonList>--%>
                                        <asp:DropDownList ID="reCity" TabIndex="1" runat="server"
                                            OnSelectedIndexChanged="reCity_SelectedIndexChanged" AutoPostBack="True"
                                            Font-Names="Verdana" Font-Size="X-Small">
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlOptions" runat="server" onChange="ResetSearch();" Font-Names="Verdana"
                                            Font-Size="X-Small">
                                            <asp:ListItem Selected="True" Value="0">Search from mobile</asp:ListItem>
                                            <asp:ListItem Value="1">Continuation Booking</asp:ListItem>
                                        </asp:DropDownList>

                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSearchMobile" TabIndex="0" runat="server" AutoPostBack="True"
                                            Font-Names="Verdana" Font-Size="X-Small" onchange="lblWait();" OnTextChanged="txtSearchMobile_TextChanged"
                                            Height="10px" Width="84px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Guest Name</td>
                                    <td>
                                        <asp:TextBox ID="txtGuestName" TabIndex="1" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            Height="10px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Guest Mobile</td>
                                    <td>
                                        <asp:TextBox ID="txtGuestMobile" TabIndex="2" runat="server" Font-Names="Verdana"
                                            Font-Size="X-Small" Height="10px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Guest EmailID</td>
                                    <td>
                                        <asp:TextBox ID="txtEmailID" TabIndex="3" runat="server" Font-Names="Verdana"
                                            Font-Size="X-Small" Height="10px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Car No</td>
                                    <td>
                                        <asp:TextBox ID="txtSearchCar" TabIndex="4" runat="server" AutoPostBack="True" Font-Names="Verdana"
                                            Font-Size="X-Small" OnTextChanged="txtSearchCar_TextChanged" Height="10px"></asp:TextBox>

                                        <asp:HiddenField ID="hdnPkgID" runat="server" />
                                        <asp:HiddenField ID="hdnPkgRate" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Select Car</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelectCar" runat="server" OnSelectedIndexChanged="ddlSelectCar_SelectedIndexChanged"
                                            AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>Select Chauffeur</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelectChauffeur" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>Package Type</td>
                                    <td>
                                        <asp:RadioButtonList ID="rdoOption" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="rdoOption_SelectedIndexChanged">
                                            <asp:ListItem Selected="True" Value="0">Transfer</asp:ListItem>
                                            <asp:ListItem Value="1">Disposal</asp:ListItem>
                                            <asp:ListItem Value="2">Outstation</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <td align="center">
                                            <asp:Label ID="message1" runat="server" Font-Size="Large" Font-Bold="True" Font-Names="Verdana" ForeColor="Red"></asp:Label>
                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblhrs" runat="server" Text="Hrs"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txthrs" TabIndex="5" runat="server" OnTextChanged="txthrs_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kms</td>
                                    <td>
                                        <asp:TextBox ID="txtkms" TabIndex="6" runat="server" AutoPostBack="true" OnTextChanged="txtkms_TextChanged"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Payment Mode</td>
                                    <td>
                                        <asp:RadioButtonList ID="ddlPayMode" runat="server">
                                            <asp:ListItem Value="2">Amex</asp:ListItem>
                                            <asp:ListItem Value="5" Selected="True">CASH</asp:ListItem>
                                            <asp:ListItem Value="7">e-Wallet</asp:ListItem>
                                            <asp:ListItem Value="1">HDFC</asp:ListItem>
                                        </asp:RadioButtonList>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Basic Amount</td>
                                    <td>
                                        <asp:TextBox ID="lblBasicAmt" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            Width="53px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Toll</td>
                                    <td>
                                        <asp:TextBox ID="txtToll" TabIndex="7" runat="server"
                                            AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small" OnTextChanged="txtToll_TextChanged"
                                            Height="10px"></asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td>
                                        <asp:Label ID="lbldiscountdesc" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbldiscountAmt" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            Width="53px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total Billing Cost</td>
                                    <td>
                                        <asp:TextBox ID="txtAdjCost" TabIndex="8" runat="server"
                                            AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small" OnTextChanged="txtAdjCost_TextChanged"
                                            Height="10px" Style="margin-bottom: 0px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%--Net Billing Cost--%>
                                        <asp:Label ID="lblNetBilling" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblNetBillingAmt" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            Width="53px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Client Legal Name</td>
                                    <td>
                                        <asp:TextBox ID="txtLegalName" TabIndex="9" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            Height="10px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GST Number</td>
                                    <td>
                                        <asp:TextBox ID="txtGstNo" TabIndex="10" runat="server" Font-Names="Verdana" Font-Size="X-Small"
                                            Height="10px" AutoPostBack="true" OnTextChanged="txtGstNo_TextChanged"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td>
                                        <asp:TextBox ID="txtRemarks" TabIndex="11" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox></td>
                                </tr>


                                <tr style="display: none">
                                    <td align="left">
                                        <img src="App_Themes/payback.png" />
                                    </td>
                                    <td style="height: 25px" align="left">
                                        <asp:CheckBox ID="chkPayBack" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center" colspan="2">
                                        <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                    <input style="width: 16px" id="ChkSave" type="hidden" value="0" runat="Server" />
                    <input style="width: 16px" id="hdnGuestID" type="hidden" value="0" runat="Server" />
                    <input style="width: 16px" id="hdnCCTypeID" type="hidden" runat="Server" />

                    <input style="width: 16px" id="hdtax" type="hidden" runat="Server" />
                    <input style="width: 16px" id="hdndiscount" type="hidden" runat="Server" />
                    <input style="width: 16px" id="hdndiscountstring" type="hidden" runat="Server" />
                    <input style="width: 16px" id="hdndiscountID" type="hidden" runat="Server" />
                    <input style="width: 16px" id="hdndiscountAmt" type="hidden" runat="Server" />
                    <center>
                        <table>
                            <tr>
                                <td align="center" colspan="3">&nbsp;
                                    <asp:Button ID="Btsave" runat="server" Font-Bold="True" OnClick="Btsave_Click"
                                        Text="Save" TabIndex="12" />
                                    <asp:Button ID="btReset" runat="server" Font-Bold="True" OnClick="btReset_Click"
                                        Text="Reset" Enabled="False" />
                                    <asp:Button ID="btnRePrint" runat="server" Font-Bold="True" Text="Re-Print" Enabled="False"
                                        OnClick="btnRePrint_Click" /></td>
                            </tr>
                        </table>
                    </center>
                    <center id="divTran" runat="server">
                        <br />
                        <hr style="width: 485px" />
                        <table style="width: 100%">
                            <tr>
                                <td valign="top" align="center">
                                    <table style="width: 100%; text-align: center">
                                        <tbody>
                                            <tr>
                                                <td>Last 3 transactions :</td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="gridData" runat="server" CssClass="table table-condensed" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="SLNo.">
                                                                <ItemTemplate>
                                                                    <%#(Container.DataItemIndex+1) %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="GuestName" HeaderText="GuestName" />
                                                            <asp:BoundField DataField="Usage Date" HeaderText="Usage Date" />
                                                            <asp:BoundField DataField="Car Model" HeaderText="Car Model" />
                                                            <asp:BoundField DataField="Rental Type" HeaderText="Rental Type" />
                                                            <asp:BoundField DataField="Total Cost" HeaderText="Total Cost" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>

            </main>

        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function calcBasicReverse() {
            var ttlTax = 0, FinalAmt = 0, BasicAmt = 0, TaxAmt = 0, parking = 0, toll = 0;

            ttlTax = document.getElementById('<%=hdtax.ClientID %>').value;

            FinalAmt = parseFloat(document.getElementById('<%=txtAdjCost.ClientID %>').value);

            //alert("Tax Amt: " + parseFloat(parseFloat(ttlTax)+100));
            //alert("Basic Amt : " + Math.round(FinalAmt/(parseFloat(parseFloat(ttlTax)+100))*100));
            //BasicAmt = Math.round(FinalAmt/(ttlTax+100)*100);
            BasicAmt = Math.round(FinalAmt / (parseFloat(parseFloat(ttlTax) + 100)) * 100);

            TaxAmt = FinalAmt - BasicAmt;
            if (isNaN(BasicAmt)) {
                BasicAmt = 0;
            }
            document.getElementById('<%=lblBasicAmt.ClientID %>').value = BasicAmt;
        }

        function validateForm() {
            var GSTIN = document.getElementById('<%=txtGstNo.ClientID %>').value;

            GSTIN = GSTIN.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
            GSTIN = GSTIN.replace(/\s+$/, ""); 	//Removes Right Blank Spaces
            if (GSTIN != "") {
                //07AABCC5486C1ZT
                var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
                if (gstinformat.test(GSTIN)) {
                    var LegalName = document.getElementById('<%=txtLegalName.ClientID %>').value;
                    LegalName = LegalName.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
                    LegalName = LegalName.replace(/\s+$/, ""); 	//Removes Right Blank Spaces
                    if (LegalName == "") {
                        alert('Please enter Client Legal Name');
                        return (false);
                    }
                }
                else {
                    alert('Please Enter Valid GSTIN Number');
                    return (false);
                }
            }

            var emailid = document.getElementById('<%=txtEmailID.ClientID %>').value;
            if (emailid != "") {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                //var address = document.getElementById[email].value;
                if (reg.test(emailid) == false) {
                    alert('Invalid Email Address');
                    return (false);
                }
            }

            var tollamt = document.getElementById('<%=txtToll.ClientID %>').value;

            if (isNaN(parseInt(tollamt))) {
                alert("Please Enter Correct toll Amt..");
                document.getElementById('<%=txtToll.ClientID %>').focus();
                return false;
            }

            if (parseInt(tollamt) < 0) {
                alert("Toll Amount cannot be less than 0..");
                //document.getElementById('<%=txtToll.ClientID %>').focus();
                return false;
            }

            //alert("you arehere .");
            //alert(tollamt);
            //alert(isNaN(parseInt(tollamt)));
            //alert(parseInt(tollamt));
            //return false;

            CompanyName = document.getElementById("<%=ddlCompanyName.ClientID %>").selectedIndex;
            CompanyText = document.getElementById("<%=ddlCompanyName.ClientID %>").options[CompanyName].value;


            if (document.getElementById("<%=ddlOptions.ClientID %>").value == '1') {
                if (document.getElementById("<%=txtSearchMobile.ClientID %>").value == '') {
                    alert("Please continuation BookingID");
                    document.getElementById("<%=txtSearchMobile.ClientID %>").focus();
                    document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                    return false;
                }
            }

            var GuestName = document.getElementById('<%=txtGuestName.ClientID %>').value;
            GuestName = GuestName.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
            GuestName = GuestName.replace(/\s+$/, ""); 	//Removes Right Blank Spaces

            if (GuestName == '') {
                alert("Please Enter Guest Name");
                document.getElementById("<%=txtGuestName.ClientID %>").focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }

            if (document.getElementById("<%=ddlSelectCar.ClientID %>").value == '') {
                alert("Please Select  a Car");
                document.getElementById("<%=ddlSelectCar.ClientID %>").focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }

            var GuestName = document.getElementById('<%=txtGuestName.ClientID %>').value;
            GuestName = GuestName.replace(/^\s+/, ""); 	//Removes Left Blank Spaces
            GuestName = GuestName.replace(/\s+$/, ""); 	//Removes Right Blank Spaces

            if (GuestName == '') {
                alert('Please Enter Guest Name');
                document.getElementById('<%=txtGuestName.ClientID %>').focus();
                document.getElementById("<%=ChkSave.ClientID %>").value = "0";
                return false;
            }
            document.getElementById("<%=ChkSave.ClientID %>").value = "1";
        }

        function resetForm() {
            document.getElementById("<%=txtGuestName.ClientID %>").value = "";
            document.getElementById("<%=txtGuestMobile.ClientID %>").value = "";
            document.getElementById("<%=txtSearchCar.ClientID %>").value = "";
            document.getElementById("<%=ddlSelectCar.ClientID %>").length = 0;
            //document.getElementById("<'%=ddlPkg.ClientID %>").length = 0;
            document.getElementById("<%=txtAdjCost.ClientID %>").value = "";
            document.getElementById("<%=txtRemarks.ClientID %>").value = "";
        }

        function lblWait() {
            document.getElementById("<%=message1.ClientID %>").innerHTML = "Please wait...";
        }

        function ResetSearch() {
            document.getElementById("<%=txtSearchMobile.ClientID %>").value = "";
        }
    </script>
</asp:Content>
