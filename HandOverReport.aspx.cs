using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ccs;

public partial class HandOverReport : clsPageAuthorization
{
    ClsAdmin objAdmin = new ClsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindLocation();
            BindAgents();
        }
    }

    protected void BindLocation()
    {
        ddlLocations.DataTextField = "CityName";
        ddlLocations.DataValueField = "UnitCityID";
        DataSet dsLocation = new DataSet();
        dsLocation = objAdmin.GetCity2(Convert.ToInt32(Session["UserID"]));
        ddlLocations.DataSource = dsLocation;
        ddlLocations.DataBind();
    }

    protected void BindAgents()
    {
        ddlAgents.DataTextField = "Name";
        ddlAgents.DataValueField = "SysUserID";
        DataSet dsAgents = new DataSet();
        dsAgents = objAdmin.GetAgents(Convert.ToInt32(Session["UserID"]));
        ddlAgents.DataSource = dsAgents;
        ddlAgents.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int UnitCityID = Convert.ToInt32(ddlLocations.SelectedValue);
        int SysUserID = Convert.ToInt32(ddlAgents.SelectedValue);
        string fromDate = txtFromDate.Text;
        string toDate = txtToDate.Text;
        ClsAdmin objadmin = new ClsAdmin();
        DataTable dtEx = new DataTable();
        DataSet dsExcel = new DataSet();

        Response.Cache.SetExpires(DateTime.Now.AddSeconds(1));
        Response.Clear();
        
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.AppendHeader("content-disposition", "attachment;filename=HandOverReport.ods");
        }
        else
        {
            Response.AppendHeader("content-disposition", "attachment;filename=HandOverReport.xls");
        }
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (ExportOption.SelectedItem.ToString() == "Open Office")
        {
            Response.ContentType = "application/vnd.oasis.opendocument.spreadsheet";
        }
        else
        {
            Response.ContentType = "application/vnd.ms-excel";
        }

        this.EnableViewState = false;
        Response.Write("\r\n");

        if (FinYear.SelectedItem.ToString() == "2016-2025")
        {
            dsExcel = objadmin.GetHandOverReport(UnitCityID, SysUserID, fromDate, toDate);
        }
        else // (FinYear.SelectedItem.ToString() == "2009-2010")
        {
            dsExcel = objadmin.GetHandOverReport_0910(UnitCityID
                , SysUserID, fromDate, toDate, FinYear.SelectedItem.ToString());
        }
        
        //int totalCount = int.Parse(dsExcel.Tables[0].Rows.Count.ToString());
        //if (totalCount > 0)
        //{
        //    DataRow dr;
        //    dr = dsExcel.Tables[0].NewRow();
        //    dsExcel.Tables[0].Rows.Add(dr);
        //}

        dtEx = dsExcel.Tables[0];
        Response.Write("<table border = 1 align = 'center'  width = 100%> ");
        int[] iColumns = { 0, 1, 2, 3, 4, 5, 6, 7, 8};
        for (int i = 0; i < dtEx.Rows.Count; i++)
        {
            if (i == 0)
            {
                Response.Write("<tr>");
                for (int j = 0; j < iColumns.Length; j++)
                {
                    Response.Write("<td align='center'><b>" + dtEx.Columns[iColumns[j]].Caption.ToString() + "</b></td>");
                }
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            for (int j = 0; j < iColumns.Length; j++)
            {
                    Response.Write("<td align='center'>" + dtEx.Rows[i][iColumns[j]].ToString() + "</td>");
            }
            Response.Write("</tr>");
        }
        Response.Write("</table>");
        Response.End();
    }
}