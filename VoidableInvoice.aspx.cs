using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ccs;

public partial class VoidableInvoice : clsPageAuthorization
{
    ClsAdmin objAdmin = new ClsAdmin();
    DataSet GetSummary = new DataSet();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindCity();
            btngetdetail.Attributes.Add("onclick", "return validate();");
        }
    }

    protected void BindCity()
    {
        lbllocation.DataTextField = "CityName";
        lbllocation.DataValueField = "CityID";
        DataSet dscity = new DataSet();
        dscity = objAdmin.GetCity(Convert.ToInt32(Session["UserID"]));
        lbllocation.DataSource = dscity;
        lbllocation.DataBind();
    }
    
    protected void getDetail()
    {
        GetSummary = objAdmin.Get_VoidDetail(Convert.ToInt32(lbllocation.SelectedValue));
        GridView1.DataSource = GetSummary;
        GridView1.DataBind();
    }

    protected void btngetdetail_Click(object sender, EventArgs e)
    {
        getDetail();
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "PrintBookingID")
        {
            string strscript = "", printurl = "";
            printurl = "RetailInvoicePrint.aspx?Bookingid=" + e.CommandArgument.ToString();
            strscript = "<script language='javascript'>window.open('" + printurl + "','HistoryWin','width=1000,height=800,top=0,left=0,menubar=0,scrollbars=yes,resizable=1');</script>";
            if (!ClientScript.IsClientScriptBlockRegistered(strscript))
                Page.ClientScript.RegisterClientScriptBlock(typeof(Label), "myScript", strscript);
        }

        if (e.CommandName == "VoidBookingID")
        { 
        
        }

        if (e.CommandName == "RejectBookingID")
        {
            objAdmin.RejecteInvoice(Convert.ToInt32(e.CommandArgument));
            Page.RegisterClientScriptBlock("script","<script>alert('BookingID Rejected!')</script>");
            GridView1.DataSource = null;
            GridView1.DataBind();

        }
    }


}
