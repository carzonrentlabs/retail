<%@ Page Language="C#" MasterPageFile="~/CIPL.master" AutoEventWireup="true" CodeFile="PostDataBulk.aspx.cs"
    Inherits="PostDataBulk" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPage" runat="Server">
    <table width="100%" align="center">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMessage" runat="server" Text="" Font-Bold="true" ForeColor="red"
                    Font-Size="small"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 23px; width: 412px;">
            </td>
            <td style="height: 23px">
            </td>
        </tr>
        <tr>
            <td style="height: 4px; vertical-align: middle; text-align: right; width: 412px;">
                From:
            <asp:TextBox ID="txtFrom" runat="server"></asp:TextBox>
                 
                <cc1:CalendarExtender ID="CaltxtFromDate" TargetControlID="txtFrom" Format="dd-MMM-yyyy" runat="server">
                </cc1:CalendarExtender>
                &nbsp;
            </td>
            <td style="height: 4px; vertical-align: middle; text-align: left;">
                To:
                <asp:TextBox ID="txtTo" runat="server"></asp:TextBox>
                   
                    &nbsp;
                        <cc1:CalendarExtender ID="CaltxtToDate" TargetControlID="txtTo" Format="dd-MMM-yyyy" runat="server">
                </cc1:CalendarExtender>
               
                <asp:Button ID="btnsubmit" runat="server"  Text="Get Data" OnClick="btnsubmit_Click"  />
            </td>
        </tr>
        <tr>
            <td style="height: 23px; width: 412px;">
            </td>
            <td style="height: 23px">
            </td>
        </tr>
         <tr>
            <td colspan="2" style="text-align:center">
                <%--<asp:GridView ID="Grv_Summary" runat="server" AutoGenerateColumns="False" OnRowCommand="Grv_Summary_RowCommand" CellSpacing="2" CellPadding="2">--%>
                <asp:GridView ID="Grv_Summary" runat="server" AutoGenerateColumns="False" CellSpacing="2" CellPadding="2">
                    <Columns>
                    <asp:TemplateField HeaderText="BookingID">
                        <ItemTemplate>
                             <asp:Label ID="lblBookingId" runat="server" Text='<%#Eval("BookingID") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Company Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCompany" runat="server" Text='<%#Eval("CompanyName") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Vehicle No.">
                        <ItemTemplate>
                            <asp:Label ID="lblVehicle" runat="server" Text='<%#Eval("VehicleNo") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Staff Id">
                        <ItemTemplate>
                            <asp:Label ID="lblStaffId" runat="server" Text='<%#Eval("StaffID") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>  
                    <asp:TemplateField HeaderText="Customer Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCustomerName" runat="server" Text='<%#Eval("CustomerName") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Out">
                        <ItemTemplate>
                            <asp:Label ID="lblDateOut" runat="server" Text='<%#Eval("DateOut") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText="Date In">
                        <ItemTemplate>
                            <asp:Label ID="lblDateDateIn" runat="server" Text='<%#Eval("DateIn") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>  
                       <asp:TemplateField HeaderText="Basic">
                        <ItemTemplate>
                            <asp:Label ID="lblBasic" runat="server" Text='<%#Eval("Basic") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>   
                     <asp:TemplateField HeaderText="Total Cost">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalCost" runat="server" Text='<%#Eval("TotalAmount") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>  
                      <asp:TemplateField HeaderText="Booking Type">
                        <ItemTemplate>
                            <asp:Label ID="lblBookingType" runat="server" Text='<%#Eval("BookingType") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>              
                     <asp:TemplateField HeaderText="Send" >
                           <ItemTemplate>
                               <asp:LinkButton ID="lnkBtn_Send" runat="server" CommandArgument='<%#Eval("BookingID") %>' CausesValidation="False" CommandName="Send"
                                   Text="Send"></asp:LinkButton>
                           </ItemTemplate>
                       </asp:TemplateField>                   
                 
                   </Columns>
                </asp:GridView>
               
            </td>
           
        </tr>
    </table>
     <div>
            <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
                top: 0px" name="CalFrame" src="App_Themes/CorCalendar.htm" frameborder="0" width="260"
                scrolling="no" height="182"></iframe>
</div>
<script language="javascript" type="text/javascript">
function validateform()
{
   
    if(document.getElementById("<%=txtFrom.ClientID %>").value == '')
    {
        alert("Please Enter From Date.");
        document.getElementById('<%=txtFrom.ClientID %>').focus();
        return false;
    }
    if(document.getElementById("<%=txtTo.ClientID %>").value == '')
    {
        alert("Please Enter To Date.");
        document.getElementById('<%=txtTo.ClientID %>').focus();
        return false;
    }        
}
</script>
</asp:Content>
