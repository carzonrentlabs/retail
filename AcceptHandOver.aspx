<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AcceptHandOver.aspx.cs" Inherits="AcceptHandOver" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<script language="javascript" src="App_Themes/CommonScript.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
function ShowMessage() 
{ 
    //alert('Current User is not Given Handover..');
    if(confirm('Current User is not given handover. Do You want to continue?'))
    {
        //window.location.href ='RetailBookingMobile.aspx';
        window.location.href = 'RetailBooking.aspx';
    }
    else
    {
        window.location.href='https://insta.carzonrent.com/login.asp';
    }
}
function goBack()
{
    window.history.go(-1)
}
</script>

<form id="form1" runat="server">
    <div>
        <tr>
            <td>
                <br>
                <br>
                <br>
                <br>
            </td>
        </tr>
        <tr align="center">
            <td>
                <table align="center">
                    <tr>
                        <td align="center" colspan="2" style="height: 21px">
                            <b><u>RETAIL (AIRPORT) LOGIN</u></b></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 199px">
                            <b>User - </b>
                            <asp:textbox id="txtuser" runat="server" borderstyle="None" enabletheming="True"
                                readonly="True"></asp:textbox>
                        </td>
                        <td align="left">
                            <b>Location - </b>
                            <asp:textbox id="txtlocation" runat="server" borderstyle="None" readonly="True"></asp:textbox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:gridview id="GridView1" runat="server" autogeneratecolumns="False" font-names="Times New Roman"
                                backcolor="LightGoldenrodYellow" bordercolor="Tan" borderwidth="1px" cellpadding="2"
                                forecolor="Black" gridlines="Both">
                                <Columns>
                                <asp:BoundField DataField="HandOverDate" HeaderText="  Date  "></asp:BoundField>
                                <asp:BoundField DataField="HandedOver_Cash" HeaderText="  CASH  "></asp:BoundField>
                                <asp:BoundField DataField="HandedOver_Amex" HeaderText="  AMEX  "></asp:BoundField>
                                <asp:BoundField DataField="HandedOver_HDFC" HeaderText="  HDFC  "></asp:BoundField>
                                <asp:BoundField DataField="HandedOver_EWallet" HeaderText="  EWallet  "></asp:BoundField>
                                <asp:BoundField DataField="TOTAL" HeaderText="  TOTAL  "></asp:BoundField>
                                </Columns>

<FooterStyle BackColor="Tan"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue"></PagerStyle>

<SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite"></SelectedRowStyle>

<HeaderStyle BackColor="Tan" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="PaleGoldenrod"></AlternatingRowStyle>
</asp:gridview>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 199px" align="center">
                            <asp:button id="btnAccept" text="Accept" runat="server" onclientclick="return confirm('Are You Sure You want to Accept the HandOver');"
                                onclick="btnAccept_Click"></asp:button>
                        </td>
                        <td align="center">
                            <asp:button id="btnReject" text="Reject" runat="server" onclientclick="return confirm('Are You Sure You Want to Reject');"
                                onclick="btnReject_Click"></asp:button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </div>
</form>
