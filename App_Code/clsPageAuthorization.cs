using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace ccs
{
    public class clsPageAuthorization : System.Web.UI.Page
    {
        /// <summary>
        /// Class Constructor - binds the Load event
        /// </summary>
        public clsPageAuthorization()
        {
            base.Load += new EventHandler(clsPageAuthorization_Load);
        }

        /// <summary>
        /// Load Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void clsPageAuthorization_Load(object sender, EventArgs e)
        {
            //CheckLang();
            CheckUser();
            CacheCleanUp();
        }

        /// <summary>
        /// Method to check whether session is valid or not
        /// </summary>
        private void CheckUser()
        {
            if (Session["Userid"] == null)
            {
                //Not a valid user; Redirect user to login page
                Response.Redirect("https://insta.carzonrent.com/login.asp");
            }
        }

        /// <summary>
        /// Method to Handle cache memory for the page
        /// </summary>
        private void CacheCleanUp()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }

        //private void CheckLang()
        //{
        //    if (Session["Language"] == null)
        //    {
        //        //Not a valid user; Redirect user to login page
        //        Response.Redirect("Login.aspx");
        //    }
        //}
    }
}