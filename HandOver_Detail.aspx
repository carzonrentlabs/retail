<%@ Page Language="C#" MasterPageFile="~/CIPL.master" AutoEventWireup="true" CodeFile="HandOver_Detail.aspx.cs"
    Inherits="HandOver_Detail" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphPage">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <center>
                <table>
                    <tbody align="left">
                        <tr>
                            <td style="width: 100px;" align="left">
                                <b>User Name :</b></td>
                            <td style="width: 155px;">
                                <asp:TextBox ID="lblUserName" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    Width="155px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td style="width: 60px;">
                                <b>Date :</b>
                            </td>
                            <td style="width: 80px;">
                                <asp:TextBox ID="lblDate" runat="server" Font-Size="Small" Font-Names="Verdana" Width="80px"
                                    ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox>
                            </td>
                            <td style="width: 80px;">
                                <b>Location :</b>
                            </td>
                            <td style="width: 130px;">
                                <asp:TextBox ID="lblLocation" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    Width="130px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table border="1">
                    <tbody>
                        <tr>
                            <td style="height: 23px" align="center" bgcolor="tan" colspan="6">
                                <b>SUMMARY OF HANDOVER</b>
                            </td>
                        </tr>
                        <tr bgcolor="#f7f9cc">
                            <td style="width: 110px">
                                PARTICULARS</td>
                            <td>
                                CASH</td>
                            <td>
                                AMEX</td>
                            <td>
                                HDFC</td>
                            <td>
                                EWallet</td>
                            <td>
                                TOTAL</td>
                        </tr>
                        <tr bgcolor="palegoldenrod">
                            <td style="height: 24px; white-space: nowrap;">
                                Opening Balance</td>
                            <td style="height: 24px">
                                <asp:TextBox ID="lblCashbal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td style="height: 24px">
                                <asp:TextBox ID="lblAmexbal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td style="height: 24px">
                                <asp:TextBox ID="lblHDFCbal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td style="height: 24px">
                                <asp:TextBox ID="lblEWalletbal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td style="height: 24px">
                                <asp:TextBox ID="lblTotbal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#f7f9cc">
                            <td>
                                Sale</td>
                            <td>
                                <asp:TextBox ID="lblCashSale" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblAmexSale" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblHDFCSale" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblEWalletSale" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblTotSale" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="palegoldenrod">
                            <td style="white-space: nowrap;">
                                Rejected from Voidable by the Manager</td>
                            <td>
                                <asp:TextBox ID="lblCashRejected" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblAmexRejected" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblHDFCRejected" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblEWalletRejected" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblTotlRejected" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="#f7f9cc">
                            <td style="font-weight: bold">
                                Total</td>
                            <td>
                                <asp:TextBox ID="lblCashTotal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblAmexTotal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblHDFCTotal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblEWalletTotal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblTotlTotal" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                        </tr>
                        <tr bgcolor="palegoldenrod">
                            <td>
                                Voidable Invoices to be selected</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr bgcolor="#f7f9cc">
                            <td>
                                Booking ID</td>
                            <td>
                                <asp:TextBox ID="txtBookingID" TabIndex="0" runat="server" Width="120px" Text=""></asp:TextBox></td>
                            <td>
                                <asp:Button ID="btnBookingId" TabIndex="1" OnClick="btnBookingId_Click" runat="server"
                                    Width="120px" Text="Add BookingID"></asp:Button></td>
                            <td colspan="3">
                                <asp:TextBox ID="lblError" runat="server" Font-Size="X-Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="231px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="False" Visible="False"></asp:TextBox>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="6">
                                <asp:GridView ID="GridView1" runat="server" ForeColor="Black" BackColor="LightGoldenrodYellow"
                                    Width="100%" BorderColor="Tan" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="False"
                                    GridLines="Both" BorderWidth="1px" CellPadding="2">
                                    <Columns>
                                        <asp:BoundField DataField="BookingID" HeaderText="Booking ID"></asp:BoundField>
                                        <asp:BoundField DataField="MODE" HeaderText="Mode"></asp:BoundField>
                                        <asp:BoundField DataField="TotalCost" HeaderText="Amount"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Reasons">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Reasons" Text='<%#Bind("Reason") %>' runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remove">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Bind("BookingID") %>'
                                                    CommandName="RemoveBookingID">Remove</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="Tan"></FooterStyle>
                                    <PagerStyle HorizontalAlign="Center" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue">
                                    </PagerStyle>
                                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite"></SelectedRowStyle>
                                    <HeaderStyle BackColor="Tan" Font-Bold="True"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="PaleGoldenrod"></AlternatingRowStyle>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr bgcolor="palegoldenrod">
                            <td style="font-weight: bold">
                                Total</td>
                            <td>
                                <asp:TextBox ID="lblCashTotalVoidable" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblAmexTotalVoidable" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblHDFCTotalVoidable" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblEWalletTotalVoidable" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:Button ID="btnVoidable" OnClick="btnVoidable_Click" runat="server" Text="Save"
                                    __designer:wfdid="w1"></asp:Button></td>
                        </tr>
                        <tr bgcolor="palegoldenrod">
                            <td style="white-space: nowrap;">
                                Hotel Invoices to be selected</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr bgcolor="#f7f9cc">
                            <td>
                                Booking ID</td>
                            <td>
                                <asp:TextBox ID="txtBookingIDNew" TabIndex="0" runat="server" Width="120px" Text=""></asp:TextBox></td>
                            <td>
                                <asp:Button ID="btnBookingIdNew" TabIndex="1" OnClick="btnBookingIdNew_Click" runat="server"
                                    Width="120px" Text="Add BookingID"></asp:Button></td>
                            <td colspan="3">
                                <asp:TextBox ID="lblErrorNew" runat="server" Font-Size="X-Small" Font-Names="Verdana"
                                    BackColor="#F7F9CC" Width="231px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="False" Visible="False"></asp:TextBox>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="6">
                                <asp:GridView ID="GridViewNew" runat="server" ForeColor="Black" BackColor="LightGoldenrodYellow"
                                    Width="100%" BorderColor="Tan" OnRowCommand="GridViewNew_RowCommand" AutoGenerateColumns="False"
                                    GridLines="Both" BorderWidth="1px" CellPadding="2">
                                    <Columns>
                                        <asp:BoundField DataField="BookingID" HeaderText="Booking ID"></asp:BoundField>
                                        <asp:BoundField DataField="MODE" HeaderText="Mode"></asp:BoundField>
                                        <asp:BoundField DataField="TotalCost" HeaderText="Amount"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Reasons">
                                            <ItemTemplate>
                                                <asp:TextBox ID="Reasons" Text='<%#Bind("Reason") %>' runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remove">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%# Bind("BookingID") %>'
                                                    CommandName="RemoveBookingID">Remove</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="Tan"></FooterStyle>
                                    <PagerStyle HorizontalAlign="Center" BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue">
                                    </PagerStyle>
                                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite"></SelectedRowStyle>
                                    <HeaderStyle BackColor="Tan" Font-Bold="True"></HeaderStyle>
                                    <AlternatingRowStyle BackColor="PaleGoldenrod"></AlternatingRowStyle>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr bgcolor="palegoldenrod">
                            <td style="font-weight: bold">
                                Total</td>
                            <td>
                                <asp:TextBox ID="lblCashTotalHotel" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblAmexTotalHotel" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblHDFCTotalHotel" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="lblEWalletTotalHotel" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Font-Bold="True"></asp:TextBox></td>
                            <td>
                                <asp:Button ID="btnHotel" OnClick="btnHotel_Click" runat="server" Text="Save" __designer:wfdid="w2">
                                </asp:Button></td>
                        </tr>
                        <tr bgcolor="#f7f9cc">
                            <td style="white-space: nowrap;">
                                Cash Deposited / Settlement Run</td>
                            <td style="height: 42px">
                                <asp:TextBox onblur="HandOver()" ID="lblCashDeposited" runat="server" Font-Size="Small"
                                    Font-Names="Verdana" Width="100px"></asp:TextBox>&nbsp;</td>
                            <td style="height: 42px">
                                &nbsp;<asp:TextBox onblur="HandOver()" ID="lblAmexSettlement" runat="server" Font-Size="Small"
                                    Font-Names="Verdana" Width="100px"></asp:TextBox></td>
                            <td style="height: 42px">
                                &nbsp;<asp:TextBox onblur="HandOver()" ID="lblHDFCSettlement" runat="server" Font-Size="Small"
                                    Font-Names="Verdana" Width="100px"></asp:TextBox></td>
                            <td style="height: 42px">
                                &nbsp;<asp:TextBox onblur="HandOver()" ID="lblEWalletSettlement" runat="server" Font-Size="Small"
                                    Font-Names="Verdana" Width="100px"></asp:TextBox></td>
                            <td style="height: 42px">
                                &nbsp;</td>
                        </tr>
                        <tr bgcolor="palegoldenrod">
                            <td style="white-space: nowrap;">
                                Cash / Credit Card Slips to Handed Over</td>
                            <td>
                                <asp:TextBox ID="CashHandOver" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox>&nbsp;</td>
                            <td>
                                &nbsp;<asp:TextBox ID="AmexHandOver" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"
                                    Height="18px"></asp:TextBox></td>
                            <td>
                                &nbsp;<asp:TextBox ID="HDFCHandOver" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                &nbsp;<asp:TextBox ID="EWalletHandOver" runat="server" Font-Size="Small" Font-Names="Verdana"
                                    BackColor="PaleGoldenrod" Width="56px" ReadOnly="True" BorderStyle="None" BorderColor="White"></asp:TextBox></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr bgcolor="#f7f9cc">
                            <td>
                                <b>Handover To</b></td>
                            <td>
                                (Select the Reliever)</td>
                            <td>
                                <asp:DropDownList ID="Reliver" runat="server" Width="100px">
                                </asp:DropDownList></td>
                            <td>
                                <asp:Button ID="Handover" OnClick="Handover_Click" runat="server" Text="Handover"></asp:Button></td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
function HandOver()
{
    //alert(document.getElementById('<%=lblAmexTotal.ClientID %>').value);
    //alert(document.getElementById('<%=lblAmexTotalVoidable.ClientID %>').value);
    if(document.getElementById('<%=lblCashDeposited.ClientID %>').value =="")
    {
        document.getElementById('<%=CashHandOver.ClientID %>').value = 
        parseFloat(parseFloat(document.getElementById('<%=lblCashTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblCashTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblCashTotalHotel.ClientID %>').value));
    }
    else
    {
        document.getElementById('<%=CashHandOver.ClientID %>').value
        = parseFloat(parseFloat(document.getElementById('<%=lblCashTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblCashDeposited.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblCashTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblCashTotalHotel.ClientID %>').value));
    }
    if(document.getElementById('<%=lblAmexSettlement.ClientID %>').value =="")
    {
        document.getElementById('<%=AmexHandOver.ClientID %>').value 
        = parseFloat(parseFloat(document.getElementById('<%=lblAmexTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblAmexTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblAmexTotalHotel.ClientID %>').value));
    }
    else
    {
        document.getElementById('<%=AmexHandOver.ClientID %>').value 
        = parseFloat(parseFloat(document.getElementById('<%=lblAmexTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblAmexSettlement.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblAmexTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblAmexTotalHotel.ClientID %>').value));
    }
    if(document.getElementById('<%=lblHDFCSettlement.ClientID %>').value =="")
    {
        document.getElementById('<%=HDFCHandOver.ClientID %>').value = 
        parseFloat(parseFloat(document.getElementById('<%=lblHDFCTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblHDFCTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblHDFCTotalHotel.ClientID %>').value));
    }
    else
    {
        document.getElementById('<%=HDFCHandOver.ClientID %>').value = 
        parseFloat(parseFloat(document.getElementById('<%=lblHDFCTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblHDFCSettlement.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblHDFCTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblHDFCTotalHotel.ClientID %>').value));
    }
    if(document.getElementById('<%=lblEWalletSettlement.ClientID %>').value =="")
    {
        document.getElementById('<%=EWalletHandOver.ClientID %>').value = 
        parseFloat(parseFloat(document.getElementById('<%=lblEWalletTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblEWalletTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblEWalletTotalHotel.ClientID %>').value));
    }
    else
    {
        document.getElementById('<%=EWalletHandOver.ClientID %>').value = 
        parseFloat(parseFloat(document.getElementById('<%=lblEWalletTotal.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblEWalletSettlement.ClientID %>').value) 
        - parseFloat(document.getElementById('<%=lblEWalletTotalVoidable.ClientID %>').value)
        - parseFloat(document.getElementById('<%=lblEWalletTotalHotel.ClientID %>').value));
    }
    //alert("You are Here..");
    //return false;
}
function validateForm()
{
    if(document.getElementById('<%=Reliver.ClientID%>').value=="")
    {
        alert("Please Selected the Reliver Name");
        document.getElementById('<%=Reliver.ClientID%>').focus();
        return false;
    }
}
function goBack()
{
    window.history.go(-1)
}
    </script>

</asp:Content>
