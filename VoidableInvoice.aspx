<%@ Page Language="C#" MasterPageFile="~/CIPL.master" AutoEventWireup="true" CodeFile="VoidableInvoice.aspx.cs" Inherits="VoidableInvoice" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphPage">
<table>
<tr>
<td align="left"><b>Location-</b></td>
<td>
<asp:DropDownList ID="lbllocation" runat="server"></asp:DropDownList>
<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0" ControlToValidate="lbllocation"
        ErrorMessage="*"></asp:RequiredFieldValidator>--%>
</td>
<td>
<asp:Button ID="btngetdetail" Text="Submit" runat="server" CausesValidation="true" OnClick="btngetdetail_Click">
</asp:Button>
</td>
</tr>
<tr><td align="left" colspan="3"><b><u>List of Pending Voidable Invoices</u></b></td></tr>
<tr>
<td colspan="3">
<asp:GridView id="GridView1" runat="server" AutoGenerateColumns="False" Font-Names="Times New Roman" OnRowCommand="GridView1_RowCommand">
<Columns>
<asp:BoundField DataField="BatchID" HeaderText="  Batch ID  "></asp:BoundField>
<asp:BoundField DataField="AccountingDate" HeaderText="  Date  "></asp:BoundField>
<asp:BoundField DataField="BookingID" HeaderText="  Booking ID  "></asp:BoundField>
<asp:BoundField DataField="UserName" HeaderText="  UserName  "></asp:BoundField>
<asp:BoundField DataField="VehicleNo" HeaderText="  Vehicle No.  "></asp:BoundField>
<asp:BoundField DataField="Mode" HeaderText="  Mode  "></asp:BoundField>
<asp:BoundField DataField="Amount" HeaderText="  Total Billing  "></asp:BoundField>
<asp:BoundField DataField="Reason" HeaderText="  Remarks  "></asp:BoundField>
<asp:TemplateField HeaderText="Print">
<ItemTemplate>
<asp:LinkButton ID="lnkPrint" runat="server" CommandArgument='<%# Bind("BookingID") %>' 
 CommandName="PrintBookingID">&nbsp;&nbsp;Print&nbsp;&nbsp;</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Void">
<ItemTemplate>
<asp:LinkButton ID="lnkVoid" runat="server" CommandArgument='<%# Bind("BookingID") %>' 
 CommandName="VoidBookingID" OnClientClick="return confirm('Are You Sure You Want to Void the Invoice');">&nbsp;&nbsp;Void&nbsp;&nbsp;</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Reject">
<ItemTemplate>
<asp:LinkButton ID="lnkReject" runat="server" CommandArgument='<%# Bind("BookingID") %>' 
 CommandName="RejectBookingID" OnClientClick="return confirm('Are You Sure You Want to Reject the Invoice');">&nbsp;&nbsp;Reject&nbsp;&nbsp;</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
function validate()
{
    if(document.getElementById('<%=lbllocation.ClientID %>').value=="0")
    {
        alert("Please select Location.");
        document.getElementById('<%=lbllocation.ClientID %>').focus();
        return false;
    }
}
</script>
</asp:Content>