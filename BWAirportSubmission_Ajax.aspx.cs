using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class BWAirportSubmission_Ajax : System.Web.UI.Page
{
    int LocationID;
    DateTime txtDate;
    ClsAdmin objAdmin2 = new ClsAdmin();
    ClsAdmin objAdmin = new ClsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        LocationID = Convert.ToInt32(Request.QueryString["LocationID"].ToString());
        txtDate = Convert.ToDateTime(Request.QueryString["txtDate"].ToString());
        getAjaxDetails(LocationID, txtDate);
    }

    protected void getAjaxDetails(int LocationID, DateTime txtDate)
    {
        string rtnStr = "";

        DataSet dsGetAjaxDetails = new DataSet();
        dsGetAjaxDetails = objAdmin.GetAjax_BWAirportDetails(LocationID, txtDate);
        if (dsGetAjaxDetails.Tables[0].Rows.Count > 0)
        {
            rtnStr = dsGetAjaxDetails.Tables[0].Rows[0]["CashBF"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["CashDuties"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["CashAmount"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["CashCollection"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["AmexBF"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["AmexDuties"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["AmexAmount"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["AmexCollection"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["HdfcBF"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["HDFCDuties"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["HdfcAmount"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["HDFCCollection"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["EWalletBF"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["EWalletDuties"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["EWalletAmount"].ToString() + '#';
            rtnStr = rtnStr + dsGetAjaxDetails.Tables[0].Rows[0]["EWalletCollection"].ToString();
        }

        Response.Write(rtnStr);
        Response.End();
    }
}
