<%@ Page Language="C#" MasterPageFile="~/CIPL.master" StylesheetTheme="StyleSheet"
    AutoEventWireup="true" CodeFile="RptDateWiseSummary.aspx.cs" Inherits="Reports_RptDateWiseSummary"
    Title="COR - Retail Module" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPage" runat="Server">

    <script language="javascript" src="App_Themes/CommonScript.js" type="text/javascript"></script>

    <table width="90%" align="center" style="margin-top: 50px">
        <tr>
            <td align="center" style="padding: 1px 1px 1px 1px; background-color: Black">
                <table width="100%" align="center" style="padding: 5px 5px 5px 5px; background-color: White">
                    <tr>
                        <td style="height: 90px">
                            <table width="100%" align="center">
                                <tr>
                                    <td style="width: 50%">
                                        Select Location:
                                        <asp:DropDownList ID="ddlLocations" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                        </asp:DropDownList></td>
                                    <td>
                                        Select Agent:
                                        <asp:DropDownList ID="ddlAgents" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="height: 4px; vertical-align: middle; text-align: right">
                                        From:
                                        <asp:TextBox ID="TextBoxFrom" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CaltxtFromDate" TargetControlID="TextBoxFrom" Format="dd-MMM-yyyy"
                                            runat="server">
                                        </cc1:CalendarExtender>
                                        &nbsp;
                                    </td>
                                    <td style="height: 4px; vertical-align: middle; text-align: left;">
                                        To:
                                        <asp:TextBox ID="TextBoxTo" runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CaltxtToDate" TargetControlID="TextBoxTo" Format="dd-MMM-yyyy"
                                            runat="server">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="Button2" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                Text="Get Sale" OnClick="Button2_Click" />
                            <asp:Button ID="Button1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                Text="Get Collection" OnClick="Button1_Click" />
                            <asp:Button ID="Button3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small"
                                Text="Reconciliation" OnClick="Button3_Click" />
                            <strong>Export In</strong>
                            <asp:DropDownList ID="ExportOption" runat="server" Font-Names="Verdana" Font-Size="X-Small">
                                <asp:ListItem Text="MS Office" Value="MS Office"></asp:ListItem>
                                <asp:ListItem Text="Open Office" Value="Open Office"></asp:ListItem>
                            </asp:DropDownList>
                            <strong>Select Financial Year</strong>
                            <asp:DropDownList ID="FinYear" runat="server"
                                Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False">
                                <asp:ListItem Text="2016-2025" Value="2016-2025"></asp:ListItem>
                                <asp:ListItem Text="2010-2015" Value="2010-2015"></asp:ListItem>
                                <asp:ListItem Text="2009-2010" Value="2009-2010"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div>
        <iframe id="CalFrame" style="z-index: 999; left: -500px; visibility: visible; position: absolute;
            top: 0px" name="CalFrame" src="App_Themes/CorCalendar.htm" frameborder="0" width="260"
            scrolling="no" height="182"></iframe>
    </div>
</asp:Content>
