using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Net;
using ccs;
using System.Xml;
public partial class PostData : System.Web.UI.Page
{
    DataSet GetBookingDetail = new DataSet();
    ClsAdmin objAdmin = new ClsAdmin();
    StringBuilder strXML = new StringBuilder();
    //public decimal ServiceTax = 0, EduTax = 0, HduTax = 0, ttlTax = 0, FinalAmt = 0, BasicAmt = 0, TaxAmt = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
        //btnsubmit.Attributes.Add("onclick", "return validateform();");

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        DateTime fromDate = Convert.ToDateTime(txtFrom.Text);
        DateTime toDate = Convert.ToDateTime(txtTo.Text);
        ds = objAdmin.GetBookingDetail_PostData(fromDate, toDate);
        Grv_Summary.DataSource = ds;
        Grv_Summary.DataBind();
    }
    protected void Grv_Summary_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string bookingid;
        DataSet GetBookingDetail = new DataSet();
        DataSet ds = new DataSet();
        //string strXML = "";
        StringBuilder strXML = new StringBuilder();
        string strXMLResponse = "";
        string booking = "", ReceiptNO = "", StatusResponse = "", ErrorCode = "", ErrorMsg = "";
        //CCTV varialble declaration Start 08 March 2015
        string transactionSessionId = string.Empty;
        string OldBookingId = string.Empty;
        DataSet dsBookingDetails = new DataSet();
        CCTVProxy.RetailInputWebService proxy = new CCTVProxy.RetailInputWebService();
        CCTVProxy.TransactionResponseObject obj = new CCTVProxy.TransactionResponseObject();
        CCTVProxy.TransactionResponseObject obj1 = new CCTVProxy.TransactionResponseObject();
        decimal unitPrice, subTotal;

        //CCTV variable declaration End 08 March 2015
        if (e.CommandName == "Send")
        {
            bookingid = e.CommandArgument.ToString();
            try
            {
                GetBookingDetail = objAdmin.GetBookingDetail_PostData_1(Convert.ToInt32(bookingid));                

                if (Convert.ToInt32(GetBookingDetail.Tables[0].Rows[0]["PickupCityId"].ToString()) == 33)
                {
                    int paymentType = 0;
                    if (GetBookingDetail.Tables[0].Rows[0]["PaymentMode"].ToString() == "Ca")
                    {
                        paymentType = 1;
                    }
                    else
                    {
                        paymentType = 3;
                    }
                    strXML = strXML.Append("<?xml version='1.0' encoding='UTF-8' ?>");
                    strXML = strXML.Append("<TransactionData xsi:noNamespaceSchemaLocation='http://localhost/_URL_/Carzonrent.xsd' "); //This is for live
                    strXML = strXML.Append("xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");
                    strXML = strXML.Append("<ServicePartnerNo>" + "1000000583" + "</ServicePartnerNo>"); //This is for live
                    strXML = strXML.Append("<Password>Crzornt</Password>");
                    strXML = strXML.Append("<Transactions> <Transaction>");
                    strXML = strXML.Append("<TransactionNo>" + GetBookingDetail.Tables[0].Rows[0]["BookingID"].ToString().Trim() + "</TransactionNo>");
                    strXML = strXML.Append("<OriginalRefNo>" + GetBookingDetail.Tables[0].Rows[0]["BookingID"].ToString().Trim() + "</OriginalRefNo>");
                    strXML = strXML.Append("<EntryType>" + 1 + "</EntryType>");
                    strXML = strXML.Append("<StoreNo>" + "CZ012" + "</StoreNo>");
                    strXML = strXML.Append("<POSNo>" + "OR101" + "</POSNo>");
                    strXML = strXML.Append("<StaffID>" + Convert.ToInt32(Session["UserID"]).ToString() + "</StaffID>");
                    strXML = strXML.Append("<StaffName>" + GetBookingDetail.Tables[0].Rows[0]["StaffName"].ToString().Trim() + "</StaffName>");
                    strXML = strXML.Append("<TransactionDate>" + GetBookingDetail.Tables[0].Rows[0]["AccountingDate"].ToString().Trim() + "</TransactionDate>");
                    strXML = strXML.Append("<TransactionTime>" + GetBookingDetail.Tables[0].Rows[0]["AccountingTime"].ToString().Trim() + "</TransactionTime>");
                    strXML = strXML.Append("<DiscountAmount>" + 00.00 + "</DiscountAmount>");
                    strXML = strXML.Append("<TotalDiscount>" + 00.00 + "</TotalDiscount>");
                    strXML = strXML.Append("<TableNo>0</TableNo>");
                    strXML = strXML.Append("<NoOfCovers>0</NoOfCovers>");
                    strXML = strXML.Append("<CustomerName>" + GetBookingDetail.Tables[0].Rows[0]["CustomerName"].ToString().Trim() + "</CustomerName>");
                    strXML = strXML.Append("<Address></Address>");
                    strXML = strXML.Append("<Gender></Gender>");
                    strXML = strXML.Append("<PassportNo></PassportNo>");
                    strXML = strXML.Append("<Nationality>" + "IN" + "</Nationality>");
                    strXML = strXML.Append("<PortofBoarding></PortofBoarding>");
                    strXML = strXML.Append("<PortofDisembarkation></PortofDisembarkation>");
                    strXML = strXML.Append("<FlightNo></FlightNo>");
                    strXML = strXML.Append("<SectorNo></SectorNo>");
                    strXML = strXML.Append("<BoardingPassNo></BoardingPassNo>");
                    strXML = strXML.Append("<SeatNo></SeatNo>");
                    strXML = strXML.Append("<Airlines></Airlines>");
                    strXML = strXML.Append("<ServiceChargeAmount>" + 0 + "</ServiceChargeAmount>");
                    strXML = strXML.Append("<NetTransactionAmount>" + GetBookingDetail.Tables[0].Rows[0]["Basic"].ToString().Trim() + "</NetTransactionAmount>");
                    strXML = strXML.Append("<GrossTransactionAmount>" + GetBookingDetail.Tables[0].Rows[0]["TotalAmount"].ToString().Trim() + "</GrossTransactionAmount>");
                    strXML = strXML.Append("<CustomerType>" + 0 + "</CustomerType>");
                    strXML = strXML.Append("<Items>");
                    strXML = strXML.Append("<Item>");
                    strXML = strXML.Append("<ItemCode>" + "Cab" + "</ItemCode>");
                    strXML = strXML.Append("<ItemDescription>" + "Delhi" + "</ItemDescription>");
                    strXML = strXML.Append("<ItemCategory>" + "Cab" + "</ItemCategory>");
                    strXML = strXML.Append("<ItemCategoryDescription>" + "Special Cab" + "</ItemCategoryDescription>");
                    strXML = strXML.Append("<ProductGroup>" + "Cab" + "</ProductGroup>");
                    strXML = strXML.Append("<ProductGroupDescription>" + GetBookingDetail.Tables[0].Rows[0]["VehicleAlloted"].ToString().Trim() + "</ProductGroupDescription>");
                    strXML = strXML.Append("<BarcodeNo >0</BarcodeNo>");
                    strXML = strXML.Append("<Quantity>" + 1 + "</Quantity>");
                    strXML = strXML.Append("<Price>" + GetBookingDetail.Tables[0].Rows[0]["Basic"].ToString().Trim() + "</Price>");
                    strXML = strXML.Append("<NetAmount>" + GetBookingDetail.Tables[0].Rows[0]["Basic"].ToString().Trim() + "</NetAmount>");
                    strXML = strXML.Append("<PriceInclusiveTax>" + 1 + "</PriceInclusiveTax>");
                    strXML = strXML.Append("<ChangedPrice>" + 1 + "</ChangedPrice>");
                    strXML = strXML.Append("<ScaleItem>" + 1 + "</ScaleItem>");
                    strXML = strXML.Append("<WeighingItem>" + 1 + "</WeighingItem>");
                    strXML = strXML.Append("<ItemSerialNo></ItemSerialNo>");
                    strXML = strXML.Append("<UOM>" + "Pics" + "</UOM>");
                    strXML = strXML.Append("<LineDiscount>" + 0 + "</LineDiscount>");
                    strXML = strXML.Append("<TotalDiscount>" + 0 + "</TotalDiscount>");
                    strXML = strXML.Append("<PeriodicDiscount>" + 0 + "</PeriodicDiscount>");
                    strXML = strXML.Append("<PromotionNo></PromotionNo>");
                    strXML = strXML.Append("<TaxAmount>" + Convert.ToDouble(00.00) + "</TaxAmount>");
                    strXML = strXML.Append("<TaxRate>" + Convert.ToDouble(00.00) + "</TaxRate>");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("</Item> ");
                    strXML = strXML.Append("</Items>");
                    strXML = strXML.Append("<ServiceCharges>");
                    strXML = strXML.Append("<Charge> ");
                    strXML = strXML.Append("<Type>" + 1 + "</Type> ");
                    strXML = strXML.Append("<TypeDescription>" + "Service Charges" + "</TypeDescription> ");
                    strXML = strXML.Append("<ServiceChargeAmount>0</ServiceChargeAmount>");
                    strXML = strXML.Append("<ServiceTaxAmount>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxAmt"].ToString().Trim() + "</ServiceTaxAmount> ");
                    strXML = strXML.Append("<ServiceTaxRate>" + GetBookingDetail.Tables[0].Rows[0]["ServiceTaxPercent"].ToString().Trim() + "</ServiceTaxRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessRate>" + GetBookingDetail.Tables[0].Rows[0]["EduCessPercent"].ToString().Trim() + "</ServiceTaxeCessRate> ");
                    strXML = strXML.Append("<ServiceTaxeCessAmount>" + GetBookingDetail.Tables[0].Rows[0]["EduTaxAmt"].ToString().Trim() + "</ServiceTaxeCessAmount> ");
                    strXML = strXML.Append("<ServiceTaxSHECessRate>" + GetBookingDetail.Tables[0].Rows[0]["HduCessPercent"].ToString().Trim() + "</ServiceTaxSHECessRate> ");
                    strXML = strXML.Append("<ServiceTaxSHECessAmount>" + GetBookingDetail.Tables[0].Rows[0]["HduTaxAmt"].ToString().Trim() + "</ServiceTaxSHECessAmount> ");
                    strXML = strXML.Append("</Charge> ");
                    strXML = strXML.Append("</ServiceCharges>");
                    strXML = strXML.Append("<Payments> ");
                    strXML = strXML.Append("<Payment> ");
                    strXML = strXML.Append("<TenderType>" + paymentType + "</TenderType> ");
                    strXML = strXML.Append("<CardNo></CardNo> ");
                    strXML = strXML.Append("<CurrencyCode>" + "INR" + "</CurrencyCode> ");
                    strXML = strXML.Append("<ExchangeRate>" + 1 + "</ExchangeRate> ");
                    strXML = strXML.Append("<AmountTendered>" + GetBookingDetail.Tables[0].Rows[0]["TotalAmount"].ToString().Trim() + "</AmountTendered> ");
                    strXML = strXML.Append("<AmountInCurrency>" + Convert.ToDouble(00.00) + "</AmountInCurrency> ");
                    strXML = strXML.Append("</Payment>");
                    strXML = strXML.Append("</Payments>");
                    strXML = strXML.Append("</Transaction>");
                    strXML = strXML.Append("</Transactions>");
                    strXML = strXML.Append("</TransactionData>");
    
                    WebReference.DIALService s1 = new WebReference.DIALService();

                    strXMLResponse = s1.SaveTransaction(strXML.ToString());
                    DataSet dt = new DataSet();
                    StringReader stream = new StringReader(strXMLResponse);
                    XmlTextReader reader = new XmlTextReader(stream);
                    dt.ReadXml(reader);
                    booking = dt.Tables[1].Rows[0]["Number"].ToString();
                    ReceiptNO = "";
                    StatusResponse = dt.Tables[1].Rows[0]["Status"].ToString();

                    if (StatusResponse == "Success")
                    {
                        ReceiptNO = "Success";
                        objAdmin.UpdateRetailStatus(Convert.ToInt32(booking), ReceiptNO, StatusResponse);
                        ClsAdmin.flagComplete = true;
                    }
                    else
                    {
                        ErrorCode = dt.Tables[1].Rows[0]["ErrorCode"].ToString();
                        ErrorMsg = dt.Tables[1].Rows[0]["ErrorMsg"].ToString();
                        StatusResponse = "Status : " + StatusResponse + ", Error Code : " + ErrorCode + "-Message: " + ErrorMsg;
                        objAdmin.UpdateRetailStatus(Convert.ToInt32(booking), ReceiptNO, StatusResponse);
                        lblMessage.Text = StatusResponse; //"Error!!!";
                    }
                    DateTime fromDate = Convert.ToDateTime(txtFrom.Text);
                    DateTime toDate = Convert.ToDateTime(txtTo.Text);
                    ds = objAdmin.GetBookingDetail_PostData(fromDate, toDate);
                    Grv_Summary.DataSource = ds;
                    Grv_Summary.DataBind();
                }

            }
            catch(Exception ex)
            {
                Page.RegisterClientScriptBlock("Script", "<script>alert('No Response!!!')</script>");
            }
        }
    }
}
