using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public class Amex_2Party 
{
    public bool errorExists;
    public   string txnResponseCode = "";
    public   string receiptNo = "";
    public string qsiResponseCode = "";
    public string acqResponseCode = "";
    public string authorizeID = "";
    public string batchNo = "";
    public string transactionNr = "";
    public string cardType = "";
    private System.Net.Sockets.TcpClient socket;
    private System.Net.Sockets.NetworkStream ns;

    // Exception message value
    private string exception = "No Exception Returned";
    private string socketCommands = "<em>Payment Client Socket Commands</em><br/><br/>";


    // _________________________________________________________________________

    /**
    * This method uses the QSIResponseCode retrieved from the Digital Receipt
    * and returns an appropriate description.
    *
    * @param vResponseCode string containing the QSIResponseCode
    *
    * @return description string containing the appropriate description
    *
    */
    private string getResponseDescription(string vResponseCode)
    {

        string result = "";

        if (vResponseCode == null || String.Compare(vResponseCode, "null", true) == 0 || vResponseCode.Equals(""))
        {
            result = "null response";
        }
        else
        {

            switch (vResponseCode)
            {
                case "0": result = "Transaction Successful"; break;
                case "1": result = "Transaction Declined"; break;
                case "2": result = "Bank Declined Transaction"; break;
                case "3": result = "No Reply from Bank"; break;
                case "4": result = "Expired Card"; break;
                case "5": result = "Insufficient Funds"; break;
                case "6": result = "Error Communicating with Bank"; break;
                case "7": result = "Payment Server detected an error"; break;
                case "8": result = "Transaction Type Not Supported"; break;
                case "9": result = "Bank declined transaction (Do not contact Bank)"; break;
                case "A": result = "Transaction Aborted"; break;
                case "B": result = "Transaction Declined - Contact the Bank"; break;
                case "C": result = "Transaction Cancelled"; break;
                case "D": result = "Deferred transaction has been received and is awaiting processing"; break;
                case "F": result = "3-D Secure Authentication failed"; break;
                case "I": result = "Card Security Code verification failed"; break;
                case "L": result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
                case "N": result = "Cardholder is not enrolled in Authentication scheme"; break;
                case "P": result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
                case "R": result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
                case "S": result = "Duplicate SessionID"; break;
                case "T": result = "Address Verification Failed"; break;
                case "U": result = "Card Security Code Failed"; break;
                case "V": result = "Address Verification and Card Security Code Failed"; break;
                default: result = "Unable to be determined"; break;
            }
        }
        return result;
    }


    // _________________________________________________________________________

    /**
    * This function uses the AVS Result Code retrieved from the Digital
    * Receipt and returns an appropriate description for this code.
    *
    * @param vAVSResultCode String containing the AVS Result Code
    * @return description String containing the appropriate description
    */
    //private string displayAVSResponse(string vAVSResultCode)
    //{
    //    string result = "";

    //    if (vAVSResultCode != null && !vAVSResultCode.Equals(""))
    //    {

    //        if (String.Compare(vAVSResultCode,"Unsupported",true) == 0)
    //        {
    //            result = "AVS not supported or there was no AVS data provided";
    //        }
    //        else
    //        {
    //            switch (vAVSResultCode)
    //            {
    //                case "Y" : result = "Exact match - address and 5 digit ZIP/postal code"; break;
    //                case "N" : result = "Address and ZIP/postal code not matched"; break;
    //                case "A" : result = "Address match only"; break;
    //                case "Z" : result = "5 digit ZIP/postal code matched, Address not Matched"; break;
    //                case "U" : result = "Address unavailable or not verified"; break;
    //                case "S" : result = "Service not supported or address not verified (international transaction)"; break;
    //                case "R" : result = "Issuer system is unavailable"; break;
    //                case "L" : result = "CM Name and Billing Postal Code match"; break;
    //                case "M" : result = "CM Name, Billing Address and Postal Code match"; break;
    //                case "0" : result = "AVS not requested"; break;
    //                case "K" : result = "CM Name matches"; break;
    //                case "G" : result = "Issuer does not participate in AVS (international transaction)"; break;
    //                case "X" : result = "Exact match - address and 9 digit ZIP/postal code"; break;
    //                case "W" : result = "9 digit ZIP/postal code matched, Address not Matched"; break;
    //                case "E" : result = "Address and ZIP/postal code not provided"; break;
    //                default  : result = "Unable to be determined"; break;
    //            }
    //        }
    //    }
    //    else
    //    {
    //        result = "null response";
    //    }
    //    return result;
    //}

    //______________________________________________________________________________

    /**
    * This function uses the CSC Result Code retrieved from the Digital
    * Receipt and returns an appropriate description for this code.
    *
    * @param vCSCResultCode string containing the CSC Result Code
    * @return description string containing the appropriate description
    */
    private string displayCSCResponse(string vCSCResultCode)
    {

        string result = "";
        if (vCSCResultCode != null && !vCSCResultCode.Equals(""))
        {

            if (String.Compare(vCSCResultCode, "Unsupported", true) == 0)
            {
                result = "CSC not supported or there was no CSC data provided";
            }
            else
            {

                switch (vCSCResultCode)
                {
                    case "M": result = "Exact code match"; break;
                    case "S": result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"; break;
                    case "P": result = "Code not processed"; break;
                    case "U": result = "Card issuer is not registered and/or certified"; break;
                    case "N": result = "Code invalid or not matched"; break;
                    default: result = "Unable to be determined"; break;
                }
            }
        }
        else
        {
            result = "null response";
        }
        return result;
    }

    // _________________________________________________________________________

    /**
     * This method sends the command to the Payment Client and retrieves the
     * response for the main body of the example. If an error is encountered
     * an error page is displayed and an Exception containing with the message
     * "Error Displayed" is thrown back to the main body of the example to
     * notify it of the error.
     *
     * @param command String that will be sent to the Payment Client Socket
     * @param debug a boolean value to sent socket output to screen
     *
     * @return cmdString String containing the Payment Client response
     *
     * @throws Exception Exception with the message "Error Displayed" is thrown
     * if an error is encountered and displayed.
     *
     */
    private string sendCommand(string command, bool debug)
    {
        try
        {
            // this code outputs socket commands to screen if debug is enabled
            // the output screen data is put through a filter to replace particular
            // data characters so they will display correctly in a HTML page
            if (debug)
            {
                string sendData = command.Replace(" ", "&nbsp;");
                sendData = sendData.Replace("<", "&lt;");
                sendData = sendData.Replace(">", "&gt;");
                sendData = sendData.Replace("\n", "<br/>");

                socketCommands += "<font color='#FF0066'>Sent: " + sendData + "</font><br/>";
            }

            /*
             * Send Data
             * ---------
             * We are performing a command to the Payment Client, the
             * command is terminated with a newline (linefeed) character.
             * We must add this character to the request data so that the
             * Payment Client sockets listener konws when it has received
             * all the data.
             */
            byte[] SendBuffer = new byte[command.Length + 1];
            SendBuffer = System.Text.Encoding.ASCII.GetBytes(command + "\n");
            ns.Write(SendBuffer, 0, SendBuffer.Length);

            /*
             * Receive Data
             * ------------
             * The command will return a String terminated with a newline
             * (linefeed) character.
             */
            string TextResponse = "";
            int BytesRead = 0;
            bool ContinueReading = true;
            while (ContinueReading)
            {
                byte[] ReceiveBuffer = new byte[socket.ReceiveBufferSize];
                BytesRead = ns.Read(ReceiveBuffer, 0, ReceiveBuffer.Length);

                // Store the received data to the TextResponse variable
                TextResponse = TextResponse + System.Text.Encoding.ASCII.GetString(ReceiveBuffer, 0, BytesRead);

                /*
                *  Check for a newline (linefeed) character to see if we
                * need to keep reading from the socket as the stream
                * buffer may have filled before we got the complete data.
                * We may have to read again. We know when we have all the
                * data as the Payment Client will terminate it's response
                * with a newline (linefeed) character. If for any reason
                * the message is malformed and we start to get into an
                * infinite loop, the buffer should empty so we can detect
                * this by checking for an empty buffer
                *  i.e.TextResponse = ""
                */
                if (TextResponse.EndsWith("\n") || TextResponse.Equals(""))
                {
                    ContinueReading = false;
                }
            }
            // Flush the stream to ensure it is empty before another
            // command is run.
            ns.Flush();

            // this code outputs socket commands to screen if debug is enabled
            if (debug)
            {
                socketCommands += "<font color='#008800'>Recd: " + TextResponse.TrimEnd('\n') + "</font><br/><br/>";
            }

            /*
             *  Return the response
             * -------------------
             * Use TrimEnd to remove the terminating newline (linefeed) or
             * carriage return characters to stop the text wrapping
             * unexpectedly as it is just a terminating character for the
             * socket response so that we know when the complete response has
             * been received.
             */
            return TextResponse.TrimEnd('\n');
        }
        catch (Exception ex2)
        {
            // There was an error
            exception = "(53) Socket Encounted Exception - sent: '" + command + "'<br/>Message: " + ex2.Message;
            if (ex2.StackTrace.Length > 0)
            {
                // Label_StackTrace.Text = ex2.ToString();
                //Panel_StackTrace.Visible = true;
            }
            return "(53) Socket Encounted Exception - sent: '" + command;
        }
    }

    // _________________________________________________________________________


    private string closeSocket()
    {
        // Use the following code to close the socket
        try
        {
            ns.Close();
            socket.Close();
            return "1";
        }
        catch (Exception ex)
        {
            exception = ex.Message;
            if (ex.StackTrace.Length > 0)
            {
                // Label_StackTrace.Text = ex.ToString();
                // Panel_StackTrace.Visible = true;
            }
            return "(53) Socket Encounted Exception on closing.";
        }
    }

    // _________________________________________________________________________

    public string AcquirePreAuth(string orderinfo, string merchantid, string purchaseamount, string mrchTxnRef, string cardno, string month, string yy)
    {

        // Define Static Constants
        // ***********************

        // This is the time that the example will allow for the socket to complete
        // sending a command to the Payment Client in milliseconds
        int SHORT_SOCKET_TIMEOUT = 50000000;
        int LONG_SOCKET_TIMEOUT = 500000000;

        /** This is the status character in the response that indicates that a
        command was run successfully */
        string OK = "1";

        // Initialisation
        // ==============

        /* *******************************************
        * Define Variables
        * *******************************************/

        //Panel_Debug.Visible = false;
        //Panel_Receipt.Visible = false;
        //Panel_Error.Visible = false;
        //Panel_StackTrace.Visible = false;

        // socket response data
        string cmdResponse = "";
        // error message value
        string message = "";
        // error exists flag
        errorExists = false;

        /* The following fields are the input fields for the command, please
        * refer to the Payment Client Reference Guide for more information on
        * the fields.
        */
        string orderInfo = orderinfo;// Request.Form["OrderInfo"];
        string merchantID = merchantid;//Page.Request.Form["MerchantID"];
        string purchaseAmount = purchaseamount;//Page.Request.Form["PurchaseAmount"];
        string merchTxnRef = mrchTxnRef;//Page.Request.Form["MerchTxnRef"];
        string cardNumber = cardno;// Page.Request.Form["Ecom_Payment_Card_Number"];
        string cardMonth = month;//Page.Request.Form["Ecom_Payment_Card_Month"];
        string cardYear = yy;// Page.Request.Form["Ecom_Payment_Card_Year"];
        /*
        string orderInfo = Request.Form["OrderInfo"];
        string merchantID = Page.Request.Form["MerchantID"];
        string purchaseAmount = Page.Request.Form["PurchaseAmount"];
        string merchTxnRef = Page.Request.Form["MerchTxnRef"];
        string cardNumber = Page.Request.Form["Ecom_Payment_Card_Number"];
        string cardMonth = Page.Request.Form["Ecom_Payment_Card_Month"];
        string cardYear = Page.Request.Form["Ecom_Payment_Card_Year"];
        */
        // receipt variables
        txnResponseCode = "";
        receiptNo = "";
        qsiResponseCode = "";
        acqResponseCode = "";
        authorizeID = "";
        batchNo = "";
        transactionNr = "";
        cardType = "";




        // *******************************************
        // START OF MAIN PROGRAM
        // *******************************************

        //   if debug = true then sockets information output to screen
        //   if debug = false then NO sockets information output to screen
        bool debug = false;

        try
        {

            // this debug parameter simply allows the user to see the sockets
            // commands going to/from the Payment Client socket listener.
            // This is not required for production code.
            string getDebug = "0";// Page.Request.Form["DebugOn"];

            if (getDebug != null && getDebug == "1")
            {
                debug = true;
            }
            else
            {
                getDebug = "0";
            }

            // Payment Client IP number
            String payClientIP = "127.0.0.1";//Page.Request.Form["HostName"];
            // Payment Client Port number
            String portNo = "9050";//Page.Request.Form["Port"];

            // check for valid parameters
            int payClientPort = 0;
            if (portNo != null && portNo.Length > 0 && payClientIP != null && payClientIP.Length > 0)
            {
                payClientPort = int.Parse(portNo);
            }
            else
            {
                message = "(44) Invalid HostName or Port. Hostname=" + payClientIP + " Port=" + portNo;
                errorExists = true;
            }

            // Step 1 - Connect to the Payment Client
            // **************************************
            // set a flag to indicate if a socket created
            bool socketCreated = false;

            try
            {
                // Create a socket and set the properties that you want
                socket = new System.Net.Sockets.TcpClient(payClientIP, payClientPort);

                // Set the socket timeout value to short value for the initial
                socket.ReceiveTimeout = SHORT_SOCKET_TIMEOUT;
                socket.SendTimeout = SHORT_SOCKET_TIMEOUT;

                // Get a Netwrok Stream to read and write to the Socket
                ns = socket.GetStream();
                socketCreated = true;

            }
            catch (Exception ex)
            {
                // Display an error page as the socket connection failed
                message = "(45) Failed to create a socket connection to Payment Client - host:" + payClientIP + " port:" + payClientPort;
                exception = ex.Message;
                if (ex.StackTrace.Length > 0)
                {
                    //Label_StackTrace.Text = ex.ToString();
                    // Panel_StackTrace.Visible = true;
                }
                errorExists = true;
            }

            // Step 2 - Test the Payment Client object was created successfully
            // ****************************************************************
            // Test the communication to the Payment Client using an "echo"
            if (!errorExists)
            {
                cmdResponse = sendCommand("1,Test", debug);
                if (!cmdResponse.StartsWith(OK))
                {
                    // a socket error has occurred
                    // The echo command failed because the status character was not "1".
                    message = cmdResponse;
                    errorExists = true;
                }
                else
                {
                    // correct status present, check for correct echo value returned
                    if (cmdResponse.Substring(2) != "echo:Test")
                    {
                        // Display an error page as the communication
                        // to the Payment Client failed
                        // The response should be "echo:" followed by the text that
                        // was send as the data in the echo command.
                        message = "(5) Socket connection to Payment Client has not connected correctly - echo test failed - should be: '1,echo:Test', but received: '" + cmdResponse;
                        errorExists = true;
                    }
                }
            }

            // Step 3 - Prepare to Generate and Send Digital Order
            // ***************************************************
            // The extra details are not accepted by the
            // sendMOTODigitalOrder() method and so we must add them
            // explicitly here for inclusion in the DO creation.
            // Adding the supplementary data to the DO is done using the
            //"addDigitalOrderField" command

            // add MerchTxnRef
            if (!errorExists)
            {
                if (merchTxnRef != null && merchTxnRef.Length > 0)
                {
                    cmdResponse = sendCommand("7,MerchTxnRef," + merchTxnRef, debug);
                    if (!cmdResponse.StartsWith(OK))
                    {
                        // Display an Error Page
                        message = "(6) Failed to add supplementary data - MerchTxnRef, received:" + cmdResponse;
                        errorExists = true;
                    }
                }
            }

            // add Card Number
            if (!errorExists)
            {
                cmdResponse = sendCommand("7,CardNum," + cardNumber, debug);
                if (!cmdResponse.StartsWith(OK))
                {
                    // Display an Error Page
                    message = "(6) Failed to add supplementary data - CardNum, received:" + cmdResponse;
                    errorExists = true;
                }
            }

            // add Card Expiry - format YYMM
            // We have the expiry month and year, but the Payment Client requires
            // a 4 digit expiry year/month (YYMM). We construct this from the
            // information we already have.
            string cardExpiry = null;
            if (cardYear != null && cardYear.Length > 2)
            {
                cardExpiry = cardYear.Substring(cardYear.Length - 2) + cardMonth;
            }
            else
            {
                cardExpiry = cardYear + cardMonth;
            }

            if (!errorExists)
            {
                cmdResponse = sendCommand("7,CardExp," + cardExpiry, debug);
                if (!cmdResponse.StartsWith(OK))
                {
                    // Display an Error Page
                    message = "(6) Failed to add supplementary data - CardExp, received:" + cmdResponse;
                    errorExists = true;
                }
            }



            // Step 4 - Generate and Send Digital Order (& receive DR)
            // *******************************************************
            // Create and send the Digital Order
            // (This primary command also receives the Digital receipt)
            if (!errorExists)
            {
                // Set the socket timeout value to long value for the primary
                // command as it sends and receives data to the Payment Server.
                // All other commands only work locally with the Payment Client
                socket.ReceiveTimeout = LONG_SOCKET_TIMEOUT;

                cmdResponse = sendCommand("6," + orderInfo + "," + merchantID + "," + purchaseAmount + ",en,", debug);
                if (!cmdResponse.StartsWith(OK))
                {
                    // Retrieve the Payment Client Error (There may be none to get)
                    cmdResponse = sendCommand("4,PaymentClient.Error", debug);
                    if (cmdResponse.StartsWith(OK))
                    {
                        exception = cmdResponse.Substring(2);
                    }
                    // Display an Error Page as the Digital Order was not processed
                    message = "(11) Digital Order has not created correctly - sendMOTODigitalOrder(" + orderInfo + "," + merchantID + "," + purchaseAmount + ",en,) failed";
                    errorExists = true;
                }
            }

            // Step 5 - Check the DR to see if there is a valid result
            // *******************************************************
            // Use the "nextResult" command to check if the DR contains a result
            if (!errorExists)
            {
                // Set the socket timeout value back to the short value for the
                // following supplementary commands
                socket.ReceiveTimeout = SHORT_SOCKET_TIMEOUT;

                cmdResponse = sendCommand("5,", debug);
                if (!cmdResponse.StartsWith(OK))
                {
                    // Retrieve the Payment Client Error (There may be none to get)
                    cmdResponse = sendCommand("4,PaymentClient.Error", debug);
                    if (cmdResponse.StartsWith(OK))
                    {
                        exception = cmdResponse.Substring(2);
                    }
                    // Display an Error as the DR doesn't contain a valid result
                    message = "(12) No Results for Digital Receipt";
                    errorExists = true;
                }
            }

            // Step 6 - Get the result data from the Digital Receipt
            // *****************************************************
            // Get the QSI Response Code for the transaction
            // Extract the available Digital Receipt fields
            if (!errorExists)
            {
                cmdResponse = sendCommand("4,DigitalReceipt.QSIResponseCode", debug);
                if (!cmdResponse.StartsWith(OK))
                {
                    // Display an Error Page as no QSIResponseCode
                    message = "(23) No result for this field: 'DigitalReceipt.QSIResponseCode'";
                    errorExists = true;
                }
                else
                {
                    qsiResponseCode = cmdResponse.Substring(2);
                }
            }

            // Check if the result contains an error message
            if (!errorExists)
            {
                if (qsiResponseCode != "null" && qsiResponseCode != "0")
                {
                    // check if an error returned from the Payment Client
                    cmdResponse = sendCommand("4,DigitalReceipt.ERROR", debug);
                    if (cmdResponse.StartsWith(OK))
                    {
                        // The response is an error message so display an Error Page
                        message = "(24) Error message returned from Payment Server - QSIResponseCode=" + qsiResponseCode;
                        exception = cmdResponse.Substring(2);
                        errorExists = true;
                    }
                }
            }

            // Use the "getAvailableFieldKeys" command to obtain a list of keys available
            // within the Payment Client Hash table containing the receipt results.
            // This is a local command that only communicates to the Payment Client and
            // nothing is sent to the Payment Server. The keys will not be used or displayed here.
            if (!errorExists)
            {
                cmdResponse = sendCommand("33,", debug);
                if (!cmdResponse.StartsWith(OK))
                {
                    // receiptKeys = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";
                    // receiptKeys = receiptKeys.Replace(",", "<br/>");
                }
            }


            // Extract the available Digital Receipt fields
            // The example retrieves the OrderInfo, MerchantID,
            // and Amount values, which should be the same as the values sent.
            // In a production system the sent values and the receive
            // values could be checked to make sure they are the same.
            if (!errorExists)
            {
                cmdResponse = sendCommand("4,DigitalReceipt.MerchTxnRef", debug);
                merchTxnRef = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.MerchantId", debug);
                merchantID = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.OrderInfo", debug);
                orderInfo = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.PurchaseAmountInteger", debug);
                purchaseAmount = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.ReceiptNo", debug);
                receiptNo = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.AcqResponseCode", debug);
                acqResponseCode = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.AuthorizeId", debug);
                authorizeID = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.BatchNo", debug);
                batchNo = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.TransactionNo", debug);
                transactionNr = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";

                cmdResponse = sendCommand("4,DigitalReceipt.CardType", debug);
                cardType = cmdResponse.StartsWith(OK) ? cmdResponse.Substring(2) : "Unknown";
            }

            // Step 7 - We are finished with the Payment Client so tidy up
            // ***********************************************************
            // close the socket C#
            if (socketCreated)
            {
                string socketClosed = closeSocket();
                if (socketClosed != "1")
                {
                    message = socketClosed;
                    errorExists = true;
                }
            }
        }
        catch (Exception ex2)
        {
            message = "(51) Exception encountered.";
            exception = ex2.Message;
            errorExists = true;
        }
        //  Response.Write("avsResultCode====" + avsResultCode);
        //	if ((qsiResponseCode == "0") && (avsResultCode == "Z"))
        //		   {
        //		     Response.Write("AVS Successful");
        //		   }

        // *******************************************
        // END OF MAIN PROGRAM
        // *******************************************
        return transactionNr;
    }

}
